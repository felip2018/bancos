<?php
  use yii\helpers\Html;
  use yii\helpers\Url;
?>
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<style>
  body{
    font-family: 'Montserrat', sans-serif;
  }
  .cont{
    border: 0px solid red;
    width: 100%;
  }
  .cont-message{
    width: 80%;
    margin: auto;
    padding: 10px;
  }
  .title{
    background: #006064;
    color: #FFFFFF;
    padding: 10px;
    text-align: center;
  }
  table{
    width: 100%;
  }
  td{
    padding: 5px;
  }
  .col-item{
    width: 30%;
    background: #0097A7;
    color: #FFFFFF;
  }
  .col-data{
    width: 70%;
    border: 1px solid #DDD;
  }
  .info-registro{
    width: 100%;
    float: left;
  }
  .info-acceso{
    width: 100%;
    float: left;
  }
  .div-link{
    padding: 10px;
    text-align: center;
  }
  .link{
    background: #0097A7;
    color: #FFFFFF;
    padding: 10px;
    border-radius: 5px;
  }
  a{
    text-decoration: none;
  }
</style>
<div class="cont">
    <div class="cont-message">
      <div style="padding-top: 20px;float: left;box-shadow: 0px 0px 5px #CCC;
      padding: 10px;">
          <div style="text-align:center;background:#0097A7;padding: 10px;">
            <img src="http://localhost/sena_forms/web/img/logoSena.png" alt="Logo Sena" style="width:80px;height: 80px;margin: auto;">
          </div>
          <div style="text-align: center;">
            <h2>Registro exitoso!</h2>
          </div>
          <p>
            Cordial saludo, <b><?php echo $data['director'];?></b> el Sistema Integral de Gestión de Proyectos (SIGP) le informa que el registro de la entidad <b><?php echo $data['entidad'];?></b> se ha realizado de forma exitosa con los siguientes datos:
          </p>
          <div class="info-registro">
              <div class="title">
                GENERALIDADES DE LA ENTIDAD
              </div>
              <table border="0">
                <tbody>
                  <tr>
                    <td class="col-item">Entidad</td>
                    <td class="col-data">
                      <?php echo $data['entidad'];?>
                    </td>
                  </tr>
                  <tr>
                    <td class="col-item">Identificación</td>
                    <td class="col-data">
                      <?php echo $data['identificacion'];?>
                    </td>
                  </tr>
                  <tr>
                    <td class="col-item">Dirección</td>
                    <td class="col-data">
                      <?php echo $data['direccion_entidad'];?>
                    </td>
                  </tr>
                  <tr>
                    <td class="col-item">Correo electrónico</td>
                    <td class="col-data">
                      <?php echo $data['correo_entidad'];?>
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              <div class="title">
                REPRESENTANTE LEGAL
              </div>
              <table border="0">
                <tbody>
                  <tr>
                    <td class="col-item">Nombre</td>
                    <td class="col-data">
                      <?php echo $data['representante'];?>
                    </td>
                  </tr>
                  <tr>
                    <td class="col-item">Identificación</td>
                    <td class="col-data">
                      <?php echo $data['r_identificacion'];?>
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              <div class="title">
                CLASIFICACIÓN DE LA ENTIDAD
              </div>
              <table border="0">
                <tbody>
                  <tr>
                    <td class="col-item">Código CIIU</td>
                    <td class="col-data">
                      <?php echo $data['codigo_ciiu'];?>
                    </td>
                  </tr>
                  <tr>
                    <td class="col-item">Sector</td>
                    <td class="col-data">
                      <?php echo $data['sector'];?>
                    </td>
                  </tr>
                  <tr>
                    <td class="col-item">Tipo de entidad</td>
                    <td class="col-data">
                      <?php echo $data['tipo_entidad'];?>
                    </td>
                  </tr>
                  <tr>
                    <td class="col-item">Tamaño de entidad</td>
                    <td class="col-data">
                      <?php echo $data['tamano_entidad'];?>
                    </td>
                  </tr>
                  <tr>
                    <td class="col-item">Tipo de empresa</td>
                    <td class="col-data">
                      <?php echo $data['tipo_empresa'];?>
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              <div class="title">
                DIRECTOR DEL PROYECTO
              </div>
              <table border="0">
                <tbody>
                  <tr>
                    <td class="col-item">Nombre</td>
                    <td class="col-data">
                      <?php echo $data['director'];?>
                    </td>
                  </tr>
                  <tr>
                    <td class="col-item">Identificación</td>
                    <td class="col-data">
                      <?php echo $data['d_identificacion'];?>
                    </td>
                  </tr>
                  <tr>
                    <td class="col-item">Correo electrónico</td>
                    <td class="col-data">
                      <?php echo $data['d_correo'];?>
                    </td>
                  </tr>
                </tbody>
              </table>
          </div>
          <div class="info-acceso">
              <br>
              <div class="title">
                DATOS DE ACCESO A LA CUENTA
              </div>
              <p>
                A continuación encontrará los datos de acceso al Sistema Integral de Gestión de Proyectos
              </p>
              <hr>
              <table border="0">
                <tbody>
                  <tr>
                    <td class="col-item">Usuario</td>
                    <td class="col-data">
                      <?php echo $data['user'];?>
                    </td>
                  </tr>
                  <tr>
                    <td class="col-item">Contraseña</td>
                    <td class="col-data">
                      <?php echo $data['pass'];?>
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              <div class="title">
                ACTIVACIÓN DE CUENTA
              </div>
              <p>
                Por favor ingrese al siguiente enlace para realizar la activación de su cuenta en nuestro sistema:
              </p>
              <div class="div-link">
                <a href="<?php echo $data['link_activacion'];?>" target="_blank">Activar cuenta</a>
              </div>
          </div>
      </div>
      <p>
        ESTE CORREO ES AUTOMÁTICO, POR FAVOR NO RESPONDER.
      </p>
    </div>
</div>
