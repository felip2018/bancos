<?php
  use yii\helpers\Html;
  use yii\helpers\Url;
?>
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<style>
  body{
    font-family: 'Montserrat', sans-serif;
  }
  .cont{
    border: 0px solid red;
    width: 100%;
  }
  .cont-message{
    width: 80%;
    margin: auto;
    padding: 10px;
  }
  .title{
    background: #006064;
    color: #FFFFFF;
    padding: 10px;
    text-align: center;
  }
  table{
    width: 100%;
  }
  td{
    padding: 5px;
  }
  .col-item{
    width: 30%;
    background: #0097A7;
    color: #FFFFFF;
  }
  .col-data{
    width: 70%;
    border: 1px solid #DDD;
  }
  .info-registro{
    width: 100%;
    float: left;
  }
  .info-acceso{
    width: 100%;
    float: left;
  }
  .div-link{
    padding: 10px;
    text-align: center;
  }
  .link{
    background: #0097A7;
    color: #FFFFFF;
    padding: 10px;
    border-radius: 5px;
  }
  a{
    text-decoration: none;
  }
</style>
<div class="cont">
    <div class="cont-message">
      <div style="padding-top: 20px;float: left;box-shadow: 0px 0px 5px #CCC;
      padding: 10px;">
          <div style="text-align:center;background:#0097A7;padding: 10px;">
            <img src="http://localhost/sena_forms/web/img/logoSena.png" alt="Logo Sena" style="width:80px;height: 80px;margin: auto;">
          </div>
          <div style="text-align: center;">
            <h2>Cambio de contraseña</h2>
          </div>
          <p>
            Cordial saludo, <b><?php echo $data['nombre'];?></b><br><br>El Sistema Integral de Gestión de Proyectos (SIGP) le informa que se ha generado un nuevo caso para actualización o cambio de contraseña asociada a su cuenta con el usuario <b><?php echo $data['usuario'];?></b>, por favor siga el siguiente enlace para realizar el cambio solicitado:
          </p>
          <a href="<?php echo $data['link_cambio_clave'];?>" target="_blank">Cambio de contraseña</a>
      </div>
      <p>
        ESTE CORREO ES AUTOMÁTICO, POR FAVOR NO RESPONDER.
      </p>
    </div>
</div>
