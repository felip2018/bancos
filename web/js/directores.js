function registrar_director() {
	//console.log("Registrar director de proyecto");

	jQuery("#alerta").html('');

	var tipo_doc 			= jQuery("#directores-tipo_doc");
	var num_doc 			= jQuery("#directores-num_doc");
	var nombres 			= jQuery("#directores-nombres");
	var primer_apellido		= jQuery("#directores-primer_apellido");
	var segundo_apellido	= jQuery("#directores-segundo_apellido");
	var correo 				= jQuery("#directores-correo");

	// Obtener los elementos de clase "registerDoc"
	var docs = jQuery(".registerDoc");
	//var allowedExtensions = /(.jpg|.jpeg|.png|.gif)$/i;
	var allowedExtensions = /(.pdf)$/i;
	

	if (tipo_doc.val() != '') {
		if (num_doc.val() != '') {
			if (nombres.val() != '') {
				if (primer_apellido.val() != '') {
					if (correo.val() != '') {

						if (docs.length > 0) {
							var docsValidation = true;
							jQuery.each(docs,function (key,value) {
								//console.log(value.getAttribute('id'));
								var id = value.getAttribute('id');
								var documento = value.getAttribute('document');

								var element = jQuery("#"+id);

								if (element.val() == "") {

									jQuery("#alerta").html('<div class="alert alert-warning">Ingrese el documento requerido "<b>'+documento+'</b>".</div>');
									element.focus();
									docsValidation = false;
									return false;//break loop

								}else{
									
									if(!allowedExtensions.exec(element.val())){
										jQuery("#alerta").html('<div class="alert alert-warning">Formato de documento inválido "<b>'+documento+'</b>" ingrese el documento requerido en formato (.pdf)".</div>');
										element.focus();
										docsValidation = false;
										return false;
									}else{

										var fileSize = element[0].files[0].size;
										var sizeKB = parseInt(fileSize/1024);

										//console.log("File Size: "+fileSize+", Kb: "+sizeKB);
										// El peso no debe superar 8Mb 0 8192 Kb
										if (sizeKB > 8192) {
											console.log(sizeKB+" Excede el peso de 8192 Kb.");
											jQuery("#alerta").html('<div class="alert alert-warning">El peso del archivo es excede el límite permitido "<b>'+documento+'</b>" el archivo no debe exceder las 8Mb".</div>');
											element.focus();
											docsValidation = false;
											return false;//break loop
										}else{
											docsValidation = true;
										}
									}

								}

							});
						}

						if (docsValidation) {

							jQuery(".loading").css('display','block');

							var form 		= document.getElementById('registro_director');
							var formData 	= new FormData(form);

							jQuery.ajax({
								type:"POST",
								url:"index.php?r=directores/add-new-director",
								data: formData,
								contentType:false,
								processData:false,
								success:function (response) {

									jQuery(".loading").css('display','none');
									
									console.log(response);
									
									jQuery("#alerta").html('');

									var jsonResponse = JSON.parse(response);

									if (jsonResponse.status == "success") {
										jQuery("#alerta").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
										jQuery("#id_director").val(jsonResponse.id_director);
									}else{
										jQuery("#alerta").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
										jQuery("#id_director").val('');
									}

								}
							});

						}

					}else{
						jQuery("#alerta").html('<div class="alert alert-warning">Ingrese el correo electrónico del director del proyecto.</div>');
						correo.focus();
					}
				}else{
					jQuery("#alerta").html('<div class="alert alert-warning">Ingrese el primer apellido del director de proyecto.</div>');
					primer_apellido.focus();
				}
			}else{
				jQuery("#alerta").html('<div class="alert alert-warning">Ingrese el nombre del director de proyecto.</div>');
				nombres.focus();
			}
		}else{
			jQuery("#alerta").html('<div class="alert alert-warning">Ingrese un número de identificación válido.</div>');
			num_doc.focus();
		}
	}else{
		jQuery("#alerta").html('<div class="alert alert-warning">Seleccione el tipo de identificación válido.</div>');
		tipo_doc.focus();
	}
}

// Buscar director de proyecto al registrar el numero de identificacion
function buscarDirector(num_doc) {
	
	var tipo_doc = jQuery("#directores-tipo_doc");

	if (tipo_doc.val() != '') {

		if (num_doc != '') {
			jQuery.ajax({
				type:"POST",
				url:"index.php?r=directores/buscar-director-doc",
				data: {
					num_doc: num_doc,
					tipo_doc: tipo_doc.val()
				},
				success:function (response) {
					console.log(response);
					var jsonResponse = JSON.parse(response);
					if (jsonResponse.status == 'success') {
						jQuery('#btnRegisterDirector').css('display','none');

						jQuery("#id_director").val(jsonResponse.datos['id_director']);

						jQuery("#directores-nombres").val(jsonResponse.datos['nombres']);
						jQuery("#directores-primer_apellido").val(jsonResponse.datos['primer_apellido']);
						jQuery("#directores-segundo_apellido").val(jsonResponse.datos['segundo_apellido']);
						jQuery("#directores-correo").val(jsonResponse.datos['correo']);

						jQuery("#directores-nombres").attr('readonly',true);
						jQuery("#directores-primer_apellido").attr('readonly',true);
						jQuery("#directores-segundo_apellido").attr('readonly',true);
						jQuery("#directores-correo").attr('readonly',true);

						buscar_experiencias(jsonResponse.datos['id_director']);
						buscar_titulos(jsonResponse.datos['id_director']);
						buscar_referencias(jsonResponse.datos['id_director']);

					}else{
						jQuery('#btnRegisterDirector').css('display','block');

						jQuery("#id_director").val('');

						jQuery("#directores-nombres").val('');
						jQuery("#directores-primer_apellido").val('');
						jQuery("#directores-segundo_apellido").val('');
						jQuery("#directores-correo").val('');

						jQuery("#directores-nombres").attr('readonly',false);
						jQuery("#directores-primer_apellido").attr('readonly',false);
						jQuery("#directores-segundo_apellido").attr('readonly',false);
						jQuery("#directores-correo").attr('readonly',false);

						jQuery("#listadoExperienciasDirector").html('');
						jQuery("#listadoTitulosDirector").html('');
						jQuery("#listadoReferenciasDirector").html('');
					}
				}
			})
		}

	}else{

		jQuery("#alerta").html('<div class="alert alert-warning">Seleccione un tipo de identificación válido.</div>');
		tipo_doc.focus();

	}
}

// Consultar experiencias registradas del director seleccionado
function buscar_experiencias(id_director) {
	
	jQuery("#listadoExperienciasDirector").html('');

	jQuery.ajax({
		type:"POST",
		url:"index.php?r=directores/search-experiences",
		data:{
			id_director: id_director
		},
		success:function (response) {
			var jsonResponse = JSON.parse(response);
			if (jsonResponse.status == "success") {

				jQuery("#listadoExperienciasDirector").html('');				
				var i = 1;
				jQuery.each(jsonResponse.experiencias, function (key,value) {
					jQuery("#listadoExperienciasDirector").append('<tr>'+
			            '<td>'+i+'</td>'+
			            '<td>'+value['tipo']+'</td>'+
			            '<td>'+value['descripcion']+'</td>'+
			            '<td><a href="convocatoria_'+value['year']+'/bancos/directores/'+value['num_doc']+'/'+value['url']+'" target="_blank">'+value['url']+'</a></td>'+
			            '<td></td>'+
			        '</tr>');

					i++;
				});

			}
		}
	})
}

// Actualizar experiencias registradas de un director
function actualizar_experiencias() {
	
	var id_director = jQuery("#id_director");

	if (id_director.val() != '') {
		buscar_experiencias(id_director.val());
	}
}

// Registrar experiencias del director
function registrar_experiencias() {
	
	jQuery("#alerta").html('');

	var id_director = jQuery("#id_director");

	if (id_director.val() != '') {
		jQuery('.modal').modal({backdrop: 'static', keyboard: false});
		jQuery('.modal-title').html('Registrar experiencia');
		jQuery('.modal-body').html('<form class="row" id="form-experiencia">'+
				'<div class="col-xs-12" id="alert-form-experiencia"></div>'+
				'<div class="col-xs-12">'+
					'<input type="hidden" class="form-control" name="id_director" id="id_director" value="'+id_director.val()+'"/>'+
				'</div>'+
				'<div class="col-xs-12">'+
					'<p>Tipo de experiencia</p>'+
					'<select class="form-control" name="tipo" id="tipo">'+
						'<option value="">-Seleccione</option>'+
						'<option value="TEMATICA">TEMATICA</option>'+
						'<option value="DOCENTE">DOCENTE</option>'+
						'<option value="EXPERIENCIA">EXPERIENCIA</option>'+
					'</select>'+
				'</div>'+
				'<div class="col-xs-12">'+
					'<p>Descripción</p>'+
					'<textarea class="form-control" rows="5" name="descripcion" id="descripcion"></textarea>'+
				'</div>'+
				'<div class="col-xs-12">'+
					'<p>Soporte</p>'+
					'<input type="file" class="form-control" name="soporte_experiencia" id="soporte"/>'+
				'</div>'+
			'</form>');
		jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Guardar <i class="fa fa-save"></i> </button>');

		jQuery('.btn-accept').click(function () {

			var tipo = jQuery('#tipo');
			var descripcion = jQuery('#descripcion');
			var soporte = jQuery('#soporte');

			if (tipo.val() != '') {
				if (descripcion.val() != '') {
					if (soporte.val() != '') {

						// Validar peso del archivo
						var fileSize = soporte[0].files[0].size;
						var sizeKB = parseInt(fileSize/1024);

						// El peso no debe superar 8Mb 0 8192 Kb
						if (sizeKB < 8192) {

							jQuery(".loading").css('display','block');

							var form 		= document.getElementById('form-experiencia');
							var formData 	= new FormData(form);

							jQuery.ajax({
								type:"POST",
								url:"index.php?r=directores/add-experience",
								data:formData,
								contentType: false,
								processData: false,
								success: function (response) {
									jQuery(".loading").css('display','none');
									console.log(response);
									var jsonResponse = JSON.parse(response);
									if (jsonResponse.status == "success") {
										jQuery(".modal-body").html('');
										jQuery(".modal-body").html('Se ha registrado la experiencia del director correctamente.');
										jQuery(".modal-footer").html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

										jQuery('.btn-accept').click(function () {
											//jQuery('.modal').toggle();
											buscar_experiencias(id_director.val());
										});
									}else{
										jQuery("#alert-form-experiencia").html('');
										jQuery("#alert-form-experiencia").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
									}
								}
							});

						}else{
							jQuery("#alert-form-experiencia").html('<div class="alert alert-warning">El peso del archivo excede el límite permitido (8Mb).</div>');
						}

					}else{
						jQuery("#alert-form-experiencia").html('<div class="alert alert-warning">Debe subir el documento (pdf) que soporte la experiencia que desea registrar.</div>');
					}
				}else{
					jQuery("#alert-form-experiencia").html('<div class="alert alert-warning">Describa la experiencia que desea registrar.</div>');	
				}
			}else{
				jQuery("#alert-form-experiencia").html('<div class="alert alert-warning">Seleccione el tipo de experiencia a registrar.</div>');		
			}

		});

	}else{
		jQuery("#alerta").html('<div class="alert alert-warning">Indique el tipo y numero de documento del director de proyecto para agregar experiencias.</div>');
	}

}

// Consultar titulos registrados del director seleccionado
function buscar_titulos(id_director) {
	
	jQuery("#listadoTitulosDirector").html('');

	jQuery.ajax({
		type:"POST",
		url:"index.php?r=directores/search-titles",
		data:{
			id_director: id_director
		},
		success:function (response) {
			var jsonResponse = JSON.parse(response);
			if (jsonResponse.status == "success") {

				jQuery("#listadoTitulosDirector").html('');				
				var i = 1;
				jQuery.each(jsonResponse.titulos, function (key,value) {
					jQuery("#listadoTitulosDirector").append('<tr>'+
			            '<td>'+i+'</td>'+
			            '<td>'+value['tipo_titulo']+'</td>'+
			            '<td>'+value['descripcion']+'</td>'+
			            '<td><a href="convocatoria_'+value['year']+'/bancos/directores/'+value['num_doc']+'/'+value['url']+'" target="_blank">'+value['url']+'</a></td>'+
			            '<td></td>'+
			        '</tr>');

					i++;
				});

			}
		}
	})
}

// Actualizar titulos registrados de un director
function actualizar_titulos() {
	
	var id_director = jQuery("#id_director");

	if (id_director.val() != '') {
		buscar_titulos(id_director.val());
	}
}

// Registrar titulos del director
function registrar_titulos() {
	
	jQuery("#alerta").html('');

	var id_director = jQuery("#id_director");

	if (id_director.val() != '') {
		jQuery('.modal').modal({backdrop: 'static', keyboard: false});
		jQuery('.modal-title').html('Registrar titulo');
		jQuery('.modal-body').html('<form class="row" id="form-titulo">'+
				'<div class="col-xs-12" id="alert-form-titulo"></div>'+
				'<div class="col-xs-12">'+
					'<input type="hidden" class="form-control" name="id_director" id="id_director" value="'+id_director.val()+'"/>'+
				'</div>'+
				'<div class="col-xs-12">'+
					'<p>Tipo de titulo</p>'+
					'<select class="form-control" name="tipo_titulo" id="tipo_titulo">'+
						'<option value="">-Seleccione</option>'+
						'<option value="BACHILLER">BACHILLER</option>'+
						'<option value="TECNICO">TECNICO</option>'+
						'<option value="TECNOLOGO">TECNOLOGO</option>'+
						'<option value="PREGRADO">PREGRADO</option>'+
						'<option value="POSGRADO">POSGRADO</option>'+
						'<option value="MAESTRIA">MAESTRIA</option>'+
						'<option value="DOCTORADO">DOCTORADO</option>'+
					'</select>'+
				'</div>'+
				'<div class="col-xs-12">'+
					'<p>Descripción</p>'+
					'<textarea class="form-control" rows="5" name="descripcion" id="descripcion"></textarea>'+
				'</div>'+
				'<div class="col-xs-12">'+
					'<p>Soporte</p>'+
					'<input type="file" class="form-control" name="soporte_titulo" id="soporte"/>'+
				'</div>'+
			'</form>');
		jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Guardar <i class="fa fa-save"></i> </button>');

		jQuery('.btn-accept').click(function () {

			var tipo 		= jQuery('#tipo_titulo');
			var descripcion = jQuery('#descripcion');
			var soporte 	= jQuery('#soporte');

			if (tipo.val() != '') {
				if (descripcion.val() != '') {
					if (soporte.val() != '') {

						// Validar peso del archivo
						var fileSize = soporte[0].files[0].size;
						var sizeKB = parseInt(fileSize/1024);

						// El peso no debe superar 8Mb 0 8192 Kb
						if (sizeKB < 8192) {
							jQuery(".loading").css('display','block');
							var form 		= document.getElementById('form-titulo');
							var formData 	= new FormData(form);

							jQuery.ajax({
								type:"POST",
								url:"index.php?r=directores/add-title",
								data:formData,
								contentType: false,
								processData: false,
								success: function (response) {
									jQuery(".loading").css('display','none');
									console.log(response);
									var jsonResponse = JSON.parse(response);
									//jQuery("#alert-form-titulo").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
									if (jsonResponse.status == "success") {
										jQuery(".modal-body").html('');
										jQuery(".modal-body").html('Se ha registrado el titulo del director correctamente.');
										jQuery(".modal-footer").html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

										jQuery('.btn-accept').click(function () {
											//jQuery('.modal').toggle();
											buscar_titulos(id_director.val());
										});
									}else{
										jQuery("#alert-form-titulo").html('');
										jQuery("#alert-form-titulo").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
									}
								}
							});

						}else{
							jQuery("#alert-form-titulo").html('<div class="alert alert-warning">El peso del archivo excede el límite permitido (8Mb).</div>');
						}

					}else{
						jQuery("#alert-form-titulo").html('<div class="alert alert-warning">Debe subir el documento (pdf) que soporte el titulo que desea registrar.</div>');
					}
				}else{
					jQuery("#alert-form-titulo").html('<div class="alert alert-warning">Describa el titulo que desea registrar.</div>');	
				}
			}else{
				jQuery("#alert-form-titulo").html('<div class="alert alert-warning">Seleccione el tipo de titulo a registrar.</div>');		
			}

		});

	}else{
		jQuery("#alerta").html('<div class="alert alert-warning">Indique el tipo y numero de documento del director de proyecto para agregar experiencias.</div>');
	}

}


// Consultar referencias registradas del director seleccionado
function buscar_referencias(id_director) {
	
	jQuery("#listadoReferenciasDirector").html('');

	jQuery.ajax({
		type:"POST",
		url:"index.php?r=directores/search-references",
		data:{
			id_director: id_director
		},
		success:function (response) {
			var jsonResponse = JSON.parse(response);
			if (jsonResponse.status == "success") {

				jQuery("#listadoReferenciasDirector").html('');				
				var i = 1;
				jQuery.each(jsonResponse.referencias, function (key,value) {
					jQuery("#listadoReferenciasDirector").append('<tr>'+
			            '<td>'+i+'</td>'+
			            '<td><p>'+value['nombre']+'</p><p>Tel: '+value['telefono']+'</p></td>'+
			            '<td>'+value['empresa']+'</td>'+
			            '<td>'+value['cargo']+'</td>'+
			            '<td><a href="convocatoria_'+value['year']+'/bancos/directores/'+value['num_doc']+'/'+value['url']+'" target="_blank">'+value['url']+'</a></td>'+
			        '</tr>');

					i++;
				});

			}
		}
	})
}

// Actualizar referencias registrados de un director
function actualizar_referencias() {
	
	var id_director = jQuery("#id_director");

	if (id_director.val() != '') {
		buscar_referencias(id_director.val());
	}
}

// Registrar referencias del director
function registrar_referencias() {
	
	jQuery("#alerta").html('');

	var id_director = jQuery("#id_director");

	if (id_director.val() != '') {
		jQuery('.modal').modal({backdrop: 'static', keyboard: false});
		jQuery('.modal-title').html('Registrar referencia');
		jQuery('.modal-body').html('<form class="row" id="form-referencia">'+
				'<div class="col-xs-12" id="alert-form-referencia"></div>'+
				'<div class="col-xs-12">'+
					'<input type="hidden" class="form-control" name="id_director" id="id_director" value="'+id_director.val()+'"/>'+
				'</div>'+
				'<div class="col-xs-12">'+
					'<p>Nombre</p>'+
					'<input type="text" class="form-control" name="nombre" id="nombre"/>'+
				'</div>'+
				'<div class="col-xs-12">'+
					'<p>Empresa</p>'+
					'<input type="text" class="form-control" name="empresa" id="empresa"/>'+
				'</div>'+
				'<div class="col-xs-12">'+
					'<p>Cargo</p>'+
					'<input type="text" class="form-control" name="cargo" id="cargo"/>'+
				'</div>'+
				'<div class="col-xs-12">'+
					'<p>Teléfono</p>'+
					'<input type="text" class="form-control" name="telefono" id="telefono"/>'+
				'</div>'+
				'<div class="col-xs-12">'+
					'<p>Soporte</p>'+
					'<input type="file" class="form-control" name="soporte_referencia" id="soporte"/>'+
				'</div>'+
			'</form>');
		jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Guardar <i class="fa fa-save"></i> </button>');

		jQuery('.btn-accept').click(function () {

			var nombre 		= jQuery('#nombre');
			var empresa 	= jQuery('#empresa');
			var cargo 		= jQuery('#cargo');
			var telefono 	= jQuery('#telefono');
			var soporte 	= jQuery('#soporte');

			if (nombre.val() != '') {
				if (empresa.val() != '') {
					if (cargo.val() != '') {
						if (telefono.val() != '') {
							if (soporte.val() != '') {

								// Validar peso del archivo
								var fileSize = soporte[0].files[0].size;
								var sizeKB = parseInt(fileSize/1024);

								// El peso no debe superar 8Mb 0 8192 Kb
								if (sizeKB < 8192) {
									jQuery(".loading").css('display','block');
									var form 		= document.getElementById('form-referencia');
									var formData 	= new FormData(form);

									jQuery.ajax({
										type:"POST",
										url:"index.php?r=directores/add-reference",
										data:formData,
										contentType: false,
										processData: false,
										success: function (response) {
											jQuery(".loading").css('display','none');
											console.log(response);
											var jsonResponse = JSON.parse(response);
											//jQuery("#alert-form-referencia").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
											if (jsonResponse.status == "success") {
												jQuery(".modal-body").html('');
												jQuery(".modal-body").html('Se ha registrado la referencia del director correctamente.');
												jQuery(".modal-footer").html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

												jQuery('.btn-accept').click(function () {
													buscar_referencias(id_director.val());
												});
											}else{
												jQuery("#alert-form-referencia").html('');
												jQuery("#alert-form-referencia").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
											}
										}
									});

								}else{
									jQuery("#alert-form-referencia").html('<div class="alert alert-warning">El peso del archivo excede el límite permitido (8Mb).</div>');
								}

							}else{
								jQuery("#alert-form-referencia").html('<div class="alert alert-warning">Debe subir el documento (pdf) que soporte la referencia que desea registrar.</div>');
							}
						}else{
							jQuery("#alert-form-referencia").html('<div class="alert alert-warning">Digite el número telefónico de la referencia que desea registrar.</div>');		
						}
					}else{
						jQuery("#alert-form-referencia").html('<div class="alert alert-warning">Digite el cargo que desempeña la referencia que desea registrar.</div>');	
					}
				}else{
					jQuery("#alert-form-referencia").html('<div class="alert alert-warning">Digite la empresa donde labora la referencia que desea registrar.</div>');	
				}
			}else{
				jQuery("#alert-form-referencia").html('<div class="alert alert-warning">Digite el nombre de la referencia que desa registrar.</div>');		
			}

		});

	}else{
		jQuery("#alerta").html('<div class="alert alert-warning"Indique el tipo y numero de documento del director de proyecto para agregar referencias.</div>');
	}

}