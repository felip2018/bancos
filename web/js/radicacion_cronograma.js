function validar_cronograma(id_proyecto) {
	//console.log("Validacion de proyecto: "+id_proyecto);
	jQuery('.modal').modal({backdrop: 'static', keyboard: false});
	jQuery('.modal-title').html('Validación de cronograma');
	jQuery('.modal-body').html('A continuación podrá realizar la validación de registro del cronograma detallado de todos los grupos por acciones de formación.<br>¿Desea continuar?');
	jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

	jQuery('.btn-accept').click(function () {
		jQuery('.loading').css('display','block');
		jQuery.ajax({
			type: "POST",
			url: "index.php?r=cronograma/validar-radicacion",
			data:{
				id_proyecto: id_proyecto
			},
			success:function (response) {
				jQuery('.loading').css('display','none');
				//console.log(response);
				var jsonResponse = JSON.parse(response);

				if (jsonResponse.status == "success") {
					jQuery('.modal-title').html('Validación de cronograma');
					jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
					jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

					jQuery('.btn-accept').click(function () {
						jQuery('#listadoGrupos').html('');
						jQuery('#contenedorBtnRadicacion').html('');

						jQuery('#contenedorBtnRadicacion').html('<button class="btn btn-primary pull-right" onclick=radicar_cronograma()>Radicar cronograma</button>');
					});
				}else if (jsonResponse.status == "warning") {
					jQuery('.modal-title').html('Validación de cronograma');
					jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
					jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

					jQuery('.btn-accept').click(function () {
						
						jQuery('#listadoGrupos').html('');
						jQuery('#contenedorBtnRadicacion').html('');

						jQuery.each(jsonResponse.arreglo,function (key,value) {
							jQuery('#listadoGrupos').append('<div style="border:1px solid #CCCCCC;padding:10px;border-radius:5px;margin-bottom:5px;"><b>Acción de formacion</b>:'+value['accion_formacion']+'<br><b>Grupo:</b>'+value['grupo']+'</div>');
						})
						
					});
				}else if (jsonResponse.status == "vacio") {
					jQuery('.modal-title').html('Validación de cronograma');
					jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
					jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

					jQuery('.btn-accept').click(function () {
						jQuery('#listadoGrupos').html('');
						jQuery('#contenedorBtnRadicacion').html('');
					});
				}

			}
		})
	});
}

function radicar_cronograma() {
	jQuery('.modal').modal({backdrop: 'static', keyboard: false});
	jQuery('.modal-title').html('Radicación de cronograma');
	jQuery('.modal-body').html('A continuación podrá realizar la radicación del cronograma en el sistema.<br>¿Desea continuar?');
	jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

	jQuery('.btn-accept').click(function () {
		jQuery('.loading').css('display','block');
		jQuery.ajax({
			type:"POST",
			url:"index.php?r=cronograma/radicar-cronograma",
			data:{
				valor: 1,
			},
			success:function (response) {
				jQuery('.loading').css('display','none');
				console.log(response);
				var jsonResponse = JSON.parse(response);

				jQuery('.modal-title').html('Radicación de cronograma');
				jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
				jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

			}
		})
	});

}