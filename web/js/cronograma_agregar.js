// Agregar sesion presencial
function agregarSesionPresencial(id_cronograma) {
	
	jQuery('.modal').modal({backdrop: 'static', keyboard: false});
	jQuery('.modal-title').html('Agregar sesión presencial.');
	jQuery('.modal-body').html(''+
       '<div class="row" id="alerta-sesion-presencial">'+
       '</div>'+
       ' <div class="row">'+
       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '     <label>Nombre de la sesión</label>'+
       '     <select class="form-control" name="Nombre de la sesión" id="nombre_sesion">'+
       '       <option value="">-Seleccione</option>'+
       '       <option value="SESION NORMAL">SESION NORMAL</option>'+
       '       <option value="SESION GRUPAL 1">SESION GRUPAL 1</option>'+
       '       <option value="SESION INDIVIDUAL 1">SESION INDIVIDUAL 1</option>'+
       '       <option value="SESION INDIVIDUAL 2">SESION INDIVIDUAL 2</option>'+
       '       <option value="SESION INDIVIDUAL 3">SESION INDIVIDUAL 3</option>'+
       '       <option value="SESION INDIVIDUAL 4">SESION INDIVIDUAL 4</option>'+
       '       <option value="SESION INDIVIDUAL 5">SESION INDIVIDUAL 5</option>'+
       '       <option value="SESION GRUPAL 2">SESION GRUPAL 2</option>'+
       '     </select>'+
       '   </div>'+
       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '     <label>Número de sesión</label>'+
       '     <input type="number" min="0" class="form-control" name="Número de sesión" id="num_sesion">'+
       '   </div>'+
       ' </div>'+
       ' <div class="row">'+
       '   <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">'+
       '     <label>Fecha de inicio</label>'+
       '     <input type="date" class="form-control" name="Fecha de inicio" id="fecha_inicio">'+
       '   </div>'+
       '   <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">'+
       '     <label>Hora de inicio (24hrs)</label>'+
       '     <input type="time" class="form-control" name="Hora de inicio" id="hora_inicial">'+
       '   </div>'+
       '   <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">'+
       '     <label>Hora de finalización (24hrs)</label>'+
       '     <input type="time" class="form-control" name="Hora de finalización" id="hora_final">'+
       '   </div>'+
       '   <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">'+
       '     <label>Número de horas totales</label>'+
       '     <input type="number" min="0" class="form-control" name="Número de horas totales" id="num_hrs_totales">'+
       '   </div>'+
       ' </div>'+
       ' <div class="row">'+
       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '     <label>Departamento</label>'+
       '     <select class="form-control" name="Departamento" id="id_departamento" onchange="buscarCiudades(this.value)">'+
       '       <option value="">-Seleccione</option>'+
       '     </select>'+
       '   </div>'+
       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '     <label>Ciudad</label>'+
       '     <select class="form-control" name="Ciudad" id="id_ciudad"><option value="">-Seleccione</option></select>'+
       '   </div>'+
       ' </div>'+
       ' <div class="row">'+
       '   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
       '     <label>Nombre de la sede</label>'+
       '     <input type="text" class="form-control" name="Nombre de la sede" id="nombre_sede">'+
       '   </div>'+
       ' </div>'+
       ' <div class="row">'+
       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '     <label>Dirección de la sede</label>'+
       '     <input type="text" class="form-control" name="Dirección de la sede" id="direccion_sede">'+
       '   </div>'+
       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '     <label>Aula / Salón</label>'+
       '     <input type="text" class="form-control" name="Aula / Salón" id="aula_salon">'+
       '   </div>'+
       ' </div>'+
       ' <div class="row">'+
       '   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
       '     <label>Nombre del capacitador</label>'+
       '     <input type="text" class="form-control" name="Nombre de capacitador" id="nombre_capacitador_presencial">'+
       '   </div>'+
       ' </div>'+
       ' <div class="row">'+
       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '     <label>Tipo de documento del capacitador</label>'+
       '     <select class="form-control" name="Tipo de documento del capacitador" id="tipo_doc_capacitador_presencial">'+
       '       <option value="">-Seleccione</option>'+
       '       <option value="CC">CC (Cédula de ciudadanía)</option>'+
       '       <option value="CEX">CEX (Cédula de extranjeria)</option>'+
       '       <option value="PAS">PAS (Pasaporte)</option>'+
       '     </select>'+
       '   </div>'+
       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '     <label>Número de identificación del capacitador</label>'+
       '     <input type="number" min="0" class="form-control" name="Número de identificación del capacitador" id="num_doc_capacitador_presencial">'+
       '   </div>'+
       ' </div>'+
       ' <div class="row">'+
       '   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
       '     <label>Nombre del capacitador suplente 1</label>'+
       '     <input type="text" class="form-control" name="Nombre de capacitador" id="nombre_capacitador_presencial_s1">'+
       '   </div>'+
       ' </div>'+
       ' <div class="row">'+
       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '     <label>Tipo de documento del capacitador suplente 1</label>'+
       '     <select class="form-control" name="Tipo de documento del capacitador" id="tipo_doc_capacitador_presencial_s1">'+
       '       <option value="">-Seleccione</option>'+
       '       <option value="CC">CC (Cédula de ciudadanía)</option>'+
       '       <option value="CEX">CEX (Cédula de extranjeria)</option>'+
       '       <option value="PAS">PAS (Pasaporte)</option>'+
       '     </select>'+
       '   </div>'+
       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '     <label>Número de identificación del capacitador suplente 1</label>'+
       '     <input type="number" min="0" class="form-control" name="Número de identificación del capacitador" id="num_doc_capacitador_presencial_s1">'+
       '   </div>'+
       ' </div>'+
       ' <div class="row">'+
       '   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
       '     <label>Nombre del capacitador suplente 2</label>'+
       '     <input type="text" class="form-control" name="Nombre de capacitador" id="nombre_capacitador_presencial_s2">'+
       '   </div>'+
       ' </div>'+
       ' <div class="row">'+
       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '     <label>Tipo de documento del capacitador suplente 2</label>'+
       '     <select class="form-control" name="Tipo de documento del capacitador" id="tipo_doc_capacitador_presencial_s2">'+
       '       <option value="">-Seleccione</option>'+
       '       <option value="CC">CC (Cédula de ciudadanía)</option>'+
       '       <option value="CEX">CEX (Cédula de extranjeria)</option>'+
       '       <option value="PAS">PAS (Pasaporte)</option>'+
       '     </select>'+
       '   </div>'+
       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '     <label>Número de identificación del capacitador suplente 2</label>'+
       '     <input type="number" min="0" class="form-control" name="Número de identificación del capacitador" id="num_doc_capacitador_presencial_s2">'+
       '   </div>'+
       ' </div>'+
       ' <div class="row">'+
       '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '    <label>Perfil del capacitador</label>'+
       '    <select class="form-control" name="Perfil del capacitador" id="perfil_capacitador_presencial">'+
       '      	<option value="">-Seleccione</option>'+

       '	 	<option>TECNICO Y/O TECNOLOGO TITULADO, CON EXPERIENCIA RELACIONADA COMPROBADA MINIMO DE CINCO (5) AÑOS.</option>'+

       '        <option>PROFESIONAL TITULADO CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+
       '      	<option>PROFESIONAL CON TITULO DE ESPECIALIZACION Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+
       '      	<option>PROFESIONAL CON TITULO DE MAESTRIA Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+
       '      	<option>PROFESIONAL CON TITULO DE DOCTORADO Y CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+

       '      	<option>PROFESIONAL TITULADO CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>'+
       '      	<option>PROFESIONAL CON TITULO DE ESPECIALIZACION Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>'+
       '      	<option>PROFESIONAL CON TITULO DE MAESTRIA Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>'+
       '      	<option>PROFESIONAL CON TITULO DE DOCTORADO, CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS, AFIN CON EL OBJETO DE LA FORMACION.</option>'+

       '    </select>'+
       '  </div>'+
       '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
       '    <label>Tipo de capacitador</label>'+
       '    <select class="form-control" name="Tipo de capacitador" id="tipo_capacitador_presencial">'+
       '      <option value="">-Seleccione</option>'+
       '      <option>CAPACITADOR NACIONAL</option>'+
       '      <option>CAPACITADOR INTERNACIONAL</option>'+
       '    </select>'+
       '  </div>'+
       '</div>');

	jQuery.ajax({
		type:"POST",
		url:"index.php?r=generalidades/buscar-all",
		data:{
			tipo_busqueda: "Departamentos",
			identificador: 170
		},
		success:function (response) {
			var jsonResponse = JSON.parse(response);
			if (jsonResponse.status == "success") {
				jQuery("#id_departamento").html('');
				jQuery("#id_departamento").html('<option value="">-Seleccione</option>');
				jQuery.each(jsonResponse.listado,function (key,value) {
					jQuery("#id_departamento").append('<option value="'+value['id_dpto']+'">'+value['nombre']+'</option>');
				})
			}
		}
	});

	jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept"><span class="fa fa-save"></span> Guardar sesión</button>');

	jQuery('.btn-accept').click(function () {
		jQuery("#alerta-sesion-presencial").html('');

		var nombre_sesion 				= jQuery("#nombre_sesion");
		var num_sesion 				   = jQuery("#num_sesion");
		var fecha_inicio 			    	= jQuery("#fecha_inicio");
		var hora_inicial 			    	= jQuery("#hora_inicial");
		var hora_final 				   = jQuery("#hora_final");
		var num_hrs_totales 				= jQuery("#num_hrs_totales");
		var id_ciudad 						= jQuery("#id_ciudad");
		var nombre_sede 					= jQuery("#nombre_sede");
		var direccion_sede 				= jQuery("#direccion_sede");
		var aula_salon 					= jQuery("#aula_salon");
		var nombre_capacitador 		  	= jQuery("#nombre_capacitador_presencial");
		var tipo_doc_capacitador 	  	= jQuery("#tipo_doc_capacitador_presencial");
		var num_doc_capacitador 	  	= jQuery("#num_doc_capacitador_presencial");
		var nombre_capacitador_s1 		= jQuery("#nombre_capacitador_presencial_s1");
		var tipo_doc_capacitador_s1 	= jQuery("#tipo_doc_capacitador_presencial_s1");
		var num_doc_capacitador_s1 	= jQuery("#num_doc_capacitador_presencial_s1");
		var nombre_capacitador_s2 		= jQuery("#nombre_capacitador_presencial_s2");
		var tipo_doc_capacitador_s2 	= jQuery("#tipo_doc_capacitador_presencial_s2");
		var num_doc_capacitador_s2 	= jQuery("#num_doc_capacitador_presencial_s2");
		var perfil_capacitador 			= jQuery("#perfil_capacitador_presencial");
		var tipo_capacitador 			= jQuery("#tipo_capacitador_presencial");

		var campos = [nombre_sesion,num_sesion,fecha_inicio,hora_inicial,hora_final,num_hrs_totales,id_ciudad,nombre_sede,direccion_sede,aula_salon,nombre_capacitador,tipo_doc_capacitador,num_doc_capacitador,perfil_capacitador,tipo_capacitador];

		var j = 15;

		for (var i = 0; i < campos.length; i++) {
			if(campos[i].val() == ""){
				
				jQuery("#alerta-sesion-presencial").html('<div class="alert alert-warning">Por favor diligencie el campo <b>'+campos[i].attr('name')+'</b> para continuar.</div>');	
				campos[i].focus();
				break;

			}else{
				j--;
			}
		}

		if (j == 0) {
			//console.log("Registrar sesión presencial");
			jQuery(".loading").css("display","block");

			jQuery.ajax({
				type:"POST",
				url:"index.php?r=cronograma/registrar-sesion-presencial",
				data:{
					id_cronograma: 			 id_cronograma,
					nombre_sesion: 			 nombre_sesion.val(),
					num_sesion: 				 num_sesion.val(),
					fecha_inicio: 				 fecha_inicio.val(),
					hora_inicial: 				 hora_inicial.val(),
					hora_final: 				 hora_final.val(),
					num_hrs_totales: 			 num_hrs_totales.val(),
					id_ciudad: 					 id_ciudad.val(),
					nombre_sede: 				 nombre_sede.val(),
					direccion_sede: 			 direccion_sede.val(),
					aula_salon: 				 aula_salon.val(),
					nombre_capacitador: 		 nombre_capacitador.val(),
					tipo_doc_capacitador: 	 tipo_doc_capacitador.val(),
					num_doc_capacitador: 	 num_doc_capacitador.val(),
					nombre_capacitador_s1: 	 nombre_capacitador_s1.val(),
					tipo_doc_capacitador_s1: tipo_doc_capacitador_s1.val(),
					num_doc_capacitador_s1:  num_doc_capacitador_s1.val(),
					nombre_capacitador_s2: 	 nombre_capacitador_s2.val(),
					tipo_doc_capacitador_s2: tipo_doc_capacitador_s2.val(),
					num_doc_capacitador_s2:  num_doc_capacitador_s2.val(),
					perfil_capacitador: 		 perfil_capacitador.val(),
					tipo_capacitador: 		 tipo_capacitador.val()
				},
				success:function (response) {
					jQuery(".loading").css("display","none");
					console.log(response);
					var jsonResponse = JSON.parse(response);

					jQuery("#alerta-sesion-presencial").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');

				}
			})

		}

	})

}

// Agregar sesión virtual
function agregarActividadVirtual(id_cronograma){
	jQuery('.modal').modal({backdrop: 'static', keyboard: false});
	jQuery('.modal-title').html('Agregar actividad virtual.');

	jQuery('.modal-body').html(''+
		'<div class="row" id="alerta-sesion-virtual">'+
        '</div>'+
        '<div class="row">'+
        '  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
        '    <label>Nombre de la actividad</label>'+
        '    <input type="text" class="form-control" name="Nombre de la actividad" id="nombre_actividad">'+
        '  </div>'+
        '</div>'+
        '<div class="row">'+
        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
        '    <label>Fecha de inicio de la actividad</label>'+
        '    <input type="date" class="form-control" name="Fecha de inicio de la actividad" id="fecha_inicio_actividad">'+
        '  </div>'+
        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
        '    <label>Fecha de finalización de la actividad</label>'+
        '    <input type="date" class="form-control" name="Fecha de finalización de la actividad" id="fecha_fin_actividad">'+
        '  </div>'+
        '</div>'+
        '<div class="row">'+
        '  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
        '    <label>Proveedor de formación virtual</label>'+
        '    <input type="text" class="form-control" name="Proveedor de formación virtual" id="proveedor_formacion">'+
        '  </div>'+
        '</div>'+
        '<div class="row">'+
        '  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
        '    <label>URL de plataforma virtual</label>'+
        '    <input type="text" class="form-control" name="URL de plataforma virtual" id="url_plataforma">'+
        '  </div>'+
        '</div>'+
        '<div class="row">'+
        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
        '    <label>Usuario SENA</label>'+
        '    <input type="text" class="form-control" name="Usuario SENA" id="usuario_sena">'+
        '  </div>'+
        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
        '    <label>Clave SENA</label>'+
        '    <input type="text" class="form-control" name="Clave SENA" id="clave_sena">'+
        '  </div>'+
        '</div>'+
        '<div class="row">'+
        '  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
        '    <label>Nombre del capacitador</label>'+
        '    <input type="text" class="form-control" name="Nombre del capacitador" id="nombre_capacitador_virtual">'+
        '  </div>'+
        '</div>'+
        '<div class="row">'+
        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
        '    <label>Tipo de documento del capacitador</label>'+
        '    <select class="form-control" name="Tipo de documento del capacitador" id="tipo_doc_capacitador_virtual">'+
        '      <option value="">-Seleccione</option>'+
        '      <option value="CC">CC (Cédula de ciudadanía)</option>'+
        '      <option value="CEX">CEX (Cédula de extranjeria)</option>'+
        '      <option value="PAS">PAS (Pasaporte)</option>'+
        '    </select>'+
        '  </div>'+
        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
        '    <label>Número de identificación del capacitador</label>'+
        '    <input type="number" min="0" class="form-control" name="Número de identificación del capacitador" id="num_doc_capacitador_virtual">'+
        '  </div>'+
        '</div>'+
        '<div class="row">'+
        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
        '    <label>Perfil del capacitador</label>'+
        '    <select class="form-control" name="Perfil del capacitador" id="perfil_capacitador_virtual">'+
        '      <option value="">-Seleccione</option>'+
        '	 	<option>TECNICO Y/O TECNOLOGO TITULADO, CON EXPERIENCIA RELACIONADA COMPROBADA MINIMO DE CINCO (5) AÑOS.</option>'+

        '       <option>PROFESIONAL TITULADO CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+
        '      	<option>PROFESIONAL CON TITULO DE ESPECIALIZACION Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+
        '      	<option>PROFESIONAL CON TITULO DE MAESTRIA Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+
        '      	<option>PROFESIONAL CON TITULO DE DOCTORADO Y CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+

        '      	<option>PROFESIONAL TITULADO CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>'+
        '      	<option>PROFESIONAL CON TITULO DE ESPECIALIZACION Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>'+
        '      	<option>PROFESIONAL CON TITULO DE MAESTRIA Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>'+
        '      	<option>PROFESIONAL CON TITULO DE DOCTORADO, CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS, AFIN CON EL OBJETO DE LA FORMACION.</option>'+
        '    </select>'+
        '  </div>'+
        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
        '    <label>Tipo de capacitador</label>'+
        '    <select class="form-control" name="Tipo de capacitador" id="tipo_capacitador_virtual">'+
        '      <option value="">-Seleccione</option>'+
        '      <option>CAPACITADOR NACIONAL</option>'+
        '      <option>CAPACITADOR INTERNACIONAL</option>'+
        '    </select>'+
        '</div>');

	jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept"><span class="fa fa-save"></span> Guardar actividad</button>');


	jQuery('.btn-accept').click(function () {
		jQuery("#alerta-sesion-virtual").html('');

		var nombre_actividad 		= jQuery("#nombre_actividad");
		var fecha_inicio 				= jQuery("#fecha_inicio_actividad");
		var fecha_fin					= jQuery("#fecha_fin_actividad");
		var proveedor_formacion		= jQuery("#proveedor_formacion");
		var url_plataforma 			= jQuery("#url_plataforma");
		var usuario_sena 				= jQuery("#usuario_sena");
		var clave_sena 				= jQuery("#clave_sena");
		var nombre_capacitador 		= jQuery("#nombre_capacitador_virtual");
		var tipo_doc_capacitador 	= jQuery("#tipo_doc_capacitador_virtual");
		var num_doc_capacitador 	= jQuery("#num_doc_capacitador_virtual");
		var perfil_capacitador 		= jQuery("#perfil_capacitador_virtual");
		var tipo_capacitador 		= jQuery("#tipo_capacitador_virtual");

		var campos = [nombre_actividad,fecha_inicio,fecha_fin,proveedor_formacion,url_plataforma,usuario_sena,clave_sena,nombre_capacitador,tipo_doc_capacitador,num_doc_capacitador,perfil_capacitador,tipo_capacitador];

		var j = 12;

		for (var i = 0; i < campos.length; i++) {
			if(campos[i].val() == ""){
				
				jQuery("#alerta-sesion-virtual").html('<div class="alert alert-warning">Por favor diligencie el campo <b>'+campos[i].attr('name')+'</b> para continuar.</div>');	
				campos[i].focus();
				break;

			}else{
				j--;
			}
		}

		if (j == 0) {
			//console.log("Registrar sesión virtual");
			jQuery(".loading").css("display","block");

			jQuery.ajax({
				type:"POST",
				url:"index.php?r=cronograma/registrar-actividad-virtual",
				data:{
					id_cronograma: 			id_cronograma,
					nombre_actividad: 		nombre_actividad.val(),
					fecha_inicio: 				fecha_inicio.val(),
					fecha_fin: 					fecha_fin.val(),
					proveedor_formacion: 	proveedor_formacion.val(),
					url_plataforma: 			url_plataforma.val(),
					usuario_sena: 				usuario_sena.val(),
					clave_sena: 				clave_sena.val(),
					nombre_capacitador: 		nombre_capacitador.val(),
					tipo_doc_capacitador: 	tipo_doc_capacitador.val(),
					num_doc_capacitador: 	num_doc_capacitador.val(),
					perfil_capacitador: 		perfil_capacitador.val(),
					tipo_capacitador: 		tipo_capacitador.val()
				},
				success:function (response) {
					jQuery(".loading").css("display","none");
					console.log(response);
					var jsonResponse = JSON.parse(response);

					jQuery("#alerta-sesion-virtual").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');

				}
			})

		}

	})

}

// Eliminar (Inactivar) sesion presencial
function eliminarSesionPresencial(id_cronograma_presencial) {
	console.log("Eliminar sesión -> "+id_cronograma_presencial);

	jQuery('.modal').modal({backdrop: 'static', keyboard: false});
	jQuery('.modal-title').html('Eliminar sesión presencial.');
	jQuery('.modal-body').html('A continuación se eliminará la sesión seleccionada.<br>¿Esta seguro de continuar?');
	jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-danger btn-accept"><span class="fa fa-trash"></span> Si, continuar</button>');

	jQuery('.btn-accept').click(function () {
		
		jQuery.ajax({
			type:"POST",
			url:"index.php?r=cronograma/eliminar-sesion-presencial",
			data:{
				id_cronograma_presencial: id_cronograma_presencial
			},
			success:function (response) {
				console.log(response);
				var jsonResponse = JSON.parse(response);
				if (jsonResponse.status == "success") {

					jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
					jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');					

					jQuery('.btn-accept').click(function () {
						buscarCronograma();
					});

				}else{
					jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
					jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
				}
			}
		})

	});

}

// Eliminar (Inactivar) actividad virtual
function eliminarActividadVirtual(id_cronograma_virtual) {
	console.log("Eliminar actividad -> "+id_cronograma_virtual);

	jQuery('.modal').modal({backdrop: 'static', keyboard: false});
	jQuery('.modal-title').html('Eliminar actividad virtual.');
	jQuery('.modal-body').html('A continuación se eliminará la actividad seleccionada.<br>¿Esta seguro de continuar?');
	jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-danger btn-accept"><span class="fa fa-trash"></span> Si, continuar</button>');

	jQuery('.btn-accept').click(function () {
		
		jQuery.ajax({
			type:"POST",
			url:"index.php?r=cronograma/eliminar-actividad-virtual",
			data:{
				id_cronograma_virtual: id_cronograma_virtual
			},
			success:function (response) {
				console.log(response);
				var jsonResponse = JSON.parse(response);
				if (jsonResponse.status == "success") {

					jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
					jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');					

					jQuery('.btn-accept').click(function () {
						buscarCronograma();
					});

				}else{
					jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
					jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
				}
			}
		})

	});

}