// Contador de estadisticas panel de inicio
jQuery('.count').each(function () {
    jQuery(this).prop('Counter',0).animate({
        Counter: jQuery(this).text()
    }, {
        duration: 1500,
        easing: 'swing',
        step: function (now) {
            jQuery(this).text(Math.ceil(now));
        }
    });
});


// Funcion de validacion de datos en input numerico
function validarNumero(element) {
	//console.log(element);
	var exp = /^([0-9])*$/;
	if (!exp.test(element.value)){
      //alert("El valor " + element.value + " no es un número");
      jQuery("#"+element.id).val('');
	}
}