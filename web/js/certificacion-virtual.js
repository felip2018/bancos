function evaluarHorasVirtuales(total_hrs,i) {
	var exp = /^([0-9])*$/;
	var hrs_cumplimiento = jQuery("#hrs_cumplimiento_"+i);

	if (!exp.test(hrs_cumplimiento.val())){
      hrs_cumplimiento.val('');
	}else{
		if (hrs_cumplimiento.val() <= total_hrs) {
			var porcentaje = Math.round((parseInt(hrs_cumplimiento.val())*100)/total_hrs);
			jQuery("#por_cumplimiento_"+i).val(porcentaje);

			var fondo = (porcentaje>=85)?'#10ac84':'#ee5253';
			var certifica = (porcentaje>=85)?'SI':'NO';

			jQuery("#certifica_"+i).css({'background':fondo,'color':'#FFFFFF'});
			jQuery("#certifica_"+i).val(certifica);

		}else{
			hrs_cumplimiento.val(0);
			jQuery("#por_cumplimiento_"+i).val(0);
			jQuery("#certifica_"+i).css({'background':'#ee5253','color':'#FFFFFF'});
			jQuery("#certifica_"+i).val('NO');
		}
	}
}

function validarCertificacionVirtual() {

	var beneficiarios = jQuery('.beneficiario');

	var arregloRegistrar = [];

	jQuery.each(beneficiarios,function (key,element) {
		var valor 	= element.id.split('_');
		var i 		= valor[1];
		//console.log(element.value);
		var beneficiario 		= element.value;
		var hrs_cumplimiento 	= jQuery('#hrs_cumplimiento_'+i).val();
		var por_cumplimiento 	= jQuery('#por_cumplimiento_'+i).val();
		var certifica 			= jQuery('#certifica_'+i).val();

		var arreglo = [beneficiario,hrs_cumplimiento,por_cumplimiento,certifica];

		arregloRegistrar.push(arreglo);

	});
	
	//console.log(arregloRegistrar);

	jQuery('.modal').modal({backdrop: 'static', keyboard: false});
    jQuery('.modal-title').html('Guardar datos de certificación.');
    jQuery('.modal-body').html('A continuación podrá realizar el registro de los datos de certificación de los beneficiarios pertenecientes a la acción de formación seleccionada.<br>¿Desea continuar?');
    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Aceptar</button>');
	
	jQuery('.btn-accept').click(function () {
		jQuery('.loading').css('display','block');

		jQuery.ajax({
			type:"POST",
			url:"index.php?r=certificacion/registrar-certificacion-virtual",
			data:{
				arregloRegistrar
			},
			success:function (response) {
				jQuery('.loading').css('display','none');
				//console.log(response);
				var jsonResponse = JSON.parse(response);
				if (jsonResponse.status == 'success') {
					jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
					jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

					jQuery('.btn-accept').click(function () {
						location.reaload();
					})
				}else{
					jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
					jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
				}
			}
		})

	})
}