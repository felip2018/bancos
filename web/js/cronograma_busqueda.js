// Buscar grupos de la accion de formacion seleccionada
function buscarGruposAF(accion) {
	
	var listadoAccionesFormacion = jQuery('#listadoAccionesFormacion');

	if (listadoAccionesFormacion.val() != '') {

		var arreglo = accion.split('_');

		jQuery.ajax({
			type:"POST",
			url:"index.php?r=cronograma/buscar-grupos-accion-formacion",
			data:{
				id_accion_formacion: arreglo[0]
			},
			success:function (response) {
				//console.log(response);
				var jsonResponse = JSON.parse(response);
				if (jsonResponse.status == 'success') {
					jQuery("#listadoGrupos").removeAttr('disabled');

					jQuery("#listadoGrupos").html('');
					jQuery("#listadoGrupos").html('<option value="">-Seleccione</option>');
					jQuery.each(jsonResponse.grupos,function (key,value) {
						var grupo = value['id_grupo']+'_'+value['nombre'];
						jQuery("#listadoGrupos").append('<option value="'+grupo+'">'+value['nombre']+'</option>');
					});

				}else{
					jQuery("#listadoGrupos").attr('disabled','true');
				}
			}
		});

	}else{
		jQuery('#listadoGrupos').attr('disabled','true');
	}
}

// Buscar unidades tematicas registradas en el cronograma del grupo seleccionado
function buscarUnidadesTematicas(grupo) {
	
	var listadoAccionesFormacion = jQuery('#listadoAccionesFormacion');
	var listadoGrupos = jQuery('#listadoGrupos');

	if (listadoGrupos.val() != '') {

		var accion 	= listadoAccionesFormacion.val().split('_');
		var grupo 	= listadoGrupos.val().split('_');

		jQuery.ajax({
			type:"POST",
			url:"index.php?r=cronograma/buscar-unidades-tematicas",
			data:{
				id_accion_formacion: accion[0],
				id_grupo: grupo[0]
			},
			success:function (response) {
				console.log(response);
				var jsonResponse = JSON.parse(response);
				if (jsonResponse.status == 'success') {
					jQuery("#listadoUnidadesTematicas").removeAttr('disabled');

					jQuery("#listadoUnidadesTematicas").html('');
					jQuery("#listadoUnidadesTematicas").html('<option value="">-Seleccione</option>');

					jQuery.each(jsonResponse.unidades_tematicas,function (key,value) {

						var unidad_tematica = 'Unidad No.'+value['num_unidad_tematica'];
						jQuery("#listadoUnidadesTematicas").append('<option value="'+value['num_unidad_tematica']+'">'+unidad_tematica+'</option>');

					});

				}else{
					jQuery("#listadoUnidadesTematicas").attr('disabled','true');
				}
			}
		});

	}else{
		jQuery('#listadoUnidadesTematicas').attr('disabled','true');
	}
}

// Buscar cronograma por accion de formacion, grupo y numero de unidad tematica
function buscarCronograma() {
	var listadoAccionesFormacion 	= jQuery("#listadoAccionesFormacion");
	var listadoGrupos 				= jQuery("#listadoGrupos");
	var listadoUnidadesTematicas 	= jQuery("#listadoUnidadesTematicas");

	jQuery("#alerta-registro-cronograma").html('');

	if (listadoAccionesFormacion.val() != '') {
		if (listadoGrupos.val() != '') {
			if (listadoUnidadesTematicas.val() != '') {

				// Limpiar campos del formulario
				jQuery("#num_beneficiarios_grupo").val('');
				jQuery("#duracion_total_eformacion").val('');
				jQuery("#total_hrs_unidad_tematica").val('');
				jQuery("#num_unidad_tematica").val('');
				jQuery("#nombre_unidad_tematica").val('');
				jQuery("#fecha_inicio_unidad_tematica").val('');
				jQuery("#fecha_finalizacion_unidad_tematica").val('');
				jQuery("#metodologia_presencial").val('');
				jQuery("#metodologia_virtual").val('');
				jQuery("#num_personas").val('');
				jQuery("#sesiones_por_persona").val('');
				jQuery("#num_sesiones").val('');
				jQuery("#num_actividades").val('');

				jQuery.ajax({
					type:"POST",
					url:"index.php?r=cronograma/buscar-cronograma",
					data:{
						accion_formacion: 	listadoAccionesFormacion.val(),
						grupo: 				listadoGrupos.val(),
						unidad_tematica: 	listadoUnidadesTematicas.val()
					},
					success:function (response) {
						//console.log(response);

						var jsonResponse = JSON.parse(response);
						//console.log(jsonResponse);

						if (jsonResponse.status == 'success') {
							jQuery("#id_cronograma").val(jsonResponse.cronograma['id_cronograma']);

							if (jsonResponse.cronograma['modalidad'] == 'PRESENCIAL') {

								jQuery("#num_beneficiarios_grupo").val(jsonResponse.cronograma['num_beneficiarios_grupo']);
								jQuery("#duracion_total_eformacion").val(jsonResponse.cronograma['duracion_total_eformacion']);
								jQuery("#total_hrs_unidad_tematica").val(jsonResponse.cronograma['total_hrs_unidad_tematica']);
								jQuery("#num_unidad_tematica").val(jsonResponse.cronograma['num_unidad_tematica']);
								jQuery("#nombre_unidad_tematica").val(jsonResponse.cronograma['nombre_unidad_tematica']);
								jQuery("#fecha_inicio_unidad_tematica").val(jsonResponse.cronograma['fecha_inicio']);
								jQuery("#fecha_finalizacion_unidad_tematica").val(jsonResponse.cronograma['fecha_finalizacion']);


								jQuery("#metodologia_presencial").removeAttr('disabled');
								jQuery("#metodologia_presencial").html('');
								jQuery("#metodologia_presencial").html('<option value="">-Seleccione</option>'+
									'<option value="TEORICA">TEORICA</option>'+
									'<option value="TEORICO - PRACTICA">TEORICO - PRACTICA</option>'+
									'<option value="APRENDIZAJE PUESTO DE TRABAJO REAL">APRENDIZAJE PUESTO DE TRABAJO REAL</option>');

								jQuery("#metodologia_presencial").val(jsonResponse.cronograma['metodologia_presencial']);

								jQuery("#metodologia_virtual").attr('disabled','true');

								jQuery("#num_personas").removeAttr('disabled');
								jQuery("#num_personas").val(jsonResponse.cronograma['num_personas']);
								jQuery("#sesiones_por_persona").removeAttr('disabled');
								jQuery("#sesiones_por_persona").val(jsonResponse.cronograma['sesiones_por_persona']);

								jQuery("#num_sesiones").removeAttr('disabled');
								jQuery("#num_sesiones").val(jsonResponse.cronograma['num_sesiones']);
								
								jQuery("#num_actividades").attr('disabled','true');

								jQuery("#listado_actividades").html('');
								jQuery("#listado_sesiones").html('');
								
								jQuery.each(jsonResponse.sesiones,function (key,value) {
									//console.log("Estado interventoria: "+jsonResponse.cronograma['estado_interventoria']);
									var background 	= (value['estado_registro'] == 'ACTIVO') ? '#ee5253' : '#10ac84';
									var icono 		= (value['estado_registro'] == 'ACTIVO') ? 'fa-times' : 'fa-check';
									var title 		= (value['estado_registro'] == 'ACTIVO') ? 'INACTIVAR SESION' : 'ACTIVAR SESION';

									//var btn 		= (jsonResponse.cronograma['estado_interventoria'] == 'RADICADO') ? '' : '<button class="btn btn-secondary pull-right" onclick=editarSesionPresencial('+value['id_cronograma_presencial']+')><span class="fa fa-pencil"></span></button>';
									var btn 		= '<button class="btn btn-secondary pull-right" onclick=editarSesionPresencial('+value['id_cronograma_presencial']+')><span class="fa fa-pencil"></span></button>';


									jQuery("#listado_sesiones").append('<div style="padding:10px;border:1px solid #DDDDDD;margin-bottom:10px;">'+

											btn+

											'<button class="btn btn-danger pull-right" onclick=eliminarSesionPresencial('+value['id_cronograma_presencial']+')><span class="fa fa-times"></span></button>'+

											'<b>Sesión No.</b>'+value['num_sesion']+',<b>'+value['nombre_sesion']+'</b><br>'+
											'<b>Fecha de inicio: </b>'+value['fecha_inicio']+' / <b>Hora inicio: </b>'+value['hora_inicial']+'- <b>Hora final: </b>'+value['hora_final']+', <b>Nùmero de hrs totales: </b>'+value['num_hrs_totales']+'<br>'+
											'<b>Sede: </b>'+value['nombre_sede']+'<br>'+
											'<b>Dirección: </b>'+value['direccion_sede']+'<br>'+
											'<b>Aula/Salón: </b>'+value['aula_salon']+'<br>'+
											'<b>Capacitador: </b>'+value['nombre_capacitador']+'<br>'+
											'<b>Tipo de identificación: </b>'+value['tipo_doc_capacitador']+' - <b>Número de idenficiación: </b>'+value['num_doc_capacitador']+'<br>'+
											'<b>Capacitador suplente 1: </b>'+value['nombre_capacitador_s1']+'<br>'+
											'<b>Tipo de identificación: </b>'+value['tipo_doc_capacitador_s1']+' - <b>Número de idenficiación: </b>'+value['num_doc_capacitador_s1']+'<br>'+
											'<b>Capacitador suplente 2: </b>'+value['nombre_capacitador_s2']+'<br>'+
											'<b>Tipo de identificación: </b>'+value['tipo_doc_capacitador_s2']+' - <b>Número de idenficiación: </b>'+value['num_doc_capacitador_s2']+'<br>'+
											'<b>Perfil: </b>'+value['perfil_capacitador']+'<br>'+
											'<b>tipo: </b>'+value['tipo_capacitador']+'<br>'+
										'</div>');
								});

								var id_cronograma = jsonResponse.cronograma['id_cronograma'];

								jQuery('#div-btn-sesiones').html('');
								jQuery('#div-btn-actividades').html('');
								
								jQuery('#div-btn-sesiones').html('<button class="btn btn-primary pull-right btn-add-session" onclick=agregarSesionPresencial('+id_cronograma+')><span class="fa fa-plus"></span> Agregar sesión presencial</button>');

							}else if (jsonResponse.cronograma['modalidad'] == 'VIRTUAL') {

								jQuery("#num_beneficiarios_grupo").val(jsonResponse.cronograma['num_beneficiarios_grupo']);
								jQuery("#duracion_total_eformacion").val(jsonResponse.cronograma['duracion_total_eformacion']);
								jQuery("#total_hrs_unidad_tematica").val(jsonResponse.cronograma['total_hrs_unidad_tematica']);
								jQuery("#num_unidad_tematica").val(jsonResponse.cronograma['num_unidad_tematica']);
								jQuery("#nombre_unidad_tematica").val(jsonResponse.cronograma['nombre_unidad_tematica']);
								jQuery("#fecha_inicio_unidad_tematica").val(jsonResponse.cronograma['fecha_inicio']);
								jQuery("#fecha_finalizacion_unidad_tematica").val(jsonResponse.cronograma['fecha_finalizacion']);
								
								jQuery("#metodologia_presencial").attr('disabled','true');
								jQuery("#metodologia_virtual").removeAttr('disabled');
								jQuery("#metodologia_virtual").html('');
								jQuery("#metodologia_virtual").html('<option value="">-Seleccione</option>'+
									'<option value="TEORICO - PRACTICA">TEORICO - PRACTICA</option>'+
									'<option value="VIRTUAL MASIVA">VIRTUAL MASIVA</option>');

								jQuery("#metodologia_virtual").val(jsonResponse.cronograma['metodologia_virtual']);


								jQuery("#num_personas").attr('disabled','true');
								jQuery("#sesiones_por_persona").attr('disabled','true');

								jQuery("#num_sesiones").attr('disabled','true');
								jQuery("#num_actividades").removeAttr('disabled');
								jQuery("#num_actividades").val(jsonResponse.cronograma['num_actividades']);

								jQuery("#listado_sesiones").html('');
								jQuery("#listado_actividades").html('');

								jQuery.each(jsonResponse.actividades,function (key,value) {

									var background 	= (value['estado_registro'] == 'ACTIVO') ? '#ee5253' : '#10ac84';
									var icono 		= (value['estado_registro'] == 'ACTIVO') ? 'fa-times' : 'fa-check';
									var title 		= (value['estado_registro'] == 'ACTIVO') ? 'INACTIVAR ACTIVIDAD' : 'ACTIVAR ACTIVIDAD';
									//var btn 		= (jsonResponse.cronograma['estado_interventoria'] == 'RADICADO') ? '' : '<button class="btn btn-secondary pull-right" onclick=editarActividadVirtual('+value['id_cronograma_virtual']+')><span class="fa fa-pencil"></span></button>';
									var btn 		= '<button class="btn btn-secondary pull-right" onclick=editarActividadVirtual('+value['id_cronograma_virtual']+')><span class="fa fa-pencil"></span></button>';

									jQuery("#listado_actividades").append('<div style="padding:10px;border:1px solid #DDDDDD;margin-bottom:10px;">'+

											btn+

											'<button class="btn btn-danger pull-right" onclick=eliminarActividadVirtual('+value['id_cronograma_virtual']+')><span class="fa fa-times"></span></button>'+

											'<b>'+value['nombre_actividad']+'</b><br>'+
											'<b>Fecha de inicio: </b>'+value['fecha_inicio_actividad']+' / <b>Fecha de finalización: </b>'+value['fecha_fin_actividad']+'<br>'+
											'<b>Proveedor: </b>'+value['proveedor_formacion']+'<br>'+
											'<b>URL Plataforma: </b>'+value['url_plataforma']+'<br>'+
											'<b>Usuario SENA: </b>'+value['usuario_sena']+' - <b>Clave SENA: </b>'+value['clave_sena']+'<br>'+
											'<b>Capacitador: </b>'+value['nombre_capacitador']+'<br>'+
											'<b>identificación: </b>'+value['tipo_doc_capacitador']+'-'+value['num_doc_capacitador']+'<br>'+
											'<b>Perfil: </b>'+value['perfil_capacitador']+'<br>'+
											'<b>tipo: </b>'+value['tipo_capacitador']+'<br>'+
										'</div>');
								});

								var id_cronograma = jsonResponse.cronograma['id_cronograma'];

								jQuery('#div-btn-sesiones').html('');
								jQuery('#div-btn-actividades').html('');

								jQuery('#div-btn-actividades').html('<button class="btn btn-primary pull-right btn-add-activity" onclick=agregarActividadVirtual('+id_cronograma+')><span class="fa fa-plus"></span> Agregar actividad virtual</button>');

							}else if (jsonResponse.cronograma['modalidad'] == 'COMBINADA') {

								jQuery("#num_beneficiarios_grupo").val(jsonResponse.cronograma['num_beneficiarios_grupo']);
								jQuery("#duracion_total_eformacion").val(jsonResponse.cronograma['duracion_total_eformacion']);
								jQuery("#total_hrs_unidad_tematica").val(jsonResponse.cronograma['total_hrs_unidad_tematica']);
								jQuery("#num_unidad_tematica").val(jsonResponse.cronograma['num_unidad_tematica']);
								jQuery("#nombre_unidad_tematica").val(jsonResponse.cronograma['nombre_unidad_tematica']);
								jQuery("#fecha_inicio_unidad_tematica").val(jsonResponse.cronograma['fecha_inicio']);
								jQuery("#fecha_finalizacion_unidad_tematica").val(jsonResponse.cronograma['fecha_finalizacion']);

								jQuery("#metodologia_presencial").removeAttr('disabled');
								jQuery("#metodologia_virtual").removeAttr('disabled');
								jQuery("#num_personas").removeAttr('disabled');
								jQuery("#sesiones_por_persona").removeAttr('disabled');
								jQuery("#num_sesiones").removeAttr('disabled');
								jQuery("#num_actividades").removeAttr('disabled');

								jQuery("#metodologia_presencial").html('');
								jQuery("#metodologia_presencial").html('<option value="">-Seleccione</option>'+
									'<option value="TEORICO - PRACTICA">TEORICO - PRACTICA</option>'+
									'<option value="APRENDIZAJE AULA INVERTIDA">APRENDIZAJE AULA INVERTIDA</option>');

								jQuery("#metodologia_virtual").html('');
								jQuery("#metodologia_virtual").html('<option value="">-Seleccione</option>'+
									'<option value="TEORICO - PRACTICA">TEORICO - PRACTICA</option>'+
									'<option value="VIRTUAL MASIVA">VIRTUAL MASIVA</option>');

								jQuery("#metodologia_presencial").val(jsonResponse.cronograma['metodologia_presencial']);
								jQuery("#metodologia_virtual").val(jsonResponse.cronograma['metodologia_virtual']);
								jQuery("#num_personas").attr('disabled','true');
								jQuery("#sesiones_por_persona").attr('disabled','true');
								jQuery("#num_sesiones").val(jsonResponse.cronograma['num_sesiones']);
								jQuery("#num_actividades").val(jsonResponse.cronograma['num_actividades']);

								jQuery("#listado_actividades").html('');
								jQuery("#listado_sesiones").html('');
								
								jQuery.each(jsonResponse.sesiones,function (key,value) {

									var background 	= (value['estado_registro'] == 'ACTIVO') ? '#ee5253' : '#10ac84';
									var icono 		= (value['estado_registro'] == 'ACTIVO') ? 'fa-times' : 'fa-check';
									var title 		= (value['estado_registro'] == 'ACTIVO') ? 'INACTIVAR SESION' : 'ACTIVAR SESION';
									//var btn 		= (jsonResponse.cronograma['estado_interventoria'] == 'RADICADO') ? '' : '<button class="btn btn-secondary pull-right" onclick=editarSesionPresencial('+value['id_cronograma_presencial']+')><span class="fa fa-pencil"></span></button>'
									var btn 		= '<button class="btn btn-secondary pull-right" onclick=editarSesionPresencial('+value['id_cronograma_presencial']+')><span class="fa fa-pencil"></span></button>'

									jQuery("#listado_sesiones").append('<div style="padding:10px;border:1px solid #DDDDDD;margin-bottom:10px;">'+

											btn+

											'<button class="btn btn-danger pull-right" onclick=eliminarSesionPresencial('+value['id_cronograma_presencial']+')><span class="fa fa-times"></span></button>'+

											'<b>Sesión No.</b>'+value['num_sesion']+',<b>'+value['nombre_sesion']+'</b><br>'+
											'<b>Fecha de inicio: </b>'+value['fecha_inicio']+' / <b>Hora inicio: </b>'+value['hora_inicial']+'- <b>Hora final: </b>'+value['hora_final']+', <b>Nùmero de hrs totales: </b>'+value['num_hrs_totales']+'<br>'+
											'<b>Sede: </b>'+value['nombre_sede']+'<br>'+
											'<b>Dirección: </b>'+value['direccion_sede']+'<br>'+
											'<b>Aula/Salón: </b>'+value['aula_salon']+'<br>'+
											'<b>Capacitador: </b>'+value['nombre_capacitador']+'<br>'+
											'<b>Tipo de identificación: </b>'+value['tipo_doc_capacitador']+' - <b>Número de idenficiación: </b>'+value['num_doc_capacitador']+'<br>'+
											'<b>Capacitador suplente 1: </b>'+value['nombre_capacitador_s1']+'<br>'+
											'<b>Tipo de identificación: </b>'+value['tipo_doc_capacitador_s1']+' - <b>Número de idenficiación: </b>'+value['num_doc_capacitador_s1']+'<br>'+
											'<b>Capacitador suplente 2: </b>'+value['nombre_capacitador_s2']+'<br>'+
											'<b>Tipo de identificación: </b>'+value['tipo_doc_capacitador_s2']+' - <b>Número de idenficiación: </b>'+value['num_doc_capacitador_s2']+'<br>'+
											'<b>Perfil: </b>'+value['perfil_capacitador']+'<br>'+
											'<b>tipo: </b>'+value['tipo_capacitador']+'<br>'+
										'</div>');
								});

								jQuery.each(jsonResponse.actividades,function (key,value) {

									var background 	= (value['estado_registro'] == 'ACTIVO') ? '#ee5253' : '#10ac84';
									var icono 		= (value['estado_registro'] == 'ACTIVO') ? 'fa-times' : 'fa-check';
									var title 		= (value['estado_registro'] == 'ACTIVO') ? 'INACTIVAR ACTIVIDAD' : 'ACTIVAR ACTIVIDAD';
									//var btn 		= (jsonResponse.cronograma['estado_interventoria'] == 'RADICADO') ? '' : '<button class="btn btn-secondary pull-right" onclick=editarActividadVirtual('+value['id_cronograma_virtual']+')><span class="fa fa-pencil"></span></button>';
									var btn 		= '<button class="btn btn-secondary pull-right" onclick=editarActividadVirtual('+value['id_cronograma_virtual']+')><span class="fa fa-pencil"></span></button>';

									jQuery("#listado_actividades").append('<div style="padding:10px;border:1px solid #DDDDDD;margin-bottom:10px;">'+

											btn+

											'<button class="btn btn-danger pull-right" onclick=eliminarActividadVirtual('+value['id_cronograma_virtual']+')><span class="fa fa-times"></span></button>'+

											'<b>'+value['nombre_actividad']+'</b><br>'+
											'<b>Fecha de inicio: </b>'+value['fecha_inicio_actividad']+' / <b>Fecha de finalización: </b>'+value['fecha_fin_actividad']+'<br>'+
											'<b>Proveedor: </b>'+value['proveedor_formacion']+'<br>'+
											'<b>URL Plataforma: </b>'+value['url_plataforma']+'<br>'+
											'<b>Usuario SENA: </b>'+value['usuario_sena']+' - <b>Clave SENA: </b>'+value['clave_sena']+'<br>'+
											'<b>Capacitador: </b>'+value['nombre_capacitador']+'<br>'+
											'<b>identificación: </b>'+value['tipo_doc_capacitador']+'-'+value['num_doc_capacitador']+'<br>'+
											'<b>Perfil: </b>'+value['perfil_capacitador']+'<br>'+
											'<b>tipo: </b>'+value['tipo_capacitador']+'<br>'+
										'</div>');
								});

								jQuery('#div-btn-sesiones').html('');
								jQuery('#div-btn-actividades').html('');

								jQuery('#div-btn-sesiones').html('<button class="btn btn-primary pull-right btn-add-session" onclick=agregarSesionPresencial('+id_cronograma+')><span class="fa fa-plus"></span> Agregar sesión presencial</button>');

								jQuery('#div-btn-actividades').html('<button class="btn btn-primary pull-right btn-add-activity" onclick=agregarActividadVirtual('+id_cronograma+')><span class="fa fa-plus"></span> Agregar actividad virtual</button>');
							}

						}else if (jsonResponse.status == 'vacio') {
							jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">No se encontraron resultados de cronograma.</div>');
						}

					}
				})

			}else{
				jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Seleccione una unidad temática para realizar la búsqueda del cronograma registrado.</div>');
			}
		}else{
			jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Seleccione un grupo de la acción de formación seleccionada.</div>');	
		}
	}else{
		jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Seleccione una acción de formación.</div>');
	}

}

// Editar cronograma en la opcion de busqueda
function actualizarCronograma() {

	jQuery("#alerta-registro-cronograma").html('');

	var listadoAccionesFormacion 	= jQuery("#listadoAccionesFormacion");
	var listadoGrupos 				= jQuery("#listadoGrupos");
	var listadoUnidadesTematicas 	= jQuery("#listadoUnidadesTematicas");

	if (listadoAccionesFormacion.val() != '') {
		if (listadoGrupos.val() != '') {
			if (listadoUnidadesTematicas.val() != '') {

				var id_cronograma 				= jQuery("#id_cronograma");

				var num_beneficiarios_grupo 	= jQuery("#num_beneficiarios_grupo");
				var duracion_total_eformacion 	= jQuery("#duracion_total_eformacion");
				var total_hrs_unidad_tematica 	= jQuery("#total_hrs_unidad_tematica");
				var num_unidad_tematica 		= jQuery("#num_unidad_tematica");
				var nombre_unidad_tematica 		= jQuery("#nombre_unidad_tematica");
				var fecha_inicio 				= jQuery("#fecha_inicio_unidad_tematica");
				var fecha_finalizacion 			= jQuery("#fecha_finalizacion_unidad_tematica");
				var metodologia_presencial 		= jQuery("#metodologia_presencial");
				var metodologia_virtual 		= jQuery("#metodologia_virtual");
				var num_personas 				= jQuery("#num_personas");
				var sesiones_por_persona 		= jQuery("#sesiones_por_persona");
				var num_sesiones 				= jQuery("#num_sesiones");
				var num_actividades 			= jQuery("#num_actividades");

				var campos = [num_beneficiarios_grupo,duracion_total_eformacion,total_hrs_unidad_tematica,num_unidad_tematica,nombre_unidad_tematica,fecha_inicio,fecha_finalizacion];

				var j = 7;

				for (var i = 0; i < campos.length; i++) {
					if(campos[i].val() == ""){
						jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Diligencie el campo <b>'+campos[i].attr('name')+'</b></div>');
						campos[i].focus();
						break;
					}else{
						j--;
					}
				}

				if (j == 0) {

					jQuery('.modal').modal({backdrop: 'static', keyboard: false});
					jQuery('.modal-title').html('Editar datos del Cronograma.');
					jQuery('.modal-body').html('¿Desea realizar la actualización de los datos del cronograma seleccionado?');
					jQuery('.modal-footer').html('<button class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept" >Aceptar</button>');

					jQuery(".btn-accept").click(function () {
						jQuery(".loading").css('display','block');
						jQuery.ajax({
							type:"POST",
							url:"index.php?r=cronograma/actualizar-cronograma",
							data:{
								id_cronograma: 				id_cronograma.val(),
								accionFormacion: 			listadoAccionesFormacion.val(),
								grupo: 						listadoGrupos.val(),
								unidadTematica: 			listadoUnidadesTematicas.val(),
								num_beneficiarios_grupo: 	num_beneficiarios_grupo.val(),
								duracion_total_eformacion: 	duracion_total_eformacion.val(),
								total_hrs_unidad_tematica: 	total_hrs_unidad_tematica.val(),
								num_unidad_tematica: 		num_unidad_tematica.val(),
								nombre_unidad_tematica: 	nombre_unidad_tematica.val(),
								fecha_inicio: 				fecha_inicio.val(),
								fecha_finalizacion: 		fecha_finalizacion.val(),
								metodologia_presencial: 	metodologia_presencial.val(),
								metodologia_virtual: 		metodologia_virtual.val(),
								num_personas: 				num_personas.val(),
								sesiones_por_persona: 		sesiones_por_persona.val(),
								num_sesiones: 				num_sesiones.val(),
								num_actividades: 			num_actividades.val()
							},
							success:function (response) {
								jQuery(".loading").css('display','none');
								console.log(response);
								var jsonResponse = JSON.parse(response);

								jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
								jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

							}
						})
					});
				}

			}else{
				jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Seleccione una Unidad Temática de la lista.</div>');
				listadoUnidadesTematicas.focus();
			}
		}else{
			jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Seleccione un Grupo de la lista.</div>');
			listadoGrupos.focus();
		}
	}else{
		jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Seleccione una Acción de Formación de la lista.</div>');
		listadoAccionesFormacion.focus();
	}

}


// Editar datos de la sesion presencial seleccionada
function editarSesionPresencial(id_cronograma_presencial) {
	console.log("id_cronograma_presencial: "+id_cronograma_presencial);

	jQuery.ajax({
		type:"POST",
		url:"index.php?r=cronograma/buscar-sesion-presencial",
		data:{
			id_cronograma_presencial: id_cronograma_presencial
		},
		success:function (response) {
			console.log(response);
			var jsonResponse = JSON.parse(response);

			if (jsonResponse.status == 'success') {

				var id_cronograma_presencial= jsonResponse.sesion['id_cronograma_presencial'];
				var nombre_sesion 			= jsonResponse.sesion['nombre_sesion'];
				var num_sesion 				= jsonResponse.sesion['num_sesion'];
				var fecha_inicio 			= jsonResponse.sesion['fecha_inicio'];
				var hora_inicial 			= jsonResponse.sesion['hora_inicial'];
				var hora_final 				= jsonResponse.sesion['hora_final'];
				var num_hrs_totales 		= jsonResponse.sesion['num_hrs_totales'];
				var id_ciudad 				= jsonResponse.sesion['id_ciudad'];
				var nombre_sede 			= jsonResponse.sesion['nombre_sede'];
				var direccion_sede 			= jsonResponse.sesion['direccion_sede'];
				var aula_salon 				= jsonResponse.sesion['aula_salon'];
				var nombre_capacitador 		= jsonResponse.sesion['nombre_capacitador'];
				var tipo_doc_capacitador 	= jsonResponse.sesion['tipo_doc_capacitador'];
				var num_doc_capacitador 	= jsonResponse.sesion['num_doc_capacitador'];
				var nombre_capacitador_s1 	= jsonResponse.sesion['nombre_capacitador_s1'];
				var tipo_doc_capacitador_s1 = jsonResponse.sesion['tipo_doc_capacitador_s1'];
				var num_doc_capacitador_s1 	= jsonResponse.sesion['num_doc_capacitador_s1'];
				var nombre_capacitador_s2 	= jsonResponse.sesion['nombre_capacitador_s2'];
				var tipo_doc_capacitador_s2 = jsonResponse.sesion['tipo_doc_capacitador_s2'];
				var num_doc_capacitador_s2 	= jsonResponse.sesion['num_doc_capacitador_s2'];
				var perfil_capacitador 		= jsonResponse.sesion['perfil_capacitador'];
				var tipo_capacitador 		= jsonResponse.sesion['tipo_capacitador'];

				jQuery('.modal').modal({backdrop: 'static', keyboard: false});
				jQuery('.modal-title').html('Editar sesión presencial.');

				jQuery('.modal-body').html(''+
			       '<div class="row" id="alerta-sesion-presencial">'+
			       '</div>'+
			       ' <div class="row">'+
			       '   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
			       '	 <input type="hidden" id="id_cronograma_presencial" value="'+id_cronograma_presencial+'">'+
			       '   </div>'+
			       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '     <label>Nombre de la sesión</label>'+
			       '     <input class="form-control" name="Nombre de la sesión" id="nombre_sesion" value="'+nombre_sesion+'">'+
			       '   </div>'+
			       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '     <label>Número de sesión</label>'+
			       '     <input type="number" min="0" class="form-control" name="Número de sesión" id="num_sesion" value="'+num_sesion+'">'+
			       '   </div>'+
			       ' </div>'+
			       ' <div class="row">'+
			       '   <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">'+
			       '     <label>Fecha de inicio</label>'+
			       '     <input type="date" class="form-control" name="Fecha de inicio" id="fecha_inicio" value="'+fecha_inicio+'">'+
			       '   </div>'+
			       '   <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">'+
			       '     <label>Hora de inicio (24hrs)</label>'+
			       '     <input type="time" class="form-control" name="Hora de inicio" id="hora_inicial" value="'+hora_inicial+'">'+
			       '   </div>'+
			       '   <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">'+
			       '     <label>Hora de finalización (24hrs)</label>'+
			       '     <input type="time" class="form-control" name="Hora de finalización" id="hora_final" value="'+hora_final+'">'+
			       '   </div>'+
			       '   <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">'+
			       '     <label>Número de horas totales</label>'+
			       '     <input type="number" min="0" class="form-control" name="Número de horas totales" id="num_hrs_totales" value="'+num_hrs_totales+'">'+
			       '   </div>'+
			       ' </div>'+
			       ' <div class="row">'+
			       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '     <label>Departamento</label>'+
			       '     <select class="form-control" name="Departamento" id="id_departamento" onchange="buscarCiudades(this.value)">'+
			       '       <option value="">-Seleccione</option>'+
			       '     </select>'+
			       '   </div>'+
			       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '     <label>Ciudad</label>'+
			       '     <select class="form-control" name="Ciudad" id="id_ciudad"><option value="">-Seleccione</option></select>'+
			       '   </div>'+
			       ' </div>'+
			       ' <div class="row">'+
			       '   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
			       '     <label>Nombre de la sede</label>'+
			       '     <input type="text" class="form-control" name="Nombre de la sede" id="nombre_sede" value="'+nombre_sede+'">'+
			       '   </div>'+
			       ' </div>'+
			       ' <div class="row">'+
			       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '     <label>Dirección de la sede</label>'+
			       '     <input type="text" class="form-control" name="Dirección de la sede" id="direccion_sede" value="'+direccion_sede+'">'+
			       '   </div>'+
			       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '     <label>Aula / Salón</label>'+
			       '     <input type="text" class="form-control" name="Aula / Salón" id="aula_salon" value="'+aula_salon+'">'+
			       '   </div>'+
			       ' </div>'+
			       ' <div class="row">'+
			       '   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
			       '     <label>Nombre del capacitador</label>'+
			       '     <input type="text" class="form-control" name="Nombre de capacitador" id="nombre_capacitador_presencial" value="'+nombre_capacitador+'">'+
			       '   </div>'+
			       ' </div>'+
			       ' <div class="row">'+
			       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '     <label>Tipo de documento del capacitador</label>'+
			       '     <select class="form-control" name="Tipo de documento del capacitador" id="tipo_doc_capacitador_presencial">'+
			       '       <option value="">-Seleccione</option>'+
			       '       <option value="CC">CC (Cédula de ciudadanía)</option>'+
			       '       <option value="CEX">CEX (Cédula de extranjeria)</option>'+
			       '       <option value="PAS">PAS (Pasaporte)</option>'+
			       '     </select>'+
			       '   </div>'+
			       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '     <label>Número de identificación del capacitador</label>'+
			       '     <input type="number" min="0" class="form-control" name="Número de identificación del capacitador" id="num_doc_capacitador_presencial" value="'+num_doc_capacitador+'">'+
			       '   </div>'+
			       ' </div>'+
			       ' <div class="row">'+
			       '   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
			       '     <label>Nombre del capacitador suplente 1</label>'+
			       '     <input type="text" class="form-control" name="Nombre de capacitador" id="nombre_capacitador_presencial_s1" value="'+nombre_capacitador_s1+'">'+
			       '   </div>'+
			       ' </div>'+
			       ' <div class="row">'+
			       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '     <label>Tipo de documento del capacitador suplente 1</label>'+
			       '     <select class="form-control" name="Tipo de documento del capacitador" id="tipo_doc_capacitador_presencial_s1">'+
			       '       <option value="">-Seleccione</option>'+
			       '       <option value="CC">CC (Cédula de ciudadanía)</option>'+
			       '       <option value="CEX">CEX (Cédula de extranjeria)</option>'+
			       '       <option value="PAS">PAS (Pasaporte)</option>'+
			       '     </select>'+
			       '   </div>'+
			       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '     <label>Número de identificación del capacitador suplente 1</label>'+
			       '     <input type="number" min="0" class="form-control" name="Número de identificación del capacitador" id="num_doc_capacitador_presencial_s1" value="'+num_doc_capacitador_s1+'">'+
			       '   </div>'+
			       ' </div>'+
			       ' <div class="row">'+
			       '   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
			       '     <label>Nombre del capacitador suplente 2</label>'+
			       '     <input type="text" class="form-control" name="Nombre de capacitador" id="nombre_capacitador_presencial_s2" value="'+nombre_capacitador_s2+'">'+
			       '   </div>'+
			       ' </div>'+
			       ' <div class="row">'+
			       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '     <label>Tipo de documento del capacitador suplente 2</label>'+
			       '     <select class="form-control" name="Tipo de documento del capacitador" id="tipo_doc_capacitador_presencial_s2">'+
			       '       <option value="">-Seleccione</option>'+
			       '       <option value="CC">CC (Cédula de ciudadanía)</option>'+
			       '       <option value="CEX">CEX (Cédula de extranjeria)</option>'+
			       '       <option value="PAS">PAS (Pasaporte)</option>'+
			       '     </select>'+
			       '   </div>'+
			       '   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '     <label>Número de identificación del capacitador suplente 2</label>'+
			       '     <input type="number" min="0" class="form-control" name="Número de identificación del capacitador" id="num_doc_capacitador_presencial_s2" value="'+num_doc_capacitador_s2+'">'+
			       '   </div>'+
			       ' </div>'+
			       ' <div class="row">'+
			       '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '    <label>Perfil del capacitador</label>'+
			       '    <select class="form-control" name="Perfil del capacitador" id="perfil_capacitador_presencial">'+
			       '      	<option value="">-Seleccione</option>'+

			       '	 	<option>TECNICO Y/O TECNOLOGO TITULADO, CON EXPERIENCIA RELACIONADA COMPROBADA MINIMO DE CINCO (5) AÑOS.</option>'+

			       '        <option>PROFESIONAL TITULADO CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+
			       '      	<option>PROFESIONAL CON TITULO DE ESPECIALIZACION Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+
			       '      	<option>PROFESIONAL CON TITULO DE MAESTRIA Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+
			       '      	<option>PROFESIONAL CON TITULO DE DOCTORADO Y CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+

			       '      	<option>PROFESIONAL TITULADO CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>'+
			       '      	<option>PROFESIONAL CON TITULO DE ESPECIALIZACION Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>'+
			       '      	<option>PROFESIONAL CON TITULO DE MAESTRIA Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>'+
			       '      	<option>PROFESIONAL CON TITULO DE DOCTORADO, CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS, AFIN CON EL OBJETO DE LA FORMACION.</option>'+

			       '    </select>'+
			       '  </div>'+
			       '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			       '    <label>Tipo de capacitador</label>'+
			       '    <select class="form-control" name="Tipo de capacitador" id="tipo_capacitador_presencial">'+
			       '      <option value="">-Seleccione</option>'+
			       '      <option>CAPACITADOR NACIONAL</option>'+
			       '      <option>CAPACITADOR INTERNACIONAL</option>'+
			       '    </select>'+
			       '  </div>'+
			       '</div>');

				jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Actualizar</button>');

				jQuery("#id_departamento").html('');
				jQuery("#id_departamento").html('<option value="">-Seleccione</option>');
				jQuery.each(jsonResponse.dptos,function (key,value) {
					jQuery("#id_departamento").append('<option value="'+value['id_dpto']+'">'+value['nombre']+'</option>');
				});

				jQuery("#id_ciudad").html('');
				jQuery("#id_ciudad").html('<option value="">-Seleccione</option>');
				jQuery.each(jsonResponse.ciudades,function (key,value) {
					jQuery("#id_ciudad").append('<option value="'+value['id_ciudad']+'">'+value['nombre']+'</option>');
				});

				jQuery("#id_ciudad").val(id_ciudad);
				jQuery("#tipo_doc_capacitador_presencial").val(tipo_doc_capacitador);
				jQuery("#tipo_doc_capacitador_presencial_s1").val(tipo_doc_capacitador_s1);
				jQuery("#tipo_doc_capacitador_presencial_s2").val(tipo_doc_capacitador_s2);
				jQuery("#perfil_capacitador_presencial").val(perfil_capacitador);
				jQuery("#tipo_capacitador_presencial").val(tipo_capacitador);

				jQuery(".btn-accept").click(function () {
					// Validar formulario de actualizacion
					var id_cronograma_presencial 		= jQuery("#id_cronograma_presencial");
					var nombre_sesion 					= jQuery("#nombre_sesion");
					var num_sesion 						= jQuery("#num_sesion");
					var fecha_inicio 					= jQuery("#fecha_inicio");
					var hora_inicial 					= jQuery("#hora_inicial");
					var hora_final 						= jQuery("#hora_final");
					var num_hrs_totales 				= jQuery("#num_hrs_totales");
					var id_ciudad 						= jQuery("#id_ciudad");
					var nombre_sede 					= jQuery("#nombre_sede");
					var direccion_sede 					= jQuery("#direccion_sede");
					var aula_salon 						= jQuery("#aula_salon");

					var nombre_capacitador_presencial 	= jQuery("#nombre_capacitador_presencial");
					var tipo_doc_capacitador_presencial = jQuery("#tipo_doc_capacitador_presencial");
					var num_doc_capacitador_presencial 	= jQuery("#num_doc_capacitador_presencial");

					var nombre_capacitador_s1 			= jQuery("#nombre_capacitador_presencial_s1");
					var tipo_doc_capacitador_s1 		= jQuery("#tipo_doc_capacitador_presencial_s1");
					var num_doc_capacitador_s1 			= jQuery("#num_doc_capacitador_presencial_s1");

					var nombre_capacitador_s2 			= jQuery("#nombre_capacitador_presencial_s2");
					var tipo_doc_capacitador_s2 		= jQuery("#tipo_doc_capacitador_presencial_s2");
					var num_doc_capacitador_s2 			= jQuery("#num_doc_capacitador_presencial_s2");


					var perfil_capacitador_presencial 	= jQuery("#perfil_capacitador_presencial");
					var tipo_capacitador_presencial 	= jQuery("#tipo_capacitador_presencial");

					var campos = [nombre_sesion,num_sesion,fecha_inicio,hora_inicial,hora_final,num_hrs_totales,id_ciudad,nombre_sede,direccion_sede,aula_salon,nombre_capacitador_presencial,tipo_doc_capacitador_presencial,num_doc_capacitador_presencial,perfil_capacitador_presencial,tipo_capacitador_presencial];

					var j = 15;

					jQuery("#alerta-sesion-presencial").html('')

					for (var i = 0; i < campos.length; i++) {
						if (campos[i].val() == '') {
							jQuery("#alerta-sesion-presencial").html('<div class="alert alert-warning">Por favor diligencie el campo <b>'+campos[i].attr('name')+'</b> para realizar la actualización de la sesión</div>');
							campos[i].focus();
							break;
						}else{
							j--;
						}
					}

					if (j == 0) {
						jQuery(".loading").css('display','block');

						jQuery.ajax({
							type:"POST",
							url:"index.php?r=cronograma/actualizar-sesion",
							data:{
								id_cronograma_presencial: 	id_cronograma_presencial.val(),
								nombre_sesion: 				nombre_sesion.val(),
								num_sesion: 				num_sesion.val(),
								fecha_inicio: 				fecha_inicio.val(),
								hora_inicial: 				hora_inicial.val(),
								hora_final: 				hora_final.val(),
								num_hrs_totales: 			num_hrs_totales.val(),
								id_ciudad: 					id_ciudad.val(),
								nombre_sede: 				nombre_sede.val(),
								direccion_sede: 			direccion_sede.val(),
								aula_salon: 				aula_salon.val(),
								nombre_capacitador: 		nombre_capacitador_presencial.val(),
								tipo_doc_capacitador: 		tipo_doc_capacitador_presencial.val(),
								num_doc_capacitador: 		num_doc_capacitador_presencial.val(),
								nombre_capacitador_s1: 		nombre_capacitador_s1.val(),
								tipo_doc_capacitador_s1: 	tipo_doc_capacitador_s1.val(),
								num_doc_capacitador_s1: 	num_doc_capacitador_s1.val(),
								nombre_capacitador_s2: 		nombre_capacitador_s2.val(),
								tipo_doc_capacitador_s2: 	tipo_doc_capacitador_s2.val(),
								num_doc_capacitador_s2: 	num_doc_capacitador_s2.val(),
								perfil_capacitador: 		perfil_capacitador_presencial.val(),
								tipo_capacitador: 			tipo_capacitador_presencial.val()
							},
							success:function (response) {
								console.log(response);
								jQuery(".loading").css('display','none');
								var jsonResponse = JSON.parse(response);

								if (jsonResponse.status == 'success') {
									jQuery('.modal-title').html('Editar sesión presencial.');
									jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
									jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

									jQuery('.btn-accept').click(function () {
										buscarCronograma();
									});
								}else{
									jQuery('.modal-title').html('Editar sesión presencial.');
									jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
									jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
								}

							}
						})
					}

				})

			}
		}
	});
}

// Buscar ciudades del departamento seleccionado
function buscarCiudades(id_departamento) {
	
	if (id_departamento != '') {

		jQuery.ajax({
			type:"POST",
			url:"index.php?r=beneficiarios/buscar-ciudades",
			data:{
				id_departamento: id_departamento
			},
			success:function(response){
				//console.log(response);
				var jsonResponse = JSON.parse(response);
				jQuery('#id_ciudad').html('');
				jQuery('#id_ciudad').html('<option value="">-Seleccione</option>');

				jQuery.each(jsonResponse,function (key,value) {

					var ciudad = value['id_ciudad']+'-'+value['nombre'];

					jQuery('#id_ciudad').append('<option value="'+value['id_ciudad']+'">'+value['nombre']+'</option>')
				});

			}
		});

	}

}

// Editar datos de la actividad virtual seleccionada
function editarActividadVirtual(id_cronograma_virtual) {
	jQuery.ajax({
		type:"POST",
		url:"index.php?r=cronograma/buscar-actividad-virtual",
		data:{
			id_cronograma_virtual: id_cronograma_virtual
		},
		success:function (response) {
			console.log(response);

			var jsonResponse = JSON.parse(response);

			if (jsonResponse.status == 'success') {

				var id_cronograma_virtual 	= jsonResponse.actividad['id_cronograma_virtual'];
				var id_cronograma 			= jsonResponse.actividad['id_cronograma'];
				var nombre_actividad 		= jsonResponse.actividad['nombre_actividad'];
				var fecha_inicio_actividad 	= jsonResponse.actividad['fecha_inicio_actividad'];
				var fecha_fin_actividad 	= jsonResponse.actividad['fecha_fin_actividad'];
				var proveedor_formacion 	= jsonResponse.actividad['proveedor_formacion'];
				var url_plataforma 			= jsonResponse.actividad['url_plataforma'];
				var usuario_sena 			= jsonResponse.actividad['usuario_sena'];
				var clave_sena 				= jsonResponse.actividad['clave_sena'];
				var nombre_capacitador 		= jsonResponse.actividad['nombre_capacitador'];
				var tipo_doc_capacitador 	= jsonResponse.actividad['tipo_doc_capacitador'];
				var num_doc_capacitador 	= jsonResponse.actividad['num_doc_capacitador'];
				var perfil_capacitador 		= jsonResponse.actividad['perfil_capacitador'];
				var tipo_capacitador 		= jsonResponse.actividad['tipo_capacitador'];

				jQuery('.modal').modal({backdrop: 'static', keyboard: false});
				jQuery('.modal-title').html('Editar actividad virtual.');

				jQuery('.modal-body').html(''+
					'<div class="row" id="alerta-sesion-virtual">'+
			        '</div>'+
			        '<div class="row">'+
			        '  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
			        '    <input type="hidden" class="form-control" id="id_cronograma_virtual" value="'+id_cronograma_virtual+'">'+
			        '  </div>'+
			        '  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
			        '    <label>Nombre de la actividad</label>'+
			        '    <input type="text" class="form-control" name="Nombre de la actividad" id="nombre_actividad" value="'+nombre_actividad+'">'+
			        '  </div>'+
			        '</div>'+
			        '<div class="row">'+
			        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			        '    <label>Fecha de inicio de la actividad</label>'+
			        '    <input type="date" class="form-control" name="Fecha de inicio de la actividad" id="fecha_inicio_actividad" value="'+fecha_inicio_actividad+'">'+
			        '  </div>'+
			        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			        '    <label>Fecha de finalización de la actividad</label>'+
			        '    <input type="date" class="form-control" name="Fecha de finalización de la actividad" id="fecha_fin_actividad" value="'+fecha_fin_actividad+'">'+
			        '  </div>'+
			        '</div>'+
			        '<div class="row">'+
			        '  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
			        '    <label>Proveedor de formación virtual</label>'+
			        '    <input type="text" class="form-control" name="Proveedor de formación virtual" id="proveedor_formacion" value="'+proveedor_formacion+'">'+
			        '  </div>'+
			        '</div>'+
			        '<div class="row">'+
			        '  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
			        '    <label>URL de plataforma virtual</label>'+
			        '    <input type="text" class="form-control" name="URL de plataforma virtual" id="url_plataforma" value="'+url_plataforma+'">'+
			        '  </div>'+
			        '</div>'+
			        '<div class="row">'+
			        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			        '    <label>Usuario SENA</label>'+
			        '    <input type="text" class="form-control" name="Usuario SENA" id="usuario_sena" value="'+usuario_sena+'">'+
			        '  </div>'+
			        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			        '    <label>Clave SENA</label>'+
			        '    <input type="text" class="form-control" name="Clave SENA" id="clave_sena" value="'+clave_sena+'">'+
			        '  </div>'+
			        '</div>'+
			        '<div class="row">'+
			        '  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
			        '    <label>Nombre del capacitador</label>'+
			        '    <input type="text" class="form-control" name="Nombre del capacitador" id="nombre_capacitador_virtual" value="'+nombre_capacitador+'">'+
			        '  </div>'+
			        '</div>'+
			        '<div class="row">'+
			        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			        '    <label>Tipo de documento del capacitador</label>'+
			        '    <select class="form-control" name="Tipo de documento del capacitador" id="tipo_doc_capacitador_virtual">'+
			        '      <option value="">-Seleccione</option>'+
			        '      <option value="CC">CC (Cédula de ciudadanía)</option>'+
			        '      <option value="CEX">CEX (Cédula de extranjeria)</option>'+
			        '      <option value="PAS">PAS (Pasaporte)</option>'+
			        '    </select>'+
			        '  </div>'+
			        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			        '    <label>Número de identificación del capacitador</label>'+
			        '    <input type="number" min="0" class="form-control" name="Número de identificación del capacitador" id="num_doc_capacitador_virtual" value="'+num_doc_capacitador+'">'+
			        '  </div>'+
			        '</div>'+
			        '<div class="row">'+
			        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			        '    <label>Perfil del capacitador</label>'+
			        '    <select class="form-control" name="Perfil del capacitador" id="perfil_capacitador_virtual">'+
			        '      <option value="">-Seleccione</option>'+
			        '	 	<option>TECNICO Y/O TECNOLOGO TITULADO, CON EXPERIENCIA RELACIONADA COMPROBADA MINIMO DE CINCO (5) AÑOS.</option>'+

			        '       <option>PROFESIONAL TITULADO CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+
			        '      	<option>PROFESIONAL CON TITULO DE ESPECIALIZACION Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+
			        '      	<option>PROFESIONAL CON TITULO DE MAESTRIA Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+
			        '      	<option>PROFESIONAL CON TITULO DE DOCTORADO Y CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>'+

			        '      	<option>PROFESIONAL TITULADO CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>'+
			        '      	<option>PROFESIONAL CON TITULO DE ESPECIALIZACION Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>'+
			        '      	<option>PROFESIONAL CON TITULO DE MAESTRIA Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>'+
			        '      	<option>PROFESIONAL CON TITULO DE DOCTORADO, CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS, AFIN CON EL OBJETO DE LA FORMACION.</option>'+
			        '    </select>'+
			        '  </div>'+
			        '  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">'+
			        '    <label>Tipo de capacitador</label>'+
			        '    <select class="form-control" name="Tipo de capacitador" id="tipo_capacitador_virtual">'+
			        '      <option value="">-Seleccione</option>'+
			        '      <option>CAPACITADOR NACIONAL</option>'+
			        '      <option>CAPACITADOR INTERNACIONAL</option>'+
			        '    </select>'+
			        '</div>');
	
				jQuery("#tipo_doc_capacitador_virtual").val(tipo_doc_capacitador);
				jQuery("#perfil_capacitador_virtual").val(perfil_capacitador);
				jQuery("#tipo_capacitador_virtual").val(tipo_capacitador);

				jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Actualizar</button>');

				jQuery(".btn-accept").click(function () {
					// Validar formulario de actualizacion
					var id_cronograma_virtual 	= jQuery("#id_cronograma_virtual");
					var nombre_actividad 		= jQuery("#nombre_actividad");
					var fecha_inicio_actividad 	= jQuery("#fecha_inicio_actividad");
					var fecha_fin_actividad 	= jQuery("#fecha_fin_actividad");
					var proveedor_formacion 	= jQuery("#proveedor_formacion");
					var url_plataforma 			= jQuery("#url_plataforma");
					var usuario_sena 			= jQuery("#usuario_sena");
					var clave_sena 				= jQuery("#clave_sena");
					var nombre_capacitador 		= jQuery("#nombre_capacitador_virtual");
					var tipo_doc_capacitador 	= jQuery("#tipo_doc_capacitador_virtual");
					var num_doc_capacitador 	= jQuery("#num_doc_capacitador_virtual");
					var perfil_capacitador 		= jQuery("#perfil_capacitador_virtual");
					var tipo_capacitador 		= jQuery("#tipo_capacitador_virtual");

					var campos = [id_cronograma_virtual,nombre_actividad,fecha_inicio_actividad,fecha_fin_actividad,proveedor_formacion,url_plataforma,usuario_sena,clave_sena,nombre_capacitador,tipo_doc_capacitador,num_doc_capacitador,perfil_capacitador,tipo_capacitador];

					var j = 13;

					jQuery("#alerta-sesion-virtual").html('')

					for (var i = 0; i < campos.length; i++) {
						if (campos[i].val() == '') {
							jQuery("#alerta-sesion-virtual").html('<div class="alert alert-warning">Por favor diligencie el campo <b>'+campos[i].attr('name')+'</b> para realizar la actualización de la actividad</div>');
							campos[i].focus();
							break;
						}else{
							j--;
						}
					}

					if (j == 0) {
						jQuery(".loading").css('display','block');

						jQuery.ajax({
							type:"POST",
							url:"index.php?r=cronograma/actualizar-actividad",
							data:{
								id_cronograma_virtual: 	id_cronograma_virtual.val(),
								nombre_actividad: 		nombre_actividad.val(),
								fecha_inicio_actividad: fecha_inicio_actividad.val(),
								fecha_fin_actividad: 	fecha_fin_actividad.val(),
								proveedor_formacion: 	proveedor_formacion.val(),
								url_plataforma: 		url_plataforma.val(),
								usuario_sena: 			usuario_sena.val(),
								clave_sena: 			clave_sena.val(),
								nombre_capacitador: 	nombre_capacitador.val(),
								tipo_doc_capacitador: 	tipo_doc_capacitador.val(),
								num_doc_capacitador: 	num_doc_capacitador.val(),
								perfil_capacitador: 	perfil_capacitador.val(),
								tipo_capacitador: 		tipo_capacitador.val(),
							},
							success:function (response) {
								console.log(response);
								jQuery(".loading").css('display','none');
								var jsonResponse = JSON.parse(response);

								if (jsonResponse.status == 'success') {
									jQuery('.modal-title').html('Editar actividad virtual.');
									jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
									jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

									jQuery('.btn-accept').click(function () {
										buscarCronograma();
									});
								}else{
									jQuery('.modal-title').html('Editar actividad virtual.');
									jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
									jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
								}

							}
						})
					}

				})

			}
		}
	});
}