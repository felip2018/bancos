// Actualizar documentos (Hoja de vida)
function actualizar_documento(i) {
	//console.log("id_director_documento: "+id_director_documento+", i: "+i);
	
	jQuery("#alerta-documentos").html('');

	var input = jQuery("#document_"+i);
	var formulario = "form_document_"+i;

	if (input.val() != '') {
		jQuery('.modal').modal({backdrop: 'static', keyboard: false});
		jQuery('.modal-title').html('Actualizar documento');
		jQuery('.modal-body').html('A continuación se realizará la actualización del documento seleccionado.<br>¿Desea continuar?');
		jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Guardar <i class="fa fa-save"></i> </button>');

		jQuery('.btn-accept').click(function () {

			// Validar peso del archivo
			var fileSize = input[0].files[0].size;
			var sizeKB = parseInt(fileSize/1024);

			// El peso no debe superar 8Mb 0 8192 Kb
			if (sizeKB < 8192) {
				jQuery(".loading").css('display','block');

				var form = document.getElementById(formulario);
				var formData = new FormData(form);

				jQuery.ajax({
					type:"POST",
					url:"index.php?r=directores/editar-documento",
					data: formData,
					contentType: false,
					processData: false,
					success:function (response) {
						jQuery(".loading").css('display','none');
						console.log(response);
						var jsonResponse = JSON.parse(response);

						if (jsonResponse.status == "success") {
							jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
							jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

							jQuery('.btn-accept').click(function () {
								location.reload();
							});

						}else{
							jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
							jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
						}

					}
				})
			}else{
				jQuery(".modal-body").html('<div class="alert alert-warning">El peso del archivo excede el límite permitido (8Mb).</div>');
			}

		})

	}else{
		jQuery("#alerta-documentos").html('<div class="alert alert-warning">Seleccione el documento que desea cargar para actualizar el registro</div>');		
		input.focus();
	}

}

// Actualizar experiencia
function actualizar_experiencia(i) {
	
	jQuery("#alerta-experiencias").html('');

	var input = jQuery("#documento_experiencia_"+i);
	var formulario = "form_experiencia_"+i;

	if (input.val() != '') {
		jQuery('.modal').modal({backdrop: 'static', keyboard: false});
		jQuery('.modal-title').html('Actualizar documento');
		jQuery('.modal-body').html('A continuación se realizará la actualización de la experiencia seleccionada.<br>¿Desea continuar?');
		jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Guardar <i class="fa fa-save"></i> </button>');

		jQuery('.btn-accept').click(function () {

			// Validar peso del archivo
			var fileSize = input[0].files[0].size;
			var sizeKB = parseInt(fileSize/1024);

			// El peso no debe superar 8Mb 0 8192 Kb
			if (sizeKB < 8192) {
				jQuery(".loading").css('display','block');

				var form = document.getElementById(formulario);
				var formData = new FormData(form);

				jQuery.ajax({
					type:"POST",
					url:"index.php?r=directores/editar-experiencia",
					data: formData,
					contentType: false,
					processData: false,
					success:function (response) {
						jQuery(".loading").css('display','none');
						console.log(response);
						var jsonResponse = JSON.parse(response);

						if (jsonResponse.status == "success") {
							jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
							jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

							jQuery('.btn-accept').click(function () {
								location.reload();
							});

						}else{
							jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
							jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
						}

					}
				})
			}else{
				jQuery(".modal-body").html('<div class="alert alert-warning">El peso del archivo excede el límite permitido (8Mb).</div>');
			}

		})

	}else{
		jQuery("#alerta-experiencias").html('<div class="alert alert-warning">Seleccione el archivo que desea cargar para actualizar la experiencia.</div>');		
		input.focus();
	}

}

// Actualizar titulo
function actualizar_titulo(i) {
	
	jQuery("#alerta-titulos").html('');

	var input = jQuery("#documento_titulo_"+i);
	var formulario = "form_titulo_"+i;

	if (input.val() != '') {
		jQuery('.modal').modal({backdrop: 'static', keyboard: false});
		jQuery('.modal-title').html('Actualizar documento');
		jQuery('.modal-body').html('A continuación se realizará la actualización del titulo seleccionado.<br>¿Desea continuar?');
		jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Guardar <i class="fa fa-save"></i> </button>');

		jQuery('.btn-accept').click(function () {

			// Validar peso del archivo
			var fileSize = input[0].files[0].size;
			var sizeKB = parseInt(fileSize/1024);

			// El peso no debe superar 8Mb 0 8192 Kb
			if (sizeKB < 8192) {
				jQuery(".loading").css('display','block');

				var form = document.getElementById(formulario);
				var formData = new FormData(form);

				jQuery.ajax({
					type:"POST",
					url:"index.php?r=directores/editar-titulo",
					data: formData,
					contentType: false,
					processData: false,
					success:function (response) {
						jQuery(".loading").css('display','none');
						console.log(response);
						var jsonResponse = JSON.parse(response);

						if (jsonResponse.status == "success") {
							jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
							jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

							jQuery('.btn-accept').click(function () {
								location.reload();
							});

						}else{
							jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
							jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
						}

					}
				})
			}else{
				jQuery(".modal-body").html('<div class="alert alert-warning">El peso del archivo excede el límite permitido (8Mb).</div>');
			}

		})

	}else{
		jQuery("#alerta-titulos").html('<div class="alert alert-warning">Seleccione el archivo que desea cargar para actualizar el titulo.</div>');		
		input.focus();
	}

}

// Actualizar referencia
function actualizar_referencia(i) {
	
	jQuery("#alerta-referencias").html('');

	var input = jQuery("#documento_referencia_"+i);
	var formulario = "form_referencia_"+i;

	if (input.val() != '') {
		jQuery('.modal').modal({backdrop: 'static', keyboard: false});
		jQuery('.modal-title').html('Actualizar documento');
		jQuery('.modal-body').html('A continuación se realizará la actualización del documento de referencia seleccionado.<br>¿Desea continuar?');
		jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Guardar <i class="fa fa-save"></i> </button>');

		jQuery('.btn-accept').click(function () {

			// Validar peso del archivo
			var fileSize = input[0].files[0].size;
			var sizeKB = parseInt(fileSize/1024);

			// El peso no debe superar 8Mb 0 8192 Kb
			if (sizeKB < 8192) {
				jQuery(".loading").css('display','block');

				var form = document.getElementById(formulario);
				var formData = new FormData(form);

				jQuery.ajax({
					type:"POST",
					url:"index.php?r=directores/editar-referencia",
					data: formData,
					contentType: false,
					processData: false,
					success:function (response) {
						jQuery(".loading").css('display','none');
						console.log(response);
						var jsonResponse = JSON.parse(response);

						if (jsonResponse.status == "success") {
							jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
							jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

							jQuery('.btn-accept').click(function () {
								location.reload();
							});

						}else{
							jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
							jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
						}

					}
				})
			}else{
				jQuery(".modal-body").html('<div class="alert alert-warning">El peso del archivo excede el límite permitido (8Mb).</div>');
			}

		})

	}else{
		jQuery("#alerta-referencias").html('<div class="alert alert-warning">Seleccione el archivo que desea cargar para actualizar la referencia.</div>');		
		input.focus();
	}

}

// Actualizar informacion del director de proyecto
function actualizar_info() {
	var tipo_doc 			= jQuery("#tipo_doc");
	var num_doc 			= jQuery("#num_doc");
	var nombres 			= jQuery("#nombres");
	var primer_apellido 	= jQuery("#primer_apellido");
	var segundo_apellido 	= jQuery("#segundo_apellido");
	var correo 				= jQuery("#correo");

	var campos = [nombres,primer_apellido,correo];
	var j = 3;

	jQuery("#alerta-info").html('');

	for (var i = 0; i < campos.length; i++) {
		if(campos[i].val() == ""){
			jQuery("#alerta-info").html('<div class="alert alert-warning">Por favor diligencie el campo <b>'+campos[i].attr('name')+'</b></div>');
			campos[i].focus();
			break;
		}else{
			j--;
		}
	}

	if (j == 0) {
		
		jQuery('.modal').modal({backdrop: 'static', keyboard: false});
		jQuery('.modal-title').html('Actualizar información');
		jQuery('.modal-body').html('A continuación se realizará la actualización la información básica del director del proyecto.<br>¿Desea continuar?');
		jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Guardar <i class="fa fa-save"></i> </button>');

		jQuery('.btn-accept').click(function () {
			
			jQuery(".loading").css('display','block');

			jQuery.ajax({
				type:"POST",
				url:"index.php?r=directores/actualizar-info",
				data:{
					tipo_doc: 			tipo_doc.val(),
					num_doc: 			num_doc.val(),
					nombres: 			nombres.val(),
					primer_apellido: 	primer_apellido.val(),
					segundo_apellido: 	segundo_apellido.val(),
					correo: 			correo.val()
				},
				success:function (response) {
					jQuery(".loading").css('display','none');
					//console.log(response);
					var jsonResponse = JSON.parse(response);
					
					jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
					jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

				}
			})

		})

	}

}