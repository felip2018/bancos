// Crear grupos para la accion de formacion seleccionada
function crearGruposAccionFormacion(id_proyecto,id_accion_formacion,numero_grupos,i) {
  //console.log("Grupos => (id_proyecto: "+id_proyecto+" , id_accion_formacion: "+id_accion_formacion+")");
  var nombreBloque = "action_"+id_accion_formacion;
  var nombreAccion = jQuery("#title_accion_"+i);

  jQuery('.modal').modal({backdrop: 'static', keyboard: false});
  jQuery('.modal-title').html('Crear Grupos');
  jQuery('.modal-body').html('A continuación podrá realizar el registro de los grupos indicados en la acción de formación:.<br>'+nombreAccion.html()+'<br>¿Desea continuar?');
  jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

  jQuery('.btn-accept').click(function () {
      jQuery(".loading").css('display','block');
      jQuery.ajax({
        type:"POST",
        url:"index.php?r=grupos/crear-grupos-accion-formacion",
        data:{
          id_proyecto:        id_proyecto,
          id_accion_formacion:id_accion_formacion,
          numero_grupos:      numero_grupos
        },
        success:function (response) {
            jQuery(".loading").css('display','none');
            console.log(response);
            var jsonResponse = JSON.parse(response);
            jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
            jQuery('.modal-footer').html('');
            jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

            jQuery('#'+nombreBloque+' .datoGrupos').html(numero_grupos+' de '+numero_grupos);

        }
     }) 
  });

}

// Consultar acciones de formacion por proyecto seleccionado
function accionesFormacion(id_proyecto) {
  jQuery.ajax({
    type:"POST",
    url:"index.php?r=grupos/consultar-acciones-formacion",
    data:{
      id_proyecto: id_proyecto
    },
    success:function (response) {
      console.log(response);
      /*var jsonResponse = JSON.parse(response);
      
      if (jsonResponse.status == "success") {

        jQuery("#listaAccionesFormacion").html('');
        jQuery.each(jsonResponse.acciones,function (key,value) {
          jQuery("#listaAccionesFormacion").append('<div class="accion-container">'+
                  '<h3>'+value['nombre']+'</h3>'+
                  '<table class="table">'+
                    '<tr>'+
                      '<td>Beneficiarios Empresa</td>'+
                      '<td>'+value['beneficiarios_empresa']+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Beneficiarios SENA</td>'+
                      '<td>'+value['beneficiarios_sena']+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Beneficiarios Totales</td>'+
                      '<td>'+value['total_beneficiarios']+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Grupos</td>'+
                      '<td>'+value['grupos_registrados']+' de '+value['numero_grupos']+'</td>'+
                    '</tr>'+
                  '</table>'+
                  '<a type="button" class="btn btn-info" href="index.php?r=grupos/configuracion&id_accion_formacion='+value['id_accion_formacion']+'">Configurar grupos</a>'+
                  '<button type="button" class="btn btn-primary pull-right" onclick=crearGruposAccionFormacion('+id_proyecto+','+value['id_accion_formacion']+','+value['numero_grupos']+',"'+value['nombre']+'")>'+
                    '<i class="fa fa-plus"></i> Crear grupos'+
                  '</button>'+
                '</div>');
        })

      }else if (jsonResponse.status == "vacio") {
        
        jQuery("#listaAccionesFormacion").html('<div class="alert alert-warning">'+jsonResponse.message+'</div>');

      }*/

    }
  })
}

// Crear los grupos de todas las acciones de formacion
function crearTodosLosGruposAF(id_proyecto){
	
  jQuery('.modal').modal('toggle');
  jQuery('.modal-title').html('Registrar grupos');
  jQuery('.modal-body').html('A continuación podrá realizar el registro de todos los grupos indicados en las acciones de formación.<br>¿Desea continuar?');
  jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

	jQuery('.btn-accept').click(function () {
		jQuery('.loading').css('display','block');
		jQuery.ajax({
			type:"POST",
			url:"index.php?r=grupos/crear-grupos",
			data:{
				'id_proyecto': id_proyecto
			},
			success:function (response) {
				console.log(response);
				var jsonResponse = JSON.parse(response);
				if (jsonResponse.status == "success") {
					jQuery('.loading').css('display','none');
					jQuery('.modal-title').html('Registrar grupos');
			  	jQuery('.modal-body').html(jsonResponse.message);
			  	jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

			  	jQuery('.btn-accept').click(function () {
			  		location.reload();
			  	});

				}else{
					jQuery('.loading').css('display','none');
					jQuery('.loading').css('display','none');
					jQuery('.modal-title').html('Registrar grupos');
			  	jQuery('.modal-body').html(jsonResponse.message);
			  	jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
				}
			}
		})
	});

}

// Buscar beneficiarios registrados de un grupo seleccionado
function buscarBeneficiarios(id_grupo) {
  
  /*Pintar boton seleccionado*/
  var btn_id = "btn_grupo_"+id_grupo;
  jQuery(".btn-grupo").css("background","#FC7323");
  jQuery('#'+btn_id).removeClass('btn-primary');
  jQuery('#'+btn_id).css({'background':'#F39C12','color':'#FFFFFF'});

  jQuery("#campo_id_grupo").val(id_grupo);



  jQuery.ajax({
    type:"POST",
    url:"index.php?r=grupos/buscar-beneficiarios",
    data:{
      id_grupo: id_grupo
    },
    success:function (response) {
      var jsonResponse = JSON.parse(response);
      if (jsonResponse.status == "success") {

        jQuery("#listadoBeneficiarios").html('');
        var i = 1;

        jQuery.each(jsonResponse.listado, function (key,value) {

          var estado_icon  = (value['estado_registro'] == "ACTIVO") ? "fa-times" : "fa-check";
          var icon = "fa-exchange-alt";
          var fondo = (value['estado_registro'] == "ACTIVO") ? "#10ac84" : "#ee5253";
          var btn   = (value['estado_registro'] == "ACTIVO") ? "#ee5253" : "#10ac84";
          var estado= (value['estado_registro'] == "ACTIVO") ? "INACTIVO" : "ACTIVO";

          jQuery("#alerta1").html('');
          jQuery("#listadoBeneficiarios").append('<tr>'+
              '<td>'+i+'</td>'+
              '<td>'+value['nombre']+'</td>'+
              '<td>'+value['identificacion']+'</td>'+
              '<td style="background:'+fondo+';color:#FFFFFF;">'+value['estado_registro']+'</td>'+
              '<td>'+
                '<button class="btn btn-primary" style="background: '+btn+' !important;color:#FFFFFF;" onclick=estado_beneficiario_grupo('+value['id_proyecto_beneficiario']+','+value['id_accion_formacion']+','+value['id_grupo']+',"'+estado+'")><i class="fa '+estado_icon+'"></i></button>'+
                '<button class="btn btn-warning" title="Cambiar grupo" onclick=cambio_beneficiario_grupo('+value['id_proyecto_beneficiario']+','+value['id_accion_formacion']+','+value['id_grupo']+')><i class="fas '+icon+'"></i></button>'+
              '</td>'+
            '</tr>');
          
          i++;

        })
      }else {
        jQuery("#alerta1").html('<div class="alert alert-warning"><p>No se han encontrado beneficiarios registrados para este grupo.</p></div>');
        jQuery("#listadoBeneficiarios").html('');
        jQuery("#listadoBeneficiarios").append('<tr>'+
              '<td>0</td>'+
              '<td>Vacío</td>'+
              '<td>Vacío</td>'+
              '<td>Vacío</td>'+
            '</tr>');
      }
    }
  })

}

// Visualizar formulario de registro de beneficiario dentro de un grupo seleccionado
function verFormularioRegistro() {

  var id_grupo = jQuery('#campo_id_grupo').val();

  jQuery("#alerta1").html("");

  if (id_grupo != '') {

    jQuery('.modal').modal({backdrop: 'static', keyboard: false});
    jQuery('.modal-title').html('Agregar beneficiario');
    jQuery(".modal-body").html('<div class="row">'+
          '<div class="col-xs-12" id="alerta">'+
          '</div>'+
          '<div class="col-xs-12">'+
            '<div class="row">'+
              '<div class="col-xs-12 col-sm-12 col-md-5">'+
                '<select class="form-control" id="tipo_documento">'+
                  '<option value="0">-Seleccione</option>'+
                  '<option value="CC">CC (Cédula de ciudadanía)</option>'+
                  '<option value="CEX">CEX (Cédula de extranjería)</option>'+
                  '<option value="PAS">PAS (Pasaporte)</option>'+
                '</select>'+
              '</div>'+
              '<div class="col-xs-12 col-sm-12 col-md-5">'+
                '<input type="number" min="0" class="form-control" id="num_documento"/>'+
              '</div>'+
              '<div class="col-xs-12 col-sm-12 col-md-2">'+
                '<button class="btn btn-primary btn-block btn-seach"><i class="fa fa-search"></i></button>'+
              '</div>'+
            '</div>'+
            '<div class="row">'+
              '<div class="col-xs-12 col-sm-12 col-md-4">'+
                '<input type="hidden" class="form-control" id="id_beneficiario">'+
              '</div>'+
              '<div class="col-xs-12 col-sm-12 col-md-4">'+
                '<input type="hidden" class="form-control" id="tipo_doc">'+
              '</div>'+
              '<div class="col-xs-12 col-sm-12 col-md-4">'+
                '<input type="hidden" min="0" class="form-control" id="num_doc"/>'+
              '</div>'+
            '</div>'+
            '<div class="row">'+
              '<div class="col-xs-12 col-sm-12 col-md-12">'+
                '<b>Nombre</b>'+
                '<input type="text" class="form-control" id="nombre" readonly="true"/>'+
              '</div>'+
            '</div>'+
            '<div class="row">'+
              '<div class="col-xs-12 col-sm-12 col-md-6">'+
                '<b>Primer apellido</b>'+
                '<input type="text" class="form-control" id="apellido_1" readonly="true"/>'+
              '</div>'+
              '<div class="col-xs-12 col-sm-12 col-md-6">'+
                '<b>Segundo apellido</b>'+
                '<input type="text" class="form-control" id="apellido_2" readonly="true"/>'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</div>');
    //jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Agregar al grupo</button>');
    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button>');

    jQuery(".btn-seach").click(function () {

        jQuery("#alerta").html('');

        var tipo_documento  = jQuery("#tipo_documento").val();
        var num_documento   = jQuery("#num_documento").val();

        if (tipo_documento != '0') {
          if (num_documento != '') {

            jQuery.ajax({
              type:"POST",
              url:"index.php?r=grupos/buscar-beneficiario-doc",
              data:{
                tipo_documento: tipo_documento,
                num_documento: num_documento
              },
              success:function (response) {
                  //console.log(response);
                  var jsonResponse = JSON.parse(response);
                  if (jsonResponse.status == "success") {

                      jQuery("#id_beneficiario").val(jsonResponse.datos['id_beneficiario']);
                      jQuery("#tipo_doc").val(jsonResponse.datos['tipo_doc']);
                      jQuery("#num_doc").val(jsonResponse.datos['num_doc']);

                      jQuery("#nombre").val(jsonResponse.datos['nombres']);
                      jQuery("#apellido_1").val(jsonResponse.datos['apellido_1']);
                      jQuery("#apellido_2").val(jsonResponse.datos['apellido_2']);

                      jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept"><span class="fa fa-save"></span> Agregar beneficiario</button>');

                      jQuery(".btn-accept").click(function () {

                        var id_grupo        = jQuery("#campo_id_grupo");
                        var id_beneficiario = jQuery("#id_beneficiario");
                        var tipo_doc        = jQuery("#tipo_doc");
                        var num_doc         = jQuery("#num_doc");

                        jQuery.ajax({
                          type:"POST",
                          url:"index.php?r=grupos/agregar-beneficiario",
                          data:{
                            id_grupo:         id_grupo.val(),
                            id_beneficiario:  id_beneficiario.val(),
                            tipo_doc:         tipo_doc.val(),
                            num_doc:          num_doc.val()
                          },
                          success: function (response) {
                            console.log(response);
                            var jsonResponse = JSON.parse(response);

                            if (jsonResponse.status == "success") {

                              jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');

                              jQuery('.modal-footer').html('<button type="button" class="btn btn-success btn-accept" data-dismiss="modal"><span class="fa fa-check"></span> Aceptar</button>');

                              jQuery('.btn-accept').click(function () {
                                buscarBeneficiarios(id_grupo.val());
                              })

                            }else{

                              jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
                              
                              jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button>');
                            }

                          }
                        })

                      });

                  }else{

                      jQuery("#alerta").html('<div class="alert alert-warning">'+jsonResponse.message+'</div>');

                      jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button>');

                      jQuery("#id_beneficiario").val('');
                      jQuery("#tipo_doc").val('');
                      jQuery("#num_doc").val('');

                      jQuery("#nombre").val('');
                      jQuery("#apellido_1").val('');
                      jQuery("#apellido_2").val('');

                  }
              }
            });

          }else{
            jQuery("#num_documento").focus();  
            jQuery("#alerta").html('<div class="alert alert-warning">Digite el número de identificación</div>');
          }
        }else{
          jQuery("#tipo_documento").focus();
          jQuery("#alerta").html('<div class="alert alert-warning">Seleccione el tipo de identificación</div>');
        }

    });

  }else{
      jQuery("#alerta1").html('<div class="alert alert-warning"><p>Seleccione un grupo de la lista para agregar los beneficiarios.</p></div>');
  }

}

// Cambiar estado de beneficiario en un grupo seleccionado
function estado_beneficiario_grupo(id_proyecto_beneficiario,id_accion_formacion,id_grupo,estado) {
    
    jQuery("#alerta1").html('');

    jQuery.ajax({
        type:"POST",
        url:"index.php?r=grupos/estado-beneficiario-grupo",
        data:{
          id_proyecto_beneficiario: id_proyecto_beneficiario,
          id_accion_formacion:      id_accion_formacion,
          id_grupo:                 id_grupo,
          estado:                   estado
        },
        success:function (response) {
            var jsonResponse = JSON.parse(response);

            if (jsonResponse.status == "success") { 

              buscarBeneficiarios(id_grupo);

            }else{

              jQuery("#alerta1").html('<div class="alert alert-danger">'+jsonResponse.message+'</div>');

            }
        }
    });
}

// Cambiar grupo de un beneficiario seleccionado
function cambio_beneficiario_grupo(id_proyecto_beneficiario,id_accion_formacion,id_grupo) {
  
  jQuery.ajax({
    type:"POST",
    url:"index.php?r=grupos/consultar-cambio-grupo",
    data:{
      id_accion_formacion:  id_accion_formacion,
      id_grupo:             id_grupo
    },
    success:function (response) {
      //console.log(response);
      var jsonResponse = JSON.parse(response);
      if (jsonResponse.status == "success") {

        jQuery('.modal').modal({backdrop: 'static', keyboard: false});
        jQuery('.modal-title').html('Cambiar grupo.');
        jQuery('.modal-body').html('<div><p>Seleccione para realizar la actulización de grupo del beneficiario seleccionado.</p></div>'+
          '<input type="hidden" id="id_grupo_seleccionado"/>'+
          '<div id="alertaModal"></div>'+
          '<div id="listado_grupos"></div>');

        jQuery('#listado_grupos').html('');

        jQuery.each(jsonResponse.listado,function (key,value) {
          jQuery('#listado_grupos').append('<button class="btn btn-primary btn-seleccion-grupo" id="btn-seleccion-grupo-'+value['id_grupo']+'" style="margin:5px;" onclick=seleccionar_grupo('+value['id_grupo']+')>'+
              '<p>'+value['nombre']+'</p>'+
            '</button>')
        });

        jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

        jQuery('.btn-accept').click(function () {

          jQuery('#alertaModal').html('');
          var id_grupo_seleccionado = jQuery('#id_grupo_seleccionado');
          if (id_grupo_seleccionado.val() != '') {
            //console.log("Nuevo grupo: "+id_grupo_seleccionado.val()+"\nId Accion de Formacion: "+id_accion_formacion+"\nId Proyecto Beneficiario: "+id_proyecto_beneficiario);
            jQuery.ajax({
              type:"POST",
              url:"index.php?r=grupos/actualizar-grupo-beneficiario",
              data:{
                id_proyecto_beneficiario: id_proyecto_beneficiario,
                id_accion_formacion: id_accion_formacion,
                id_grupo: id_grupo_seleccionado.val()
              },
              success:function (response) {
                console.log(response);
                var jsonResponse = JSON.parse(response);
                if (jsonResponse.status == "success") {
                   jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
                   jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
                   buscarBeneficiarios(id_grupo_seleccionado.val());
                }
              }
            })
          }else{
            jQuery('#alertaModal').html('<div class="alert alert-warning">Seleccione un grupo de la lista para realizar la actualización.</div>');
          }

        })

      }else if(jsonResponse.status == "vacio"){

        jQuery('.modal').modal({backdrop: 'static', keyboard: false});
        jQuery('.modal-title').html('Cambiar grupo.');
        jQuery('.modal-body').html('<p>Lo sentimos, no se han encontrado grupos disponibles para realizar la actualización de grupo del beneficiario seleccionado.</p>');
        jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-cancel" data-dismiss="modal">Aceptar</button>');
      
      }
    }
  });

}

// Seleccionar grupo para hacer el cambio
function seleccionar_grupo(id_grupo) {

  jQuery('#alertaModal').html('');

  jQuery("#id_grupo_seleccionado").val(id_grupo);

  var btn_id = "btn-seleccion-grupo-"+id_grupo;
  jQuery(".btn-seleccion-grupo").css("background","#FC7323");
  jQuery('#'+btn_id).removeClass('btn-primary');
  jQuery('#'+btn_id).css({'background':'#F39C12','color':'#FFFFFF'});
}

// Ver listado de beneficiarios de un grupo seleccionado
function verListadoBeneficiariosGrupo() {
    var id_grupo = jQuery('#campo_id_grupo').val();

    jQuery("#alerta1").html("");

    if (id_grupo != '') {
      window.open('index.php?r=grupos/reporte-beneficiarios-grupo&id_grupo='+id_grupo,'_blank');
    }else{
      jQuery("#alerta1").html('<div class="alert alert-warning"><p>Seleccione un grupo de la lista para ver el listado de los beneficiarios registrados.</p></div>');
    }
}

// Ver listado de beneficiarios de una accion de formacion seleccionada
function verListadoBeneficiariosAccionFormacion(id_accion_formacion) {
    //console.log("Id Accion de Formacion: "+id_accion_formacion);
    window.open('index.php?r=grupos/reporte-beneficiarios-accion-formacion&id_accion_formacion='+id_accion_formacion,'_blank');
}

// Descargar excel - listado de beneficiarios de un grupo seleccionado
function descargarExcelBeneficiarios(id_accion_formacion) {
    
    //window.open('index.php?r=grupos/exportar-beneficiarios-grupo&id_grupo='+id_grupo,'_blank');
    window.open('index.php?r=grupos/exportar-beneficiarios-accion-formacion&id_accion_formacion='+id_accion_formacion,'_blank');
    
}