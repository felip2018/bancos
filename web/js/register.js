function search(tipo, ID) {
  console.log('TIPO (' + tipo + ') | ID (' + ID + ')');
  if (ID !== '') {

    jQuery.ajax({
      type: 'POST',
      url: 'index.php?r=generalidades/buscar',
      data: {
        tipo_busqueda: tipo,
        identificador: ID,
      },
      success: function (response) {
        console.log(response);
        var jsonResponse = JSON.parse(response);
        if (jsonResponse.status == 'success') {
          console.log(jsonResponse.listado);
          switch (tipo) {
            case 'Departamentos':
              // Renderizar listado de departamentos
              jQuery('#entidades-departamento').empty();
              jQuery('#entidades-departamento').attr('disabled', false);
              jQuery('#entidades-departamento').html('<option value="">-Seleccione</option>');
              jQuery.each(jsonResponse.listado, function (key, value) {
                //console.log('KEY -> ' + key + '| ID_Departamento -> ' + value.ID_Departamentos + '| Nombre -> ' + value.Nombre);
                jQuery('#entidades-departamento').append('<option value="' + value.ID_Departamentos + '">"' + value.Nombre + '"</option>');
              });
              break;
            case 'Ciudades':
              // Renderizar listado de ciudades
              jQuery('#entidades-id_ciudad').empty();
              jQuery('#entidades-id_ciudad').attr('disabled', false);
              jQuery('#entidades-id_ciudad').html('<option value="">-Seleccione</option>');
              jQuery.each(jsonResponse.listado, function (key, value) {
                //console.log('KEY -> ' + key + '| ID_Departamento -> ' + value.ID_Departamentos + '| Nombre -> ' + value.Nombre);
                jQuery('#entidades-id_ciudad').append('<option value="' + value.ID_Ciudad + '">"' + value.Nombre + '"</option>');
              });
              break;

          }

        }else {
          console.log('Se ha producido un error.');
        }
      },
    });
  }
}

// Cerrar ventana de alerta
function closeAlert() {
  jQuery('.alerta').css('display', 'none');
}
