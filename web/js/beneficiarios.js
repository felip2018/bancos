function registrarBeneficiario() {

	var tipo_doc 			 = jQuery("#beneficiarios-tipo_doc");
	var num_doc 			 = jQuery("#beneficiarios-num_doc");
	var nombres 			 = jQuery("#beneficiarios-nombres");
	var apellido_1 			 = jQuery("#beneficiarios-apellido_1");
	var apellido_2 			 = jQuery("#beneficiarios-apellido_2");
	var genero 				 = jQuery("#beneficiarios-genero");
	var estrato 			 = jQuery("#beneficiarios-estrato");
	var fecha_nacimiento 	 = jQuery("#beneficiarios-fecha_nacimiento");
	var celular 			 = jQuery("#beneficiarios-celular");
	var id_departamento 	 = jQuery("#beneficiarios-id_departamento");
	var id_ciudad 			 = jQuery("#beneficiarios-id_ciudad");
	var barrio_vereda 		 = jQuery("#beneficiarios-barrio_vereda");
	var direccion 			 = jQuery("#beneficiarios-direccion");
	var edad 				 = jQuery("#beneficiarios-edad");
	var antiguedad 			 = jQuery("#beneficiarios-antiguedad");
	var empresa_labora 		 = jQuery("#beneficiarios-empresa_labora");
	var tamanio_empresa 	 = jQuery("#beneficiarios-tamanio_empresa_labora");
	var caracterizacion 	 = jQuery("#beneficiarios-caracterizacion");
	var nivel_ocupacional 	 = jQuery("#beneficiarios-nivel_ocupacional");
	var transferencia 		 = jQuery("#beneficiarios-transferencia");
	var perfil_transferencia = jQuery("#beneficiarios-perfil_transferencia");

	var campos = [tipo_doc,num_doc,nombres,apellido_1,genero,estrato,fecha_nacimiento,celular,id_departamento,id_ciudad,barrio_vereda,direccion,edad,antiguedad,empresa_labora,tamanio_empresa,caracterizacion,nivel_ocupacional,transferencia,perfil_transferencia];

	var j = 20;

	jQuery("#alerta-registro").html('');

	// Validacion de campos del formulario

	for (var i = 0; i < campos.length; i++) {
		if (campos[i].val() == '') {
			jQuery("#alerta-registro").html('<div class="alert alert-warning">Por favor diligencie el campo <b>'+campos[i].attr('name')+'</b></div>');
			campos[i].focus();
			break;
		}else{
			j--;
		}
	}

	if (j==0) {
		//Validacion de campos exitosa.
		// Evaluamos si hay grupos de asignacion para el beneficiario
		if(localStorage.asignacion){

			var arregloIDAcciones = JSON.parse(localStorage.getItem("arregloIDAcciones"));

			if (arregloIDAcciones.length > 0) {

				jQuery('.modal').modal({backdrop: 'static', keyboard: false});
			    jQuery('.modal-title').html('Registrar beneficiario.');
			    jQuery('.modal-body').html('A continuación realizará el registro del beneficiario en el sistema.<br><b>¿Desea continuar?</b>');
			    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

			    jQuery(".btn-accept").click(function () {

			    	jQuery(".loading").css('display','block');

					jQuery.ajax({
						type:"POST",
						url:"index.php?r=beneficiarios/agregar-beneficiario",
						data:{
							tipo_doc: 			  tipo_doc.val(),
							num_doc: 			  num_doc.val(),
							nombres: 			  nombres.val(),
							apellido_1: 		  apellido_1.val(),
							apellido_2: 		  apellido_2.val(),
							genero: 			  genero.val(),
							estrato: 			  estrato.val(),
							fecha_nacimiento: 	  fecha_nacimiento.val(),
							celular: 			  celular.val(),
							id_departamento: 	  id_departamento.val(),
							id_ciudad: 			  id_ciudad.val(),
							barrio_vereda: 		  barrio_vereda.val(),
							direccion: 			  direccion.val(),
							edad: 				  edad.val(),
							antiguedad: 		  antiguedad.val(),
							empresa_labora: 	  empresa_labora.val(),
							tamanio_empresa: 	  tamanio_empresa.val(),
							caracterizacion: 	  caracterizacion.val(),
							nivel_ocupacional: 	  nivel_ocupacional.val(),
							transferencia: 		  transferencia.val(),
							perfil_transferencia: perfil_transferencia.val(),
							acciones: 			  JSON.parse(localStorage.getItem("arregloIDAcciones")),
							grupos: 			  JSON.parse(localStorage.getItem("arregloIDGrupos"))
						},
						success:function (response) {
							console.log(response);
							jQuery(".loading").css('display','none');

							var jsonResponse = JSON.parse(response);

							if (jsonResponse.status == 'success') {
								jQuery('.modal-title').html('Registrar beneficiario.');
							    jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
							    jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

							    jQuery(".btn-accept").click(function () {
							    	tipo_doc.val('');
									num_doc.val('');
									nombres.val('');
									apellido_1.val('');
									apellido_2.val('');
									genero.val('');
									estrato.val('');
									fecha_nacimiento.val('');
									celular.val('');
									id_departamento.val('');
									id_ciudad.val('');
									barrio_vereda.val('');
									direccion.val('');
									edad.val('');
									antiguedad.val('');
									empresa_labora.val('');
									tamanio_empresa.val('');
									caracterizacion.val('');
									nivel_ocupacional.val('');
									transferencia.val('');
							    });

							    // Vaciar arreglo de grupos por accion de formacion
							    localStorage.setItem('arregloIDAcciones', JSON.stringify([]));
							    localStorage.setItem('arregloNombreAcciones', JSON.stringify([]));
							    localStorage.setItem('arregloIDGrupos', JSON.stringify([]));
							    localStorage.setItem('arregloNombreGrupos', JSON.stringify([]));
							    verAccionesPorAsignar();

							}else{
								jQuery('.modal-title').html('Registrar beneficiario.');
							    jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
							    jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
							}

						}
					});

				});

			}else{
				jQuery("#alerta-registro").html('<div class="alert alert-warning">Por favor indique a que grupos de acciones de formación sera asignado el beneficiario.</div>');
			}

		}else{
			jQuery("#alerta-registro").html('<div class="alert alert-warning">Por favor indique a que grupos de acciones de formación sera asignado el beneficiario.</div>');
		}
	}

}

function buscarCiudades(id_departamento) {
	
	if (id_departamento != '') {

		jQuery.ajax({
			type:"POST",
			url:"index.php?r=beneficiarios/buscar-ciudades",
			data:{
				id_departamento: id_departamento
			},
			success:function(response){
				//console.log(response);
				var jsonResponse = JSON.parse(response);
				console.log(jsonResponse);
				jQuery('#beneficiarios-id_ciudad').html('');
				jQuery('#beneficiarios-id_ciudad').html('<option value="">-Seleccione</option>');

				jQuery.each(jsonResponse,function (key,value) {
					jQuery('#beneficiarios-id_ciudad').append('<option value="'+value['id_ciudad']+'">'+value['nombre']+'</option>')
				})

			}
		});

	}

}

// Buscar informacion de usuario por numero de documento
function buscarDatos(num_doc) {
	//console.log("Buscar (" + num_doc + ")");
	var tipo_doc = jQuery("#beneficiarios-tipo_doc");

	var id_ciudad;

	if (tipo_doc.val() != '') {

		if (num_doc != '') {
			jQuery.ajax({
				type:"POST",
				url:"index.php?r=beneficiarios/validar-datos",
				data:{
					num_doc: num_doc,
					tipo_doc: tipo_doc.val()
				},
				success:function(response){
					var jsonResponse = JSON.parse(response);
					if (jsonResponse.status == "success") {
						// Se ha encontrado un benefeciario registrado para el numero de documento
						console.log(jsonResponse);

						jQuery("#id_proyecto_beneficiario").val(jsonResponse.datos['id_proyecto_beneficiario']);

						jQuery("#beneficiarios-nombres").val(jsonResponse.datos['nombres']);
						jQuery("#beneficiarios-apellido_1").val(jsonResponse.datos['apellido_1']);
						jQuery("#beneficiarios-apellido_2").val(jsonResponse.datos['apellido_2']);
						jQuery("#beneficiarios-genero").val(jsonResponse.datos['genero']);
						jQuery("#beneficiarios-estrato").val(jsonResponse.datos['estrato']);
						jQuery("#beneficiarios-fecha_nacimiento").val(jsonResponse.datos['fecha_nacimiento']);
						jQuery("#beneficiarios-celular").val(jsonResponse.datos['celular']);
						jQuery("#beneficiarios-id_departamento").val(jsonResponse.datos['id_dpto']);
						jQuery("#beneficiarios-id_ciudad").val(jsonResponse.datos['id_ciudad']);
						jQuery("#beneficiarios-barrio_vereda").val(jsonResponse.datos['barrio_vereda']);
						jQuery("#beneficiarios-direccion").val(jsonResponse.datos['direccion']);

						// Datos de postulacion
						jQuery("#beneficiarios-edad").val(jsonResponse.datos['edad']);
						jQuery("#beneficiarios-antiguedad").val(jsonResponse.datos['antiguedad']);
						jQuery("#beneficiarios-empresa_labora").val(jsonResponse.datos['empresa_labora']);
						jQuery("#beneficiarios-tamanio_empresa_labora").val(jsonResponse.datos['tamanio_empresa']);
						jQuery("#beneficiarios-caracterizacion").val(jsonResponse.datos['caracterizacion']);
						jQuery("#beneficiarios-nivel_ocupacional").val(jsonResponse.datos['nivel_ocupacional']);
						jQuery("#beneficiarios-transferencia").val(jsonResponse.datos['transferencia']);
						jQuery("#beneficiarios-perfil_transferencia").val(jsonResponse.datos['perfil_transferencia']);

						jQuery("#beneficiarios-nombres").attr('readonly','true');
						jQuery("#beneficiarios-apellido_1").attr('readonly','true');
						jQuery("#beneficiarios-apellido_2").attr('readonly','true');
						jQuery("#beneficiarios-genero").attr('readonly','true');
						jQuery("#beneficiarios-estrato").attr('readonly','true');
						jQuery("#beneficiarios-fecha_nacimiento").attr('readonly','true');
						jQuery("#beneficiarios-celular").attr('readonly','true');
						jQuery("#beneficiarios-id_departamento").attr('readonly','true');
						jQuery("#beneficiarios-id_ciudad").attr('readonly','true');
						jQuery("#beneficiarios-barrio_vereda").attr('readonly','true');
						jQuery("#beneficiarios-direccion").attr('readonly','true');

						//jQuery("#btn-guardar-beneficiario").css('display','none');

						

					}else{
						
						jQuery("#id_proyecto_beneficiario").val("");

						jQuery("#beneficiarios-nombres").val("");
						jQuery("#beneficiarios-apellido_1").val("");
						jQuery("#beneficiarios-apellido_2").val("");
						jQuery("#beneficiarios-genero").val("");
						jQuery("#beneficiarios-estrato").val("");
						jQuery("#beneficiarios-fecha_nacimiento").val("");
						jQuery("#beneficiarios-celular").val("");
						jQuery("#beneficiarios-id_departamento").val("");
						jQuery("#beneficiarios-id_ciudad").val("");
						jQuery("#beneficiarios-barrio_vereda").val("");
						jQuery("#beneficiarios-direccion").val("");

						jQuery("#beneficiarios-edad").val("");
						jQuery("#beneficiarios-antiguedad").val("");
						jQuery("#beneficiarios-empresa_labora").val("");
						jQuery("#beneficiarios-tamanio_empresa_labora").val("");
						jQuery("#beneficiarios-caracterizacion").val("");
						jQuery("#beneficiarios-nivel_ocupacional").val("");
						jQuery("#beneficiarios-transferencia").val("");
						jQuery("#beneficiarios-perfil_transferencia").val("");

						jQuery("#beneficiarios-nombres").removeAttr("readonly");
						jQuery("#beneficiarios-apellido_1").removeAttr("readonly");
						jQuery("#beneficiarios-apellido_2").removeAttr("readonly");
						jQuery("#beneficiarios-genero").removeAttr("readonly");
						jQuery("#beneficiarios-estrato").removeAttr("readonly");
						jQuery("#beneficiarios-fecha_nacimiento").removeAttr("readonly");
						jQuery("#beneficiarios-celular").removeAttr("readonly");
						jQuery("#beneficiarios-id_departamento").removeAttr("readonly");
						jQuery("#beneficiarios-id_ciudad").removeAttr("readonly");
						jQuery("#beneficiarios-barrio_vereda").removeAttr("readonly");
						jQuery("#beneficiarios-direccion").removeAttr("readonly");

						//jQuery("#btn-guardar-beneficiario").css('display','block');										
					}
				}
			});
		}
	}else{
		jQuery("#alerta-registro").html('<div class="alert alert-warning">Seleccione el tipo de identificación del beneficiario.</div>');
		jQuery("#beneficiarios-tipo_doc").focus();
		jQuery("#beneficiarios-num_doc").val('');
	}
}

// Buscar grupos de la accion de formacion seleccionada
function buscarGruposAF(accion) {
	//console.log("Id AF: "+id_accion_formacion);
	var arreglo = accion.split('_');

	jQuery.ajax({
		type:"POST",
		url:"index.php?r=beneficiarios/buscar-grupos-accion-formacion",
		data:{
			id_accion_formacion: arreglo[0]
		},
		success:function (response) {
			console.log(response);
			var jsonResponse = JSON.parse(response);
			if (jsonResponse.status == 'success') {
				jQuery("#listadoGrupos").removeAttr('disabled');

				jQuery("#listadoGrupos").html('');
				jQuery("#listadoGrupos").html('<option value="">-Seleccione</option>');
				jQuery.each(jsonResponse.grupos,function (key,value) {
					var grupo = value['id_grupo']+'_'+value['nombre'];
					jQuery("#listadoGrupos").append('<option value="'+grupo+'">'+value['nombre']+'</option>');
				})

			}else{
				jQuery("#listadoGrupos").attr('disabled','true');
			}
		}
	})
}

// Agregar grupo al arreglo de asignacion de grupos
function agregarGrupoAccionFormacion() {
	
	var listadoAccionesFormacion = jQuery("#listadoAccionesFormacion");
	var listadoGrupos = jQuery("#listadoGrupos");

	jQuery("#alerta-asignacion-grupos").html('');

	if (listadoAccionesFormacion.val() != '') {
		if (listadoGrupos.val() != '') {
			
			//console.log("Accion de formacion ("+listadoAccionesFormacion.val()+") Grupo ("+listadoGrupos.val()+")");
			console.log("Accion de formación: " + listadoAccionesFormacion.val());

			if(localStorage.asignacion){
				
				arregloIDAcciones 		= JSON.parse(localStorage.getItem("arregloIDAcciones"));
				arregloNombreAcciones 	= JSON.parse(localStorage.getItem("arregloNombreAcciones"));
				arregloIDGrupos 		= JSON.parse(localStorage.getItem("arregloIDGrupos"));
				arregloNombreGrupos 	= JSON.parse(localStorage.getItem("arregloNombreGrupos"));

				
				var accion 	= listadoAccionesFormacion.val().split('_');
				var grupo 	= listadoGrupos.val().split('_');

				var eval = arregloIDAcciones.indexOf(accion[0]);

				if (eval == -1) {

					arregloIDAcciones.push(accion[0]);
					arregloNombreAcciones.push(accion[1]);
					arregloIDGrupos.push(grupo[0]);
					arregloNombreGrupos.push(grupo[1]);

					localStorage.setItem("arregloIDAcciones",JSON.stringify(arregloIDAcciones));
					localStorage.setItem("arregloNombreAcciones",JSON.stringify(arregloNombreAcciones));
					localStorage.setItem("arregloIDGrupos",JSON.stringify(arregloIDGrupos));
					localStorage.setItem("arregloNombreGrupos",JSON.stringify(arregloNombreGrupos));

					verAccionesPorAsignar();

				}else{
					jQuery("#alerta-asignacion-grupos").html('<div class="alert alert-warning">Ya ha seleccionado un grupo para esta acción de formación.</div>');
					verAccionesPorAsignar();
				}
				

			}else{
				var arregloIDAcciones 		= new Array();
				var arregloNombreAcciones 	= new Array();
				var arregloIDGrupos 		= new Array();
				var arregloNombreGrupos 	= new Array();

				var accion 	= listadoAccionesFormacion.val().split('_');
				var grupo 	= listadoGrupos.val().split('_');

				arregloIDAcciones.push(accion[0]);
				arregloNombreAcciones.push(accion[1]);
				arregloIDGrupos.push(grupo[0]);
				arregloNombreGrupos.push(grupo[1]);

				localStorage.setItem("arregloIDAcciones",JSON.stringify(arregloIDAcciones));
				localStorage.setItem("arregloNombreAcciones",JSON.stringify(arregloNombreAcciones));
				localStorage.setItem("arregloIDGrupos",JSON.stringify(arregloIDGrupos));
				localStorage.setItem("arregloNombreGrupos",JSON.stringify(arregloNombreGrupos));
				localStorage.setItem("asignacion",true);

				verAccionesPorAsignar();
			}

		}else{
			jQuery("#alerta-asignacion-grupos").html('<div class="alert alert-warning">Seleccione un grupo de la lista.</div>');	
		}
	}else{
		jQuery("#alerta-asignacion-grupos").html('<div class="alert alert-warning">Seleccione una acción de formación de la lista.</div>');
	}
}

// Ver acciones de formacion seleccionadas por asignar al beneficiario
function verAccionesPorAsignar() {
	
	//jQuery("#alerta-asignacion-grupos").html('');

	if(localStorage.asignacion){

		var arregloIDAcciones 		= JSON.parse(localStorage.getItem("arregloIDAcciones"));
		var arregloNombreAcciones 	= JSON.parse(localStorage.getItem("arregloNombreAcciones"));
		var arregloIDGrupos 		= JSON.parse(localStorage.getItem("arregloIDGrupos"));
		var arregloNombreGrupos 	= JSON.parse(localStorage.getItem("arregloNombreGrupos"));

		jQuery("#listadoGruposAsignados").html('');

		var i = 1;
		jQuery.each(arregloIDAcciones,function (key,value) {
			jQuery("#listadoGruposAsignados").append('<tr>'+
					'<td>'+arregloNombreAcciones[key]+'</td>'+
					'<td>'+arregloNombreGrupos[key]+'</td>'+
					'<td><button type="button" class="btn btn-danger" title="Eliminar" onclick=removerGrupo('+arregloIDAcciones[key]+')><span class="fa fa-times"></span></button></td>'+
				'</tr>');

			i++;
		})

	}
}

// Remover un item del listado de arreglo de asignacion
function removerGrupo(id_accion_formacion) {
	console.log("Remover: "+id_accion_formacion);

	jQuery("#alerta-asignacion-grupos").html('');

	if(localStorage.asignacion){

		var arregloIDAcciones 		= JSON.parse(localStorage.getItem("arregloIDAcciones"));
		var arregloNombreAcciones 	= JSON.parse(localStorage.getItem("arregloNombreAcciones"));
		var arregloIDGrupos 		= JSON.parse(localStorage.getItem("arregloIDGrupos"));
		var arregloNombreGrupos 	= JSON.parse(localStorage.getItem("arregloNombreGrupos"));

		var pos = null;

		arregloIDAcciones.forEach(function (elemento,indice) {
			console.log(elemento, indice);
			if (elemento == id_accion_formacion) {
				pos = indice;
			}
		});

		console.log("El indice a eliminar es: ["+pos+"]");

		arregloIDAcciones.splice(pos, 1);
		arregloNombreAcciones.splice(pos, 1);
		arregloIDGrupos.splice(pos, 1);
		arregloNombreGrupos.splice(pos, 1);

		localStorage.setItem("arregloIDAcciones",JSON.stringify(arregloIDAcciones));
		localStorage.setItem("arregloNombreAcciones",JSON.stringify(arregloNombreAcciones));
		localStorage.setItem("arregloIDGrupos",JSON.stringify(arregloIDGrupos));
		localStorage.setItem("arregloNombreGrupos",JSON.stringify(arregloNombreGrupos));
		
		verAccionesPorAsignar();

	}
}

// Evaluar campo de perfil de transferencia si aplica o no la transferencia
function evaluarPerfilTransferencia(transferencia) {
	console.log("Transferencia: "+transferencia);
	
	var perfil_transferencia = jQuery("#beneficiarios-perfil_transferencia");
	perfil_transferencia.val('');

	//if (transferencia != '') {
		if (transferencia == "SI") {

			perfil_transferencia.html('<option value="">-Seleccione</option>'+
				'<option value="FUNCIONARIO">FUNCIONARIO</option>'+
				'<option value="CONTRATISTA">CONTRATISTA</option>'+
				'<option value="INSTRUCTOR">INSTRUCTOR</option>'+
				'<option value="APRENDIZ">APRENDIZ</option>'+
				'<option value="EGRESADO">EGRESADO</option>'+
				'<option value="BENEFICIARIO DEL FONDO EMPRENDER">BENEFICIARIO DEL FONDO EMPRENDER</option>'+
				'<option value="REGISTRADO EN AGENCIA PÚBLICA DE EMPLEO (APE)">REGISTRADO EN AGENCIA PÚBLICA DE EMPLEO (APE)</option>');

		}else if (transferencia == "NO") {

			perfil_transferencia.html('<option value="">-Seleccione</option>'+
				'<option value="NINGUNO">NINGUNO</option>');

		}
	//}
}

// Calcular edad del beneficiario con la fecha de nacimiento
function calcularEdad(fecha_nacimiento) {
	
	if (fecha_nacimiento != '') {

		var hoy = new Date();
	    var cumpleanos = new Date(fecha_nacimiento);
	    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
	    var m = hoy.getMonth() - cumpleanos.getMonth();

	    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
	        edad--;
	    }

	    jQuery("#beneficiarios-edad").val(edad);

	}
}