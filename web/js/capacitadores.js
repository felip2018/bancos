function registrar_capacitador() {
  jQuery("#alerta").html('');

  var tipo_doc          = jQuery("#capacitadores-tipo_doc");
  var num_doc           = jQuery("#capacitadores-num_doc");
  var nombres           = jQuery("#capacitadores-nombres");
  var primer_apellido   = jQuery("#capacitadores-primer_apellido");
  var segundo_apellido  = jQuery("#capacitadores-segundo_apellido");
  var correo            = jQuery("#capacitadores-correo");

  // Obtener los elementos de clase "registerDoc"
  var docs = jQuery(".registerDoc");
  //var allowedExtensions = /(.jpg|.jpeg|.png|.gif)$/i;
  var allowedExtensions = /(.pdf)$/i;

  if (tipo_doc.val() != '') {
    if (num_doc.val() != '') {
      if (nombres.val() != '') {
        if (primer_apellido.val() != '') {
          if (correo.val() != '') {

            if (docs.length > 0) {
              var docsValidation = true;
              jQuery.each(docs,function (key,value) {
                //console.log(value.getAttribute('id'));
                var id = value.getAttribute('id');
                var documento = value.getAttribute('document');

                var element = jQuery("#"+id);

                if (element.val() == "") {

                  jQuery("#alerta").html('<div class="alert alert-warning">Ingrese el documento requerido "<b>'+documento+'</b>".</div>');
                  element.focus();
                  docsValidation = false;
                  return false;//break loop

                }else{
                  
                  if(!allowedExtensions.exec(element.val())){
                    jQuery("#alerta").html('<div class="alert alert-warning">Formato de documento inválido "<b>'+documento+'</b>" ingrese el documento requerido en formato (.pdf)".</div>');
                    element.focus();
                    docsValidation = false;
                    return false;
                  }else{

                    var fileSize = element[0].files[0].size;
                    var sizeKB = parseInt(fileSize/1024);

                    //console.log("File Size: "+fileSize+", Kb: "+sizeKB);
                    // El peso no debe superar 8Mb 0 8192 Kb
                    if (sizeKB > 8192) {
                      console.log(sizeKB+" Excede el peso de 8192 Kb.");
                      jQuery("#alerta").html('<div class="alert alert-warning">El peso del archivo es excede el límite permitido "<b>'+documento+'</b>" el archivo no debe exceder las 8Mb".</div>');
                      element.focus();
                      docsValidation = false;
                      return false;//break loop
                    }else{
                      docsValidation = true;
                    }
                  }

                }

              });
            }

            if (docsValidation) {

              var form      = document.getElementById('registro_capacitador');
              var formData  = new FormData(form);

              jQuery.ajax({
                type:"POST",
                url:"index.php?r=capacitadores/add-new-capacitador",
                data: formData,
                contentType:false,
                processData:false,
                success:function (response) {
                  
                  console.log(response);                
                  jQuery("#alerta").html('');

                  var jsonResponse = JSON.parse(response);

                  if (jsonResponse.status == "success") {
                    jQuery("#btnRegisterCapacitador").css("display","none");
                    jQuery("#alerta").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
                    jQuery("#id_capacitador").val(jsonResponse.id_capacitador);
                  }else{
                    jQuery("#btnRegisterCapacitador").css("display","block");
                    jQuery("#alerta").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
                    jQuery("#id_capacitador").val('');
                  }
                }
              });

            }

          }else{
            jQuery("#alerta").html('<div class="alert alert-warning">Ingrese el correo electrónico del capacitador del proyecto.</div>');
            correo.focus();
          }
        }else{
          jQuery("#alerta").html('<div class="alert alert-warning">Ingrese el primer apellido del capacitador de proyecto.</div>');
          primer_apellido.focus();
        }
      }else{
        jQuery("#alerta").html('<div class="alert alert-warning">Ingrese el nombre del capacitador de proyecto.</div>');
        nombres.focus();
      }
    }else{
      jQuery("#alerta").html('<div class="alert alert-warning">Ingrese un número de identificación válido.</div>');
      num_doc.focus();
    }
  }else{
    jQuery("#alerta").html('<div class="alert alert-warning">Seleccione el tipo de identificación válido.</div>');
    tipo_doc.focus();
  }
}

// Buscar capacitador al registrar el numero de identificacion
function buscarCapacitador(num_doc) {
  
  var tipo_doc = jQuery("#capacitadores-tipo_doc");

  if (tipo_doc.val() != '') {

    if (num_doc != '') {
      jQuery.ajax({
        type:"POST",
        url:"index.php?r=capacitadores/buscar-capacitador-doc",
        data: {
          num_doc: num_doc,
          tipo_doc: tipo_doc.val()
        },
        success:function (response) {
          console.log(response);
          var jsonResponse = JSON.parse(response);
          if (jsonResponse.status == 'success') {
            
            jQuery('#btnRegisterCapacitador').css('display','none');

            jQuery("#id_capacitador").val(jsonResponse.datos['id_capacitador']);

            jQuery("#capacitadores-nombres").val(jsonResponse.datos['nombres']);
            jQuery("#capacitadores-primer_apellido").val(jsonResponse.datos['primer_apellido']);
            jQuery("#capacitadores-segundo_apellido").val(jsonResponse.datos['segundo_apellido']);
            jQuery("#capacitadores-correo").val(jsonResponse.datos['correo']);

            jQuery("#capacitadores-nombres").attr('readonly',true);
            jQuery("#capacitadores-primer_apellido").attr('readonly',true);
            jQuery("#capacitadores-segundo_apellido").attr('readonly',true);
            jQuery("#capacitadores-correo").attr('readonly',true);

            buscar_experiencias(jsonResponse.datos['id_capacitador']);
            buscar_titulos(jsonResponse.datos['id_capacitador']);
            buscar_referencias(jsonResponse.datos['id_capacitador']);
            buscar_palabras(jsonResponse.datos['id_capacitador']);

          }else{
            jQuery('#btnRegisterCapacitador').css('display','block');

            jQuery("#id_capacitador").val('');

            jQuery("#capacitadores-nombres").val('');
            jQuery("#capacitadores-primer_apellido").val('');
            jQuery("#capacitadores-segundo_apellido").val('');
            jQuery("#capacitadores-correo").val('');

            jQuery("#capacitadores-nombres").attr('readonly',false);
            jQuery("#capacitadores-primer_apellido").attr('readonly',false);
            jQuery("#capacitadores-segundo_apellido").attr('readonly',false);
            jQuery("#capacitadores-correo").attr('readonly',false);

            jQuery("#listadoExperienciasCapacitador").html('');
            jQuery("#listadoTitulosCapacitador").html('');
            jQuery("#listadoReferenciasCapacitador").html('');
            jQuery("#listaPalabrasClavePorAdicionar").html('');
            jQuery("#listaPalabrasClaveAsociadas").html('');
          }
        }
      })
    }

  }else{

    jQuery("#alerta").html('<div class="alert alert-warning">Seleccione un tipo de identificación válido.</div>');
    tipo_doc.focus();

  }
}

function registrar_experiencias() {
  
  jQuery("#alerta").html('');

  var id_capacitador = jQuery("#id_capacitador");

  if (id_capacitador.val() != '') {
    jQuery('.modal').modal({backdrop: 'static', keyboard: false});
    jQuery('.modal-title').html('Registrar experiencia');
    jQuery('.modal-body').html('<form class="row" id="form-experiencia">'+
        '<div class="col-xs-12" id="alert-form-experiencia"></div>'+
        '<div class="col-xs-12">'+
          '<input type="hidden" class="form-control" name="id_capacitador" id="id_capacitador" value="'+id_capacitador.val()+'"/>'+
        '</div>'+
        '<div class="col-xs-12">'+
          '<p>Tipo de experiencia</p>'+
          '<select class="form-control" name="tipo" id="tipo">'+
            '<option value="">-Seleccione</option>'+
            '<option value="TEMATICA">TEMATICA</option>'+
            '<option value="DOCENTE">DOCENTE</option>'+
            '<option value="EXPERIENCIA">EXPERIENCIA</option>'+
          '</select>'+
        '</div>'+
        '<div class="col-xs-12">'+
          '<p>Descripción</p>'+
          '<textarea class="form-control" rows="5" name="descripcion" id="descripcion"></textarea>'+
        '</div>'+
        '<div class="col-xs-12">'+
          '<p>Soporte</p>'+
          '<input type="file" class="form-control" name="soporte_experiencia" id="soporte"/>'+
        '</div>'+
      '</form>');
    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Guardar <i class="fa fa-save"></i> </button>');

    jQuery('.btn-accept').click(function () {

      var tipo = jQuery('#tipo');
      var descripcion = jQuery('#descripcion');
      var soporte = jQuery('#soporte');

      if (tipo.val() != '') {
        if (descripcion.val() != '') {
          if (soporte.val() != '') {

            // Validar peso del archivo
            var fileSize = soporte[0].files[0].size;
            var sizeKB = parseInt(fileSize/1024);

            // El peso no debe superar 8Mb 0 8192 Kb
            if (sizeKB < 8192) {

              var form    = document.getElementById('form-experiencia');
              var formData  = new FormData(form);

              jQuery.ajax({
                type:"POST",
                url:"index.php?r=capacitadores/add-experience",
                data:formData,
                contentType: false,
                processData: false,
                success: function (response) {
                  console.log(response);
                  var jsonResponse = JSON.parse(response);

                  if (jsonResponse.status == "success") {
                    jQuery(".modal-body").html('');
                    jQuery(".modal-body").html('Se ha registrado la experiencia del capacitador correctamente.');
                    jQuery(".modal-footer").html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

                    jQuery('.btn-accept').click(function () {
                  
                      buscar_experiencias(id_capacitador.val());
                    });
                  }else{
                    jQuery("#alert-form-experiencia").html('');
                    jQuery("#alert-form-experiencia").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
                  }
                }
              })
            }else{
              jQuery("#alert-form-experiencia").html('<div class="alert alert-warning">El peso del archivo excede el límite permitido (8Mb).</div>');
            }
          }else{
            jQuery("#alert-form-experiencia").html('<div class="alert alert-warning">Debe subir el documento (pdf) que soporte la experiencia que desea registrar.</div>');
          }
        }else{
          jQuery("#alert-form-experiencia").html('<div class="alert alert-warning">Describa la experiencia que desea registrar.</div>');  
        }
      }else{
        jQuery("#alert-form-experiencia").html('<div class="alert alert-warning">Seleccione el tipo de experiencia a registrar.</div>');    
      }

    });

  }else{
    jQuery("#alerta").html('<div class="alert alert-warning">Seleccione un capacitador de proyecto para agregar experiencias.</div>');
  }

}

function buscar_experiencias(id_capacitador) {
  
  jQuery("#listadoExperienciasCapacitador").html('');

  jQuery.ajax({
    type:"POST",
    url:"index.php?r=capacitadores/search-experiences",
    data:{
      id_capacitador: id_capacitador
    },
    success:function (response) {
      var jsonResponse = JSON.parse(response);
      if (jsonResponse.status == "success") {

        jQuery("#listadoExperienciasCapacitador").html('');        
        var i = 1;
        jQuery.each(jsonResponse.experiencias, function (key,value) {
          jQuery("#listadoExperienciasCapacitador").append('<tr>'+
                  '<td>'+i+'</td>'+
                  '<td>'+value['tipo']+'</td>'+
                  '<td>'+value['descripcion']+'</td>'+
                  '<td><a href="convocatoria_'+value['year']+'/bancos/capacitadores/'+value['num_doc']+'/'+value['url']+'" target="_blank">'+value['url']+'</a></td>'+
                  '<td></td>'+
              '</tr>');

          i++;
        });

      }
    }
  })
}

function actualizar_experiencias() {
  
  var id_capacitador = jQuery("#id_capacitador");

  if (id_capacitador.val() != '') {
    buscar_experiencias(id_capacitador.val());
  }
}


function registrar_titulos() {
  
  jQuery("#alerta").html('');

  var id_capacitador = jQuery("#id_capacitador");

  if (id_capacitador.val() != '') {
    jQuery('.modal').modal({backdrop: 'static', keyboard: false});
    jQuery('.modal-title').html('Registrar titulo');
    jQuery('.modal-body').html('<form class="row" id="form-titulo">'+
        '<div class="col-xs-12" id="alert-form-titulo"></div>'+
        '<div class="col-xs-12">'+
          '<input type="hidden" class="form-control" name="id_capacitador" id="id_capacitador" value="'+id_capacitador.val()+'"/>'+
        '</div>'+
        '<div class="col-xs-12">'+
          '<p>Tipo de titulo</p>'+
          '<select class="form-control" name="tipo_titulo" id="tipo_titulo">'+
            '<option value="">-Seleccione</option>'+
            '<option value="BACHILLER">BACHILLER</option>'+
            '<option value="TECNICO">TECNICO</option>'+
            '<option value="TECNOLOGO">TECNOLOGO</option>'+
            '<option value="PREGRADO">PREGRADO</option>'+
            '<option value="POSGRADO">POSGRADO</option>'+
            '<option value="MAESTRIA">MAESTRIA</option>'+
          '</select>'+
        '</div>'+
        '<div class="col-xs-12">'+
          '<p>Descripción</p>'+
          '<textarea class="form-control" rows="5" name="descripcion" id="descripcion"></textarea>'+
        '</div>'+
        '<div class="col-xs-12">'+
          '<p>Soporte</p>'+
          '<input type="file" class="form-control" name="soporte_titulo" id="soporte"/>'+
        '</div>'+
      '</form>');
    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Guardar <i class="fa fa-save"></i> </button>');

    jQuery('.btn-accept').click(function () {

      var tipo    = jQuery('#tipo_titulo');
      var descripcion = jQuery('#descripcion');
      var soporte   = jQuery('#soporte');

      if (tipo.val() != '') {
        if (descripcion.val() != '') {
          if (soporte.val() != '') {

            // Validar peso del archivo
            var fileSize = soporte[0].files[0].size;
            var sizeKB = parseInt(fileSize/1024);

            // El peso no debe superar 8Mb 0 8192 Kb
            if (sizeKB < 8192) {

              var form    = document.getElementById('form-titulo');
              var formData  = new FormData(form);

              jQuery.ajax({
                type:"POST",
                url:"index.php?r=capacitadores/add-title",
                data:formData,
                contentType: false,
                processData: false,
                success: function (response) {
                  console.log(response);
                  var jsonResponse = JSON.parse(response);
                 
                  if (jsonResponse.status == "success") {
                    jQuery(".modal-body").html('');
                    jQuery(".modal-body").html('Se ha registrado el titulo del capacitador correctamente.');
                    jQuery(".modal-footer").html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

                    jQuery('.btn-accept').click(function () {
         
                      buscar_titulos(id_capacitador.val());
                    });
                  }else{
                    jQuery("#alert-form-titulo").html('');
                    jQuery("#alert-form-titulo").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
                  }
                }
              });

            }else{
              jQuery("#alert-form-titulo").html('<div class="alert alert-warning">El peso del archivo excede el límite permitido (8Mb).</div>');
            } 

          }else{
            jQuery("#alert-form-titulo").html('<div class="alert alert-warning">Debe subir el documento (pdf) que soporte el titulo que desea registrar.</div>');
          }
        }else{
          jQuery("#alert-form-titulo").html('<div class="alert alert-warning">Describa el titulo que desea registrar.</div>');  
        }
      }else{
        jQuery("#alert-form-titulo").html('<div class="alert alert-warning">Seleccione el tipo de titulo a registrar.</div>');    
      }

    });

  }else{
    jQuery("#alerta").html('<div class="alert alert-warning">Seleccione un capacitador de proyecto para agregar experiencias.</div>');
  }
}

function buscar_titulos(id_capacitador) {
  
  jQuery("#listadoTitulosCapacitador").html('');

  jQuery.ajax({
    type:"POST",
    url:"index.php?r=capacitadores/search-titles",
    data:{
      id_capacitador: id_capacitador
    },
    success:function (response) {
      var jsonResponse = JSON.parse(response);
      if (jsonResponse.status == "success") {

        jQuery("#listadoTitulosCapacitador").html('');       
        var i = 1;
        jQuery.each(jsonResponse.titulos, function (key,value) {
          jQuery("#listadoTitulosCapacitador").append('<tr>'+
                  '<td>'+i+'</td>'+
                  '<td>'+value['tipo_titulo']+'</td>'+
                  '<td>'+value['descripcion']+'</td>'+
                  '<td><a href="convocatoria_'+value['year']+'/bancos/capacitadores/'+value['num_doc']+'/'+value['url']+'" target="_blank">'+value['url']+'</a></td>'+
                  '<td></td>'+
              '</tr>');

          i++;
        });

      }
    }
  })
}

function actualizar_titulos() {
  
  var id_capacitador = jQuery("#id_capacitador");

  if (id_capacitador.val() != '') {
    buscar_titulos(id_capacitador.val());
  }
}

function registrar_referencias() {
  
  jQuery("#alerta").html('');

  var id_capacitador = jQuery("#id_capacitador");

  if (id_capacitador.val() != '') {
    jQuery('.modal').modal({backdrop: 'static', keyboard: false});
    jQuery('.modal-title').html('Registrar referencia');
    jQuery('.modal-body').html('<form class="row" id="form-referencia">'+
        '<div class="col-xs-12" id="alert-form-referencia"></div>'+
        '<div class="col-xs-12">'+
          '<input type="hidden" class="form-control" name="id_capacitador" id="id_capacitador" value="'+id_capacitador.val()+'"/>'+
        '</div>'+
        '<div class="col-xs-12">'+
          '<p>Nombre</p>'+
          '<input type="text" class="form-control" name="nombre" id="nombre"/>'+
        '</div>'+
        '<div class="col-xs-12">'+
          '<p>Empresa</p>'+
          '<input type="text" class="form-control" name="empresa" id="empresa"/>'+
        '</div>'+
        '<div class="col-xs-12">'+
          '<p>Cargo</p>'+
          '<input type="text" class="form-control" name="cargo" id="cargo"/>'+
        '</div>'+
        '<div class="col-xs-12">'+
          '<p>Teléfono</p>'+
          '<input type="text" class="form-control" name="telefono" id="telefono"/>'+
        '</div>'+
        '<div class="col-xs-12">'+
          '<p>Soporte</p>'+
          '<input type="file" class="form-control" name="soporte_referencia" id="soporte"/>'+
        '</div>'+
      '</form>');
    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Guardar <i class="fa fa-save"></i> </button>');

    jQuery('.btn-accept').click(function () {

      var nombre    = jQuery('#nombre');
      var empresa   = jQuery('#empresa');
      var cargo     = jQuery('#cargo');
      var telefono  = jQuery('#telefono');
      var soporte   = jQuery('#soporte');

      if (nombre.val() != '') {
        if (empresa.val() != '') {
          if (cargo.val() != '') {
            if (telefono.val() != '') {
              if (soporte.val() != '') {

                // Validar peso del archivo
                var fileSize = soporte[0].files[0].size;
                var sizeKB = parseInt(fileSize/1024);

                // El peso no debe superar 8Mb 0 8192 Kb
                if (sizeKB < 8192) {

                var form    = document.getElementById('form-referencia');
                var formData  = new FormData(form);

                  jQuery.ajax({
                    type:"POST",
                    url:"index.php?r=capacitadores/add-reference",
                    data:formData,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                      console.log(response);
                      var jsonResponse = JSON.parse(response);
                
                      if (jsonResponse.status == "success") {
                        jQuery(".modal-body").html('');
                        jQuery(".modal-body").html('Se ha registrado la referencia del capacitador correctamente.');
                        jQuery(".modal-footer").html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

                        jQuery('.btn-accept').click(function () {
                          buscar_referencias(id_capacitador.val());
                        });
                      }else{
                        jQuery("#alert-form-referencia").html('');
                        jQuery("#alert-form-referencia").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
                      }
                    }
                  });

                }else{
                  jQuery("#alert-form-referencia").html('<div class="alert alert-warning">El peso del archivo excede el límite permitido (8Mb).</div>');
                }

              }else{
                jQuery("#alert-form-referencia").html('<div class="alert alert-warning">Debe subir el documento (pdf) que soporte la referencia que desea registrar.</div>');
              }
            }else{
              jQuery("#alert-form-referencia").html('<div class="alert alert-warning">Digite el número telefónico de la referencia que desea registrar.</div>');    
            }
          }else{
            jQuery("#alert-form-referencia").html('<div class="alert alert-warning">Digite el cargo que desempeña la referencia que desea registrar.</div>'); 
          }
        }else{
          jQuery("#alert-form-referencia").html('<div class="alert alert-warning">Digite la empresa donde labora la referencia que desea registrar.</div>');  
        }
      }else{
        jQuery("#alert-form-referencia").html('<div class="alert alert-warning">Digite el nombre de la referencia que desa registrar.</div>');    
      }

    });

  }else{
    jQuery("#alerta").html('<div class="alert alert-warning">Seleccione un capacitador de proyecto para agregar experiencias.</div>');
  }
}

function buscar_referencias(id_capacitador) {
  
  jQuery("#listadoReferenciasCapacitador").html('');

  jQuery.ajax({
    type:"POST",
    url:"index.php?r=capacitadores/search-references",
    data:{
      id_capacitador: id_capacitador
    },
    success:function (response) {
      var jsonResponse = JSON.parse(response);
      if (jsonResponse.status == "success") {

        jQuery("#listadoReferenciasCapacitador").html('');       
        var i = 1;
        jQuery.each(jsonResponse.referencias, function (key,value) {
          jQuery("#listadoReferenciasCapacitador").append('<tr>'+
                  '<td>'+i+'</td>'+
                  '<td><p>'+value['nombre']+'</p><p>Tel: '+value['telefono']+'</p></td>'+
                  '<td>'+value['empresa']+'</td>'+
                  '<td>'+value['cargo']+'</td>'+
                  '<td><a href="convocatoria_'+value['year']+'/bancos/capacitadores/'+value['num_doc']+'/'+value['url']+'" target="_blank">'+value['url']+'</a></td>'+
              '</tr>');
          i++;
        });

      }
    }
  })
}

function actualizar_referencias() {
  
  var id_capacitador = jQuery("#id_capacitador");

  if (id_capacitador.val() != '') {
    buscar_referencias(id_capacitador.val());
  }
}

// Buscar palabras clave del capacitador
function buscar_palabras(id_capacitador) {
  
  jQuery("#listaPalabrasClavePorAdicionar").html('');
  jQuery("#listaPalabrasClaveAsociadas").html('');

  jQuery.ajax({
    type:"POST",
    url:"index.php?r=capacitadores/search-words",
    data:{
      id_capacitador: id_capacitador
    },
    success:function (response) {
        //console.log(response);
        var jsonResponse = JSON.parse(response);
        //console.log(jsonResponse.pendientes);
        //console.log(jsonResponse.registradas);

        jQuery.each(jsonResponse.pendientes,function (key,value) {

          var id_palabra_clave  = value['id_palabra_clave'];
          var descripcion       = value['descripcion'];

          jQuery("#listaPalabrasClavePorAdicionar").append('<button class="btn btn-info" style="margin:5px;" onclick=agregarPalabraClave('+id_palabra_clave+','+id_capacitador+')>'+
              '<p>'+descripcion+'</p>'+
            '</button>');
        });

        jQuery.each(jsonResponse.registradas,function (key,value) {

          var id_capacitador_palabra  = value['id_capacitador_palabra'];
          var descripcion             = value['descripcion'];

          jQuery("#listaPalabrasClaveAsociadas").append('<button class="btn btn-info" style="margin:5px;" onclick=removerPalabraClave('+id_capacitador_palabra+','+id_capacitador+')>'+
              '<p>'+descripcion+'</p>'+
            '</button>');
        });

    }
  });
}

// Agregar palabra clave a un capacitador
function agregarPalabraClave(id_palabra_clave,id_capacitador) {
  //console.log("Id Palabra: "+id_palabra_clave+", Id Capacitador: "+id_capacitador);
  jQuery.ajax({
    type:"POST",
    url:"index.php?r=capacitadores/add-word",
    data:{
      id_palabra_clave: id_palabra_clave,
      id_capacitador: id_capacitador 
    },
    success:function (response) {
      buscar_palabras(id_capacitador);
    }
  })
}

// Remover palabra clave
function removerPalabraClave(id_capacitador_palabra,id_capacitador) {
  //console.log("Remover: "+id_capacitador_palabra);
  jQuery.ajax({
    type:"POST",
    url:"index.php?r=capacitadores/remove-word",
    data:{
      id_capacitador_palabra: id_capacitador_palabra,
      id_capacitador: id_capacitador 
    },
    success:function (response) {
      buscar_palabras(id_capacitador);
    }
  })
}