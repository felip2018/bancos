// Buscar grupos de la accion de formacion seleccionada
function buscarGruposAF(accion) {
	//console.log("Id AF: "+id_accion_formacion);
	var arreglo = accion.split('_');

	jQuery.ajax({
		type:"POST",
		url:"index.php?r=cronograma/buscar-grupos-accion-formacion",
		data:{
			id_accion_formacion: arreglo[0]
		},
		success:function (response) {
			console.log(response);
			var jsonResponse = JSON.parse(response);
			if (jsonResponse.status == 'success') {
				jQuery("#listadoGrupos").removeAttr('disabled');

				jQuery("#listadoGrupos").html('');
				jQuery("#listadoGrupos").html('<option value="">-Seleccione</option>');
				jQuery.each(jsonResponse.grupos,function (key,value) {
					var grupo = value['id_grupo']+'_'+value['nombre'];
					jQuery("#listadoGrupos").append('<option value="'+grupo+'">'+value['nombre']+'</option>');
				});

				jQuery("#evento_formacion").val(jsonResponse.info['evento_formacion']);
				jQuery("#modalidad").val(jsonResponse.info['modalidad']);

				if (jsonResponse.info['modalidad'] == 'PRESENCIAL') {

					jQuery("#metodologia_presencial").html('<option value="">-Seleccione</option>'+
						'<option value="TEORICA">TEORICA</option>'+
						'<option value="TEORICO - PRACTICA">TEORICO - PRACTICA</option>'+
						'<option value="APRENDIZAJE PUESTO DE TRABAJO REAL">APRENDIZAJE PUESTO DE TRABAJO REAL</option>');

					jQuery("#num_sesiones").removeAttr('readonly');
					jQuery("#num_actividades").attr('readonly','true');

					jQuery("#proveedor_formacion_virtual").attr('readonly','true');
					jQuery("#url_plataforma_virtual").attr('readonly','true');
					jQuery("#usuario_sena_virtual").attr('readonly','true');
					jQuery("#clave_sena_virtual").attr('readonly','true');

					jQuery("#metodologia_presencial").removeAttr('readonly');
					jQuery("#metodologia_virtual").attr('readonly','true');

					verFormularioSesion();
					verSesionesPresenciales();

				}else if (jsonResponse.info['modalidad'] == 'VIRTUAL') {

					jQuery("#metodologia_virtual").html('<option value="">-Seleccione</option>'+
						'<option value="VIRTUAL MASIVA">VIRTUAL MASIVA</option>'+
						'<option value="TEORICO - PRACTICA">TEORICO - PRACTICA</option>');

					jQuery("#num_sesiones").attr('readonly','true');
					jQuery("#num_actividades").removeAttr('readonly','true');

					jQuery("#num_personas").attr('readonly','true');
					jQuery("#sesiones_por_persona").attr('readonly','true');

					jQuery("#proveedor_formacion_virtual").removeAttr('readonly');
					jQuery("#url_plataforma_virtual").removeAttr('readonly');
					jQuery("#usuario_sena_virtual").removeAttr('readonly');
					jQuery("#clave_sena_virtual").removeAttr('readonly');


					jQuery("#metodologia_virtual").removeAttr('readonly');
					jQuery("#metodologia_presencial").attr('readonly','true');

					verFormularioSesion();
					verSesionesVirtuales();

				}else if (jsonResponse.info['modalidad'] == 'COMBINADA') {

					jQuery("#num_sesiones").removeAttr('readonly');
					jQuery("#num_actividades").removeAttr('readonly');
					jQuery("#metodologia_virtual").removeAttr('readonly');
					jQuery("#metodologia_presencial").removeAttr('readonly');

					jQuery("#metodologia_presencial").html('<option value="">-Seleccione</option>'+
						'<option value="TEORICO - PRACTICA">TEORICO - PRACTICA</option>'+
						'<option value="APRENDIZAJE AULA INVERTIDA">APRENDIZAJE AULA INVERTIDA</option>');

					jQuery("#metodologia_virtual").html('<option value="">-Seleccione</option>'+
						'<option value="VIRTUAL MASIVA">VIRTUAL MASIVA</option>'+
						'<option value="TEORICO - PRACTICA">TEORICO - PRACTICA</option>');

					verFormularioSesion();
					verSesionesPresenciales();
					verSesionesVirtuales();
				}

			}else{
				jQuery("#listadoGrupos").attr('disabled','true');
			}
		}
	})
}


// Validar metodologia seleccionada para habilitar campos 
function validarMetodologia(metodologia) {
	//console.log(metodologia);
	if (metodologia == 'APRENDIZAJE PUESTO DE TRABAJO REAL') {
		jQuery("#num_personas").removeAttr('readonly','false');
		jQuery("#sesiones_por_persona").removeAttr('readonly','false');
		jQuery("#num_sesiones").attr('readonly','true');

		jQuery("#nombre_sesion").html('');
		jQuery("#nombre_sesion").html('<option value="">-Seleccione</option>'+
									  '<option value="SESION GRUPAL 1">SESION GRUPAL 1</option>'+
									  '<option value="SESION INDIVIDUAL 1">SESION INDIVIDUAL 1</option>'+
									  '<option value="SESION INDIVIDUAL 2">SESION INDIVIDUAL 2</option>'+
									  '<option value="SESION INDIVIDUAL 3">SESION INDIVIDUAL 3</option>'+
									  '<option value="SESION INDIVIDUAL 4">SESION INDIVIDUAL 4</option>'+
									  '<option value="SESION INDIVIDUAL 5">SESION INDIVIDUAL 5</option>'+
									  '<option value="SESION GRUPAL 2">SESION GRUPAL 2</option>');

	}else{
		jQuery("#num_personas").attr('readonly','true');
		jQuery("#sesiones_por_persona").attr('readonly','true');
		jQuery("#num_sesiones").removeAttr('readonly','false');

		jQuery("#nombre_sesion").html('');
		jQuery("#nombre_sesion").html('<option value="">-Seleccione</option>'+
									  '<option value="SESION NORMAL">SESION NORMAL</option>');
	}
}

// Calcular el numero de sesiones por #personas * #sesiones por persona
function calcularNumeroSesiones() {
	var num_personas = jQuery("#num_personas");
	var sesiones_por_persona = jQuery("#sesiones_por_persona");
	var num_sesiones = jQuery("#num_sesiones");

	var total;

	if (num_personas.val() != '') {
		if (sesiones_por_persona.val() != '') {
			total = (parseInt(num_personas.val()) * parseInt(sesiones_por_persona.val())) + 2;
			num_sesiones.val(total);
		}else{
			sesiones_por_persona.focus();
		}
	}else{
		num_personas.focus();
	}

}

// Validar cambio de numero de sesiones
function validarNumeroSesiones() {
	console.log("Validar arreglo de sesiones.");
	var num_sesiones = jQuery("#num_sesiones");
	if (localStorage.asignacionPresencial) {
		var arregloSesionesPresenciales = JSON.parse(localStorage.getItem('arregloSesionesPresenciales'));

		if (arregloSesionesPresenciales.length > num_sesiones.val()) {
			jQuery('.modal').modal({backdrop: 'static', keyboard: false});
    		jQuery('.modal-title').html('Advertencia.');
    		jQuery('.modal-body').html('<div class="alert alert-warning">El número de sesiones ingresado no corresponde con el número de sesiones registrado hasta el momento, por favor verifique.</div>');
    		jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

    		jQuery('.btn-accept').click(function () {
    			num_sesiones.val(arregloSesionesPresenciales.length);
    		});

		}

	}
}

// Validar cambio de numero de actividades
function validarNumeroActividades() {
	console.log("Validar arreglo de actividades.");
	var num_actividades = jQuery("#num_actividades");
	if (localStorage.asignacionVirtual) {
		var arregloSesionesVirtuales = JSON.parse(localStorage.getItem('arregloSesionesVirtuales'));

		if (arregloSesionesVirtuales.length > num_actividades.val()) {
			jQuery('.modal').modal({backdrop: 'static', keyboard: false});
    		jQuery('.modal-title').html('Advertencia.');
    		jQuery('.modal-body').html('<div class="alert alert-warning">El número de actividades ingresado no corresponde con el número de actividades registrado hasta el momento, por favor verifique.</div>');
    		jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

    		jQuery('.btn-accept').click(function () {
    			num_actividades.val(arregloSesionesVirtuales.length);
    		});

		}

	}
}

// Validar que formulario de configuración se muestra de acuerdo a la modalidad de la AF
function verFormularioSesion() {

	jQuery(".alert-configuracion-sesiones").html('');

	var modalidad 	= jQuery("#modalidad");
	var metodologia = jQuery("#metodologia");

	if (modalidad.val() != '') {
		if (modalidad.val() == 'PRESENCIAL') {
			mostrar_FormularioPresencial();
		}else if (modalidad.val() == 'VIRTUAL') {
			mostrar_FormularioVirtual();
		}else if (modalidad.val() == 'COMBINADA') {
			mostrar_FormularioCombinado();
		}
	}else{
		jQuery(".alert-configuracion-sesiones").html('<div class="alert alert-warning">Seleccione una acción de formación de la lista para determinar la modalidad de configuración.</div>');
	}
}

// Mostrar formulario de sesion en modalidad PRESENCIAL
function mostrar_FormularioPresencial() {
	jQuery("#formulario-sesion-presencial").css('display','block');
	jQuery("#formulario-sesion-virtual").css('display','none');
}

// Mostrar formulario de sesiones virtuales
function mostrar_FormularioVirtual() {
	jQuery("#formulario-sesion-presencial").css('display','none');
	jQuery("#formulario-sesion-virtual").css('display','block');
}

// Mostrar formulario de sesiones combinadas
function mostrar_FormularioCombinado() {
	jQuery("#formulario-sesion-presencial").css('display','block');
	jQuery("#formulario-sesion-virtual").css('display','block');	
}

// Buscar ciudades del departamento seleccionado
function buscarCiudades(id_departamento) {
	
	if (id_departamento != '') {

		jQuery.ajax({
			type:"POST",
			url:"index.php?r=beneficiarios/buscar-ciudades",
			data:{
				id_departamento: id_departamento
			},
			success:function(response){
				//console.log(response);
				var jsonResponse = JSON.parse(response);
				jQuery('#id_ciudad').html('');
				jQuery('#id_ciudad').html('<option value="">-Seleccione</option>');

				jQuery.each(jsonResponse,function (key,value) {

					var ciudad = value['id_ciudad']+'-'+value['nombre'];

					jQuery('#id_ciudad').append('<option value="'+ciudad+'">'+value['nombre']+'</option>')
				});

			}
		});

	}

}

// Agregar datos de sesion presencial
function agregarSesionPresencial() {

	jQuery("#alerta-sesion-presencial").html('');

	var nombre_sesion 			= jQuery("#nombre_sesion");
	var num_sesion 				= jQuery("#num_sesion");
	var fecha_inicio 			= jQuery("#fecha_inicio");
	var hora_inicial 			= jQuery("#hora_inicial");
	var hora_final 				= jQuery("#hora_final");
	var num_hrs_totales 		= jQuery("#num_hrs_totales");
	var id_departamento 		= jQuery("#id_departamento");
	var id_ciudad 				= jQuery("#id_ciudad");
	var nombre_sede 			= jQuery("#nombre_sede");
	var direccion_sede 			= jQuery("#direccion_sede");
	var aula_salon 				= jQuery("#aula_salon");

	var nombre_capacitador 		= jQuery("#nombre_capacitador_presencial");
	var tipo_doc_capacitador 	= jQuery("#tipo_doc_capacitador_presencial");
	var num_doc_capacitador 	= jQuery("#num_doc_capacitador_presencial");

	var nombre_capacitador_s1 	= jQuery("#nombre_capacitador_presencial_s1");
	var tipo_doc_capacitador_s1 = jQuery("#tipo_doc_capacitador_presencial_s1");
	var num_doc_capacitador_s1 	= jQuery("#num_doc_capacitador_presencial_s1");

	var nombre_capacitador_s2 	= jQuery("#nombre_capacitador_presencial_s2");
	var tipo_doc_capacitador_s2 = jQuery("#tipo_doc_capacitador_presencial_s2");
	var num_doc_capacitador_s2 	= jQuery("#num_doc_capacitador_presencial_s2");
	

	var perfil_capacitador 		= jQuery("#perfil_capacitador_presencial");
	var tipo_capacitador 		= jQuery("#tipo_capacitador_presencial");

	var campos = [nombre_sesion,num_sesion,fecha_inicio,hora_inicial,hora_final,num_hrs_totales,id_departamento,id_ciudad,nombre_sede,direccion_sede,aula_salon,nombre_capacitador,tipo_doc_capacitador,num_doc_capacitador,perfil_capacitador,tipo_capacitador];

	var j = 16;

	for (var i = 0; i < campos.length; i++) {
		if(campos[i].val() == ''){
			jQuery("#alerta-sesion-presencial").html('<div class="alert alert-warning">Por favor diligencie el campo <b>'+campos[i].attr('name')+'</b></div>');
			campos[i].focus();
			break;
		}else{
			j--;
		}
	}

	if (j == 0) {
		//console.log("Agregar sesion.");
		var datos = [
			nombre_sesion.val(),
			num_sesion.val(),
			fecha_inicio.val(),
			hora_inicial.val(),
			hora_final.val(),
			num_hrs_totales.val(),
			id_departamento.val(),
			id_ciudad.val(),
			nombre_sede.val(),
			direccion_sede.val(),
			aula_salon.val(),

			nombre_capacitador.val(),
			tipo_doc_capacitador.val(),
			num_doc_capacitador.val(),

			nombre_capacitador_s1.val(),
			tipo_doc_capacitador_s1.val(),
			num_doc_capacitador_s1.val(),

			nombre_capacitador_s2.val(),
			tipo_doc_capacitador_s2.val(),
			num_doc_capacitador_s2.val(),

			perfil_capacitador.val(),
			tipo_capacitador.val()
		];
		//console.log(datos);

		if (localStorage.asignacionPresencial) {

			arregloSesionesPresenciales = JSON.parse(localStorage.getItem('arregloSesionesPresenciales'));

			if (arregloSesionesPresenciales.length < jQuery("#num_sesiones").val()) {

				arregloSesionesPresenciales.push(datos);

				localStorage.setItem('arregloSesionesPresenciales',JSON.stringify(arregloSesionesPresenciales));

				jQuery('.modal').modal({backdrop: 'static', keyboard: false});
	    		jQuery('.modal-title').html('Sesión agregada exitosamente.');
	    		jQuery('.modal-body').html('<div class="alert alert-success">Se ha registrado la información de la sesión correctamente.</div>');
	    		jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

				jQuery("#alerta-sesion-presencial").html('');

				verSesionesPresenciales();

			}else{
				jQuery('.modal').modal({backdrop: 'static', keyboard: false});
	    		jQuery('.modal-title').html('Error de sesión.');
	    		jQuery('.modal-body').html('<div class="alert alert-warning">No fue posible posible registrar la sesión porque excede el número de sesiones registrado en el campo <b>Número total de sesiones</b>.</div>');
	    		jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');				
			}

		}else{
			localStorage.setItem("asignacionPresencial",true);

			var arregloSesionesPresenciales = new Array();
			arregloSesionesPresenciales.push(datos);

			localStorage.setItem('arregloSesionesPresenciales',JSON.stringify(arregloSesionesPresenciales));

			jQuery('.modal').modal({backdrop: 'static', keyboard: false});
    		jQuery('.modal-title').html('Sesión agregada exitosamente.');
    		jQuery('.modal-body').html('<div class="alert alert-success">Se ha registrado la información de la sesión correctamente.</div>');
    		jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

			jQuery("#alerta-sesion-presencial").html('');

			verSesionesPresenciales();
		}

	}

}

// Visualizar el listado de sesiones presenciales registradas
function verSesionesPresenciales() {
	if (localStorage.asignacionPresencial) {

		var arregloSesionesPresenciales = JSON.parse(localStorage.getItem("arregloSesionesPresenciales"));
		
		jQuery("#unidad-tematica-presencial").html('<div style="padding:10px;border:1px solid #CCCCCC;border-radius:5px;text-align:center;margin-top:10px;"><b>'+jQuery("#nombre_unidad_tematica").val().toUpperCase()+'</b></div>');
		jQuery("#listado-sesiones").html('');
		
		var i = 1;
		jQuery.each(arregloSesionesPresenciales,function (key,value) {
			var color = (i%2==0) ? 'rgb(255,255,255)':'rgba(254,202,87,0.5)';

			jQuery("#listado-sesiones").append('<div style="background:'+color+';padding:10px;border-radius:5px;margin-bottom:10px;margin-top:10px;">'+
						'<button type="button" class="btn btn-danger pull-right" title="Eliminar" onclick=removerSesionPresencial('+key+')><span class="fa fa-times"></span></button>'+
						'<p>'+
							'<b>Sesión presencial No.</b>: '+value[1]+' / <b>Nombre de la sesión</b>: '+value[0]+'<br>'+
							'<b>Fecha de inicio</b>: '+value[2]+' - <b>Hora de inicio</b>: '+value[3]+' / <b>Hora de fin</b>: '+value[4]+' / <b>Número de horas totales</b>: '+value[5]+'<br>'+
							'<b>Ciudad</b>: '+value[7]+'<br>'+
							'<b>Nombre de la sede</b>: '+value[8]+'<br>'+
							'<b>Dirección de la sede</b>: '+value[9]+'<br>'+
							'<b>Aula / Salón</b>: '+value[10]+'<br>'+
							'<b>Nombre del capacitador</b>: '+value[11]+'<br>'+
							'<b>Identificación</b>: '+value[12]+'-'+value[13]+'<br>'+
							'<b>Nombre del capacitador suplente 1</b>: '+value[14]+'<br>'+
							'<b>Identificación</b>: '+value[15]+'-'+value[16]+'<br>'+
							'<b>Nombre del capacitador suplente 2</b>: '+value[17]+'<br>'+
							'<b>Identificación</b>: '+value[18]+'-'+value[19]+'<br>'+
							'<b>Perfil del capacitador</b>: '+value[20]+'<br>'+
							'<b>Tipo de capacitador</b>: '+value[21]+
						'</p>'+
				'</div>');

			i++;
		})
	}
}

// Remover sesión presencial
function removerSesionPresencial(key) {
	console.log("Remover: "+key);
	jQuery('.modal').modal({backdrop: 'static', keyboard: false});
    jQuery('.modal-title').html('Eliminar sesión seleccionada.');
    jQuery('.modal-body').html('A continuación borrará de la lista la sesión seleccionada.<br><b>¿Desea continuar?</b>');
    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

    jQuery('.btn-accept').click(function () {
    	var arregloSesionesPresenciales = JSON.parse(localStorage.getItem('arregloSesionesPresenciales'));
    	arregloSesionesPresenciales.splice(key,1);
    	localStorage.setItem('arregloSesionesPresenciales',JSON.stringify(arregloSesionesPresenciales));

    	jQuery('.modal-body').html('Se ha eliminado la sesión selecionada de la lista.');
    	jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

    	jQuery('.btn-accept').click(function () {
    		verSesionesPresenciales();
    	});
    	
    });
}

// Agregar sesion virtual
function agregarSesionVirtual() {
	jQuery("#alerta-sesion-virtual").html('');

	var nombre_actividad 				= jQuery("#nombre_actividad");
	var fecha_inicio_actividad 			= jQuery("#fecha_inicio_actividad");
	var fecha_fin_actividad 			= jQuery("#fecha_fin_actividad");
	var proveedor_formacion 			= jQuery("#proveedor_formacion");
	var url_plataforma 					= jQuery("#url_plataforma");
	var usuario_sena 					= jQuery("#usuario_sena");
	var clave_sena 						= jQuery("#clave_sena");
	var nombre_capacitador_virtual 		= jQuery("#nombre_capacitador_virtual");
	var tipo_doc_capacitador_virtual 	= jQuery("#tipo_doc_capacitador_virtual");
	var num_doc_capacitador_virtual 	= jQuery("#num_doc_capacitador_virtual");
	var perfil_capacitador_virtual 		= jQuery("#perfil_capacitador_virtual");
	var tipo_capacitador_virtual 		= jQuery("#tipo_capacitador_virtual");

	var campos = [nombre_actividad,fecha_inicio_actividad,fecha_fin_actividad,proveedor_formacion,url_plataforma,usuario_sena,clave_sena,nombre_capacitador_virtual,tipo_doc_capacitador_virtual,num_doc_capacitador_virtual,perfil_capacitador_virtual,tipo_capacitador_virtual];

	var j = 12;

	for (var i = 0; i < campos.length; i++) {
		if(campos[i].val() == ''){
			jQuery("#alerta-sesion-virtual").html('<div class="alert alert-warning">Por favor diligencie el campo <b>'+campos[i].attr('name')+'</b></div>');
			campos[i].focus();
			break;
		}else{
			j--;
		}
	}

	if (j == 0) {
		//console.log("Agregar sesion.");
		var datos = [
			nombre_actividad.val(),
			fecha_inicio_actividad.val(),
			fecha_fin_actividad.val(),
			proveedor_formacion.val(),
			url_plataforma.val(),
			usuario_sena.val(),
			clave_sena.val(),
			nombre_capacitador_virtual.val(),
			tipo_doc_capacitador_virtual.val(),
			num_doc_capacitador_virtual.val(),
			perfil_capacitador_virtual.val(),
			tipo_capacitador_virtual.val()
		];
		//console.log(datos);

		if (localStorage.asignacionVirtual) {

			arregloSesionesVirtuales = JSON.parse(localStorage.getItem('arregloSesionesVirtuales'));

			if (arregloSesionesVirtuales.length < jQuery("#num_actividades").val()) {

				arregloSesionesVirtuales.push(datos);

				localStorage.setItem('arregloSesionesVirtuales',JSON.stringify(arregloSesionesVirtuales));

				jQuery('.modal').modal({backdrop: 'static', keyboard: false});
	    		jQuery('.modal-title').html('Sesión agregada exitosamente.');
	    		jQuery('.modal-body').html('<div class="alert alert-success">Se ha registrado la información de la sesión correctamente.</div>');
	    		jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

				jQuery("#alerta-sesion-virtual").html('');

				verSesionesVirtuales();

			}else{

				jQuery('.modal').modal({backdrop: 'static', keyboard: false});
	    		jQuery('.modal-title').html('Error de sesión.');
	    		jQuery('.modal-body').html('<div class="alert alert-warning">No fue posible registrar la actividad porque excede el número de actividades registrado en el campo <b>Número total de actividades</b>.</div>');
	    		jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');	

			}

		}else{
			localStorage.setItem("asignacionVirtual",true);

			var arregloSesionesVirtuales = new Array();
			arregloSesionesVirtuales.push(datos);

			localStorage.setItem('arregloSesionesVirtuales',JSON.stringify(arregloSesionesVirtuales));

			jQuery('.modal').modal({backdrop: 'static', keyboard: false});
    		jQuery('.modal-title').html('Sesión agregada exitosamente.');
    		jQuery('.modal-body').html('<div class="alert alert-success">Se ha registrado la información de la sesión correctamente.</div>');
    		jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

			jQuery("#alerta-sesion-virtual").html('');

			verSesionesVirtuales();
		}

	}

}

// Visualizar el listado de sesiones virtuales registradas
function verSesionesVirtuales() {
	if (localStorage.asignacionVirtual) {

		var arregloSesionesVirtuales = JSON.parse(localStorage.getItem("arregloSesionesVirtuales"));
		
		jQuery("#unidad-tematica-virtual").html('<div style="padding:10px;border:1px solid #CCCCCC;border-radius:5px;text-align:center;margin-top:10px;"><b>'+jQuery("#nombre_unidad_tematica").val().toUpperCase()+'</b></div>');
		jQuery("#listado-actividades").html('');

		var i = 1;
		
		jQuery.each(arregloSesionesVirtuales,function (key,value) {
			var color = (i%2==0) ? 'rgb(255,255,255)':'rgba(254,202,87,0.5)'
			jQuery("#listado-actividades").append('<div style="background:'+color+';padding:10px;border-radius:5px;margin-top:10px;margin-bottom:10px;">'+
					'<button type="button" class="btn btn-danger pull-right" title="Eliminar" onclick=removerSesionVirtual('+key+')><span class="fa fa-times"></span></button>'+
						'<p>'+
						'<b>Nombre de la actividad</b>: '+value[0]+'<br>'+
						'<b>Fecha de inicio</b>: '+value[1]+' - <b>Fecha de finalización</b>: '+value[2]+'<br>'+
						'<b>Proveedor de la formación</b>: '+value[3]+'<br>'+
						'<b>URL de la plataforma</b>: '+value[4]+'<br>'+
						'<b>Usuario SENA</b>: '+value[5]+' - <b>Clave SENA</b>: '+value[6]+'<br>'+
						'<b>Nombre del capacitador</b>: '+value[7]+'<br>'+
						'<b>Identificación</b>: '+value[8]+'-'+value[9]+'<br>'+
						'<b>Perfil del capacitador</b>: '+value[10]+'<br>'+
						'<b>Tipo de capacitador</b>: '+value[11]+'<br>'+
						'</p>'+
				'</div>');

			i++;
		})
	}
}

// Remover sesion virtual
function removerSesionVirtual(key) {
	console.log("Remover: "+key);
	jQuery('.modal').modal({backdrop: 'static', keyboard: false});
    jQuery('.modal-title').html('Eliminar sesión seleccionada.');
    jQuery('.modal-body').html('A continuación borrará de la lista la sesión seleccionada.<br><b>¿Desea continuar?</b>');
    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

    jQuery('.btn-accept').click(function () {
    	var arregloSesionesVirtuales = JSON.parse(localStorage.getItem('arregloSesionesVirtuales'));
    	arregloSesionesVirtuales.splice(key,1);
    	localStorage.setItem('arregloSesionesVirtuales',JSON.stringify(arregloSesionesVirtuales));

    	jQuery('.modal-body').html('Se ha eliminado la sesión selecionada de la lista.');
    	jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

    	jQuery('.btn-accept').click(function () {
    		verSesionesVirtuales();
    	});
    	
    });
}

// Registrar cronograma
function registrarCronograma() {

	var modalidad 	= jQuery("#modalidad");
	var metodologia = jQuery("#metodologia");

	if (modalidad.val() != '') {
		if (modalidad.val() == 'PRESENCIAL') {
			registrar_CronogramaPresencial();
		}else if (modalidad.val() == 'VIRTUAL') {
			registrar_CronogramaVirtual();
		}else if (modalidad.val() == 'COMBINADA') {
			registrar_CronogramaCombinado();
		}
	}else{
		jQuery('.modal').modal({backdrop: 'static', keyboard: false});
	    jQuery('.modal-title').html('Advertencia');
	    jQuery('.modal-body').html('No es posible realizar este paso, por favor seleccione una acción de formación y grupo de la lista para realizar el proceso de registro del cronograma.');
	    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Aceptar</button>');
	}
}

// Registrar cronograma presencial
function registrar_CronogramaPresencial() {
	var listadoAccionesFormacion 	= jQuery("#listadoAccionesFormacion");
	var listadoGrupos 				= jQuery("#listadoGrupos");
	var num_beneficiarios_grupo 	= jQuery("#num_beneficiarios_grupo");
	var evento_formacion 			= jQuery("#evento_formacion");
	var duracion_total_eformacion 	= jQuery("#duracion_total_eformacion");
	var modalidad 					= jQuery("#modalidad");
	var total_hrs_unidad_tematica 	= jQuery("#total_hrs_unidad_tematica");
	var num_unidad_tematica 		= jQuery("#num_unidad_tematica");
	var nombre_unidad_tematica 		= jQuery("#nombre_unidad_tematica");
	var fecha_inicio 				= jQuery("#fecha_inicio_unidad_tematica");
	var fecha_finalizacion 			= jQuery("#fecha_finalizacion_unidad_tematica");
	var metodologia_presencial 		= jQuery("#metodologia_presencial");
	var num_personas 				= jQuery("#num_personas");
	var sesiones_por_persona 		= jQuery("#sesiones_por_persona");
	var num_sesiones 				= jQuery("#num_sesiones");

	var campos = [listadoAccionesFormacion,listadoGrupos,num_beneficiarios_grupo,evento_formacion,duracion_total_eformacion,modalidad,total_hrs_unidad_tematica,num_unidad_tematica,nombre_unidad_tematica,fecha_inicio,fecha_finalizacion,metodologia_presencial,num_sesiones];

	var j = 13;

	jQuery("#alerta-registro-cronograma").html('');

	for (var i = 0; i < campos.length; i++) {
		if (campos[i].val() == '') {
			jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Diligencie el campo <b>'+campos[i].attr('name')+'</b> para continuar con el registro.</div>');
			campos[i].focus();
			break;
		}else{
			j--;
		}
	}

	if (j==0) {
		// Validacion de arreglo de sesiones presenciales
		if (localStorage.asignacionPresencial) {
			
			var arregloSesionesPresenciales = JSON.parse(localStorage.getItem('arregloSesionesPresenciales'));
			
			if (arregloSesionesPresenciales.length > 0) {

				if (arregloSesionesPresenciales.length == num_sesiones.val()) {
				
					jQuery('.modal').modal({backdrop: 'static', keyboard: false});
				    jQuery('.modal-title').html('Registrar cronograma');
				    jQuery('.modal-body').html('A continuación se realizará el registro del cronograma para el grupo de la acción de formación seleccionado.');
				    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

				    jQuery('.btn-accept').click(function () {
				    	jQuery(".loading").css('display','block');
				    	jQuery.ajax({
							type:"POST",
							url:"index.php?r=cronograma/registrar-cronograma-presencial",
							data:{
								listadoAccionesFormacion: 	listadoAccionesFormacion.val(),
								listadoGrupos: 				listadoGrupos.val(),
								num_beneficiarios_grupo: 	num_beneficiarios_grupo.val(),
								evento_formacion: 			evento_formacion.val(),
								duracion_total_eformacion: 	duracion_total_eformacion.val(),
								modalidad: 					modalidad.val(),
								total_hrs_unidad_tematica: 	total_hrs_unidad_tematica.val(),
								num_unidad_tematica: 		num_unidad_tematica.val(),
								nombre_unidad_tematica: 	nombre_unidad_tematica.val(),
								fecha_inicio: 				fecha_inicio.val(),
								fecha_finalizacion:  		fecha_finalizacion.val(),
								metodologia_presencial: 	metodologia_presencial.val(),
								num_personas: 				num_personas.val(),
								sesiones_por_persona: 		sesiones_por_persona.val(),
								num_sesiones: 				num_sesiones.val(),
								arregloSesiones: 			arregloSesionesPresenciales
							},
							success:function (response) {
								console.log(response);
								jQuery(".loading").css('display','none');
								var jsonResponse = JSON.parse(response);

								if (jsonResponse.status == 'success') {
									// Se registro el cronograma correctamente
									jQuery('.modal-title').html('Registro exitoso');
				    				jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
				    				jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

				    				jQuery('.btn-accept').click(function () {

				    					listadoAccionesFormacion.val('');
										listadoGrupos.val('');
										num_beneficiarios_grupo.val('');
										evento_formacion.val('');
										duracion_total_eformacion.val('');
										modalidad.val('');
										total_hrs_unidad_tematica.val('');
										num_unidad_tematica.val('');
										nombre_unidad_tematica.val('');
										fecha_inicio.val('');
										fecha_finalizacion.val('');
										metodologia_presencial.val('');
										num_personas.val('');
										sesiones_por_persona.val('');
										num_sesiones.val('');

				    					localStorage.setItem('arregloSesionesPresenciales',JSON.stringify([]));

				    					verSesionesPresenciales();

				    					location.reload();
				    				});

								}else{
									jQuery('.modal-title').html("Registrar cronograma");
								    jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
								    jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-cancel" data-dismiss="modal">Aceptar</button>');
								}

							}
						});
				    });
				}else{
					//jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Ingrese el número de sesiones de acuerdo a lo diligenciado en el campo <b>Número total de sesiones</b></div>');
					jQuery('.modal').modal({backdrop: 'static', keyboard: false});
				    jQuery('.modal-title').html('Advertencia');
				    jQuery('.modal-body').html('<div class="alert alert-warning">Ingrese el número de sesiones de acuerdo a lo diligenciado en el campo <b>Número total de sesiones</b></div>');
				    jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
				}
			}else{
				jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Debe realizar el registro de las sesiones presenciales del cronograma a registrar.</div>');
			}
		}else{
			jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Debe realizar el registro de las sesiones presenciales del cronograma a registrar.</div>');
		}
	}

}

// Registrar cronograma virtual
function registrar_CronogramaVirtual() {
	
	var listadoAccionesFormacion 	= jQuery("#listadoAccionesFormacion");
	var listadoGrupos 				= jQuery("#listadoGrupos");
	var num_beneficiarios_grupo 	= jQuery("#num_beneficiarios_grupo");
	var evento_formacion 			= jQuery("#evento_formacion");
	var duracion_total_eformacion 	= jQuery("#duracion_total_eformacion");
	var modalidad 					= jQuery("#modalidad");
	var total_hrs_unidad_tematica 	= jQuery("#total_hrs_unidad_tematica");
	var num_unidad_tematica 		= jQuery("#num_unidad_tematica");
	var nombre_unidad_tematica 		= jQuery("#nombre_unidad_tematica");
	var fecha_inicio 				= jQuery("#fecha_inicio_unidad_tematica");
	var fecha_finalizacion 			= jQuery("#fecha_finalizacion_unidad_tematica");
	var metodologia_virtual 		= jQuery("#metodologia_virtual");
	var num_actividades 			= jQuery("#num_actividades");

	var campos = [listadoAccionesFormacion,listadoGrupos,num_beneficiarios_grupo,evento_formacion,duracion_total_eformacion,modalidad,total_hrs_unidad_tematica,num_unidad_tematica,nombre_unidad_tematica,fecha_inicio,fecha_finalizacion,metodologia_virtual,num_actividades];

	var j = 13;

	jQuery("#alerta-registro-cronograma").html('');

	for (var i = 0; i < campos.length; i++) {
		if (campos[i].val() == '') {
			jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Diligencie el campo <b>'+campos[i].attr('name')+'</b> para continuar con el registro.</div>');
			campos[i].focus();
			break;
		}else{
			j--
		}
	}

	if (j==0) {
		// Validacion de arreglo de sesiones presenciales
		if (localStorage.asignacionVirtual) {
			var arregloSesionesVirtuales = JSON.parse(localStorage.getItem('arregloSesionesVirtuales'));
			if (arregloSesionesVirtuales.length > 0) {

				if (arregloSesionesVirtuales.length == num_actividades.val()) {
				
					jQuery('.modal').modal({backdrop: 'static', keyboard: false});
				    jQuery('.modal-title').html('Registrar cronograma');
				    jQuery('.modal-body').html('A continuación se realizará el registro del cronograma para el grupo de la acción de formación seleccionado.');
				    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

				    jQuery('.btn-accept').click(function () {
				    	jQuery(".loading").css('display','block');
				    	jQuery.ajax({
							type:"POST",
							url:"index.php?r=cronograma/registrar-cronograma-virtual",
							data:{
								listadoAccionesFormacion: 	listadoAccionesFormacion.val(),
								listadoGrupos: 				listadoGrupos.val(),
								num_beneficiarios_grupo: 	num_beneficiarios_grupo.val(),
								evento_formacion: 			evento_formacion.val(),
								duracion_total_eformacion: 	duracion_total_eformacion.val(),
								modalidad: 					modalidad.val(),
								total_hrs_unidad_tematica: 	total_hrs_unidad_tematica.val(),
								num_unidad_tematica: 		num_unidad_tematica.val(),
								nombre_unidad_tematica: 	nombre_unidad_tematica.val(),
								fecha_inicio: 				fecha_inicio.val(),
								fecha_finalizacion: 		fecha_finalizacion.val(),
								metodologia_virtual: 		metodologia_virtual.val(),
								num_actividades: 			num_actividades.val(),
								arregloSesiones: 			arregloSesionesVirtuales
							},
							success:function (response) {
								console.log(response);
								jQuery(".loading").css('display','none');
								var jsonResponse = JSON.parse(response);

								if (jsonResponse.status == 'success') {
									// Se registro el cronograma correctamente
									jQuery('.modal-title').html('Registro exitoso');
				    				jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
				    				jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

				    				jQuery('.btn-accept').click(function () {

				    					listadoAccionesFormacion.val('');
										listadoGrupos.val('');
										num_beneficiarios_grupo.val('');
										evento_formacion.val('');
										duracion_total_eformacion.val('');
										modalidad.val('');
										total_hrs_unidad_tematica.val('');
										num_unidad_tematica.val('');
										nombre_unidad_tematica.val('');
										fecha_inicio.val();
										fecha_finalizacion.val();
										metodologia_virtual.val('');
										num_actividades.val('');

				    					localStorage.setItem('arregloSesionesVirtuales',JSON.stringify([]));
				    					verSesionesVirtuales();

				    					location.reload();
				    				});

								}else{
									jQuery('.modal-title').html("Registrar cronograma");
								    jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
								    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Aceptar</button>');
								}

							}
						});
				    });
				}else{
					jQuery('.modal').modal({backdrop: 'static', keyboard: false});
				    jQuery('.modal-title').html('Advertencia');
				    jQuery('.modal-body').html('<div class="alert alert-warning">Ingrese el número de actividades de acuerdo a lo diligenciado en el campo <b>Número total de actividades</b></div>');
				    jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
				}
			}else{
				jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Debe realizar el registro de las sesiones virtuales del cronograma a registrar.</div>');
			}
		}else{
			jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Debe realizar el registro de las sesiones virtuales del cronograma a registrar.</div>');
		}
	}

}

// Registrar cronograma combinado
function registrar_CronogramaCombinado() {
	var listadoAccionesFormacion	= jQuery("#listadoAccionesFormacion");
	var listadoGrupos 				= jQuery("#listadoGrupos");
	var num_beneficiarios_grupo 	= jQuery("#num_beneficiarios_grupo");
	var duracion_total_eformacion 	= jQuery("#duracion_total_eformacion");
	var total_hrs_unidad_tematica 	= jQuery("#total_hrs_unidad_tematica");
	var num_unidad_tematica 		= jQuery("#num_unidad_tematica");
	var nombre_unidad_tematica 		= jQuery("#nombre_unidad_tematica");
	var fecha_inicio 				= jQuery("#fecha_inicio_unidad_tematica");
	var fecha_finalizacion 			= jQuery("#fecha_finalizacion_unidad_tematica");
	var metodologia_presencial 		= jQuery("#metodologia_presencial");
	var metodologia_virtual 		= jQuery("#metodologia_virtual");
	var num_sesiones 				= jQuery("#num_sesiones");
	var num_actividades 			= jQuery("#num_actividades");

	var campos = [listadoAccionesFormacion,listadoGrupos,num_beneficiarios_grupo,duracion_total_eformacion,total_hrs_unidad_tematica,num_unidad_tematica,nombre_unidad_tematica,fecha_inicio,fecha_finalizacion,metodologia_presencial,metodologia_virtual,num_sesiones,num_actividades];

	var j = 13;

	jQuery("#alerta-registro-cronograma").html('');

	for (var i = 0; i < campos.length; i++) {
		if (campos[i].val() == '') {
			jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Diligencie el campo <b>'+campos[i].attr('name')+'</b> para continuar con el registro.</div>');
			campos[i].focus();
			break;
		}else{
			j--;
		}
	}

	if (j==0) {
		// Validamos el tamaño del arreglo de las sesiones presenciales
		// Validacion de arreglo de sesiones presenciales
		if (localStorage.asignacionPresencial) {
			
			var arregloSesionesPresenciales = JSON.parse(localStorage.getItem('arregloSesionesPresenciales'));
			
			if (arregloSesionesPresenciales.length > 0) {

				if (arregloSesionesPresenciales.length == num_sesiones.val()) {

					if (localStorage.asignacionVirtual) {

						var arregloSesionesVirtuales = JSON.parse(localStorage.getItem('arregloSesionesVirtuales'));

						if (arregloSesionesVirtuales.length > 0) {

							if (arregloSesionesVirtuales.length == num_actividades.val()) {

								jQuery('.modal').modal({backdrop: 'static', keyboard: false});
							    jQuery('.modal-title').html('Registrar cronograma');
							    jQuery('.modal-body').html('A continuación se realizará el registro del cronograma para el grupo de la acción de formación seleccionado.');
							    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

							    jQuery('.btn-accept').click(function () {
							    	jQuery(".loading").css('display','block');
							    	jQuery.ajax({
										type:"POST",
										url:"index.php?r=cronograma/registrar-cronograma-combinado",
										data:{
											listadoAccionesFormacion: 	listadoAccionesFormacion.val(),
											listadoGrupos: 				listadoGrupos.val(),
											num_beneficiarios_grupo: 	num_beneficiarios_grupo.val(),
											duracion_total_eformacion: 	duracion_total_eformacion.val(),
											total_hrs_unidad_tematica: 	total_hrs_unidad_tematica.val(),
											num_unidad_tematica: 		num_unidad_tematica.val(),
											nombre_unidad_tematica: 	nombre_unidad_tematica.val(),
											fecha_inicio: 				fecha_inicio.val(),
											fecha_finalizacion:  		fecha_finalizacion.val(),
											metodologia_presencial: 	metodologia_presencial.val(),
											metodologia_virtual: 		metodologia_virtual.val(),
											num_sesiones: 				num_sesiones.val(),
											num_actividades: 			num_actividades.val(),
											arregloSesionesPresenciales:arregloSesionesPresenciales,
											arregloSesionesVirtuales: 	arregloSesionesVirtuales
										},
										success:function (response) {
											console.log(response);
											jQuery(".loading").css('display','none');
											var jsonResponse = JSON.parse(response);

											if (jsonResponse.status == 'success') {
												// Se registro el cronograma correctamente
												jQuery('.modal-title').html('Registro exitoso');
							    				jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
							    				jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');

							    				jQuery('.btn-accept').click(function () {

							    					listadoAccionesFormacion.val('');
													listadoGrupos.val('');
													num_beneficiarios_grupo.val('');
													duracion_total_eformacion.val('');
													total_hrs_unidad_tematica.val('');
													num_unidad_tematica.val('');
													nombre_unidad_tematica.val('');
													fecha_inicio.val('');
													fecha_finalizacion.val('');
													metodologia_presencial.val('');
													metodologia_virtual.val('');
													num_sesiones.val('');
													num_actividades.val('');

							    					localStorage.setItem('arregloSesionesPresenciales',JSON.stringify([]));
							    					localStorage.setItem('arregloSesionesVirtuales',JSON.stringify([]));

							    					verSesionesPresenciales();
							    					verSesionesVirtuales();

							    					location.reload();
							    				});

											}else{
												jQuery('.modal-title').html("Registrar cronograma");
											    jQuery('.modal-body').html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');
											    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Aceptar</button>');
											}

										}
									});
							    });

							}else{
								jQuery('.modal').modal({backdrop: 'static', keyboard: false});
							    jQuery('.modal-title').html('Advertencia');
							    jQuery('.modal-body').html('<div class="alert alert-warning">Ingrese el número de actividades de acuerdo a lo diligenciado en el campo <b>Número total de actividades</b></div>');
							    jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
							}
						}else{
							jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Debe realizar el registro de las sesiones virtuales del cronograma a registrar.</div>');
						}
					}else{
						jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Debe realizar el registro de las sesiones virtuales del cronograma a registrar.</div>');
					}

				}else{
					
					jQuery('.modal').modal({backdrop: 'static', keyboard: false});
				    jQuery('.modal-title').html('Advertencia');
				    jQuery('.modal-body').html('<div class="alert alert-warning">Ingrese el número de sesiones de acuerdo a lo diligenciado en el campo <b>Número total de sesiones</b></div>');
				    jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
				}
			}else{
				jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Debe realizar el registro de las sesiones presenciales del cronograma a registrar.</div>');
			}
		}else{
			jQuery("#alerta-registro-cronograma").html('<div class="alert alert-warning">Debe realizar el registro de las sesiones presenciales del cronograma a registrar.</div>');
		}
	}

}