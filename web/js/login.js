function signup() {
  	jQuery('.modal').modal('toggle');
  	jQuery('.modal-title').html('Formulario de registro');
  	jQuery('.modal-body').load('')
}

function changePassword() {
	jQuery('.modal').modal({backdrop: 'static', keyboard: false});
  	jQuery('.modal-title').html('Cambio de contraseña');
  	jQuery('.modal-body').html('<form action="site/Changepassword">' +
  		'<p>Se enviará un correo electrónico asociado a su cuenta con el enlace para realizar el cambio de contraseña.Por favor ingrese su usuario a continuación:</p>' +
  		'<label>Usuario</label><br>' +
  		'<input type="text" name="usuario" id="usuario" class="form-control">' +
  		'<div class="alert alert-warning hidden" id="alerta-pop"></div>' +
  	'</form>');
  	jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

	jQuery('.btn-accept').click(function () {
		var usuario = jQuery('#usuario').val();
		//console.log('Validation -> ' + validateEmail(usuario));
		if (usuario !== '') {

			if (validateEmail(usuario)) {
			
				jQuery.ajax({
					type: 	'POST',
					url: 	'index.php?r=generalidades%2Fchangepassword',
					data: {
						usuario: usuario
					},
					success:function (response) {
						console.log(response);
					}
				});

			}else {
				jQuery('#alerta-pop').removeClass('hidden');
				jQuery('#alerta-pop').html('El Usuario ingresado no cumple con una estructura de correo electrónico válida');	
			}

		}else {
			jQuery('#alerta-pop').removeClass('hidden');
			jQuery('#alerta-pop').html('El campo "Usuario" no puede estar vacío.');
		}

	})  	
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}