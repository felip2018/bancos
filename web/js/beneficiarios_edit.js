// Buscar información del beneficiario
function buscar_info_beneficiario() {

	jQuery("#alerta-busqueda").html('');

	var tipo_identi = jQuery("#tipo_identi");
	var num_identi = jQuery("#num_identi");

	if (tipo_identi.val() != '') {
		if (num_identi.val() != '') {
			jQuery.ajax({
				type:"POST",
				url:"index.php?r=beneficiarios/buscar-informacion",
				data:{
					tipo_doc: tipo_identi.val(),
					num_doc:  num_identi.val()
				},
				success:function (response) {
				 	//console.log(response);
				 	var jsonResponse = JSON.parse(response);
				 	//console.log(jsonResponse);
				 	if (jsonResponse.status == "success") {

				 		// Datos del beneficiario
				 		jQuery("#id_proyecto_beneficiario").val(jsonResponse.informacion['id_proyecto_beneficiario']);
				 		jQuery("#id_beneficiario").val(jsonResponse.informacion['id_beneficiario']);

				 		jQuery("#beneficiarios-tipo_doc").val(jsonResponse.informacion['tipo_doc']);
				 		jQuery("#beneficiarios-num_doc").val(jsonResponse.informacion['num_doc']);
				 		jQuery("#beneficiarios-nombres").val(jsonResponse.informacion['nombres']);
				 		jQuery("#beneficiarios-apellido_1").val(jsonResponse.informacion['apellido_1']);
				 		jQuery("#beneficiarios-apellido_2").val(jsonResponse.informacion['apellido_2']);
				 		jQuery("#beneficiarios-genero").val(jsonResponse.informacion['genero']);
				 		jQuery("#beneficiarios-estrato").val(jsonResponse.informacion['estrato']);
				 		jQuery("#beneficiarios-fecha_nacimiento").val(jsonResponse.informacion['fecha_nacimiento']);
				 		jQuery("#beneficiarios-celular").val(jsonResponse.informacion['celular']);
				 		jQuery("#beneficiarios-id_departamento").val(jsonResponse.informacion['id_departamento']);
				 		jQuery("#beneficiarios-id_ciudad").val(jsonResponse.informacion['id_ciudad']);
				 		jQuery("#beneficiarios-barrio_vereda").val(jsonResponse.informacion['barrio_vereda']);
				 		jQuery("#beneficiarios-direccion").val(jsonResponse.informacion['direccion']);

				 		// Datos de postulacion
				 		jQuery("#beneficiarios-edad").val(jsonResponse.informacion['edad']);
				 		jQuery("#beneficiarios-antiguedad").val(jsonResponse.informacion['antiguedad']);
				 		jQuery("#beneficiarios-empresa_labora").val(jsonResponse.informacion['empresa_labora']);
				 		jQuery("#beneficiarios-tamanio_empresa_labora").val(jsonResponse.informacion['tamanio_empresa']);
				 		jQuery("#beneficiarios-caracterizacion").val(jsonResponse.informacion['caracterizacion']);
				 		jQuery("#beneficiarios-nivel_ocupacional").val(jsonResponse.informacion['nivel_ocupacional']);
				 		jQuery("#beneficiarios-transferencia").val(jsonResponse.informacion['transferencia']);
				 		jQuery("#beneficiarios-perfil_transferencia").val(jsonResponse.informacion['perfil_transferencia']);

				 	}else{
						jQuery("#alerta-busqueda").html('<div class="alert '+jsonResponse.alert+'">'+jsonResponse.message+'</div>');

						jQuery("#id_proyecto_beneficiario").val('');
						jQuery("#id_beneficiario").val('');

						jQuery("#beneficiarios-tipo_doc").val('');
						jQuery("#beneficiarios-num_doc").val('');
						jQuery("#beneficiarios-nombres").val('');
						jQuery("#beneficiarios-apellido_1").val('');
						jQuery("#beneficiarios-apellido_2").val('');
						jQuery("#beneficiarios-genero").val('');
						jQuery("#beneficiarios-estrato").val('');
						jQuery("#beneficiarios-fecha_nacimiento").val('');
						jQuery("#beneficiarios-celular").val('');
						jQuery("#beneficiarios-id_departamento").val('');
						jQuery("#beneficiarios-id_ciudad").val('');
						jQuery("#beneficiarios-barrio_vereda").val('');
						jQuery("#beneficiarios-direccion").val('');

						jQuery("#beneficiarios-edad").val('');
				 		jQuery("#beneficiarios-antiguedad").val('');
				 		jQuery("#beneficiarios-empresa_labora").val('');
				 		jQuery("#beneficiarios-tamanio_empresa_labora").val('');
				 		jQuery("#beneficiarios-caracterizacion").val('');
				 		jQuery("#beneficiarios-nivel_ocupacional").val('');
				 		jQuery("#beneficiarios-transferencia").val('');
				 		jQuery("#beneficiarios-perfil_transferencia").val('');

				 	}
				}
			})
		}else{
			jQuery("#alerta-busqueda").html('<div class="alert alert-warning">Ingrese el <b>Número de identificación</b> del beneficiario que desea buscar.</div>');
			num_identi.focus();
		}
	}else{
		jQuery("#alerta-busqueda").html('<div class="alert alert-warning">Seleccione el <b>Tipo de identificación</b> del beneficiario que desea buscar.</div>');
		tipo_identi.focus();
	}

}

function actualizar_beneficiario() {

	jQuery("#alerta-busqueda").html('');

	var id_proyecto_beneficiario= jQuery("#id_proyecto_beneficiario");
	var id_beneficiario 		= jQuery("#id_beneficiario");

	var tipo_doc 				= jQuery("#beneficiarios-tipo_doc");
	var num_doc 				= jQuery("#beneficiarios-num_doc");
	var nombres 				= jQuery("#beneficiarios-nombres");
	var apellido_1 				= jQuery("#beneficiarios-apellido_1");
	var apellido_2 				= jQuery("#beneficiarios-apellido_2");
	var genero 					= jQuery("#beneficiarios-genero");
	var estrato 				= jQuery("#beneficiarios-estrato");
	var fecha_nacimiento 		= jQuery("#beneficiarios-fecha_nacimiento");
	var celular 				= jQuery("#beneficiarios-celular");
	var id_departamento 		= jQuery("#beneficiarios-id_departamento");
	var id_ciudad 				= jQuery("#beneficiarios-id_ciudad");
	var barrio_vereda 			= jQuery("#beneficiarios-barrio_vereda");
	var direccion 				= jQuery("#beneficiarios-direccion");
	var edad 					= jQuery("#beneficiarios-edad");
	var antiguedad 				= jQuery("#beneficiarios-antiguedad");
	var empresa_labora 			= jQuery("#beneficiarios-empresa_labora");
	var tamanio_empresa 		= jQuery("#beneficiarios-tamanio_empresa_labora");
	var caracterizacion 		= jQuery("#beneficiarios-caracterizacion");
	var nivel_ocupacional 		= jQuery("#beneficiarios-nivel_ocupacional");
	var transferencia 			= jQuery("#beneficiarios-transferencia");
	var perfil_transferencia 	= jQuery("#beneficiarios-perfil_transferencia");

	var campos = [tipo_doc,num_doc,nombres,apellido_1,genero,estrato,fecha_nacimiento,celular,id_ciudad,barrio_vereda,direccion,edad,antiguedad,empresa_labora,tamanio_empresa,caracterizacion,nivel_ocupacional,transferencia,perfil_transferencia];

	var j = 19;

	for (var i = 0; i < campos.length; i++) {
		if(campos[i].val() == ""){
			jQuery("#alerta-busqueda").html('<div class="alert alert-warning">Diligencie el campo <b>'+campos[i].attr('name')+'</b> del beneficiario.</div>');
			campos[i].focus();
			break;
		}else{
			j--;
		}
	}

	if (j == 0) {
		jQuery('.modal').modal({backdrop: 'static', keyboard: false});
	    jQuery('.modal-title').html('Actualizar beneficiario.');
	    jQuery('.modal-body').html('A continuación realizará la actualización del beneficiario en el sistema.<br><b>¿Desea continuar?</b>');
	    jQuery('.modal-footer').html('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancelar</button><button type="button" class="btn btn-primary btn-accept">Aceptar</button>');

	    jQuery('.btn-accept').click(function () {
	    	jQuery(".loading").css("display","block");

	    	jQuery.ajax({
	    		type:"POST",
	    		url:"index.php?r=beneficiarios/actualizar-informacion",
	    		data:{
	    			id_proyecto_beneficiario: id_proyecto_beneficiario.val(),
	    			id_beneficiario: 		  id_beneficiario.val(),
					tipo_doc: 				  tipo_doc.val(),
					num_doc: 				  num_doc.val(),
					nombres: 				  nombres.val(),
					apellido_1: 			  apellido_1.val(),
					apellido_2: 			  apellido_2.val(),
					genero: 				  genero.val(),
					estrato: 				  estrato.val(),
					fecha_nacimiento: 		  fecha_nacimiento.val(),
					celular: 				  celular.val(),
					id_departamento: 		  id_departamento.val(),
					id_ciudad: 				  id_ciudad.val(),
					barrio_vereda: 			  barrio_vereda.val(),
					direccion: 				  direccion.val(),
					edad: 					  edad.val(),
					antiguedad: 			  antiguedad.val(),
					empresa_labora: 		  empresa_labora.val(),
					tamanio_empresa: 		  tamanio_empresa.val(),
					caracterizacion: 		  caracterizacion.val(),
					nivel_ocupacional: 		  nivel_ocupacional.val(),
					transferencia: 			  transferencia.val(),
					perfil_transferencia: 	  perfil_transferencia.val()
	    		},
	    		success:function (response) {
	    			jQuery(".loading").css("display","none");
	    			
	    			console.log(response);
	    			var jsonResponse = JSON.parse(response);

	    			jQuery('.modal-body').html('<div class="alert alert-info"><b>Información del beneficiario: </b><br><p>'+jsonResponse.beneficiarios+'</p><br><b>Datos de postulación: </b><br><p>'+jsonResponse.beneficiarios_grupo+'</p></div>');
	    			jQuery('.modal-footer').html('<button type="button" class="btn btn-primary btn-accept" data-dismiss="modal">Aceptar</button>');
	    		}
	    	})
	    });
	}
}