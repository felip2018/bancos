<?php

namespace app\models;

use Yii;
use yii\base\Model;

use app\models\Mysql;

class Referencias extends Model
{
    // Varibles para registro de Referencias
    public $nombre;
    public $empresa;
    public $cargo;
    public $telefono;
    // Reglas de validacion para el formulario de registro de Referencias
    public function rules()
    {
        return [
            // username and password are both required
            [
              [
                'nombre',
                'empresa',
                'cargo',
                'telefono',
                'fecha_registro',
                'estado_registro',
              ], 'required', 'message' => 'Este campo no puede estar vacío.'
            ],
            [
              [
                'nombre',
                'empresa',
                'cargo',
              ],'string', 'message' => 'Este campo es de tipo texto'
            ],
            [
              ['telefono'],'number', 'message' => 'Este campo es de tipo numerico'
            ],
            [
              [
                'fecha_registro',

              ],'number', 'message' => 'Este campo es de tipo numérico'
            ],
        ];
    }

    public function registrarCapacitador()
    {
        $db = Mysql::connection();

        // Validaciones
        // 1. Que el tipo y numero de documento no existan
        $sql_3 = 'SELECT COUNT(*) cant FROM capacitadores WHERE tipo_doc = "'.$this->tipo_doc.'" AND num_doc = "'.$this->num_doc.'"';

        $validacion_1 = $db->createCommand($sql_3)->queryOne();

        if ($validacion_1['cant'] == 0) {
          // El tipo y numero de documento no estan registrados

          // Validaciones
          // 2. Que el nombre completo del capacitador no este registrado
          $nombre_completo = $this->nombres.' '.$this->primer_apellido.' '.$this->segundo_apellido;
          $sql_4 = "SELECT COUNT(*) cant FROM capacitadores WHERE CONCAT(nombres,' ',primer_apellido,' ',segundo_apellido) = '".$nombre_completo."'";

          $validacion_2 = $db->createCommand($sql_4)->queryOne();

          if ($validacion_2['cant'] == 0) {
              // nombre de capacitador valido
              $sql_1 = 'SELECT MAX(id_capacitador) id_capacitador FROM capacitadores';

              $capacitador = $db->createCommand($sql_1)->queryOne();

              $id_capacitador = ($capacitador['id_capacitador'] == null) ? 1 : $capacitador['id_capacitador'] + 1;

              $sql_2 = 'INSERT INTO capacitadores(id_capacitador, tipo_doc, num_doc, nombres, primer_apellido, segundo_apellido, correo, experiencia_tematica, experiencia_docente, fecha_registro, estado_registro) VALUES ("'.$id_capacitador.'","' . $this->tipo_doc . '","'.$this->num_doc.'","' . $this->nombres . '","' . $this->primer_apellido . '","' . $this->segundo_apellido . '","' . $this->correo . '","' . $this->experiencia_tematica . '","' . $this->experiencia_docente . '",NOW(),"ACTIVO")';

              $insert = $db->createCommand($sql_2)->execute();

              if($insert){
                  // Se registro correctamente
                  $respuesta = json_encode([
                    'status'  => 'Registro exitoso',
                    'alert'   => 'alert-success',
                    'message' => 'Se ha registrado el capacitador correctamente en el sistema.'
                  ]);

                  $this->tipo_doc = '';
                  $this->num_doc = '';
                  $this->nombres = '';
                  $this->primer_apellido = '';
                  $this->segundo_apellido = '';
                  $this->correo = '';
                  $this->experiencia_tematica = '';
                  $this->experiencia_docente = '';

              }else{
                  // Error en el insert
                  $respuesta = json_encode([
                    'status'  => 'Error en registro',
                    'alert'   => 'alert-danger',
                    'message' => 'Ha ocurrido un error al registrar el capacitador.'
                  ]);
              }

          }else {
              // Error por validacion 2
              $respuesta = json_encode([
                'status'  => 'Error de validacion',
                'alert'   => 'alert-danger',
                'message' => 'El nombre del capacitador ya esta registrado en el sistema.'
              ]);
          }

        }else{
            // Error por validacion 1
            $respuesta = json_encode([
              'status'  => 'Error de validacion',
              'alert'   => 'alert-danger',
              'message' => 'El tipo y numero de Identificación ya estan registrados en el sistema.'
            ]);
        }


        return $respuesta;
    }
}
