<?php
namespace app\models;
use Yii;
use yii\base\Model;
use app\models\Mysql;
use app\models\Users;
use app\models\Capacitadores;

class Capacitadores extends Model
{
    // Varibles para registro de Capacitadores
    public $id_proyecto;
    public $tipo_doc;
    public $num_doc;
    public $nombres;
    public $primer_apellido;
    public $segundo_apellido;
    public $correo;

    // Reglas de validacion para el formulario de registro de Capacitadores
    public function rules()
    {
        return [
            // username and password are both required
            [
              [
                'tipo_doc',
                'num_doc',
                'nombres',
                'primer_apellido',
                'segundo_apellido',
                'correo',
                'fecha_registro',
                'estado_registro'
              ], 'required', 'message' => 'Este campo no puede estar vacío.'
            ],
            [
              [
                'tipo_doc',
                'nombres',
                'primer_apellido',
                'segundo_apellido'
              ],'string', 'message' => 'Este campo es de tipo texto'
            ],
            [
              [
                'correo'
              ],'email', 'message' => 'El formato no corresponde con un email valido.'
            ],
            [
              ['num_doc'],'number', 'message' => 'Este campo es de tipo numérico'
            ],
            [
              [
                'fecha_registro',

              ],'number', 'message' => 'Este campo es de tipo numérico'
            ],
        ];
    }

    // Buscar datos del capacitador por tipo y numero de documento
    public function buscarCapacitador($tipo_doc,$num_doc)
    {
        $db = Mysql::connection();

        $sql = "SELECT * FROM capacitadores WHERE tipo_doc = '".$tipo_doc."'AND num_doc = '".$num_doc."'";

        $consulta = $db->createCommand($sql)->queryOne();

        if (!empty($consulta)) {
            $response = json_encode([
                'status' => 'success',
                'datos' => $consulta
            ]);
        }else{
            $response = json_encode([
                'status' => 'vacio',
                'message' => 'No se han encontrado resultados.'
            ]);
        }

        return $response;
    }
    
    // Ver listado de capacitadores registrados dentro del proyecto en sesion
    public function listaCapacitadores($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                    capacitadores.id_capacitador,
                    CONCAT(capacitadores.tipo_doc,'-',capacitadores.num_doc) AS identificacion,
                    CONCAT(capacitadores.nombres,' ',capacitadores.primer_apellido,' ',capacitadores.segundo_apellido) AS nombre,
                    capacitadores.fecha_registro,
                    capacitadores.estado_registro
                    FROM capacitadores
                    /*INNER JOIN capacitadores_accion_formacion caf ON caf.id_capacitador = capacitadores.id_capacitador
                    INNER JOIN acciones_formacion ON acciones_formacion.id_accion_formacion = caf.id_accion_formacion 
                    WHERE acciones_formacion.id_proyecto = '". $id_proyecto ."'*/
                    GROUP BY id_capacitador";

        $consulta = $db->createCommand($sql)->queryAll();
        return $consulta;
    }

    // Registrar capacitador en el sistema
    public function registrarCapacitador($POST,$id_proyecto)
    {
        $db = Mysql::connection();

        $sql1 = "SELECT     COUNT(*) cant
                    FROM    capacitadores
                    WHERE   tipo_doc = '". $POST['tipo_doc'] ."'
                    AND     num_doc  = '". $POST['num_doc'] ."'";

        $consulta1 = $db->createCommand($sql1)->queryOne();

        if ($consulta1['cant'] == 0) {
            
            $nombre = $POST['nombres'].' '.$POST['primer_apellido'].' '.$POST['segundo_apellido'];
            
            $sql2 = "SELECT COUNT(*) cant
                        FROM    capacitadores
                        WHERE   CONCAT(nombres,' ',primer_apellido,' ',segundo_apellido) = '". $nombre ."'";

            $consulta2 = $db->createCommand($sql2)->queryOne();

            if ($consulta2['cant'] == 0) {

            
                $id_capacitador = Generalidades::getConsecutivo("capacitadores","id_capacitador");

                $sql3 = "INSERT INTO capacitadores(id_capacitador, tipo_doc, num_doc, nombres, primer_apellido, segundo_apellido, correo, fecha_vencimiento, fecha_registro, estado_registro) VALUES (".$id_capacitador.",'".$POST['tipo_doc']."','".$POST['num_doc']."','".strtoupper($POST['nombres'])."','".strtoupper($POST['primer_apellido'])."','".strtoupper($POST['segundo_apellido'])."','".strtoupper($POST['correo'])."',NOW(),NOW(),'ACTIVO')";

                $insert = $db->createCommand($sql3)->execute();

                if ($insert) {

                    // Cargar documentos requeridos del capacitador
                    Capacitadores::uploadCapacitadorFiles($id_capacitador);

                    $response = json_encode([
                        'status'        => 'success',
                        'message'       => 'El capacitador ha sido registrado correctamente en el sistema. Código: '.$id_capacitador,
                        'id_capacitador'   => $id_capacitador,
                        'alert'         => 'alert-success',
                    ]); 

                }else {
             
                    $response = json_encode([
                        'status'    => 'error',
                        'message'   => 'No fue posible registrar el capacitador:<br>'.$sql3,
                        'alert'     => 'alert-danger'
                    ]);        
                }

            }else{
          
                $response = json_encode([
                    'status'    => 'error',
                    'message'   => 'El capacitador "'.$nombre.'" ya esta registrado en el sistema con otro número de identificación',
                    'alert'     => 'alert-danger'
                ]);    
            }
              

        }else{
            
            $response = json_encode([
                'status'    => 'error',
                'message'   => 'El tipo y número de identificación ya estan en uso para un capacitador registrado.',
                'alert'     => 'alert-danger'
            ]);
        }
            
        

        return $response;

    }


    // Upload director files 
    public static function uploadCapacitadorFiles($id_capacitador)
    {
        $db = Mysql::connection();

        // Consultamos la informacion registrada del director 
        $sql = "SELECT * FROM capacitadores WHERE id_capacitador = '".$id_capacitador."'";
        $consulta = $db->createCommand($sql)->queryOne();

        // Consultamos los documentos requeridos por el banco

        $sql2 = "SELECT
                documentos_bancos.id_documento_banco,
                documentos_bancos.id_documento,
                documentos.nombre,
                documentos_bancos.obligatorio,
                documentos_bancos.tipo_banco,
                documentos_bancos.input_name,
                documentos_bancos.fecha_registro,
                documentos_bancos.estado_registro
                FROM documentos_bancos
                INNER JOIN documentos ON documentos.id_documento = documentos_bancos.id_documento
                WHERE documentos_bancos.tipo_banco = 'BANCO DE CAPACITADORES'
                AND documentos_bancos.estado_registro = 'ACTIVO'";

        $consulta2 = $db->createCommand($sql2)->queryAll();

        if (!empty($consulta2)) {
            foreach ($consulta2 as $key => $value) {
                
                if ($_FILES[$value['input_name']]['name'] != "") {

                    $mimeType = $_FILES[$value['input_name']]['type'];

                    if ($mimeType == "application/pdf") {
                    
                        $year = date('Y');
                        $directorio = "../web/convocatoria_".$year."/bancos/capacitadores/".$consulta['num_doc']."/";

                        if (!is_dir($directorio)) {
                            mkdir($directorio);
                        }

                        $mimeType = $_FILES[$value['input_name']]['type'];

                        //$target_file = $directorio."/".basename($_FILES[$value['input_name']]['name']);

                        $nombre_doc = $value['input_name'].".pdf";
                        
                        $target_file    = $directorio."/".$nombre_doc;

                        move_uploaded_file($_FILES[$value['input_name']]['tmp_name'], $target_file);

                        $id_capacitador_documento = Generalidades::getConsecutivo("capacitadores_documentos","id_capacitador_documento");

                        // Registrar documento en el sistema para el director de proyecto
                        $sql3 = "INSERT INTO capacitadores_documentos(id_capacitador_documento, id_documento_banco, id_capacitador, id_capacitador_juridico, id_usuario_sena, url, estado_doc, fecha_registro, estado_registro) VALUES (".$id_capacitador_documento.",".$value['id_documento_banco'].",".$id_capacitador.",0,0,'".$nombre_doc."','PENDIENTE',NOW(),'ACTIVO')";

                        $insertDoc = $db->createCommand($sql3)->execute();
                        //$response = json_encode(["sql_doc"=>$sql3]);
                    }

                }

            }
        }

        //return $response;

    }

    // Registrar experiencia del capacitador
    public function registrarExperiencia($POST)
    {
        $db = Mysql::connection();

      
        $sql = "SELECT 
                id_capacitador,
                tipo_doc,
                num_doc
                FROM capacitadores
                WHERE id_capacitador = '".$POST['id_capacitador']."'";

        $consulta = $db->createCommand($sql)->queryOne();

   
        if (isset($_FILES['soporte_experiencia']) && !empty($_FILES['soporte_experiencia'])) {

       
            $year = date('Y');
            $directorio = "../web/convocatoria_".$year."/bancos/capacitadores/".$consulta['num_doc']."/";

            $mimeType       = $_FILES['soporte_experiencia']['type'];

            if ($mimeType == "application/pdf") {

      
                if (!is_dir($directorio)) {
                    mkdir($directorio);
                }

                $id_capacitador_experiencia = Generalidades::getConsecutivo("capacitadores_experiencias","id_capacitador_experiencia");

                $nombre_doc = "Experiencia_id_".$id_capacitador_experiencia.".pdf";

                $ext = explode('/', $mimeType);

                $target_file    = $directorio."/".$nombre_doc;

            
                if (!file_exists($target_file)) {

                    if (move_uploaded_file($_FILES['soporte_experiencia']['tmp_name'], $target_file)) {

                        $sql2 = "INSERT INTO capacitadores_experiencias(id_capacitador_experiencia, id_capacitador, id_capacitador_juridico, id_usuario_sena, tipo, descripcion, url, estado_doc, fecha_registro, estado_registro) VALUES (".$id_capacitador_experiencia.",".$POST['id_capacitador'].",0,0,'".$POST['tipo']."','".strtoupper($POST['descripcion'])."','".$nombre_doc."','PENDIENTE',NOW(),'ACTIVO')";

                        $insert = $db->createCommand($sql2)->execute();

                        if ($insert) {

                            $response = json_encode([
                                'status' => 'success',
                                'alert' => 'alert-warning',
                                'message'=> 'La experiencia ha sido registrada correctamente.'
                            ]);

                        }else{
                            
                            $response = json_encode([
                                'status' => 'error',
                                'alert' => 'alert-warning',
                                'message'=> 'Ha ocurrido un error al registrar la experiencia.'
                            ]);

                        }
                    }else{
                        $response = json_encode([
                            'status' => 'error',
                            'alert' => 'alert-danger',
                            'message'=> 'No se ha podido subir el documento al servidor.'
                        ]);
                    }

                }else{
          
                    $response = json_encode([
                        'status' => 'error',
                        'alert' => 'alert-warning',
                        'message'=> 'El archivo ya se encuentra cargado al servidor.'
                    ]);    
                }
            }else{
        
                $response = json_encode([
                    'status' => 'error',
                    'alert' => 'alert-warning',
                    'message'=> 'Formato de documento inválido.'
                ]);
            }

        }else{
        
            $response = json_encode([
                'status' => 'vacio',
                'alert' => 'alert-warning',
                'message' => 'Ingrese el soporte de la experiencia.'
            ]);
        }

        return $response;

    }

    // Buscar experiencias registradas del capacitador
    public function buscarExperiencias($id_capacitador)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                capacitadores_experiencias.id_capacitador_experiencia,
                capacitadores_experiencias.id_capacitador,
                capacitadores.num_doc,
                capacitadores_experiencias.tipo,
                capacitadores_experiencias.descripcion,
                capacitadores_experiencias.url,
                capacitadores_experiencias.estado_doc,
                capacitadores_experiencias.fecha_registro,
                DATE_FORMAT(capacitadores_experiencias.fecha_registro,'%Y') AS year,
                capacitadores_experiencias.estado_registro
                FROM capacitadores_experiencias
                INNER JOIN capacitadores ON capacitadores.id_capacitador = capacitadores_experiencias.id_capacitador
                WHERE capacitadores_experiencias.id_capacitador = '".$id_capacitador."'";


        $consulta = $db->createCommand($sql)->queryAll();

        $response = json_encode([
            'status'        => 'success',
            'experiencias'  => $consulta
        ]);

        return $response;

    }

    // Buscar titulos registrados del capacitador
    public function buscarTitulos($id_capacitador)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                capacitadores_titulos.id_capacitador_titulo,
                capacitadores_titulos.id_capacitador,
                capacitadores.num_doc,
                capacitadores_titulos.tipo_titulo,
                capacitadores_titulos.descripcion,
                capacitadores_titulos.url,
                capacitadores_titulos.estado_doc,
                capacitadores_titulos.fecha_registro,
                DATE_FORMAT(capacitadores_titulos.fecha_registro,'%Y') AS year,
                capacitadores_titulos.estado_registro
                FROM capacitadores_titulos
                INNER JOIN capacitadores ON capacitadores.id_capacitador = capacitadores_titulos.id_capacitador
                WHERE capacitadores_titulos.id_capacitador = '".$id_capacitador."'";


        $consulta = $db->createCommand($sql)->queryAll();

        $response = json_encode([
            'status'        => 'success',
            'titulos'  => $consulta
        ]);

        return $response;

    }

    // Registrar titulo para el capacitador
    public function registrarTitulo($POST)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                id_capacitador,
                tipo_doc,
                num_doc
                FROM capacitadores
                WHERE id_capacitador = '".$POST['id_capacitador']."'";

        $consulta = $db->createCommand($sql)->queryOne();


        if (isset($_FILES['soporte_titulo']) && !empty($_FILES['soporte_titulo'])) {

            $year = date('Y');
            $directorio = "../web/convocatoria_".$year."/bancos/capacitadores/".$consulta['num_doc']."/";

            $mimeType       = $_FILES['soporte_titulo']['type'];

            if ($mimeType == "application/pdf") {

                if (!is_dir($directorio)) {
                    mkdir($directorio);
                }

                $id_capacitador_titulo = Generalidades::getConsecutivo("capacitadores_titulos","id_capacitador_titulo");

                $nombre_doc = "Titulo_id_".$id_capacitador_titulo.".pdf";

                
                $target_file    = $directorio."/".$nombre_doc;

               
                if (!file_exists($target_file)) {

                    if (move_uploaded_file($_FILES['soporte_titulo']['tmp_name'], $target_file)) {

                        

                        $sql2 = "INSERT INTO capacitadores_titulos(id_capacitador_titulo, id_capacitador, id_usuario_sena, tipo_titulo, descripcion, url, estado_doc, fecha_registro, estado_registro) VALUES (".$id_capacitador_titulo.",".$POST['id_capacitador'].",0,'".$POST['tipo_titulo']."','".strtoupper($POST['descripcion'])."','".$nombre_doc."','PENDIENTE',NOW(),'ACTIVO')";

                        $insert = $db->createCommand($sql2)->execute();

                        if ($insert) {

                            $response = json_encode([
                                'status' => 'success',
                                'alert' => 'alert-warning',
                                'message'=> 'El titulo ha sido registrado correctamente.'
                            ]);

                        }else{
                            
                            $response = json_encode([
                                'status' => 'error',
                                'alert' => 'alert-warning',
                                'message'=> 'Ha ocurrido un error al registrar el titulo.'
                            ]);

                        }
                    }else{
                        $response = json_encode([
                            'status' => 'error',
                            'alert' => 'alert-danger',
                            'message'=> 'No se ha podido subir el documento al servidor.'
                        ]);
                    }

                }else{
                   
                    $response = json_encode([
                        'status' => 'error',
                        'alert' => 'alert-warning',
                        'message'=> 'El archivo ya se encuentra cargado al servidor.'
                    ]);    
                }
            }else{
                
                $response = json_encode([
                    'status' => 'error',
                    'alert' => 'alert-warning',
                    'message'=> 'Formato de documento inválido.'
                ]);
            }

        }else{
          
            $response = json_encode([
                'status' => 'vacio',
                'alert' => 'alert-warning',
                'message' => 'Ingrese el soporte del titulo.'
            ]);
        }

        return $response;

    }

    // Buscar referencias registradas del capacitador
    public function buscarReferencias($id_capacitador)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                capacitadores_referencias.id_capacitador_referencia,
                capacitadores_referencias.id_capacitador,
                capacitadores.num_doc,
                capacitadores_referencias.nombre,
                capacitadores_referencias.empresa,
                capacitadores_referencias.cargo,
                capacitadores_referencias.telefono,
                capacitadores_referencias.url,
                capacitadores_referencias.estado_doc,
                capacitadores_referencias.fecha_registro,
                DATE_FORMAT(capacitadores_referencias.fecha_registro,'%Y') AS year,
                capacitadores_referencias.estado_registro
                FROM capacitadores_referencias
                INNER JOIN capacitadores ON capacitadores.id_capacitador = capacitadores_referencias.id_capacitador
                WHERE capacitadores_referencias.id_capacitador = '".$id_capacitador."'";


        $consulta = $db->createCommand($sql)->queryAll();

        $response = json_encode([
            'status'        => 'success',
            'referencias'  => $consulta
        ]);

        return $response;

    }

    // Registrar referencia del capacitador
    public function registrarReferencia($POST)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                id_capacitador,
                tipo_doc,
                num_doc
                FROM capacitadores
                WHERE id_capacitador = '".$POST['id_capacitador']."'";

        $consulta = $db->createCommand($sql)->queryOne();

   
        if (isset($_FILES['soporte_referencia']) && !empty($_FILES['soporte_referencia'])) {

          
            $year = date('Y');
            $directorio = "../web/convocatoria_".$year."/bancos/capacitadores/".$consulta['num_doc']."/";

            $mimeType       = $_FILES['soporte_referencia']['type'];

            if ($mimeType == "application/pdf") {

                if (!is_dir($directorio)) {
                    mkdir($directorio);
                }

                $id_capacitador_referencia = Generalidades::getConsecutivo("capacitadores_referencias","id_capacitador_referencia");

                $nombre_doc = "Referencia_id_".$id_capacitador_referencia.".pdf";

                $target_file    = $directorio."/".$nombre_doc;

           
                if (!file_exists($target_file)) {

                    if (move_uploaded_file($_FILES['soporte_referencia']['tmp_name'], $target_file)) {


                        $sql2 = "INSERT INTO capacitadores_referencias(id_capacitador_referencia, id_capacitador, id_usuario_sena, nombre, empresa, cargo, telefono, url, estado_doc, fecha_registro, estado_registro) VALUES (".$id_capacitador_referencia.",".$POST['id_capacitador'].",0,'".strtoupper($POST['nombre'])."','".strtoupper($POST['empresa'])."','".strtoupper($POST['cargo'])."','".$POST['telefono']."','".$nombre_doc."','PENDIENTE',NOW(),'ACTIVO')";

                        $insert = $db->createCommand($sql2)->execute();

                        if ($insert) {

                            $response = json_encode([
                                'status' => 'success',
                                'alert' => 'alert-warning',
                                'message'=> 'La referencia ha sido registrada correctamente.'
                            ]);

                        }else{
                            
                            $response = json_encode([
                                'status' => 'error',
                                'alert' => 'alert-warning',
                                'message'=> 'Ha ocurrido un error al registrar la referencia.'
                            ]);

                        }
                    }else{
                        $response = json_encode([
                            'status' => 'error',
                            'alert' => 'alert-danger',
                            'message'=> 'No se ha podido subir el documento al servidor.'
                        ]);
                    }

                }else{
                 
                    $response = json_encode([
                        'status' => 'error',
                        'alert' => 'alert-warning',
                        'message'=> 'El archivo ya se encuentra cargado al servidor.'
                    ]);    
                }
            }else{
               
                $response = json_encode([
                    'status' => 'error',
                    'alert' => 'alert-warning',
                    'message'=> 'Formato de documento inválido.'
                ]);
            }

        }else{
          
            $response = json_encode([
                'status' => 'vacio',
                'alert' => 'alert-warning',
                'message' => 'Ingrese el soporte de la referencia.'
            ]);
        }

        return $response;

    }

    // Buscar palabras clave del capacitador
    public function buscarPalabrasClave($id_capacitador)
    {
        $db = Mysql::connection();

        // 1. Consultamos las palabras pendientes
        $sql1 = "SELECT
                palabras_claves.id_palabra_clave,
                palabras_claves.descripcion
                FROM palabras_claves
                WHERE palabras_claves.id_palabra_clave NOT IN (SELECT id_palabra_clave 
                FROM capacitadores_palabras WHERE id_capacitador = ".$id_capacitador." AND estado_registro = 'ACTIVO')
                AND palabras_claves.estado_registro = 'ACTIVO'
                ORDER BY palabras_claves.descripcion";

        $consulta1 = $db->createCommand($sql1)->queryAll();

        // 2. Consultamos las palabras registradas
        $sql2 = "SELECT 
                capacitadores_palabras.id_capacitador_palabra,
                capacitadores_palabras.id_palabra_clave,
                palabras_claves.descripcion,
                capacitadores_palabras.estado_registro
                FROM capacitadores_palabras
                INNER JOIN palabras_claves ON palabras_claves.id_palabra_clave = capacitadores_palabras.id_palabra_clave
                WHERE capacitadores_palabras.id_capacitador = ".$id_capacitador."
                AND capacitadores_palabras.estado_registro = 'ACTIVO'
                AND palabras_claves.estado_registro = 'ACTIVO'
                ORDER BY palabras_claves.descripcion";

        $consulta2 = $db->createCommand($sql2)->queryAll();

        $response = [
            "pendientes"    => $consulta1,
            "registradas"   => $consulta2
        ];

        return json_encode($response);
    }

    // Agregar palabra clave a un capacitador
    public function agregarPalabraClave($id_palabra_clave,$id_capacitador)
    {
        $db = Mysql::connection();

        // Consultamos si la palabra ya esta registrada
        $sql1 = "SELECT 
                id_capacitador_palabra,
                id_palabra_clave,
                id_capacitador,
                estado_registro
                FROM capacitadores_palabras
                WHERE id_palabra_clave = '".$id_palabra_clave."'
                AND id_capacitador = '".$id_capacitador."'";

        $consulta1 = $db->createCommand($sql1)->queryOne();

        if (!empty($consulta1)) {
            // La palabra esta registrada
            if ($consulta1['estado_registro'] == 'INACTIVO') {
                // Activamos la palabra
                $sql2 = "UPDATE capacitadores_palabras 
                        SET estado_registro = 'ACTIVO'
                        WHERE id_capacitador_palabra = '".$consulta1['id_capacitador_palabra']."'
                        AND id_palabra_clave = '".$consulta1['id_palabra_clave']."'
                        AND id_capacitador = '".$consulta1['id_capacitador']."'";

                $update = $db->createCommand($sql2)->execute();

                if ($update) {
                    $response = json_encode([
                        'status' => 'success'
                    ]);
                }else{
                    $response = json_encode([
                        'status' => 'error'
                    ]);
                }
            }

        }else{
            // Registramos la palabra
            $id_capacitador_palabra = Generalidades::getConsecutivo("capacitadores_palabras","id_capacitador_palabra");

            $sql2 = "INSERT INTO capacitadores_palabras(id_capacitador_palabra, id_palabra_clave, id_capacitador, estado_registro) VALUES (".$id_capacitador_palabra.",".$id_palabra_clave.",".$id_capacitador.",'ACTIVO')";

            $insert = $db->createCommand($sql2)->execute();

            if ($insert) {
                $response = json_encode([
                    'status' => 'success'
                ]);
            }else{
                $response = json_encode([
                    'status' => 'error'
                ]);
            }
        }

        return $response;
    }

    // Remover palabra clave de un capacitador
    public function removerPalabraClave($id_capacitador_palabra,$id_capacitador)
    {
        $db = Mysql::connection();

        $sql = "UPDATE capacitadores_palabras
                SET estado_registro = 'INACTIVO'
                WHERE id_capacitador_palabra = '".$id_capacitador_palabra."'";

        $update = $db->createCommand($sql)->execute();

        if ($update) {
            $response = json_encode([
                'status' => 'success'
            ]);
        }else{
            $response = json_encode([
                'status' => 'error'
            ]);
        }

        return $response;
    }
}
