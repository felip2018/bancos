<?php

namespace app\models;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\base\Model;

use app\models\Mysql;
use app\models\Generalidades;

class Cronograma extends Model
{
	// Registrar cronograma presencial y sesiones
	public function registrarCronogramaPresencial($POST)
	{
		$db = Mysql::connection();

		$listadoAccionesFormacion 	= $POST['listadoAccionesFormacion'];
		$listadoGrupos 				= $POST['listadoGrupos'];

		$accion_formacion = explode('_',$listadoAccionesFormacion);
		$grupo = explode('_',$listadoGrupos);

		$id_accion_formacion = $accion_formacion[0];
		$id_grupo = $grupo[0];

		

		$num_beneficiarios_grupo 	= $POST['num_beneficiarios_grupo'];
		$evento_formacion 			= $POST['evento_formacion'];
		$duracion_total_eformacion 	= $POST['duracion_total_eformacion'];
		$modalidad 					= $POST['modalidad'];
		$total_hrs_unidad_tematica 	= $POST['total_hrs_unidad_tematica'];
		$num_unidad_tematica 		= $POST['num_unidad_tematica'];
		$fecha_inicio 				= $POST['fecha_inicio'];
		$fecha_finalizacion 		= $POST['fecha_finalizacion'];
		$nombre_unidad_tematica 	= $POST['nombre_unidad_tematica'];
		$metodologia_presencial 	= $POST['metodologia_presencial'];
		$num_personas 				= (empty($POST['num_personas'])) ? 0 : $POST['num_personas'];
		$sesiones_por_persona 		= (empty($POST['sesiones_por_persona'])) ? 0 : $POST['sesiones_por_persona'];
		$num_sesiones 				= $POST['num_sesiones'];

		// Consultamos si el cronograma ya esta registrado para el grupo de la AF seleccionada
		$sql1 = "SELECT COUNT(*) cant FROM cronograma WHERE id_accion_formacion = '".$id_accion_formacion."' AND id_grupo = '".$id_grupo."' AND num_unidad_tematica = '".$num_unidad_tematica."'";

		
		$consulta1 = $db->createCommand($sql1)->queryOne();
		
		if ($consulta1['cant'] == 0) {
			// Procedemos con el registro del cronograma
			// Paso 1: Realizamos el registro en la tabla: cronograma

			$id_cronograma = Generalidades::getConsecutivo('cronograma','id_cronograma');

			$sql2 = "INSERT INTO cronograma(id_cronograma, id_accion_formacion, id_grupo, num_beneficiarios_grupo, duracion_total_eformacion, metodologia_presencial, metodologia_virtual, num_unidad_tematica, nombre_unidad_tematica, fecha_inicio, fecha_finalizacion, total_hrs_unidad_tematica, num_personas, sesiones_por_persona, num_sesiones, num_actividades, radicado, id_usuario_interventoria, estado_interventoria, observaciones, fecha_registro, estado_registro) VALUES ('".$id_cronograma."','".$id_accion_formacion."','".$id_grupo."','".$num_beneficiarios_grupo."','".$duracion_total_eformacion."','".strtoupper($metodologia_presencial)."','','".$num_unidad_tematica."','".strtoupper($nombre_unidad_tematica)."','".$fecha_inicio."','".$fecha_finalizacion."','".$total_hrs_unidad_tematica."','".$num_personas."','".$sesiones_por_persona."','".$num_sesiones."',0,'',0,'','',NOW(),'ACTIVO')";

			$consulta2 = $db->createCommand($sql2)->execute();

			$registro_sesiones = true;
			$arregloSQL = [];
			$query = '';

			if ($consulta2) {
				// Se ha registrado el cronograma correctamente
				// Paso 2: Realizamos el registro de las sesiones del cronograma

				foreach ($POST['arregloSesiones'] as $key => $value) {

					$id_cronograma_presencial = Generalidades::getConsecutivo('cronograma_presencial','id_cronograma_presencial');

					$ciudad = explode('-', $value[7]);

					$nombre_sesion 			= $value[0];
					$num_sesion 			= $value[1];
					$fecha_inicio 			= $value[2];
					$hora_inicial 			= $value[3];
					$hora_final 			= $value[4];
					$num_hrs_totales 		= $value[5];
					$id_ciudad 				= $ciudad[0];
					$nombre_sede 			= $value[8];
					$direccion_sede 		= $value[9];
					$aula_salon 			= $value[10];

					$nombre_capacitador 	= $value[11];
					$tipo_doc_capacitador 	= $value[12];
					$num_doc_capacitador 	= $value[13];

					$nombre_capacitador_s1 	= $value[14];
					$tipo_doc_capacitador_s1= $value[15];
					$num_doc_capacitador_s1 = ($value[16] == null) ? 0 : $value[16];

					$nombre_capacitador_s2 	= $value[17];
					$tipo_doc_capacitador_s2= $value[18];
					$num_doc_capacitador_s2 = ($value[19] == null) ? 0 : $value[19];

					$perfil_capacitador 	= $value[20];
					$tipo_capacitador 		= $value[21];

					$sql3 = "INSERT INTO cronograma_presencial(id_cronograma_presencial, id_cronograma, nombre_sesion, num_sesion, fecha_inicio, hora_inicial, hora_final, num_hrs_totales, id_ciudad, nombre_sede, direccion_sede, aula_salon, nombre_capacitador, tipo_doc_capacitador, num_doc_capacitador, nombre_capacitador_s1, tipo_doc_capacitador_s1, num_doc_capacitador_s1, nombre_capacitador_s2, tipo_doc_capacitador_s2, num_doc_capacitador_s2, perfil_capacitador, tipo_capacitador, fecha_registro, estado_registro) VALUES ('".$id_cronograma_presencial."','".$id_cronograma."','".strtoupper($nombre_sesion)."','".$num_sesion."','".$fecha_inicio."','".$hora_inicial."','".$hora_final."','".$num_hrs_totales."','".$id_ciudad."','".strtoupper($nombre_sede)."','".strtoupper($direccion_sede)."','".strtoupper($aula_salon)."','".strtoupper($nombre_capacitador)."','".$tipo_doc_capacitador."','".$num_doc_capacitador."','".strtoupper($nombre_capacitador_s1)."','".$tipo_doc_capacitador_s1."',".$num_doc_capacitador_s1.",'".strtoupper($nombre_capacitador_s2)."','".$tipo_doc_capacitador_s2."','".$num_doc_capacitador_s2."','".$perfil_capacitador."','".$tipo_capacitador."',NOW(),'ACTIVO')";

					$consulta3 = $db->createCommand($sql3)->execute();

					if ($consulta3) {
						$registro_sesiones = true;
					}else{
						$registro_sesiones = false;
						$query = $sql3;
						break;
					}
					//array_push($arregloSQL, $sql3);
				}

				//$response = json_encode(['status' => 'test', 'SQL' => $arregloSQL]);

				if ($registro_sesiones) {
					// Se han registrado todas las sesiones
					$response = json_encode([
						'status' 	=> 'success',
						'message'	=> 'Se ha realizado el registro del cronograma y cada una de las sesiones correctamente.',
						'alert'		=> 'alert-success'
					]);

				}else{
					// Ocurrio un error en el registro de las sesiones
					$response = json_encode([
						'status' 	=> 'warning',
						'message'	=> 'Se ha realizado el registro del cronograma pero ocurrió un inconveniente al registrar las sesiones del mismo.<br>'.$query,
						'alert'		=> 'alert-warning'
					]);
				}

			}else{
				// Ha ocurrido un error
				$response = json_encode([
					'status' 	=> 'error',
					'message'	=> 'No fue posible registrar el cronograma del grupo seleccionado.<br>'.$sql2,
					'alert'		=> 'alert-danger'
				]);
			}

		}else{
			// El grupo ya cuenta con un cronograma
			$response = json_encode([
				'status' 	=> 'warning',
				'message' 	=> 'El grupo seleccionado ya cuenta con un cronograma registrado para esta unidad temática, por favor verifique.',
				'alert' 	=> 'alert-warning'
			]);
		}
		return $response;
	}

	// Registrar cronograma virtual y sesiones
	public function registrarCronogramaVirtual($POST)
	{
		$db = Mysql::connection();

		$listadoAccionesFormacion 	= $POST['listadoAccionesFormacion'];
		$listadoGrupos 				= $POST['listadoGrupos'];

		$accion_formacion = explode('_',$listadoAccionesFormacion);
		$grupo = explode('_',$listadoGrupos);

		$id_accion_formacion = $accion_formacion[0];
		$id_grupo = $grupo[0];

		$num_beneficiarios_grupo 	= $POST['num_beneficiarios_grupo'];
		$evento_formacion 			= $POST['evento_formacion'];
		$duracion_total_eformacion 	= $POST['duracion_total_eformacion'];
		$modalidad 					= $POST['modalidad'];
		$total_hrs_unidad_tematica 	= $POST['total_hrs_unidad_tematica'];
		$num_unidad_tematica 		= $POST['num_unidad_tematica'];
		$nombre_unidad_tematica 	= $POST['nombre_unidad_tematica'];
		$fecha_inicio 				= $POST['fecha_inicio'];
		$fecha_finalizacion 		= $POST['fecha_finalizacion'];
		$metodologia_virtual 		= $POST['metodologia_virtual'];
		$num_actividades 			= $POST['num_actividades'];

		// Consultamos si el cronograma ya esta registrado para el grupo de la AF seleccionada
		$sql1 = "SELECT COUNT(*) cant FROM cronograma WHERE id_accion_formacion = '".$id_accion_formacion."' AND id_grupo = '".$id_grupo."' AND num_unidad_tematica = '".$num_unidad_tematica."'";

		$consulta1 = $db->createCommand($sql1)->queryOne();		

		if ($consulta1['cant'] == 0) {
			// Procedemos con el registro del cronograma
			// Paso 1: Realizamos el registro en la tabla: cronograma

			$id_cronograma = Generalidades::getConsecutivo('cronograma','id_cronograma');

			$sql2 = "INSERT INTO cronograma(id_cronograma, id_accion_formacion, id_grupo, num_beneficiarios_grupo, duracion_total_eformacion, metodologia_presencial, metodologia_virtual, num_unidad_tematica, nombre_unidad_tematica, fecha_inicio, fecha_finalizacion, total_hrs_unidad_tematica, num_personas, sesiones_por_persona, num_sesiones, num_actividades, radicado, id_usuario_interventoria, estado_interventoria, observaciones, fecha_registro, estado_registro) VALUES ('".$id_cronograma."','".$id_accion_formacion."','".$id_grupo."','".$num_beneficiarios_grupo."','".$duracion_total_eformacion."','','".strtoupper($metodologia_virtual)."','".$num_unidad_tematica."','".strtoupper($nombre_unidad_tematica)."','".$fecha_inicio."','".$fecha_finalizacion."','".$total_hrs_unidad_tematica."','0','0','0',".$num_actividades.",'',0,'','',NOW(),'ACTIVO')";

			$consulta2 = $db->createCommand($sql2)->execute();

			if ($consulta2) {
				// Se ha registrado el cronograma correctamente
				// Paso 2: Realizamos el registro de las actividades del cronograma
				$registro_actividades = true;
				$arregloSQL = [];
				$query = '';

				foreach ($POST['arregloSesiones'] as $key => $value) {

					$id_cronograma_virtual = Generalidades::getConsecutivo('cronograma_virtual','id_cronograma_virtual');

					$nombre_actividad 		= $value[0];
					$fecha_inicio_actividad = $value[1];
					$fecha_fin_actividad 	= $value[2];
					$proveedor_formacion 	= $value[3];
					$url_plataforma 		= $value[4];
					$usuario_sena 			= $value[5];
					$clave_sena 			= $value[6];
					$nombre_capacitador 	= $value[7];
					$tipo_doc_capacitador 	= $value[8];
					$num_doc_capacitador 	= $value[9];
					$perfil_capacitador 	= $value[10];
					$tipo_capacitador 		= $value[11];


					$sql3 = "INSERT INTO cronograma_virtual(id_cronograma_virtual, id_cronograma, nombre_actividad, fecha_inicio_actividad, fecha_fin_actividad, proveedor_formacion, url_plataforma, usuario_sena, clave_sena, nombre_capacitador, tipo_doc_capacitador, num_doc_capacitador, perfil_capacitador, tipo_capacitador, fecha_registro, estado_registro) VALUES (".$id_cronograma_virtual.",".$id_cronograma.",'".strtoupper($nombre_actividad)."','".$fecha_inicio_actividad."','".$fecha_fin_actividad."','".strtoupper($proveedor_formacion)."','".$url_plataforma."','".$usuario_sena."','".$clave_sena."','".strtoupper($nombre_capacitador)."','".$tipo_doc_capacitador."','".$num_doc_capacitador."','".strtoupper($perfil_capacitador)."','".strtoupper($tipo_capacitador)."',NOW(),'ACTIVO')";

					$consulta3 = $db->createCommand($sql3)->execute();

					if ($consulta3) {
						$registro_actividades = true;
					}else{
						$registro_actividades = false;
						$query = $sql3;
						break;
					}
					//array_push($arregloSQL, $sql3);
				}
				//$response = json_encode(['status' => 'test', 'SQL' => $arregloSQL]);
				if ($registro_actividades) {
					// Se han registrado todas las actividades
					$response = json_encode([
						'status' 	=> 'success',
						'message'	=> 'Se ha realizado el registro del cronograma y cada una de las actividades correctamente.',
						'alert'		=> 'alert-success'
					]);

				}else{
					// Ocurrio un error en el registro de las actividades
					$response = json_encode([
						'status' 	=> 'warning',
						'message'	=> 'Se ha realizado el registro del cronograma pero ocurrió un inconveniente al registrar las actividades del mismo.<br>'.$query,
						'alert'		=> 'alert-warning'
					]);
				}

			}else{
				// Ha ocurrido un error
				$response = json_encode([
					'status' 	=> 'error',
					'message'	=> 'No fue posible registrar el cronograma del grupo seleccionado.<br>'.$sql2,
					'alert'		=> 'alert-danger'
				]);
			}

		}else{
			// El grupo ya cuenta con un cronograma
			$response = json_encode([
				'status' 	=> 'warning',
				'message' 	=> 'El grupo seleccionado ya cuenta con un cronograma registrado para esta unidad temática, por favor verifique.',
				'alert' 	=> 'alert-warning'
			]);
		}
		return $response;
	}

	// Registrar cronograma combinado, sesiones y actividades
	public function registrarCronogramaCombinado($POST)
	{
		$db = Mysql::connection();

		// Evaluamos si ya esta registrado el cronograma para el numero de unidad tematica
		$listadoAccionesFormacion 	= $POST['listadoAccionesFormacion'];
		$listadoGrupos 				= $POST['listadoGrupos'];

		$accion_formacion = explode('_',$listadoAccionesFormacion);
		$grupo = explode('_',$listadoGrupos);

		$id_accion_formacion = $accion_formacion[0];
		$id_grupo = $grupo[0];

		$num_beneficiarios_grupo 	= $POST['num_beneficiarios_grupo'];
		$duracion_total_eformacion  = $POST['duracion_total_eformacion'];
		$total_hrs_unidad_tematica 	= $POST['total_hrs_unidad_tematica'];
		$num_unidad_tematica 		= $POST['num_unidad_tematica'];
		$nombre_unidad_tematica 	= $POST['nombre_unidad_tematica'];
		$fecha_inicio 				= $POST['fecha_inicio'];
		$fecha_finalizacion 		= $POST['fecha_finalizacion'];
		$metodologia_presencial 	= $POST['metodologia_presencial'];
		$metodologia_virtual 		= $POST['metodologia_virtual'];
		$num_sesiones 				= $POST['num_sesiones'];
		$num_actividades 			= $POST['num_actividades'];

		// Consultamos si el cronograma ya esta registrado para el grupo de la AF seleccionada
		$sql1 = "SELECT COUNT(*) cant FROM cronograma WHERE id_accion_formacion = '".$id_accion_formacion."' AND id_grupo = '".$id_grupo."' AND num_unidad_tematica = '".$num_unidad_tematica."'";

		$consulta1 = $db->createCommand($sql1)->queryOne();		

		if ($consulta1['cant'] == 0) {
			// Procedemos con el registro del cronograma
			// Paso 1: Realizamos el registro en la tabla: cronograma

			$id_cronograma = Generalidades::getConsecutivo('cronograma','id_cronograma');

			$sql2 = "INSERT INTO cronograma(id_cronograma, id_accion_formacion, id_grupo, num_beneficiarios_grupo, duracion_total_eformacion, metodologia_presencial, metodologia_virtual, num_unidad_tematica, nombre_unidad_tematica, fecha_inicio, fecha_finalizacion, total_hrs_unidad_tematica, num_personas, sesiones_por_persona, num_sesiones, num_actividades, radicado, id_usuario_interventoria, estado_interventoria, observaciones, fecha_registro, estado_registro) VALUES ('".$id_cronograma."','".$id_accion_formacion."','".$id_grupo."','".$num_beneficiarios_grupo."','".$duracion_total_eformacion."','".strtoupper($metodologia_presencial)."','".strtoupper($metodologia_virtual)."','".$num_unidad_tematica."','".strtoupper($nombre_unidad_tematica)."','".$fecha_inicio."','".$fecha_finalizacion."','".$total_hrs_unidad_tematica."','0','0','".$num_sesiones."',".$num_actividades.",'',0,'','',NOW(),'ACTIVO')";

			$consulta2 = $db->createCommand($sql2)->execute();
			
			if ($consulta2) {

				// Paso 2: Realizamos el registro de las sesiones del cronograma
				$registro_sesiones = true;
				$query_sesiones = '';

				foreach ($POST['arregloSesionesPresenciales'] as $key => $value) {

					$id_cronograma_presencial = Generalidades::getConsecutivo('cronograma_presencial','id_cronograma_presencial');

					$ciudad = explode('-', $value[7]);

					$nombre_sesion 			= $value[0];
					$num_sesion 			= $value[1];
					$fecha_inicio 			= $value[2];
					$hora_inicial 			= $value[3];
					$hora_final 			= $value[4];
					$num_hrs_totales 		= $value[5];
					$id_ciudad 				= $ciudad[0];
					$nombre_sede 			= $value[8];
					$direccion_sede 		= $value[9];
					$aula_salon 			= $value[10];

					$nombre_capacitador 	= $value[11];
					$tipo_doc_capacitador 	= $value[12];
					$num_doc_capacitador 	= $value[13];

					$nombre_capacitador_s1 	= $value[14];
					$tipo_doc_capacitador_s1= $value[15];
					$num_doc_capacitador_s1 = ($value[16] == null) ? 0 : $value[16];

					$nombre_capacitador_s2 	= $value[17];
					$tipo_doc_capacitador_s2= $value[18];
					$num_doc_capacitador_s2 = ($value[19] == null) ? 0 : $value[19];

					$perfil_capacitador 	= $value[20];
					$tipo_capacitador 		= $value[21];

					$sql3 = "INSERT INTO cronograma_presencial(id_cronograma_presencial, id_cronograma, nombre_sesion, num_sesion, fecha_inicio, hora_inicial, hora_final, num_hrs_totales, id_ciudad, nombre_sede, direccion_sede, aula_salon, nombre_capacitador, tipo_doc_capacitador, num_doc_capacitador, nombre_capacitador_s1, tipo_doc_capacitador_s1, num_doc_capacitador_s1, nombre_capacitador_s2, tipo_doc_capacitador_s2, num_doc_capacitador_s2, perfil_capacitador, tipo_capacitador, fecha_registro, estado_registro) VALUES ('".$id_cronograma_presencial."','".$id_cronograma."','".strtoupper($nombre_sesion)."','".$num_sesion."','".$fecha_inicio."','".$hora_inicial."','".$hora_final."','".$num_hrs_totales."','".$id_ciudad."','".strtoupper($nombre_sede)."','".strtoupper($direccion_sede)."','".strtoupper($aula_salon)."','".strtoupper($nombre_capacitador)."','".$tipo_doc_capacitador."','".$num_doc_capacitador."','".strtoupper($nombre_capacitador_s1)."','".$tipo_doc_capacitador_s1."',".$num_doc_capacitador_s1.",'".strtoupper($nombre_capacitador_s2)."','".$tipo_doc_capacitador_s2."','".$num_doc_capacitador_s2."','".$perfil_capacitador."','".$tipo_capacitador."',NOW(),'ACTIVO')";

					$consulta3 = $db->createCommand($sql3)->execute();

					if ($consulta3) {
						$registro_sesiones = true;
					}else{
						$registro_sesiones = false;
						$query_sesiones = $sql3;
						break;
					}

				}

				if ($registro_sesiones) {
					
					// Paso 3: Realizamos el registro de las actividades del cronograma
					$registro_actividades = true;
					$query_actividades = '';

					foreach ($POST['arregloSesionesVirtuales'] as $key => $value) {

						$id_cronograma_virtual = Generalidades::getConsecutivo('cronograma_virtual','id_cronograma_virtual');

						$nombre_actividad 		= $value[0];
						$fecha_inicio_actividad = $value[1];
						$fecha_fin_actividad 	= $value[2];
						$proveedor_formacion 	= $value[3];
						$url_plataforma 		= $value[4];
						$usuario_sena 			= $value[5];
						$clave_sena 			= $value[6];
						$nombre_capacitador 	= $value[7];
						$tipo_doc_capacitador 	= $value[8];
						$num_doc_capacitador 	= $value[9];
						$perfil_capacitador 	= $value[10];
						$tipo_capacitador 		= $value[11];


						$sql3 = "INSERT INTO cronograma_virtual(id_cronograma_virtual, id_cronograma, nombre_actividad, fecha_inicio_actividad, fecha_fin_actividad, proveedor_formacion, url_plataforma, usuario_sena, clave_sena, nombre_capacitador, tipo_doc_capacitador, num_doc_capacitador, perfil_capacitador, tipo_capacitador, fecha_registro, estado_registro) VALUES (".$id_cronograma_virtual.",".$id_cronograma.",'".strtoupper($nombre_actividad)."','".$fecha_inicio_actividad."','".$fecha_fin_actividad."','".strtoupper($proveedor_formacion)."','".$url_plataforma."','".$usuario_sena."','".$clave_sena."','".strtoupper($nombre_capacitador)."','".$tipo_doc_capacitador."','".$num_doc_capacitador."','".strtoupper($perfil_capacitador)."','".strtoupper($tipo_capacitador)."',NOW(),'ACTIVO')";

						$consulta3 = $db->createCommand($sql3)->execute();

						if ($consulta3) {
							$registro_actividades = true;
						}else{
							$registro_actividades = false;
							$query_actividades = $sql3;
							break;
						}

					}

					if ($registro_actividades) {
						// Se han registrado todas las sesiones
						$response = json_encode([
							'status' 	=> 'success',
							'message'	=> 'Se ha realizado el registro del cronograma y cada una de las sesiones correctamente.',
							'alert'		=> 'alert-success'
						]);

					}else{
						// Ocurrio un error en el registro de las sesiones
						$response = json_encode([
							'status' 	=> 'warning',
							'message'	=> 'Se ha realizado el registro del cronograma pero ocurrió un inconveniente al registrar las actividades del mismo.<br>'.$query_actividades,
							'alert'		=> 'alert-warning'
						]);
					}

				}else{
					// Ocurrio un error en el registro de las sesiones
					$response = json_encode([
						'status' 	=> 'warning',
						'message'	=> 'Se ha realizado el registro del cronograma pero ocurrió un inconveniente al registrar las sesiones del mismo.<br>'.$query_sesiones,
						'alert'		=> 'alert-warning'
					]);
				}

				

			}else{
				// Ha ocurrido un error
				$response = json_encode([
					'status' 	=> 'error',
					'message'	=> 'No fue posible registrar el cronograma del grupo seleccionado.<br>'.$sql2,
					'alert'		=> 'alert-danger'
				]);
			}
		}else{
			// El grupo ya cuenta con un cronograma para esta Unidad Tematica
			$response = json_encode([
				'status' 	=> 'warning',
				'message' 	=> 'El grupo seleccionado ya cuenta con un cronograma registrado para esta unidad temática, por favor verifique.',
				'alert' 	=> 'alert-warning'
			]);
		}

		return $response;
	}

	// Buscar unidades tematicas
	public function buscarUnidadesTematicas($id_accion_formacion,$id_grupo)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				id_cronograma,
				num_unidad_tematica
				FROM cronograma
				WHERE id_accion_formacion = ".$id_accion_formacion."
				AND id_grupo = ".$id_grupo."
				ORDER BY num_unidad_tematica ASC";

		$consulta = $db->createCommand($sql)->queryAll();

		if (!empty($consulta)) {

			$response = json_encode([
				'status' => 'success',
				'unidades_tematicas' => $consulta
			]);

		}else{
			$response = json_encode([
				'status' => 'vacio'
			]);
		}
		return $response;
	}

	// Buscar cronograma
	public function buscarCronograma($POST)
	{
		$db = Mysql::connection();

		$accion_formacion = explode('_', $POST['accion_formacion']);
		$grupo = explode('_', $POST['grupo']);

		$id_accion_formacion 	= $accion_formacion[0];
		$id_grupo 				= $grupo[0];
		$num_unidad_tematica	= $POST['unidad_tematica'];

		// Consultamos la informacion del cronograma global

		$sql = "SELECT
				cronograma.id_cronograma,
				cronograma.id_accion_formacion,
				cronograma.id_grupo,
				cronograma.num_beneficiarios_grupo,
				cronograma.duracion_total_eformacion,
				cronograma.metodologia_presencial,
				cronograma.metodologia_virtual,
				cronograma.num_unidad_tematica,
				cronograma.nombre_unidad_tematica,
				DATE_FORMAT(cronograma.fecha_inicio,'%Y-%m-%d') AS fecha_inicio,
				DATE_FORMAT(cronograma.fecha_finalizacion,'%Y-%m-%d') AS fecha_finalizacion,
				cronograma.total_hrs_unidad_tematica,
				cronograma.num_personas,
				cronograma.sesiones_por_persona,
				cronograma.num_sesiones,
				cronograma.num_actividades,
				cronograma.estado_interventoria,
				acciones_formacion.modalidad,
				acciones_formacion.evento_formacion
				FROM cronograma
				INNER JOIN acciones_formacion ON acciones_formacion.id_accion_formacion = cronograma.id_accion_formacion
				WHERE cronograma.id_accion_formacion = ".$id_accion_formacion."
				AND cronograma.id_grupo = ".$id_grupo."
				AND cronograma.num_unidad_tematica = ".$num_unidad_tematica;

		$cronograma = $db->createCommand($sql)->queryOne();

		if (!empty($cronograma)) {
			
			// Consultamos el listado de sesiones del cronograma
			switch ($cronograma['modalidad']) {
				case 'PRESENCIAL':
					// Consultamos las sesiones presenciales
					$sesiones = "SELECT 
									id_cronograma_presencial,
									id_cronograma,
									nombre_sesion,
									num_sesion,
									fecha_inicio,
									hora_inicial,
									hora_final,
									num_hrs_totales,
									id_ciudad,
									nombre_sede,
									direccion_sede,
									aula_salon,
									nombre_capacitador,
									tipo_doc_capacitador,
									num_doc_capacitador,
									nombre_capacitador_s1,
									tipo_doc_capacitador_s1,
									num_doc_capacitador_s1,
									nombre_capacitador_s2,
									tipo_doc_capacitador_s2,
									num_doc_capacitador_s2,
									perfil_capacitador,
									tipo_capacitador,
									estado_registro
									FROM cronograma_presencial
									WHERE id_cronograma = ".$cronograma['id_cronograma']."
									AND estado_registro = 'ACTIVO'
									ORDER BY num_sesion ASC";

					$buscarSesiones = $db->createCommand($sesiones)->queryAll();

					$response = json_encode([
						'status' 		=> 'success',
						'cronograma' 	=> $cronograma,
						'sesiones'		=> $buscarSesiones
					]);

					break;
				
				case 'VIRTUAL':
					// Consultamos las actividades virtuales
					$actividades = "SELECT 
									id_cronograma_virtual,
									id_cronograma,
									nombre_actividad,
									fecha_inicio_actividad,
									fecha_fin_actividad,
									proveedor_formacion,
									url_plataforma,
									usuario_sena,
									clave_sena,
									nombre_capacitador,
									tipo_doc_capacitador,
									num_doc_capacitador,
									perfil_capacitador,
									tipo_capacitador,
									estado_registro
									FROM cronograma_virtual
									WHERE id_cronograma = ".$cronograma['id_cronograma']."
									AND estado_registro = 'ACTIVO'
									ORDER BY id_cronograma_virtual ASC";

					$buscarActividades = $db->createCommand($actividades)->queryAll();

					$response = json_encode([
						'status' 		=> 'success',
						'cronograma' 	=> $cronograma,
						'actividades'	=> $buscarActividades
					]);

					break;

				case 'COMBINADA':
					// Consultamos las sesiones presenciales y las actividades virtuales
					$sesiones = "SELECT 
									id_cronograma_presencial,
									id_cronograma,
									nombre_sesion,
									num_sesion,
									fecha_inicio,
									hora_inicial,
									hora_final,
									num_hrs_totales,
									id_ciudad,
									nombre_sede,
									direccion_sede,
									aula_salon,
									nombre_capacitador,
									tipo_doc_capacitador,
									num_doc_capacitador,
									nombre_capacitador_s1,
									tipo_doc_capacitador_s1,
									num_doc_capacitador_s1,
									nombre_capacitador_s2,
									tipo_doc_capacitador_s2,
									num_doc_capacitador_s2,
									perfil_capacitador,
									tipo_capacitador,
									estado_registro
									FROM cronograma_presencial
									WHERE id_cronograma = ".$cronograma['id_cronograma']."
									AND estado_registro = 'ACTIVO'
									ORDER BY num_sesion ASC";

					$buscarSesiones = $db->createCommand($sesiones)->queryAll();

					$actividades = "SELECT 
									id_cronograma_virtual,
									id_cronograma,
									nombre_actividad,
									fecha_inicio_actividad,
									fecha_fin_actividad,
									proveedor_formacion,
									url_plataforma,
									usuario_sena,
									clave_sena,
									nombre_capacitador,
									tipo_doc_capacitador,
									num_doc_capacitador,
									perfil_capacitador,
									tipo_capacitador,
									estado_registro
									FROM cronograma_virtual
									WHERE id_cronograma = ".$cronograma['id_cronograma']."
									AND estado_registro = 'ACTIVO'
									ORDER BY id_cronograma_virtual ASC";

					$buscarActividades = $db->createCommand($actividades)->queryAll();

					$response = json_encode([
						'status' 		=> 'success',
						'cronograma' 	=> $cronograma,
						'sesiones'		=> $buscarSesiones,
						'actividades'	=> $buscarActividades
					]);

					break;
			}


		}else{
			$response = json_encode([
				'status' => 'vacio'
			]);
		}

		return $response;

	}

	// Consultar informacion del cronograma presencial
	public function buscarSesionPresencial($id_cronograma_presencial)
	{
		$db = Mysql::connection();

		// Consultamos la informacion del cronograma presencial
		$sql1 = "SELECT id_cronograma_presencial, id_cronograma, nombre_sesion, num_sesion, fecha_inicio, hora_inicial, hora_final, num_hrs_totales, id_ciudad, nombre_sede, direccion_sede, aula_salon, nombre_capacitador, tipo_doc_capacitador, num_doc_capacitador, nombre_capacitador_s1, tipo_doc_capacitador_s1, num_doc_capacitador_s1, nombre_capacitador_s2, tipo_doc_capacitador_s2, num_doc_capacitador_s2, perfil_capacitador, tipo_capacitador, estado_registro FROM cronograma_presencial WHERE id_cronograma_presencial = ".$id_cronograma_presencial;

		$info = $db->createCommand($sql1)->queryOne();

		// Consultamos el listado de departamentos
		$sql2 = "SELECT id_dpto, id_pais, nombre FROM departamentos WHERE id_pais = 170 ORDER BY nombre ASC";
		$dptos = $db->createCommand($sql2)->queryAll();

		// Consultamos el listado de ciudades
		$sql3 = "SELECT id_ciudad, id_dpto, nombre, nom_ciu_dep FROM ciudades WHERE id_ciudad != 1 ORDER BY nombre ASC";
		$ciudades = $db->createCommand($sql3)->queryAll();

		$response = json_encode([
			'status' 	=> 'success',
			'sesion'	=> $info,
			'dptos'		=> $dptos,
			'ciudades'	=> $ciudades
		]);

		return $response;
	}

	// Actualizar información de la sesion seleccionada
	public function actualizarSesion($POST,$id_proyecto)
	{
		$db = Mysql::connection();
		
		$sqlSeguridad = "SELECT estado_interventoria FROM proyectos WHERE id_proyecto = '".$id_proyecto."'";
		$consultaSeguridad = $db->createCommand($sqlSeguridad)->queryOne();
		

		if ($consultaSeguridad['estado_interventoria'] != 'RADICADO' ||
			$consultaSeguridad['estado_interventoria'] != 'APROBADO') {
			
			// Se actualiza el registro correctamente

			$id_cronograma_presencial 	= $POST['id_cronograma_presencial'];
			$nombre_sesion 				= $POST['nombre_sesion'];
			$num_sesion 				= $POST['num_sesion'];
			$fecha_inicio 				= $POST['fecha_inicio'];
			$hora_inicial 				= $POST['hora_inicial'];
			$hora_final 				= $POST['hora_final'];
			$num_hrs_totales 			= $POST['num_hrs_totales'];
			$id_ciudad 					= $POST['id_ciudad'];
			$nombre_sede 				= $POST['nombre_sede'];
			$direccion_sede 			= $POST['direccion_sede'];
			$aula_salon 				= $POST['aula_salon'];
			$nombre_capacitador 		= $POST['nombre_capacitador'];
			$tipo_doc_capacitador 		= $POST['tipo_doc_capacitador'];
			$num_doc_capacitador 		= $POST['num_doc_capacitador'];
			$nombre_capacitador_s1 		= $POST['nombre_capacitador_s1'];
			$tipo_doc_capacitador_s1 	= $POST['tipo_doc_capacitador_s1'];
			$num_doc_capacitador_s1 	= $POST['num_doc_capacitador_s1'];
			$nombre_capacitador_s2 		= $POST['nombre_capacitador_s2'];
			$tipo_doc_capacitador_s2 	= $POST['tipo_doc_capacitador_s2'];
			$num_doc_capacitador_s2 	= $POST['num_doc_capacitador_s2'];
			$perfil_capacitador 		= $POST['perfil_capacitador'];
			$tipo_capacitador 			= $POST['tipo_capacitador'];

			$sql = "UPDATE 	cronograma_presencial
					SET 	nombre_sesion 			= '".strtoupper($nombre_sesion)."',
							num_sesion				= '".$num_sesion."',
							fecha_inicio 			= '".$fecha_inicio."',
							hora_inicial 			= '".$hora_inicial."',
							hora_final				= '".$hora_final."',
							num_hrs_totales			= '".$num_hrs_totales."',
							id_ciudad 				= '".$id_ciudad."',
							nombre_sede 			= '".strtoupper($nombre_sede)."',
							direccion_sede			= '".strtoupper($direccion_sede)."',
							aula_salon 				= '".strtoupper($aula_salon)."',
							nombre_capacitador 		= '".strtoupper($nombre_capacitador)."',
							tipo_doc_capacitador 	= '".$tipo_doc_capacitador."',
							num_doc_capacitador		= '".$num_doc_capacitador."',
							nombre_capacitador_s1	= '".strtoupper($nombre_capacitador_s1)."',
							tipo_doc_capacitador_s1 = '".$tipo_doc_capacitador_s1."',
							num_doc_capacitador_s1	= '".$num_doc_capacitador_s1."',
							nombre_capacitador_s2	= '".strtoupper($nombre_capacitador_s2)."',
							tipo_doc_capacitador_s2 = '".$tipo_doc_capacitador_s2."',
							num_doc_capacitador_s2	= '".$num_doc_capacitador_s2."',
							perfil_capacitador 		= '".$perfil_capacitador."',
							tipo_capacitador		= '".$tipo_capacitador."',
							fecha_registro 			= NOW()
					WHERE 	id_cronograma_presencial= '".$id_cronograma_presencial."'";

			$update = $db->createCommand($sql)->execute();

			if ($update) {

				$response = json_encode([
					'status' 	=> 'success',
					'message' 	=> 'Se ha actualizado la información de la sesión correctamente.',
					'alert'		=> 'alert-success'
				]);

			}else{

				$response = json_encode([
					'status' 	=> 'error',
					'message' 	=> 'No fue posible actualizar los datos de la sesión seleccionada.',
					'alert'		=> 'alert-danger'
				]);

			}

		}else{
			$response = json_encode([
				'status' 	=> 'error',
				'message' 	=> 'No es posible realizar el proceso de modificación del cronograma ya que el estado registrado para el mismo por parte de la interventoría es: '.$consultaSeguridad['estado_interventoria'].'.',
				'alert'		=> 'alert-warning'
			]);
		}



		return $response;
	}

	// Consultar informacion del cronograma virtual
	public function buscarActividadVirtual($id_cronograma_virtual)
	{
		$db = Mysql::connection();

		// Consultamos la informacion del cronograma virtual
		$sql1 = "SELECT id_cronograma_virtual, id_cronograma, nombre_actividad, fecha_inicio_actividad, fecha_fin_actividad, proveedor_formacion, url_plataforma, usuario_sena, clave_sena, nombre_capacitador, tipo_doc_capacitador, num_doc_capacitador, perfil_capacitador, tipo_capacitador, fecha_registro, estado_registro FROM cronograma_virtual WHERE id_cronograma_virtual = '".$id_cronograma_virtual."'";

		$info = $db->createCommand($sql1)->queryOne();

		$response = json_encode([
			'status' 	=> 'success',
			'actividad'	=> $info,
		]);

		return $response;

	}

	// Actualizar información de la actividad seleccionada
	public function actualizarActividad($POST,$id_proyecto)
	{
		$db = Mysql::connection();

		$sqlSeguridad = "SELECT estado_interventoria FROM proyectos WHERE id_proyecto = '".$id_proyecto."'";
		$consultaSeguridad = $db->createCommand($sqlSeguridad)->queryOne();
		
		$editar = true;

		switch ($consultaSeguridad['estado_interventoria']) {
			case "RADICADO":
				$editar = false;
				break;

			case "APROBADO":
				$editar = false;
				break;
			
			default:
				$editar = true;
				break;
		}

		if ($editar) {

			$id_cronograma_virtual 	= $POST['id_cronograma_virtual'];
			$nombre_actividad 		= $POST['nombre_actividad'];
			$fecha_inicio_actividad = $POST['fecha_inicio_actividad'];
			$fecha_fin_actividad 	= $POST['fecha_fin_actividad'];
			$proveedor_formacion 	= $POST['proveedor_formacion'];
			$url_plataforma 		= $POST['url_plataforma'];
			$usuario_sena 			= $POST['usuario_sena'];
			$clave_sena 			= $POST['clave_sena'];
			$nombre_capacitador 	= $POST['nombre_capacitador'];
			$tipo_doc_capacitador 	= $POST['tipo_doc_capacitador'];
			$num_doc_capacitador 	= $POST['num_doc_capacitador'];
			$perfil_capacitador 	= $POST['perfil_capacitador'];
			$tipo_capacitador 		= $POST['tipo_capacitador'];

			$sql = "UPDATE 	cronograma_virtual
					SET 	nombre_actividad 			= '".strtoupper($nombre_actividad)."',
							fecha_inicio_actividad 		= '".$fecha_inicio_actividad."',
							fecha_fin_actividad			= '".$fecha_fin_actividad."',
							proveedor_formacion 		= '".strtoupper($proveedor_formacion)."',
							url_plataforma 				= '".$url_plataforma."',
							usuario_sena 				= '".$usuario_sena."',
							clave_sena 					= '".$clave_sena."',
							nombre_capacitador 			= '".strtoupper($nombre_capacitador)."',
							tipo_doc_capacitador 		= '".$tipo_doc_capacitador."',
							num_doc_capacitador 		= '".$num_doc_capacitador."',
							perfil_capacitador 			= '".$perfil_capacitador."',
							tipo_capacitador			= '".$tipo_capacitador."',
							fecha_registro 				= NOW()
					WHERE 	id_cronograma_virtual= '".$id_cronograma_virtual."'";

			$update = $db->createCommand($sql)->execute();

			if ($update) {

				$response = json_encode([
					'status' 	=> 'success',
					'message' 	=> 'Se ha actualizado la información de la actividad correctamente.',
					'alert'		=> 'alert-success'
				]);

			}else{

				$response = json_encode([
					'status' 	=> 'error',
					'message' 	=> 'No fue posible actualizar los datos de la actividad seleccionada.',
					'alert'		=> 'alert-danger'
				]);

			}
		}else{
			$response = json_encode([
				'status' 	=> 'error',
				'message' 	=> 'No es posible realizar el proceso de modificación del cronograma ya que el estado registrado para el mismo por parte de la interventoría es: '.$consultaSeguridad['estado_interventoria'].'.',
				'alert'		=> 'alert-warning'
			]);
		}

		return $response;
	}

	// Actualizar cronograma desde la seccion de busqueda
	public function actualizarCronograma($POST,$id_proyecto)
	{
		$db = Mysql::connection();

		$sqlSeguridad = "SELECT estado_interventoria FROM proyectos WHERE id_proyecto = '".$id_proyecto."'";
		$consultaSeguridad = $db->createCommand($sqlSeguridad)->queryOne();
		
		$editar = true;

		switch ($consultaSeguridad['estado_interventoria']) {
			case "RADICADO":
				$editar = false;
				break;

			case "APROBADO":
				$editar = false;
				break;
			
			default:
				$editar = true;
				break;
		}

		if ($editar) {

			$id_cronograma 				= $POST['id_cronograma'];

		    $accionFormacion 			= explode('_', $POST['accionFormacion']);
			$grupo 						= explode('_', $POST['grupo']);
			$unidadTematica 			= $POST['unidadTematica'];

			$id_accion_formacion 		= $accionFormacion[0];
			$id_grupo 					= $grupo[0];
			$num_unidad_tematica 		= $unidadTematica;

			$num_beneficiarios_grupo 	= $POST['num_beneficiarios_grupo'];
			$duracion_total_eformacion 	= $POST['duracion_total_eformacion'];
			$total_hrs_unidad_tematica 	= $POST['total_hrs_unidad_tematica'];
			$num_unidad_tematica 		= $POST['num_unidad_tematica'];
			$nombre_unidad_tematica 	= $POST['nombre_unidad_tematica'];
			$fecha_inicio 				= $POST['fecha_inicio'];
			$fecha_finalizacion 		= $POST['fecha_finalizacion'];
			$metodologia_presencial 	= $POST['metodologia_presencial'];
			$metodologia_virtual 		= $POST['metodologia_virtual'];
			$num_personas 				= ($POST['num_personas'] == null) ? 0 : $POST['num_personas'];
			$sesiones_por_persona 		= ($POST['sesiones_por_persona'] == null) ? 0 : $POST['sesiones_por_persona'];
			$num_sesiones 				= ($POST['num_sesiones'] == null) ? 0 : $POST['num_sesiones'];
			$num_actividades 			= ($POST['num_actividades'] == null) ? 0 : $POST['num_actividades'];

			$sql = "UPDATE cronograma SET num_beneficiarios_grupo = '".$num_beneficiarios_grupo."', duracion_total_eformacion = '".$duracion_total_eformacion."', metodologia_presencial = '".strtoupper($metodologia_presencial)."', metodologia_virtual = '".strtoupper($metodologia_virtual)."', num_unidad_tematica = '".$num_unidad_tematica."', nombre_unidad_tematica = '".strtoupper($nombre_unidad_tematica)."', fecha_inicio = '".$fecha_inicio."', fecha_finalizacion = '".$fecha_finalizacion."', total_hrs_unidad_tematica = '".$total_hrs_unidad_tematica."', num_personas = '".$num_personas."', sesiones_por_persona = '".$sesiones_por_persona."', num_sesiones = '".$num_sesiones."', num_actividades = '".$num_actividades."' WHERE id_cronograma = '".$id_cronograma."' AND id_accion_formacion = '".$id_accion_formacion."' AND id_grupo = '".$id_grupo."'";

			

			$update = $db->createCommand($sql)->execute();

			if ($update) {

				$response = json_encode([
					'status'	=> 'success',
					'message'	=> 'Se ha realizado la actualizacion de datos correctamente.',
					'alert'		=> 'alert-success'
				]);			

			}else{

				$response = json_encode([
					'status'	=> 'error',
					'message'	=> 'No se pudo realizar la actualizacion de datos.',
					'sql'		=> $sql,
					'update'	=> $update,
					'alert'		=> 'alert-warning'
				]);
			}

		}else{
			$response = json_encode([
				'status' 	=> 'error',
				'message' 	=> 'No es posible realizar el proceso de modificación del cronograma ya que el estado registrado para el mismo por parte de la interventoría es: '.$consultaSeguridad['estado_interventoria'].'.',
				'alert'		=> 'alert-warning'
			]);
		}


		return $response;

	}

	// Buscar listado del cronograma para exportar a excel
	public function buscarListadoCronograma($n_radicado)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				cronograma.id_cronograma,
				acciones_formacion.nombre AS accion_formacion,
				accion_formacion_grupos.nombre AS grupo,
				cronograma.num_beneficiarios_grupo,
				cronograma.duracion_total_eformacion,
				cronograma.metodologia_presencial,
				cronograma.metodologia_virtual,
				cronograma.num_unidad_tematica,
				cronograma.nombre_unidad_tematica,
				cronograma.fecha_inicio,
				cronograma.fecha_finalizacion,
				cronograma.total_hrs_unidad_tematica,
				cronograma.num_sesiones,
				cronograma.num_actividades,
				cronograma_presencial.id_cronograma_presencial,
				cronograma_presencial.nombre_sesion,
				cronograma_presencial.num_sesion,
				cronograma_presencial.fecha_inicio AS fecha_inicio_sesion,
				cronograma_presencial.hora_inicial,
				cronograma_presencial.hora_final,
				cronograma_presencial.num_hrs_totales,
				ciudades.nombre AS ciudad,
				cronograma_presencial.nombre_sede,
				cronograma_presencial.direccion_sede,
				cronograma_presencial.aula_salon,
				cronograma_presencial.nombre_capacitador AS capacitador,
				cronograma_presencial.tipo_doc_capacitador AS tipo_doc_capacitador_p,
				cronograma_presencial.num_doc_capacitador AS num_doc_capacitador_p,
				cronograma_presencial.nombre_capacitador_s1 AS capacitador_suplente1,
				cronograma_presencial.tipo_doc_capacitador_s1,
				cronograma_presencial.num_doc_capacitador_s1,
				cronograma_presencial.nombre_capacitador_s2 AS capacitador_suplente2,
				cronograma_presencial.tipo_doc_capacitador_s2,
				cronograma_presencial.num_doc_capacitador_s2,
				cronograma_presencial.perfil_capacitador AS perfil_capacitador_p,
				cronograma_presencial.tipo_capacitador AS tipo_capacitador_p,
				cronograma_virtual.nombre_actividad,
				cronograma_virtual.fecha_inicio_actividad,
				cronograma_virtual.fecha_fin_actividad,
				cronograma_virtual.proveedor_formacion,
				cronograma_virtual.url_plataforma,
				cronograma_virtual.usuario_sena,
				cronograma_virtual.clave_sena,
				cronograma_virtual.nombre_capacitador AS capacitador_virtual,
				cronograma_virtual.tipo_doc_capacitador AS tipo_doc_capacitador_v,
				cronograma_virtual.num_doc_capacitador AS num_doc_capacitador_v,
				cronograma_virtual.perfil_capacitador AS perfil_capacitador_v,
				cronograma_virtual.tipo_capacitador AS tipo_capacitador_v
				FROM cronograma
                
                INNER JOIN accion_formacion_grupos ON accion_formacion_grupos.id_grupo = cronograma.id_grupo
                AND accion_formacion_grupos.id_accion_formacion = cronograma.id_accion_formacion
				INNER JOIN acciones_formacion ON acciones_formacion.id_accion_formacion = accion_formacion_grupos.id_accion_formacion
				
				LEFT JOIN cronograma_presencial ON cronograma_presencial.id_cronograma = cronograma.id_cronograma
				AND cronograma_presencial.estado_registro = 'ACTIVO'
				LEFT JOIN ciudades ON ciudades.id_ciudad = cronograma_presencial.id_ciudad
				LEFT JOIN cronograma_virtual ON cronograma_virtual.id_cronograma = cronograma.id_cronograma
				AND cronograma_virtual.estado_registro = 'ACTIVO'
				WHERE cronograma.id_accion_formacion IN (SELECT id_accion_formacion 
														FROM acciones_formacion 
														INNER JOIN proyectos ON proyectos.id_proyecto = acciones_formacion.id_proyecto 
														WHERE proyectos.n_radicado = '".$n_radicado."')";

		$cronograma = $db->createCommand($sql)->queryAll();
		return $cronograma;
	}

	// Registrar sesion presencial al cronograma seleccionado
	public function registrarSesionPresencial($POST)
	{
		$db = Mysql::connection();

		$id_cronograma 				= $POST['id_cronograma'];
		$nombre_sesion 				= $POST['nombre_sesion'];
		$num_sesion 				= $POST['num_sesion'];
		$fecha_inicio 				= $POST['fecha_inicio'];
		$hora_inicial 				= $POST['hora_inicial'];
		$hora_final 				= $POST['hora_final'];
		$num_hrs_totales 			= $POST['num_hrs_totales'];
		$id_ciudad 					= $POST['id_ciudad'];
		$nombre_sede 				= strtoupper($POST['nombre_sede']);
		$direccion_sede 			= strtoupper($POST['direccion_sede']);
		$aula_salon 				= strtoupper($POST['aula_salon']);
		$nombre_capacitador 		= strtoupper($POST['nombre_capacitador']);
		$tipo_doc_capacitador 		= $POST['tipo_doc_capacitador'];
		$num_doc_capacitador 		= $POST['num_doc_capacitador'];
		$nombre_capacitador_s1 		= strtoupper($POST['nombre_capacitador_s1']);
		$tipo_doc_capacitador_s1 	= $POST['tipo_doc_capacitador_s1'];
		$num_doc_capacitador_s1 	= ($POST['num_doc_capacitador_s1'] == null) ? 0 : $POST['num_doc_capacitador_s1'];
		$nombre_capacitador_s2		= strtoupper($POST['nombre_capacitador_s2']);
		$tipo_doc_capacitador_s2 	= $POST['tipo_doc_capacitador_s2'];
		$num_doc_capacitador_s2 	= ($POST['num_doc_capacitador_s2'] == null) ? 0 : $POST['num_doc_capacitador_s2'];
		$perfil_capacitador 		= $POST['perfil_capacitador'];
		$tipo_capacitador 			= $POST['tipo_capacitador'];

		// Evaluamos el numero de la sesion a registrar
		$sql = "SELECT COUNT(*) cantidad FROM cronograma_presencial WHERE id_cronograma = '".$id_cronograma."' AND num_sesion = '".$num_sesion."' AND estado_registro = 'ACTIVO'";
		$consulta = $db->createCommand($sql)->queryOne();

		if ($consulta['cantidad'] == 0) {
			// Procedemos con el registro de la sesion

			$id_cronograma_presencial = Generalidades::getConsecutivo('cronograma_presencial','id_cronograma_presencial');

			$sql2 = "INSERT INTO cronograma_presencial(id_cronograma_presencial, id_cronograma, nombre_sesion, num_sesion, fecha_inicio, hora_inicial, hora_final, num_hrs_totales, id_ciudad, nombre_sede, direccion_sede, aula_salon, nombre_capacitador, tipo_doc_capacitador, num_doc_capacitador, nombre_capacitador_s1, tipo_doc_capacitador_s1, num_doc_capacitador_s1, nombre_capacitador_s2, tipo_doc_capacitador_s2, num_doc_capacitador_s2, perfil_capacitador, tipo_capacitador, fecha_registro, estado_registro) VALUES ('".$id_cronograma_presencial."','".$id_cronograma."','".$nombre_sesion."','".$num_sesion."','".$fecha_inicio."','".$hora_inicial."','".$hora_final."','".$num_hrs_totales."','".$id_ciudad."','".$nombre_sede."','".$direccion_sede."','".$aula_salon."','".$nombre_capacitador."','".$tipo_doc_capacitador."','".$num_doc_capacitador."','".$nombre_capacitador_s1."','".$tipo_doc_capacitador_s1."','".$num_doc_capacitador_s1."','".$nombre_capacitador_s2."','".$tipo_doc_capacitador_s2."','".$num_doc_capacitador_s2."','".$perfil_capacitador."','".$tipo_capacitador."',NOW(),'ACTIVO')";

			$insert = $db->createCommand($sql2)->execute();

			if ($insert) {
				$response = json_encode([
					'status' 	=> 'success',
					'message'	=> 'Se ha realizado el registro de la <b>sesión número '.$num_sesion.'</b> correctamente.',
					'alert'		=> 'alert-success'
				]);
			}else{
				$response = json_encode([
					'status'	=> 'error',
					'message' 	=> 'No fue posible registrar la sesión en el sistema.',
					'alert'		=> 'alert-warning'
				]);
			}

		}else{
			$response = json_encode([
				'status' 	=> 'error',
				'message'	=> 'No es posible realizar el registro, el número de sesión ya se encuentra en uso.',
				'alert'		=> 'alert-warning'
			]);
		}

		return $response;
	}

	// Registrar actividad virtual al cronograma seleccionado
	public function registrarActividadVirtual($POST)
	{
		$db = Mysql::connection();

		$id_cronograma 			= $POST['id_cronograma'];
		$nombre_actividad 		= strtoupper($POST['nombre_actividad']);
		$fecha_inicio 			= $POST['fecha_inicio'];
		$fecha_fin 				= $POST['fecha_fin'];
		$proveedor_formacion 	= strtoupper($POST['proveedor_formacion']);
		$url_plataforma 		= $POST['url_plataforma'];
		$usuario_sena 			= $POST['usuario_sena'];
		$clave_sena 			= $POST['clave_sena'];
		$nombre_capacitador 	= strtoupper($POST['nombre_capacitador']);
		$tipo_doc_capacitador 	= $POST['tipo_doc_capacitador'];
		$num_doc_capacitador 	= $POST['num_doc_capacitador'];
		$perfil_capacitador 	= $POST['perfil_capacitador'];
		$tipo_capacitador 		= $POST['tipo_capacitador'];

		
		// Procedemos con el registro de la sesion

		$id_cronograma_virtual = Generalidades::getConsecutivo('cronograma_virtual','id_cronograma_virtual');

		$sql = "INSERT INTO cronograma_virtual(id_cronograma_virtual, id_cronograma, nombre_actividad, fecha_inicio_actividad, fecha_fin_actividad, proveedor_formacion, url_plataforma, usuario_sena, clave_sena, nombre_capacitador, tipo_doc_capacitador, num_doc_capacitador, perfil_capacitador, tipo_capacitador, fecha_registro, estado_registro) VALUES ('".$id_cronograma_virtual."','".$id_cronograma."','".$nombre_actividad."','".$fecha_inicio."','".$fecha_fin."','".$proveedor_formacion."','".$url_plataforma."','".$usuario_sena."','".$clave_sena."','".$nombre_capacitador."','".$tipo_doc_capacitador."','".$num_doc_capacitador."','".$perfil_capacitador."','".$tipo_capacitador."',NOW(),'ACTIVO')";

		$insert = $db->createCommand($sql)->execute();

		if ($insert) {
			$response = json_encode([
				'status' 	=> 'success',
				'message'	=> 'Se ha realizado el registro de la actividad <b>'.$nombre_actividad.'</b> correctamente.',
				'alert'		=> 'alert-success'
			]);
		}else{
			$response = json_encode([
				'status'	=> 'error',
				'message' 	=> 'No fue posible registrar la actividad en el sistema.',
				'alert'		=> 'alert-warning'
			]);
		}

		return $response;
	}

	// Eliminar sesion presencial
	public function eliminarSesionPresencial($id_cronograma_presencial)
	{
		$db = Mysql::connection();

		$sql = "UPDATE cronograma_presencial SET estado_registro = 'INACTIVO' WHERE id_cronograma_presencial = '".$id_cronograma_presencial."'";
		$update = $db->createCommand($sql)->execute();

		if ($update) {
			$response = json_encode([
				'status' 	=> 'success',
				'message'	=> 'Se ha eliminado la sesión correctamente.',
				'alert' 	=> 'alert-success'
			]);
		}else{
			$response = json_encode([
				'status' 	=> 'error',
				'message'	=> 'Ha ocurrido un error al eliminar la sesión.',
				'alert' 	=> 'alert-warning'
			]);
		}

		return $response;

	}

	// Eliminar actividad virtual
	public function eliminarActividadVirtual($id_cronograma_virtual)
	{
		$db = Mysql::connection();

		$sql = "UPDATE cronograma_virtual SET estado_registro = 'INACTIVO' WHERE id_cronograma_virtual = '".$id_cronograma_virtual."'";
		$update = $db->createCommand($sql)->execute();

		if ($update) {
			$response = json_encode([
				'status' 	=> 'success',
				'message'	=> 'Se ha eliminado la actividad correctamente.',
				'alert' 	=> 'alert-success'
			]);
		}else{
			$response = json_encode([
				'status' 	=> 'error',
				'message'	=> 'Ha ocurrido un error al eliminar la actividad.',
				'alert' 	=> 'alert-warning'
			]);
		}

		return $response;

	}

}