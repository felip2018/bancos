<?php

namespace app\models;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use yii\base\Model;

use app\models\Mysql;
use app\models\Generalidades;

class Grupos extends Model
{
	// Consultar listado de acciones de formacion del proyecto en sesion
	public function obtenerAccionesFormacion($id_proyecto)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				acciones_formacion.id_accion_formacion,
				acciones_formacion.id_proyecto,
				acciones_formacion.nombre,
				acciones_formacion.beneficiarios_empresa,
				acciones_formacion.beneficiarios_sena,
				(acciones_formacion.beneficiarios_empresa + acciones_formacion.beneficiarios_sena) AS total_beneficiarios,
				acciones_formacion.numero_grupos
				FROM acciones_formacion 
				WHERE id_proyecto = " . $id_proyecto . "
				AND nombre != 'TRANSFERENCIA' 
				AND estado_registro = 'ACTIVO'
				ORDER BY acciones_formacion.nombre ASC";

		$consulta = $db->createCommand($sql)->queryAll();
        return $consulta;
	}

	// Consultar grupos creados por accion de formacion
	public function gruposCount($id_accion_formacion)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				COUNT(*) cant
				FROM accion_formacion_grupos
				WHERE id_accion_formacion = " . $id_accion_formacion . " 
				AND estado_registro = 'ACTIVO'";

		$consulta = $db->createCommand($sql)->queryOne();
        return $consulta['cant'];	
	}

	// Registrar los grupos de las acciones de formacion
	public function crearGruposAF($id_proyecto)
	{
		$db = Mysql::connection();

		// Consultamos las acciones de formacion registradas en el proyecto
		$sql1 = "SELECT 
					id_accion_formacion,
					nombre,
					numero_grupos
					FROM acciones_formacion
					WHERE id_proyecto = '" . $id_proyecto . "'";

		$consulta1 = $db->createCommand($sql1)->queryAll();

		if (!empty($consulta1)) {
			// Recorremos el arreglo de acciones de formacion
			foreach ($consulta1 as $key => $value) {
				// Evaluamos si hay grupos creados para la accion de formacion
				$sql2 = "SELECT 
							COUNT(*) cant 
							FROM accion_formacion_grupos 
							WHERE id_accion_formacion = '". $value['id_accion_formacion'] ."'";

				$consulta2 = $db->createCommand($sql2)->queryOne();

				if ($consulta2['cant'] == 0) {
					// Procedemos a registrar los grupos de la accion de formacion

					for ($i=0; $i < $value['numero_grupos']; $i++) { 
						

						$id_grupo = Generalidades::getConsecutivo('accion_formacion_grupos','id_grupo');
						$nombre = "Grupo numero " . ($i+1);

						$sql3 = "INSERT INTO accion_formacion_grupos(id_grupo, id_accion_formacion, nombre, fecha_registro, estado_registro) VALUES (". $id_grupo .",". $value['id_accion_formacion'] .",'". $nombre ."',NOW(),'ACTIVO')";

						$consulta3 = $db->createCommand($sql3)->execute();

					}

				}

			}

			$response = json_encode([
				'status' 	=> 'success',
				'message' 	=> 'Se han generado los grupos para las acciones de formación.'
			]);

		}else{
			// No se han encontrado acciones de formacion para el proyecto
			$response = json_encode([
				'status' 	=> 'error',
				'message' 	=> 'No se han encontrado acciones de formación para el proyecto.'
			]);
		}
		return $response;
	}

	// Registrar grupos de una accion de formacion seleccionada
	public function crearGruposAccionFormacion($id_proyecto,$id_accion_formacion,$numero_grupos)
	{
		$db = Mysql::connection();

		// Consultamos si la accion de formacion ya tiene grupos registrados
		$sql = "SELECT COUNT(*) cant 
				FROM accion_formacion_grupos
				WHERE id_accion_formacion = '".$id_accion_formacion."'";

		$consulta = $db->createCommand($sql)->queryOne();

		if ($consulta['cant'] == 0) {
			// Registramos los grupos de la accion de formacion

			$errores = 0;

			for ($i=1; $i <= $numero_grupos; $i++) { 
				
				$id_grupo = Generalidades::getConsecutivo('accion_formacion_grupos','id_grupo');
				$nombre = "Grupo numero " . ($i);

				$sql1 = "INSERT INTO accion_formacion_grupos(id_grupo, id_accion_formacion, nombre, fecha_registro, estado_registro) VALUES (".$id_grupo.",".$id_accion_formacion.",'".$nombre."',NOW(),'ACTIVO')";

				$insert = $db->createCommand($sql1)->execute();

				if (!$insert) {
					$errores++;
				}

			}

			if ($errores == 0) {
				$response = json_encode([
					'status'	=> 'Registro exitoso',
					'message'	=> 'Se han registrados los grupos de la acción de formación de manera exitosa.',
					'alert'		=> 'alert-success'
				]);
			}else{
				$response = json_encode([
					'status'	=> 'Error de consulta',
					'message'	=> 'Ha ocurrido un error al registrar los grupos.',
					'alert'		=> 'alert-danger'
				]);
			}

		}else{
			// Los grupos de la accion de formacion ya estan registrados en el sistema
			$response = json_encode([
				'status'	=> 'Error por validacion',
				'message'	=> 'Ya se han registrados los grupos de la acción de formación seleccionada.',
				'alert'		=> 'alert-danger'
			]);
		}

		return $response;
	}

	// Consultar acciones de formacion
	public function consultarAccionesFormacion($id_proyecto)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				acciones_formacion.id_accion_formacion,
				acciones_formacion.id_proyecto,
				acciones_formacion.nombre,
				acciones_formacion.beneficiarios_empresa,
				acciones_formacion.beneficiarios_sena,
				(acciones_formacion.beneficiarios_empresa + acciones_formacion.beneficiarios_sena) AS total_beneficiarios,
				acciones_formacion.numero_grupos,
                (SELECT COUNT(*) FROM accion_formacion_grupos WHERE acciones_formacion.id_accion_formacion = accion_formacion_grupos.id_accion_formacion) grupos_registrados
				FROM acciones_formacion 
				WHERE id_proyecto = ".$id_proyecto." 
				AND estado_registro = 'ACTIVO'";

		$consulta = $db->createCommand($sql)->queryAll();

		if (!empty($consulta)) {
			$response = json_encode([
				'status' 	=> 'success',
				'acciones'	=> $consulta
			]);
		}else{
			$response = json_encode([
				'status' 	=> 'vacio',
				'message'	=> 'No se han encontrado acciones de formación para el presente proyecto'
			]);
		}

        return $response;
	}

	// Consultar información de la acción de formación
	public function infoAccionFormacion($id_accion_formacion)
	{
		$db = Mysql::connection();

		$sql = "SELECT * FROM acciones_formacion WHERE id_accion_formacion = '". $id_accion_formacion ."'";

		$consulta = $db->createCommand($sql)->queryOne();

		return $consulta;
	}

	// Consultar grupos de la accion de formacion
	public function gruposAccionFormacion($id_accion_formacion)
	{
		$db = Mysql::connection();

		$sql = "SELECT * FROM accion_formacion_grupos WHERE id_accion_formacion = '". $id_accion_formacion ."'";

		$consulta = $db->createCommand($sql)->queryAll();

		return $consulta;	
	}

	// Consultar beneficiarios registrados en una grupo
	public function buscarBeneficiarios($id_grupo)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				beneficiarios_grupo.id_proyecto_beneficiario,
				beneficiarios_grupo.id_accion_formacion,
				beneficiarios_grupo.id_grupo,
				CONCAT(beneficiarios.nombres,' ',beneficiarios.apellido_1,' ',beneficiarios.apellido_2) AS nombre,
				CONCAT(beneficiarios.tipo_doc,' ',beneficiarios.num_doc) AS identificacion,
				beneficiarios_grupo.estado_registro
				FROM beneficiarios
				INNER JOIN proyecto_beneficiarios ON proyecto_beneficiarios.id_beneficiario = beneficiarios.id_beneficiario
				INNER JOIN beneficiarios_grupo ON beneficiarios_grupo.id_proyecto_beneficiario = proyecto_beneficiarios.id_proyecto_beneficiario
				WHERE beneficiarios_grupo.id_grupo = '".$id_grupo."'";

		$consulta = $db->createCommand($sql)->queryAll();

		if (!empty($consulta)) {
			$response = json_encode([
				'status' 	=> 'success',
				'listado'	=> $consulta
			]);
		}else{
			$response = json_encode([
				'status'	=> 'vacio',
				'message'	=> 'No se han encontrado beneficiarios registrados para el grupo seleccionado.'
			]);
		}

		return $response;
	}

	// Consultar informacion de beneficiario por tipo y numero de identificacion
	public function buscarBeneficiarioDoc($tipo_documento,$num_documento,$id_proyecto)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				beneficiarios.id_beneficiario,
				beneficiarios.tipo_doc,
				beneficiarios.num_doc,
				beneficiarios.nombres,
				beneficiarios.apellido_1,
				beneficiarios.apellido_2,
				proyecto_beneficiarios.id_proyecto_beneficiario,
				beneficiarios_grupo.id_grupo,
				beneficiarios_grupo.edad,
				beneficiarios_grupo.rango_edad,
				beneficiarios_grupo.antiguedad,
				beneficiarios_grupo.empresa_labora,
				beneficiarios_grupo.tamanio_empresa,
				beneficiarios_grupo.transferencia,
				beneficiarios_grupo.perfil_transferencia,
				beneficiarios_grupo.caracterizacion,
				beneficiarios_grupo.nivel_ocupacional
				FROM beneficiarios
				INNER JOIN proyecto_beneficiarios ON proyecto_beneficiarios.id_beneficiario = beneficiarios.id_beneficiario
				LEFT JOIN beneficiarios_grupo ON beneficiarios_grupo.id_proyecto_beneficiario = proyecto_beneficiarios.id_proyecto_beneficiario
				INNER JOIN proyectos ON proyectos.id_proyecto = proyecto_beneficiarios.id_proyecto
				WHERE beneficiarios.tipo_doc = '".$tipo_documento."'
				AND beneficiarios.num_doc = '".$num_documento."'
				AND proyectos.id_proyecto = '".$id_proyecto."'";

		$consulta = $db->createCommand($sql)->queryOne();

		if (!empty($consulta)) {
			// Se ha encontrado informacion
			$response = json_encode([
				'status'	=> 'success',
				'datos'		=> $consulta
			]);
		}else{
			// No hay datos de usuario
			$response = json_encode([
				'status' 	=> 'error',
				'message'	=> 'No se han encontrado datos del beneficiario.'
			]);
		}

		return $response;
	}

	// Agregar beneficiario a un grupo seleccionado
	public function agregarBeneficiario($POST,$id_proyecto)
	{
		$db = Mysql::connection();

		$id_grupo 			= $POST['id_grupo'];
		$id_beneficiario 	= $POST['id_beneficiario'];
		$tipo_doc 			= $POST['tipo_doc'];
		$num_doc 			= $POST['num_doc'];

		// Consultamos el id_proyecto_beneficiario para el beneficiario registrado
		$sql1 = "SELECT  
				id_proyecto_beneficiario
				FROM proyecto_beneficiarios
				WHERE id_proyecto = '". $id_proyecto ."'
				AND id_beneficiario = '". $id_beneficiario ."'
				AND estado_registro = 'ACTIVO'";

		$consulta1 = $db->createCommand($sql1)->queryOne();

		if (!empty($consulta1)) {

			// Consultamos la informacion del grupo seleccionado
			$sql2 = "SELECT 
					id_grupo,
					id_accion_formacion
					FROM accion_formacion_grupos
					WHERE id_grupo = '". $id_grupo ."'";

			$consulta2 = $db->createCommand($sql2)->queryOne();

			// Consultamos si el beneficiario esta registrado en la accion de formacion en cualquier grupo
			$sql3 = "SELECT 
					beneficiarios_grupo.id_proyecto_beneficiario,
					beneficiarios_grupo.id_accion_formacion,
					beneficiarios_grupo.id_grupo,
					accion_formacion_grupos.nombre
					FROM beneficiarios_grupo
					INNER JOIN accion_formacion_grupos ON accion_formacion_grupos.id_grupo = beneficiarios_grupo.id_grupo
					WHERE beneficiarios_grupo.id_proyecto_beneficiario = '". $consulta1['id_proyecto_beneficiario'] ."'
					AND beneficiarios_grupo.id_accion_formacion = '". $consulta2['id_accion_formacion'] ."'";

			$consulta3 = $db->createCommand($sql3)->queryOne();

			//$rango_edad = Generalidades::calcularRangoEdad($POST['edad']);

			//if ($consulta3['cant'] == 0) {
			if (empty($consulta3)) {

				// Procedemos a registrar el beneficiario en el grupo seleccionado

				// 1.Consultamos los datos de postulacion del beneficiario en el proyecto actual

				$sql_info = "SELECT 
							edad,
							rango_edad,
							antiguedad,
							empresa_labora,
							tamanio_empresa,
							transferencia,
							perfil_transferencia,
							caracterizacion,
							nivel_ocupacional
							FROM beneficiarios_grupo
							WHERE id_proyecto_beneficiario = '".$consulta1['id_proyecto_beneficiario']."'
							LIMIT 1";

				$info = $db->createCommand($sql_info)->queryOne();


				$sql_insert = "INSERT INTO beneficiarios_grupo(id_proyecto_beneficiario, id_accion_formacion, id_grupo, edad, rango_edad, antiguedad, empresa_labora, tamanio_empresa, transferencia, perfil_transferencia, caracterizacion, nivel_ocupacional, hrs_asistencia, por_asistencia, hrs_cumplimiento, por_cumplimiento, certifica, fecha_registro, estado_registro) VALUES ('".$consulta1['id_proyecto_beneficiario']."','".$consulta2['id_accion_formacion']."','".$id_grupo."','".$info['edad']."','".$info['rango_edad']."','".$info['antiguedad']."','".$info['empresa_labora']."','".$info['tamanio_empresa']."','".$info['transferencia']."','".$info['perfil_transferencia']."','".$info['caracterizacion']."','".$info['nivel_ocupacional']."',0,0,0,0,'',NOW(),'ACTIVO')";

				$insert = $db->createCommand($sql_insert)->execute();

				if ($insert) {
					// Se ha registrado el beneficiario en el grupo
					$response = json_encode([
						'status' 	=> 'success',
						'message'	=> 'El beneficiario ha sido registrado correctamente.',
						'alert'		=> 'alert-success'
					]);
				}else{
					// Ha ocurrido un error
					$response = json_encode([
						'status' 	=> 'error',
						'message'	=> 'No se ha podido registrar el beneficiario en el grupo.<br>'.$sql4,
						'alert'		=> 'alert-warning'
					]);
				}

			}else{
				// El beneficiario ya se encuentra registrado
				$response = json_encode([
					'status' 	=> 'error',
					'message'	=> 'El beneficiario ya se encuentra registrado en la presente acción de formación en el grupo: <b>'.$consulta3['nombre'].'</b>',
					'alert'		=> 'alert-warning'
				]);
			}
			

		}else{
			// El beneficiario no se encuentra asociado al presente proyecto
			$response = json_encode([
				'status' 	=> 'vacio',
				'message'	=> 'El beneficiario seleccionado no se encuentra registrado en el proyecto actual.'
			]);
		}

		return $response;
	}

	// Actualizar estado de un beneficiario en un grupo seleccionado
	public function estadoBeneficiarioGrupo($id_proyecto_beneficiario,$id_accion_formacion,$id_grupo,$estado)
	{
		$db = Mysql::connection();

		$sql = "UPDATE 	beneficiarios_grupo
				SET 	estado_registro = '".$estado."'
				WHERE 	id_proyecto_beneficiario 	= '".$id_proyecto_beneficiario."'
				AND 	id_accion_formacion 		= '".$id_accion_formacion."'
				AND 	id_grupo 					= '".$id_grupo."'";

		$update = $db->createCommand($sql)->execute();

		if ($update) {
			// Se ha actualizado el estado correctamente
			$response = json_encode([
				'status' => 'success'
			]);
		}else{
			// Ha ocurrido un error
			$response = json_encode([
				'status' 	=> 'error',
				'message'	=> 'No se pudo completar el proceso.<br>'.$sql
			]);
		}

		return $response;
	}

	// Buscar grupos de la accion de formacion para realizar un cambio de grupo de un beneficiario
	public function buscarGruposCambio($id_accion_formacion,$id_grupo)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				id_grupo,
				id_accion_formacion,
				nombre
				FROM accion_formacion_grupos
				WHERE id_accion_formacion = '". $id_accion_formacion ."'
				AND id_grupo != '". $id_grupo ."'";

		$consulta = $db->createCommand($sql)->queryAll();

		if (!empty($consulta)) {
			
			$response = json_encode([
				'status' 	=> 'success',
				'listado'	=> $consulta,
				'message'	=> 'Grupos disponibles para realizar el cambio.'
			]);

		}else{

			$response = json_encode([
				'status' 	=> 'vacio',
				'message' 	=> 'No se ha encontrado grupos disponibles para realizar el cambio.'
			]);

		}

		return $response;
	}

	// Actualizar grupo de un beneficiario seleccionado
	public function actualizarGrupoBeneficiario($id_proyecto_beneficiario,$id_accion_formacion,$id_grupo)
	{
		$db = Mysql::connection();

		$sql = "UPDATE beneficiarios_grupo
				SET id_grupo = '".$id_grupo."'
				WHERE id_proyecto_beneficiario = '".$id_proyecto_beneficiario."'
				AND id_accion_formacion = '".$id_accion_formacion."'";

		$update = $db->createCommand($sql)->execute();

		if ($update) {
			// Se ha actualizado el grupo del beneficiario
			$response = json_encode([
				'status' 	=> 'success',
				'message' 	=> 'Se ha actualizado el grupo del beneficiario de forma exitosa.',
				'alert'		=> 'alert-success'
			]);
		}else{
			// Ha ocurrido un error
			$response = json_encode([
				'status' 	=> 'error',
				'message' 	=> 'Ha ocurrido un error al actualizar el grupo del beneficiario.<br>'+$sql,
				'alert'		=> 'alert-danger'
			]);
		}

		return $response;
	}

	// Consultar numero de beneficiarios registrados por accion de formacion
	public function beneficiariosRegistrados($id_accion_formacion)
	{
		$db = Mysql::connection();

		$sql = "SELECT COUNT(*) cant FROM beneficiarios_grupo WHERE id_accion_formacion = '".$id_accion_formacion."' AND estado_registro = 'ACTIVO'";

		$beneficiarios = $db->createCommand($sql)->queryOne();

		return $beneficiarios['cant'];
	}
	
}