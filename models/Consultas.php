<?php

namespace app\models;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\base\Model;

use app\models\Mysql;

class Consultas extends Model
{
	// Consulta estadistica del panel de inicio
	public function informacionPanelInicio($id_proyecto)
	{
		$db = Mysql::connection();

		$sql = 'SELECT 
				(SELECT COUNT(*) FROM acciones_formacion WHERE id_proyecto = '. $id_proyecto .' AND estado_registro = "ACTIVO" AND nombre != "TRANSFERENCIA") acciones_formacion,
				(SELECT COUNT(*) FROM accion_formacion_grupos INNER JOIN acciones_formacion ON acciones_formacion.id_accion_formacion = accion_formacion_grupos.id_accion_formacion WHERE acciones_formacion.id_proyecto = '. $id_proyecto .') grupos,
                (SELECT COUNT(*) FROM proyecto_beneficiarios WHERE id_proyecto = '. $id_proyecto .' AND estado_registro = "ACTIVO") beneficiarios
				FROM DUAL';

		$consulta = $db->createCommand($sql)->queryAll();
        return $consulta;
	}

	// Consultar numero de beneficiarios registrados por acciones de formacion del proyecto en sesion
	public function beneficiariosAccionesFormacion($id_proyecto)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				acciones_formacion.nombre,
				(SELECT COUNT(*) FROM beneficiarios_grupo WHERE beneficiarios_grupo.id_accion_formacion = acciones_formacion.id_accion_formacion AND beneficiarios_grupo.estado_registro = 'ACTIVO') beneficiarios
				FROM acciones_formacion
				INNER JOIN proyectos ON proyectos.id_proyecto = acciones_formacion.id_proyecto
				WHERE proyectos.id_proyecto = '".$id_proyecto."'";

		$consulta = $db->createCommand($sql)->queryAll();
        return $consulta;
	}

	// Consultar informacion registrada del director del proyecto
	public function informacionDirectorProyecto($id_proyecto)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				directores.id_director,
				CONCAT(directores.tipo_doc,' ',directores.num_doc) AS identificacion,
				CONCAT(directores.nombres,' ',directores.primer_apellido,' ',directores.segundo_apellido) AS nombre,
				directores.correo
				FROM directores 
				LEFT JOIN directores_experiencias ON directores_experiencias.id_director = directores.id_director
				LEFT JOIN directores_proyectos ON directores_proyectos.id_director = directores.id_director
				WHERE directores_proyectos.id_proyecto = '".$id_proyecto."'";

		$consulta = $db->createCommand($sql)->queryOne();

		return $consulta;
	}

	// Consultar los documentos registrados del director de proyecto registrado
	public function documentosDirectorProyecto($id_proyecto)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				directores_documentos.id_director_documento,
				directores_documentos.id_director,
				documentos_bancos.input_name,
				directores_documentos.url,
				DATE_FORMAT(directores_documentos.fecha_registro,'%Y') AS year,
				documentos.nombre,
				directores.num_doc
				FROM directores_documentos
				INNER JOIN documentos_bancos ON documentos_bancos.id_documento_banco = directores_documentos.id_documento_banco
				INNER JOIN documentos ON documentos.id_documento = documentos_bancos.id_documento
				INNER JOIN directores ON directores.id_director = directores_documentos.id_director
				INNER JOIN directores_proyectos ON directores_proyectos.id_director = directores.id_director
				WHERE directores_proyectos.id_proyecto = '".$id_proyecto."'";

		$consulta = $db->createCommand($sql)->queryAll();

		return $consulta;	

	}

	// Consultar numero de beneficiarios estimados por acciones de formacion del proyecto en sesion
	public function beneficiariosEstimadosAccionFormacion($id_proyecto)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				acciones_formacion.id_proyecto,
				acciones_formacion.nombre,
				(acciones_formacion.beneficiarios_empresa + acciones_formacion.beneficiarios_sena) AS total_beneficiarios
				FROM acciones_formacion
				WHERE acciones_formacion.id_proyecto = '".$id_proyecto."'
				AND acciones_formacion.nombre != 'TRANSFERENCIA'
				AND acciones_formacion.estado_registro = 'ACTIVO'
				ORDER BY acciones_formacion.nombre ASC";

		$consulta = $db->createCommand($sql)->queryAll();

		return $consulta;	
	}

	// Consultar numero de beneficiarios registrados por acciones de formacion del proyecto en sesion
	public function beneficiariosRegistradosAccionFormacion($id_proyecto)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				acciones_formacion.id_proyecto,
				acciones_formacion.nombre,
				(SELECT COUNT(*) FROM beneficiarios_grupo WHERE beneficiarios_grupo.id_accion_formacion = acciones_formacion.id_accion_formacion) AS registrados
				FROM acciones_formacion
				WHERE acciones_formacion.id_proyecto = '".$id_proyecto."'
				AND acciones_formacion.nombre != 'TRANSFERENCIA'
				AND acciones_formacion.estado_registro = 'ACTIVO'
				ORDER BY acciones_formacion.nombre ASC";

		$consulta = $db->createCommand($sql)->queryAll();

		return $consulta;	
	}
}