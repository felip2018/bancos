<?php
namespace app\models;

use Yii;
use yii\base\Model;

use app\models\Mysql;
use app\models\Generalidades;

class Recovery extends Model 
{
	public $email;
	public $password;
	public $repeat_password;


	public function rules()
    {
        return [
            // username and password are both required
            [
              [
                'email',
                'password',
                'repeat_password',
              ], 'required', 'message' => 'Este campo no puede estar vacío.'
            ],
            [
              [
                'password',
                'repeat_password',
              ],'string', 'message' => 'Este campo es de tipo texto'
            ],
            [
              [
                'email',
              ], 'email', 'message' => 'El valor ingresado no es una dirección de correo válida.'
            ],
        ];
    }

    // Función para recuperación de cuenta
    public function accountRecovery()
    {
    	$db = Mysql::connection();

    	$sql = 'SELECT Nick, Clave, Token, Estado FROM Usuarios_Entidad WHERE Nick = "' . $this->email . '"';
    	$consulta = $db->createCommand($sql)->queryOne();

    	if (!empty($consulta)) {
    		
    		if ($consulta['Estado'] == 'BLOQUEO DE SEGURIDAD') {
    		
	    		// Actualizamos la clave y activamos la cuenta nuevamente

	    		$clave_encriptada = Generalidades::encriptarClave($this->password);

	    		$sql_2 = 'UPDATE Usuarios_Entidad SET clave = "' . $clave_encriptada . '" AND Estado = "ACTIVO" WHERE Nick = "' . $this->email . '"';

	    		$update = $db->createCommand($sql_2)->execute();

	    		if ($update) {
	    			// Actualizacion exitosa
	    			$response = json_encode([
	    				'status' 	=> 'Recuperación de cuenta.',
	    				'message'	=> 'Se ha realizado la recuperación de su cuenta correctamente, por favor dirijase a la sección de inicion de sesión para acceder al sistema.',
	    				'alert'		=> 'alert-success'
	    			]);
	    		}else {
	    			// Error de actualizacion
	    			$response = json_encode([
	    				'status' 	=> 'Error',
	    				'message'	=> 'Ha ocurrido un error.<br>'.$update,
	    				'alert'		=> 'alert-warning'
	    			]);
	    		}

	    	}else {
	    		// La cuenta no esta bloqueada
	    		$response = json_encode([
	    			'status' 	=> 'Advertencia',
	    			'message'	=> 'No es posible realizar la recuperación de la cuenta con estado: <b>' . $consulta['Estado'] . '</b>',
	    			'alert' 	=> 'alert-warning'
	    		]);
	    	}

    	}else {
    		// El email no corresponde
    		$response = json_encode([
    			'status' 	=> 'Error',
    			'message'	=> 'El usuario ingresado no corresponde a una cuenta registrada.',
    			'alert' 	=> 'alert-danger'
    		]);
    	}
    	return $response;
    }

}

?>