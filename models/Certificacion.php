<?php

namespace app\models;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use yii\base\Model;

use app\models\Mysql;
use app\models\Generalidades;

class Certificacion extends Model
{
	
	// Consultar listado de beneficiarios de la accion de formacion
	public function beneficiariosAccionFormacion($id_accion_formacion)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				beneficiarios_grupo.id_proyecto_beneficiario,
				beneficiarios_grupo.id_accion_formacion,
				beneficiarios_grupo.id_grupo,
				beneficiarios_grupo.hrs_asistencia,
				beneficiarios_grupo.por_asistencia,
				beneficiarios_grupo.hrs_cumplimiento,
				beneficiarios_grupo.por_cumplimiento,
				beneficiarios_grupo.certifica,
				proyecto_beneficiarios.id_proyecto,
				proyecto_beneficiarios.id_beneficiario,
				beneficiarios.tipo_doc,
				beneficiarios.num_doc,
				beneficiarios.nombres,
				CONCAT(beneficiarios.nombres,' ',beneficiarios.apellido_1,' ',beneficiarios.apellido_2) AS nombre
				FROM beneficiarios_grupo
				INNER JOIN proyecto_beneficiarios ON proyecto_beneficiarios.id_proyecto_beneficiario = beneficiarios_grupo.id_proyecto_beneficiario
				INNER JOIN beneficiarios ON beneficiarios.id_beneficiario = proyecto_beneficiarios.id_beneficiario
				WHERE beneficiarios_grupo.id_accion_formacion = '".$id_accion_formacion."'
				AND beneficiarios_grupo.estado_registro = 'ACTIVO'
				ORDER BY beneficiarios.nombres ASC";

		$beneficiarios = $db->createCommand($sql)->queryAll();

		if (!empty($beneficiarios)) {

			$response = json_encode([
				'status' => 'success',
				'beneficiarios' => $beneficiarios
			]);

		}else{
			$response = json_encode([
				'status' => 'vacio',
			]);
		}

		return $response;
	}

	// Obtener informacion de la accion de formacion
	public function infoAccionFormacion($id_accion_formacion)
	{
		$db = Mysql::connection();

		$sql = "SELECT 
				id_proyecto,
				nombre,
				beneficiarios_empresa,
				beneficiarios_sena,
				numero_grupos,
				dias_grupo,
				total_dias,
				modalidad,
				presencial_teorica,
				presencial_practica,
				virtual_teorica,
				virtual_practica,
				evento_formacion
				FROM acciones_formacion
				WHERE id_accion_formacion = '".$id_accion_formacion."'";

		$accion = $db->createCommand($sql)->queryOne();

		if (!empty($accion)) {
			
			$response = json_encode([
				'status' 	=> 'success',
				'accion'	=> $accion
			]);

		}else{

			$response = json_encode([
				'status' 	=> 'vacio'
			]);

		}

		return $response;
	}

	// Registrar certificacion de beneficiarios modalidad presencial
	public function registrarCertificacionPresencial($POST)
	{
		$db = Mysql::connection();
		
		$continuar = true;
		$query = "";
		//$respuesta = json_encode(['status'=>'test','Arreglo'=>$POST]);
		$respuesta = "";

		if (!empty($POST['arregloRegistrar'])) {
			
			
			foreach ($POST['arregloRegistrar'] as $key => $value) {
				$beneficiario = explode('_', $value[0]);

				$id_proyecto_beneficiario 	= $beneficiario[0];
				$id_accion_formacion 		= $beneficiario[1];
				$id_grupo 					= $beneficiario[2];
				$hrs_asistencia				= $value[1];
				$por_asistencia 			= $value[2];
				$certifica 					= $value[3];

				$sql = "UPDATE 	beneficiarios_grupo
						SET 	hrs_asistencia = '".$hrs_asistencia."',
								por_asistencia = '".$por_asistencia."',
								certifica 	   = '".$certifica."'
						WHERE  	id_proyecto_beneficiario = '".$id_proyecto_beneficiario."'
						AND 	id_accion_formacion 	 = '".$id_accion_formacion."'
						AND 	id_grupo 				 = '".$id_grupo."'";

				//$respuesta = json_encode(['status'=>'test','SQL'=>$sql]);
				$update = $db->createCommand($sql)->execute();

				/*if ($update) {
					$continuar = true;
				}else{
					$continuar = false;
					$query = $sql;
					break;
				}*/

			}

			if ($continuar) {
				// Se ha realizado la certificacion de beneficiarios
				$respuesta = json_encode([
					'status' 	=> 'success',
					'message' 	=> 'Se ha realizado el proceso de certificacion correctamente.',
					'alert' 	=> 'alert-success'
				]);
			}else{
				// Ha ocurrido un error
				$respuesta = json_encode([
					'status' 	=> 'error',
					'message' 	=> 'Se ha presentado un error al realizar la certificación.<br>'.$query,
					'alert' 	=> 'alert-danger'
				]);
			}

		}else{
			$respuesta = json_encode([
				'status' 	=> 'vacio',
				'message'	=> 'El arreglo de beneficiarios se encuentra vacío.',
				'alert'		=> 'alert-warning'
			]);
		}

		return $respuesta;

	}

	// Registrar certificacion de beneficiarios modalidad virtual
	public function registrarCertificacionVirtual($POST)
	{
		$db = Mysql::connection();
		
		$continuar = true;
		$query = "";
		//$respuesta = json_encode(['status'=>'test','Arreglo'=>$POST]);
		$respuesta = "";

		if (!empty($POST['arregloRegistrar'])) {
			
			
			foreach ($POST['arregloRegistrar'] as $key => $value) {
				$beneficiario = explode('_', $value[0]);

				$id_proyecto_beneficiario 	= $beneficiario[0];
				$id_accion_formacion 		= $beneficiario[1];
				$id_grupo 					= $beneficiario[2];
				$hrs_cumplimiento			= $value[1];
				$por_cumplimiento 			= $value[2];
				$certifica 					= $value[3];

				$sql = "UPDATE 	beneficiarios_grupo
						SET 	hrs_cumplimiento = '".$hrs_cumplimiento."',
								por_cumplimiento = '".$por_cumplimiento."',
								certifica 	   	 = '".$certifica."'
						WHERE  	id_proyecto_beneficiario = '".$id_proyecto_beneficiario."'
						AND 	id_accion_formacion 	 = '".$id_accion_formacion."'
						AND 	id_grupo 				 = '".$id_grupo."'";

				//$respuesta = json_encode(['status'=>'test','SQL'=>$sql]);
				$update = $db->createCommand($sql)->execute();

				/*if ($update) {
					$continuar = true;
				}else{
					$continuar = false;
					$query = $sql;
					break;
				}*/

			}

			if ($continuar) {
				// Se ha realizado la certificacion de beneficiarios
				$respuesta = json_encode([
					'status' 	=> 'success',
					'message' 	=> 'Se ha realizado el proceso de certificacion correctamente.',
					'alert' 	=> 'alert-success'
				]);
			}else{
				// Ha ocurrido un error
				$respuesta = json_encode([
					'status' 	=> 'error',
					'message' 	=> 'Se ha presentado un error al realizar la certificación.<br>'.$query,
					'alert' 	=> 'alert-danger'
				]);
			}

		}else{
			$respuesta = json_encode([
				'status' 	=> 'vacio',
				'message'	=> 'El arreglo de beneficiarios se encuentra vacío.',
				'alert'		=> 'alert-warning'
			]);
		}

		return $respuesta;

	}

	// Registrar certificacion de beneficiarios modalidad combinada
	public function registrarCertificacionCombinada($POST)
	{
		$db = Mysql::connection();
		
		$continuar = true;
		$query = "";
		//$respuesta = json_encode(['status'=>'test','Arreglo'=>$POST]);
		$respuesta = "";

		if (!empty($POST['arregloRegistrar'])) {
			
			
			foreach ($POST['arregloRegistrar'] as $key => $value) {
				$beneficiario = explode('_', $value[0]);

				$id_proyecto_beneficiario 	= $beneficiario[0];
				$id_accion_formacion 		= $beneficiario[1];
				$id_grupo 					= $beneficiario[2];
				$hrs_asistencia 			= $value[1];
				$por_asistencia 			= $value[2];
				$hrs_cumplimiento			= $value[3];
				$por_cumplimiento 			= $value[4];
				$certifica 					= $value[5];

				$sql = "UPDATE 	beneficiarios_grupo
						SET 	hrs_asistencia   = '".$hrs_asistencia."',
								por_asistencia   = '".$por_asistencia."',
								hrs_cumplimiento = '".$hrs_cumplimiento."',
								por_cumplimiento = '".$por_cumplimiento."',
								certifica 	   	 = '".$certifica."'
						WHERE  	id_proyecto_beneficiario = '".$id_proyecto_beneficiario."'
						AND 	id_accion_formacion 	 = '".$id_accion_formacion."'
						AND 	id_grupo 				 = '".$id_grupo."'";

				//$respuesta = json_encode(['status'=>'test','SQL'=>$sql]);
				$update = $db->createCommand($sql)->execute();

				/*if ($update) {
					$continuar = true;
				}else{
					$continuar = false;
					$query = $sql;
					break;
				}*/

			}

			if ($continuar) {
				// Se ha realizado la certificacion de beneficiarios
				$respuesta = json_encode([
					'status' 	=> 'success',
					'message' 	=> 'Se ha realizado el proceso de certificacion correctamente.',
					'alert' 	=> 'alert-success'
				]);
			}else{
				// Ha ocurrido un error
				$respuesta = json_encode([
					'status' 	=> 'error',
					'message' 	=> 'Se ha presentado un error al realizar la certificación.<br>'.$query,
					'alert' 	=> 'alert-danger'
				]);
			}

		}else{
			$respuesta = json_encode([
				'status' 	=> 'vacio',
				'message'	=> 'El arreglo de beneficiarios se encuentra vacío.',
				'alert'		=> 'alert-warning'
			]);
		}

		return $respuesta;

	}
}