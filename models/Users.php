<?php

namespace app\models;

use Yii;
use yii\base\Model;

use app\models\Mysql;

class Users extends Model
{
    public $doctype;
    public $document;
    public $name;
    public $lastname;
    public $phone;
    public $email;
    public $address;
    public $gender;
    public $doctype_sponsor_dir;
    public $document_sponsor_dir;

    public $usersList = [];

    //  Reglas para los datos del formulario de registro
    public function rules()
    {
        return [
            [
                [
                    'doctype',
                    'document',
                    'name',
                    'lastname',
                    'phone',
                    'email',
                    'address',
                    'gender',
                    'doctype_sponsor_dir',
                    'document_sponsor_dir'
                ], 'required'],

            ['email', 'email'],
            [['doctype','name','lastname','phone','address','gender','doctype_sponsor_dir'], 'string'],
            [['document','document_sponsor_dir'], 'number'],
        ];
    }

    //  Registro de usuario en el sistema
    public function registerUser()
    {

    }

    // Consultar datos de usuario
    public function getUserInfo($docNumber)
    {
        $db = Mysql::connection();

        $sql = 'SELECT COUNT(*) cant FROM users WHERE document = "' . $docNumber . '"';

        $user = $db->createCommand($sql)->queryOne();

        if ($user['cant'] != 0) {
            $user = $db->createCommand('SELECT user_id, nit, doctype, document, name, lastname, email, phone, address FROM users WHERE document = "' . $docNumber . '"')->queryOne();
          $response = json_encode([
            'status'  => 'success',
            'message' => 'El usuario esta registrado en la base de datos',
            'info'    => $user
          ]);
        }else{
          $response = json_encode([
            'status'  => 'error',
            'message' => 'El usuario ingresado no existe.'
          ]);
        }

        return $response;
    }

    public function findIdentity()
    {

    }

}
