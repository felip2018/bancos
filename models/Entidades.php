<?php

namespace app\models;

use Yii;
use yii\base\Model;

use app\models\Mysql;
use app\models\Users;
use app\models\Generalidades;

class Entidades extends Model
{
    // Varibles para registro de entidad
    public $Entidad;
    public $Pais;
    public $Departamento;
    public $ID_Ciudad;
    public $Direccion;
    public $Telefono;
    public $Tipo_Doc;
    public $Doc;
    public $Digito_Verificacion;
    public $Direccion_Electronica;

    // Variables para registro de representante legal
    public $RNombre;
    public $RTipo_Doc;
    public $RDoc;

    // Variables para registro de clasificacion de entidad
    public $ID_CIIU;
    public $ID_Sector;
    public $ID_Tipo_Entidad;
    public $ID_Tamano_Entidad;
    public $ID_Tipo_Empresa;

    // Variables para registro del director del proyecto
    public $DNombre;
    public $DTipo_Doc;
    public $DDoc;
    public $DCorreo;


    // Reglas de validacion para el formulario de registro de entidad
    public function rules()
    {
        return [
            // username and password are both required
            [
              [
                'Entidad',
                'Pais',
                'Departamento',
                'ID_Ciudad',
                'Direccion',
                'Telefono',
                'Tipo_Doc',
                'Doc',
                'Digito_Verificacion',
                'Direccion_Electronica',
                'RNombre',
                'RTipo_Doc',
                'RDoc',
                //'ID_CIIU',
                'ID_Sector',
                'ID_Tipo_Entidad',
                //'ID_Tamano_Entidad',
                //'ID_Tipo_Empresa',
                'DNombre',
                'DTipo_Doc',
                'DDoc',
                'DCorreo',
              ], 'required', 'message' => 'Este campo no puede estar vacío.'
            ],
            [
              [
                'Entidad',
                'Tipo_Doc',
                'RTipo_Doc',
                'DTipo_Doc',
                'Direccion',
                'RNombre',
                'DNombre',
              ],'string', 'message' => 'Este campo es de tipo texto'
            ],
            [
              [
                'Pais',
                'Departamento',
                'ID_Ciudad',
                'Telefono',
                'Doc',
                'Digito_Verificacion',
                'RDoc',
                'ID_CIIU',
                'ID_Sector',
                'ID_Tipo_Entidad',
                'ID_Tamano_Entidad',
                'ID_Tipo_Empresa',
                'DDoc',
              ],'number', 'message' => 'Este campo es de tipo numérico'
            ],
            [
              [
                'Direccion_Electronica',
                'DCorreo'
              ], 'email'
            ],
        ];
    }

    // Realizamos el registro de la entidad en el sistema
    public function register()
    {
        $db = Mysql::connection();

        // 1) Consultar si la entidad ya se encuentra registrada
        $sql_1 = 'SELECT COUNT(*) cant FROM Entidades WHERE Doc = "' . $this->Doc . '" AND Digito_Verificacion = "' . $this->Digito_Verificacion . '"';

        $consulta_1 = $db->createCommand($sql_1)->queryOne();

        if ($consulta_1['cant'] == 0) {
            // Registramos la entidad en el sistema
            $ID_Entidad = Generalidades::getConsecutivo('Entidades','ID_Entidad');

            $sql_2 = 'INSERT INTO entidades(`ID_Entidad`, `Entidad`, `Tipo_Doc`, `Doc`, `Digito_Verificacion`, `Direccion_Electronica`, `Web_Site`, `ID_Ciudad`, `Telefono`, `Direccion`, `Fax`, `ID_CIIU`, `ID_Sector`, `ID_Tipo_Entidad`, `ID_Tipo_Empresa`, `ID_Tamano_Entidad`, `Estado`, `Fecha_Registro`) VALUES (' . $ID_Entidad . ',"' . $this->Entidad . '","' . $this->Tipo_Doc . '",' . $this->Doc . ',' . $this->Digito_Verificacion . ',"' . $this->Direccion_Electronica . '","",' . $this->ID_Ciudad . ',"' . $this->Telefono . '","' . $this->Direccion . '","",' . $this->ID_CIIU . ',' . $this->ID_Sector . ',' . $this->ID_Tipo_Entidad . ',' . $this->ID_Tipo_Empresa . ',' . $this->ID_Tamano_Entidad . ',"ACTIVO",NOW())';

            $insertEntidad = $db->createCommand($sql_2)->execute();

            if ($insertEntidad) {
                // Se ha registrado correctamente la entidad, procedemos con el registro de personas

                // Representante legal

                $ID_Entidades_Persona = Generalidades::getConsecutivo('Entidades_Persona','ID_Entidades_Persona');

                $sql_4 = 'INSERT INTO `entidades_persona`(`ID_Entidades_Persona`, `ID_Entidad`, `Tipo_Doc`, `Doc`, `Nombre`, `Cargo`, `Telefono`, `Correo`, `Tipo_Persona`, `Fecha_Registro`, `Estado`) VALUES (' . $ID_Entidades_Persona . ',' . $ID_Entidad . ',"' . $this->RTipo_Doc . '",' . $this->RDoc . ',"' . $this->RNombre . '","","","","REPRESENTANTE LEGAL",NOW(),"ACTIVO")';

                $insert_4 = $db->createCommand($sql_4)->execute();

                // Director del proyecto

                $ID_Entidades_Persona = Generalidades::getConsecutivo('Entidades_Persona','ID_Entidades_Persona');

                $sql_5 = 'INSERT INTO `entidades_persona`(`ID_Entidades_Persona`, `ID_Entidad`, `Tipo_Doc`, `Doc`, `Nombre`, `Cargo`, `Telefono`, `Correo`, `Tipo_Persona`, `Fecha_Registro`, `Estado`) VALUES (' . $ID_Entidades_Persona . ',' . $ID_Entidad . ',"' . $this->DTipo_Doc . '",' . $this->DDoc . ',"' . $this->DNombre . '","","","' . $this->DCorreo . '","DIRECTOR DEL PROYECTO",NOW(),"ACTIVO")';

                $insert_5 = $db->createCommand($sql_5)->execute();

                // Registro de usuario del director del proyecto

                $ID_Usuarios_Entidad = Generalidades::getConsecutivo('Usuarios_Entidad','ID_Usuarios_Entidad');

                $clave = Generalidades::generateRandomString(8);
                $clave_encriptada = Generalidades::encriptarClave($clave);
                $token = Generalidades::generateRandomString(40);

                $link_activacion = 'http://localhost/sena_forms/web/index.php?r=site%2Factivation&email='.$this->DCorreo.'&token='.$token;

                $sql_6 = 'INSERT INTO `usuarios_entidad`(`ID_Usuarios_Entidad`, `ID_Entidades_Persona`, `Nick`, `Clave`, `Tipo_Usuario`, `Intentos`, `Token`, `Fecha_Registro`, `Sesion`, `Estado`) VALUES (' . $ID_Usuarios_Entidad . ',' . $ID_Entidades_Persona . ',"' . $this->DCorreo . '","' . $clave_encriptada . '","ADMINISTRADOR",0,"' . $token . '",NOW(),"DESCONECTADO","PENDIENTE ACTIVACION")';

                $insert_6 = $db->createCommand($sql_6)->execute();

                if ($insert_6) {
                    // Envio de correo electronico de registro de usuario.
                    $template = 'Template_RegistroEntidad';
                    $data = [
                      'entidad'           => $this->Entidad,
                      'identificacion'    => $this->Tipo_Doc . ' ' . $this->Doc . ' - ' . $this->Digito_Verificacion,
                      'direccion_entidad' => $this->Direccion,
                      'correo_entidad'    => $this->Direccion_Electronica,
                      'representante'     => $this->RNombre,
                      'r_identificacion'  => $this->RTipo_Doc . ' ' . $this->RDoc,
                      'codigo_ciiu'       => Generalidades::getData('ciiu','Descripcion','Codigo',$this->ID_CIIU),
                      'sector'            => Generalidades::getData('sub_tipo_definicion','Descripcion','ID_Sub_Tipo_Definicion',$this->ID_Sector),
                      'tipo_entidad'      => Generalidades::getData('sub_tipo_definicion','Descripcion','ID_Sub_Tipo_Definicion',$this->ID_Tipo_Entidad),
                      'tamano_entidad'    => Generalidades::getData('sub_tipo_definicion','Descripcion','ID_Sub_Tipo_Definicion',$this->ID_Tamano_Entidad),
                      'tipo_empresa'      => Generalidades::getData('sub_tipo_definicion','Descripcion','ID_Sub_Tipo_Definicion',$this->ID_Tipo_Empresa),
                      'director'          => $this->DNombre,
                      'd_identificacion'  => $this->DTipo_Doc . ' ' . $this->DDoc,
                      'd_correo'          => $this->DCorreo,
                      'user'              => $this->DCorreo,
                      'pass'              => $clave,
                      'link_activacion'   => $link_activacion,
                    ];
                    $to = $this->DCorreo;
                    $subject = 'Registro exitoso';

                    // Descomentar linea para hacer el envío del email.                    
                    //$sendMessage = Generalidades::sendMessage($template,$data,$to,$subject);
                }

                if ($insert_4 && $insert_5 && $insert_6) {
                    // Se han registrado correctamente
                    $response = json_encode([
                        'status' => 'success',
                        'message'=> 'Se ha registrado la entidad correctamente.',
                        'alert'  => 'alert-success',
                    ]);

                    // Limpiar variables del objeto Entidad
                    // Varibles para registro de entidad
                    $this->Entidad                = '';
                    $this->Pais                   = '';
                    $this->Departamento           = '';
                    $this->ID_Ciudad              = '';
                    $this->Direccion              = '';
                    $this->Telefono               = '';
                    $this->Tipo_Doc               = '';
                    $this->Doc                    = '';
                    $this->Digito_Verificacion    = '';
                    $this->Direccion_Electronica  = '';

                    // Variables para registro de representante legal
                    $this->RNombre                = '';
                    $this->RTipo_Doc              = '';
                    $this->RDoc                   = '';

                    // Variables para registro de clasificacion de entidad
                    $this->ID_CIIU                = '';
                    $this->ID_Sector              = '';
                    $this->ID_Tipo_Entidad        = '';
                    $this->ID_Tamano_Entidad      = '';
                    $this->ID_Tipo_Empresa        = '';

                    // Variables para registro del director del proyecto
                    $this->DNombre                = '';
                    $this->DTipo_Doc              = '';
                    $this->DDoc                   = '';
                    $this->DCorreo                = '';

                }else {
                    // Se han presentado inconvenientes
                    $response = json_encode([
                        'status'    => 'error',
                        'message'   => 'Han ocurrido algunos inconvenientes.',
                        'alert'     => 'alert-danger'
                    ]);

                }

            }else {
                // Ha ocurrido un problema con el registro
                $response = json_encode([
                    'status'    => 'error',
                    'message'   => 'No se pudo registrar la entidad en el sistema.<br>' . $insertEntidad . '<hr>',
                    'alert'     => 'alert-danger'
                ]);
            }

        }else {
            // La entidad ya se encuentra registrada
            $response = json_encode([
              'status'  =>  'error',
              'message' =>  'La entidad ya se encuentra registrada en el sistema.',
              'alert'   =>  'alert-danger',
            ]);
        }

        return $response;
    }
}
