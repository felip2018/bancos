<?php

namespace app\models;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\base\Model;

use app\models\Mysql;
use app\models\Generalidades;

class RadicacionC extends Model
{
	// Validar si el proyecto puede radicar el cronograma
	public function validarRadicacion($id_proyecto)
	{
		$db = Mysql::connection();

		// Consultamos el listado de grupos por accion de formacion del proyecto en sesion
		$sql1 = "SELECT 
				accion_formacion_grupos.id_accion_formacion,
				acciones_formacion.nombre AS accion_formacion,
				accion_formacion_grupos.id_grupo,
				accion_formacion_grupos.nombre AS grupo 
				FROM accion_formacion_grupos 
				INNER JOIN acciones_formacion ON acciones_formacion.id_accion_formacion = accion_formacion_grupos.id_accion_formacion 
				WHERE acciones_formacion.id_proyecto = '".$id_proyecto."'";

		$consulta1 = $db->createCommand($sql1)->queryAll();

		$arreglo = [];

		if (!empty($consulta1)) {
			
			foreach ($consulta1 as $key => $value) {
				// Evaluamos si el grupo de la lista tiene algun registro en la tabla "cronograma"
				$sql2 = "SELECT COUNT(*) cant 
						 FROM cronograma 
						 WHERE id_accion_formacion = '".$value['id_accion_formacion']."'
						 AND id_grupo = '".$value['id_grupo']."'";

				$consulta2 = $db->createCommand($sql2)->queryOne();

				if ($consulta2['cant'] == 0) {
					// Agregamos el grupo a la lista
					array_push($arreglo, $value);
				}

			}


			if (!empty($arreglo)) {
				// Grupos pendientes por asignar cronograma
				$response = json_encode([
					'status' 	=> 'warning',
					'message' 	=> 'El sistema informa que aun tiene grupos pendientes por asignación de cronogramas.',
					'alert'		=> 'alert-warning',
					'arreglo'	=> $arreglo
				]);
			}else{
				// Validacion exitosa
				$response = json_encode([
					'status' 	=> 'success',
					'message' 	=> 'La validación ha sido exitosa, proceda a radicar el cronograma',
					'alert'		=> 'alert-success'
				]);
			}


		}else{
			$response = json_encode([
				'status' 	=> 'vacio',
				'message'	=> 'No se han encontrado grupos registrados para el presente proyecto',
				'alert'		=> 'alert-warning'
			]);
		}
		return $response;
	}

	public function radicarCronograma($id_proyecto)
	{

		$db = Mysql::connection();

		$radicado = Generalidades::obtenerRadicado($id_proyecto);

		$sql0 = "SELECT radicado, estado_interventoria FROM proyectos WHERE id_proyecto = '".$id_proyecto."'";

		$consulta0 = $db->createCommand($sql0)->queryOne();
		

		if ($consulta0['estado_interventoria'] != 'RADICADO') {

			// Seleccionamos el listado de cronogramas registrados para los grupos de AF del proyecto en sesion
			//$sql = "UPDATE cronograma SET radicado = '".$radicado."', estado_interventoria = 'RADICADO' WHERE id_accion_formacion IN (SELECT id_accion_formacion FROM acciones_formacion WHERE id_proyecto = '".$id_proyecto."')";
			$sql = "UPDATE proyectos SET radicado = '".$radicado."', fecha_radicacion = NOW(), estado_interventoria = 'RADICADO' WHERE id_proyecto = '".$id_proyecto."'";

		    $radicacion = $db->createCommand($sql)->execute();

		    if ($radicacion) {

		    	$sql2 = "SELECT titulo, radicado, estado_interventoria, DATE_FORMAT(fecha_radicacion,'%d-%m-%Y %H:%m:%s') AS fecha_radicacion FROM proyectos WHERE id_proyecto = '".$id_proyecto."'";

				$consulta2 = $db->createCommand($sql2)->queryOne();

		    	$response = json_encode([
		    		'status' 	=> 'success',
		    		'message' 	=> 'El proyecto <b>'.$consulta2['titulo'].'</b> ha radicado el cronograma de manera correcta.<br>Fecha de radicación: '.$consulta2['fecha_radicacion'],
		    		'alert'		=> 'alert-success'
		    	]);
		    }else{
		    	$response = json_encode([
		    		'status' 	=> 'error',
		    		'message' 	=> 'Ha ocurrido un problema al momento de radicar el cronograma, comuniquese con el administrador del sistema.',
		    		'alert'		=> 'alert-success'
		    	]);
		    }
		    
		}else{
			$response = json_encode([
				'status' 	=> 'error',
				'message'	=> 'El cronograma ya tiene el radicado No.'.$consulta0['radicado'].', y se encuentra a la espera de la revisión por parte de interventoria.',
				'alert'		=> 'alert-warning'
			]);
		}

	    return $response;

	}

}