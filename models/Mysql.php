<?php
	namespace app\models;
	
	use Yii;
	use yii\base\Model;

	class Mysql
	{
		/*
			Gestionar conexion a la base de datos
		*/
		public static function connection()
		{
			$db = new yii\db\Connection(Yii::$app->db);
			return $db;
		}
	}
?>