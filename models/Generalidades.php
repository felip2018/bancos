<?php

namespace app\models;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use yii\base\Model;

use app\models\Mysql;

class Generalidades extends Model
{
    // Consulta de paises
    public static function getCountries()
    {
        $db = Mysql::connection();
        $sql = 'SELECT ID_Pais, Nombre FROM Paises WHERE ID_Pais != 1 ORDER BY Nombre';
        $countriesList = $db->createCommand($sql)->queryAll();
        return $countriesList;
    }

    // Consulta de Departamentos y Ciudades
    public static function getLocations($tipo_busqueda, $identificador)
    {
        $db = Mysql::connection();

        switch ($tipo_busqueda) {
            case 'Departamentos':
                // Busqueda de departamentos por pais seleccionado
                $sql = 'SELECT
                        id_dpto,
                        nombre
                        FROM departamentos
                        WHERE id_pais = ' . $identificador . '
                        ORDER BY nombre ASC';
                break;

            case 'Ciudades':
                // Busqueda de ciudades por departamento seleccionado
                $sql = 'SELECT
                        id_ciudad,
                        nombre
                        FROM ciudades
                        WHERE id_dpto = ' . $identificador . '
                        ORDER BY nombre ASC';
                break;

            case 'All':
                // Buscar todas las ciudades registradas
                $sql = 'SELECT
                        id_ciudad,
                        nombre
                        FROM ciudades
                        ORDER BY nombre ASC';
                break;
        }

        $busqueda = $db->createCommand($sql)->queryAll();

        /*if (!empty($busqueda)) {
          $response = json_encode(['status' => 'success', 'listado' => $busqueda, 'sql' => $sql]);
        }else{
          $response = json_encode(['status' => 'error', 'message' => 'No se pudo realizar la consulta.', 'sql' => $sql]);
        }*/
        //return $response;
        return $busqueda;
    }

    // Consulta de Departamentos y Ciudades
    public static function getAllLocations($tipo_busqueda, $identificador)
    {
        $db = Mysql::connection();

        switch ($tipo_busqueda) {
            case 'Departamentos':
                // Busqueda de departamentos por pais seleccionado
                $sql = 'SELECT
                        id_dpto,
                        nombre
                        FROM departamentos
                        WHERE id_pais = ' . $identificador . '
                        ORDER BY nombre ASC';
                break;

            case 'Ciudades':
                // Busqueda de ciudades por departamento seleccionado
                $sql = 'SELECT
                        id_ciudad,
                        nombre
                        FROM ciudades
                        WHERE id_dpto = ' . $identificador . '
                        ORDER BY nombre ASC';
                break;

            case 'All':
                // Buscar todas las ciudades registradas
                $sql = 'SELECT
                        id_ciudad,
                        nombre
                        FROM ciudades
                        ORDER BY nombre ASC';
                break;
        }

        $busqueda = $db->createCommand($sql)->queryAll();

        if (!empty($busqueda)) {
          $response = json_encode(['status' => 'success', 'listado' => $busqueda, 'sql' => $sql]);
        }else{
          $response = json_encode(['status' => 'error', 'message' => 'No se pudo realizar la consulta.', 'sql' => $sql]);
        }
        return $response;
        //return $busqueda;
    }

    //  Consulta de codigos CIIU
    public static function getCIIU()
    {
        $db = Mysql::connection();
        $sql = 'SELECT Codigo, Descripcion
                FROM CIIU
                ORDER BY Codigo ASC';
        $CIIUList = $db->createCommand($sql)->queryAll();
        return $CIIUList;
    }

    // Consulta de sub tipos de definicion
    public static function getSubTipos($IDTipoDefinicion)
    {
        $db = Mysql::connection();
        $sql = 'SELECT ID_Sub_Tipo_Definicion, Descripcion
                FROM sub_tipo_definicion
                WHERE ID_Tipo_Definicion = ' . $IDTipoDefinicion . '
                AND estado = "Activo"
                ORDER BY Descripcion ASC';
        $SubTiposLista = $db->createCommand($sql)->queryAll();
        return $SubTiposLista;
    }

    // Consultar el consecutivo de registro de una tabla
    public static function getConsecutivo($tabla,$campo)
    {
        $db = Mysql::connection();
        $sql = 'SELECT
                MAX('.$campo.') '.$campo.'
                FROM '.$tabla;

        $consulta = $db->createCommand($sql)->queryOne();

        $ID = ($consulta[$campo] == null) ? 1 : $consulta[$campo] + 1;

        return $ID;
    }

    // Generar token
    public static function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    // Envio de correos electrónicos
    public static function sendMessage($template,$data,$to,$subject)
    {
        $message = Yii::$app->mailer->compose($template,['data' => $data])
        ->setFrom('felipe.sena.sigp@gmail.com')
        ->setTo($to)
        ->setSubject($subject)
        ->send();

        return $message;
    }

    // Consultar nombre o descripcion de acuerdo al identificador seleccionado
    public static function getData($tabla,$campo,$campo_id,$id)
    {
        $db = Mysql::connection();
        $sql = 'SELECT ' . $campo . ' FROM ' . $tabla . ' WHERE ' . $campo_id . ' = ' . $id;
        $consulta = $db->createCommand($sql)->queryOne();
        $response = $id . ' - ' . $consulta[$campo];
        return $response;
    }

    // Encriptacion de contraseña
    public static function encriptarClave($clave)
    {
        $clave_encriptada = Yii::$app->getSecurity()->generatePasswordHash($clave);
        return $clave_encriptada;
    }

    // Activacion de cuenta por correo de verificacion
    public static function activationAccount($email,$token)
    {
        $db = Mysql::connection();
        //$sql = 'SELECT COUNT(*) cant FROM usuarios_entidad WHERE nick = "' . $email . '"';
        $sql = 'SELECT nick, token, estado FROM usuarios_entidad WHERE nick = "' . $email . '"';
        $consulta = $db->createCommand($sql)->queryOne();

        if (!empty($consulta)) {
            // Evaluamos el estado de la cuenta
            if ($consulta['estado'] == 'PENDIENTE ACTIVACION') {
                
                // Procedemos a evaluar el token para activar la cuenta
                if ($consulta['token'] === $token) {
                    
                    $sql_2 = 'UPDATE usuarios_entidad SET estado = "ACTIVO" WHERE nick = "' . $email . '" AND token = "' . $token . '"';

                    $update = $db->createCommand($sql_2)->execute();

                    if ($update) {
                        $response = json_encode([
                            'status'    => 'Activación exitosa',
                            'message'   => 'Se ha realizado la activación de la cuenta correctamente',
                            'alert'     => 'alert-success',
                        ]);
                    }else {
                        $response = json_encode([
                            'status'    => 'Error',
                            'message'   => 'No se pudo completar el proceso de activacion de la cuenta.<br>' . $update,
                            'alert'     => 'alert-danger',
                        ]);
                    }

                }else {
                    
                    // El token es inválido
                    $response = json_encode([
                        'status'    => 'Error',
                        'message'   => 'El token es inválido',
                        'alert'     => 'alert-danger',
                    ]);    

                }

            }else {
                
                // No se puede activar la cuenta por que esta en otro estado
                $response = json_encode([
                    'status'    => 'Advertencia!',
                    'message'   => 'No es posible realizar el proceso de activación porque el estado de la cuenta es: <b>' . $consulta['estado'] .'</b>',
                    'alert'     => 'alert-warning',
                ]);

            }            
        }else{
            
            // El nick no corresponde con una cuenta registrada
            $response = json_encode([
                'status'    => 'Error',
                'message'   => 'El nick no corresponde con una cuenta registrada.',
                'alert'     => 'alert-danger',
            ]);

        }
        return $response;
    }

    // Generar caso de cambio de contraseña
    public static function generateChangePassword($usuario)
    {
        $db = Mysql::connection();

        $sql = 'SELECT 
        usuarios_entidad.ID_Usuarios_Entidad,
        usuarios_entidad.Nick,
        entidades_persona.Nombre
        FROM Usuarios_Entidad 
        INNER JOIN Entidades_Persona ON Entidades_PErsona.ID_Entidades_Persona = Usuarios_Entidad.ID_Entidades_Persona
        WHERE Nick = "' . $usuario . '"';

        $consulta = $db->createCommand($sql)->queryOne();

        if (!empty($consulta)) {

            // Consultar si hay token activo para el cambio de contraseña
            $sql_2 = 'SELECT COUNT(*) cant FROM Tokens_Cambio WHERE ID_Usuarios_Entidad = ' . $consulta['ID_Usuarios_Entidad'] . ' AND Estado = "ACTIVO"';

            $consulta_2 = $db->createCommand($sql_2)->queryOne();

            if ($consulta_2['cant'] == 0) {
                // Registramos el caso
                $token              = Generalidades::generateRandomString(45);
                $ID_Token_Cambio    = Generalidades::getConsecutivo('Tokens_Cambio','ID_Token_Cambio');
                $Fecha_Registro     = date("d-m-Y H:i:s");
                $Fecha_Caducidad    = date("d-m-Y H:i:s",strtotime($fecha_actual."+ 1 days"));

                $sql_insert = 'INSERT INTO `tokens_cambio`(`ID_Token_Cambio`, `ID_Usuarios_Entidad`, `Token`, `Fecha_Registro`, `Fecha_Caducidad`, `Estado`) VALUES (' . $ID_Token_Cambio . ',' . $consulta['ID_Usuarios_Entidad'] .',"' . $token . '","' . $Fecha_Registro . '","' . $Fecha_Caducidad . '","ACTIVO")';

                $insert = $db->createCommand($sql_insert)->execute();

                if ($insert) {
                    // Enviar correo electronico con el link para el cambio de contraseña
                    $template = 'template_CambioClave';
                    $link_cambio_clave = 'http://localhost/sena_forms/web/index.php?r=site%2Fchangepassword&email='.$consulta['Nick'].'&token='.$token;
                    $data = [
                        'nombre'    => $consulta['Nombre'],
                        'usuario'   => $consulta['Nick'],
                        'link_cambio_clave' => $link_cambio_clave,
                    ];

                    $to = $consulta['Nick'];
                    $subject = 'Cambio de clave';

                    $sendMessage = Generalidades::sendMessage($template,$data,$to,$subject);

                    $response = json_encode([
                        'status'    => 'success',
                        'message'   => 'Se ha enviado el mensaje para '
                    ]);

                }else {
                    $response = json_encode([
                        'status'    => 'Error',
                        'message'   => 'No se pudo generar el registro del caso para el cambio de contraseña.'
                    ]);    
                }

            }else {
                // Ya hay un token activo
                $response = json_encode([
                    'status'    => 'Warning',
                    'message'   => 'Ya se existe un caso activo para el cambio de contraseña de esta cuenta, por favor verifique su cuenta de correo electrónico.'
                ]);
            }         

        }else {
            $response = json_encode([
                'status'    => 'Error',
                'message'   => 'El usuario ingresado no se encuentra registrado en el sistema.',
            ]);
        }
        return $response;
    }

    // Obtener informacion de usuario por documento ingresado
    public static function getInfoByDoc($tipo_doc,$num_doc)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                beneficiarios.id_beneficiario,
                beneficiarios.tipo_doc,
                beneficiarios.num_doc,
                beneficiarios.nombres,
                beneficiarios.apellido_1,
                beneficiarios.apellido_2,
                beneficiarios.genero,
                beneficiarios.estrato,
                DATE_FORMAT(beneficiarios.fecha_nacimiento,'%Y-%m-%d') AS fecha_nacimiento,
                beneficiarios.celular,
                ciudades.id_dpto,
                beneficiarios.id_ciudad,
                beneficiarios.barrio_vereda,
                beneficiarios.direccion,
                proyecto_beneficiarios.id_proyecto_beneficiario,
                beneficiarios_grupo.edad,
                beneficiarios_grupo.antiguedad,
                beneficiarios_grupo.empresa_labora,
                beneficiarios_grupo.tamanio_empresa,
                beneficiarios_grupo.transferencia,
                beneficiarios_grupo.perfil_transferencia,
                beneficiarios_grupo.caracterizacion,
                beneficiarios_grupo.nivel_ocupacional
                FROM beneficiarios 
                INNER JOIN ciudades ON ciudades.id_ciudad = beneficiarios.id_ciudad
                INNER JOIN proyecto_beneficiarios ON proyecto_beneficiarios.id_beneficiario = beneficiarios.id_beneficiario
                LEFT JOIN beneficiarios_grupo ON beneficiarios_grupo.id_proyecto_beneficiario = proyecto_beneficiarios.id_proyecto_beneficiario
                WHERE beneficiarios.tipo_doc = '".$tipo_doc."'
                AND beneficiarios.num_doc = '".$num_doc."'
                LIMIT 1";

        $consulta = $db->createCommand($sql)->queryOne();

        if (!empty($consulta)) {
            // Se ha encontrado informacion
            $response = json_encode([
                'status'    => 'success',
                'datos'     => $consulta
            ]);
        }else{
            // No se ha encontrado informacion
            $response = json_encode([
                'status'    => 'error',
                'message'   => 'No se ha encontrado información para el número de documento.'
            ]);
        }

        return $response;
    }

    // Calcular el rango de edad de un beneficiario
    public static function calcularRangoEdad($edad)
    {
        if ($edad <= 17) {
            $response = "De 17 años y menores";
        }elseif ($edad >= 18 && $edad <= 24) {
            $response = "De 18 a 24 años";
        }elseif ($edad >= 25 && $edad <= 30) {
            $response = "De 25 a 30 años";
        }elseif ($edad >= 31 && $edad <= 40) {
            $response = "De 31 a 40 años";
        }elseif ($edad >= 41 && $edad <= 55) {
            $response = "De 41 a 55 años";
        }elseif ($edad >= 56) {
            $response = "De 56 años y mayores";
        }


        return $response;
    }

    // Consultar informacion de la convocatoria para el documento pdf
    public static function infoConvocatoria($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT
                convocatorias.nombre,
                convocatorias_tipo.modalidad
                FROM convocatorias
                INNER JOIN convocatorias_tipo ON convocatorias_tipo.id_convocatoria = convocatorias.id_convocatoria
                INNER JOIN proyectos ON proyectos.id_tipo_convocatoria = convocatorias_tipo.id_tipo_convocatoria
                WHERE proyectos.id_proyecto = '".$id_proyecto."'";

        $consulta = $db->createCommand($sql)->queryOne();

        return $consulta;
    }

    // Consultar informacion de un proyecto
    public static function infoProyecto($id_grupo)
    {
        $db = Mysql::connection();

        $sql = "SELECT
                proyectos.titulo,
                proyectos.n_radicado,
                acciones_formacion.nombre AS accion_formacion,
                accion_formacion_grupos.nombre AS grupo
                FROM proyectos
                INNER JOIN acciones_formacion ON acciones_formacion.id_proyecto = proyectos.id_proyecto
                INNER JOIN accion_formacion_grupos ON accion_formacion_grupos.id_accion_formacion = acciones_formacion.id_accion_formacion
                WHERE accion_formacion_grupos.id_grupo = '".$id_grupo."'";

        $consulta = $db->createCommand($sql)->queryOne();

        return $consulta;
    }

    // Consultar informacion del proyecto por accion de formacion
    public static function infoProyectoByAccion($id_accion_formacion)
    {
        $db = Mysql::connection();

        $sql = "SELECT
                proyectos.titulo,
                proyectos.n_radicado,
                acciones_formacion.nombre AS accion_formacion
                FROM proyectos
                INNER JOIN acciones_formacion ON acciones_formacion.id_proyecto = proyectos.id_proyecto
                WHERE acciones_formacion.id_accion_formacion = '".$id_accion_formacion."'";

        $consulta = $db->createCommand($sql)->queryOne();

        return $consulta;
    }

    // Consultar listado de beneficiarios registrados en un grupo
    public static function beneficiariosGrupo($id_grupo)
    {
        $db = Mysql::connection();

        $sql = "SELECT
                beneficiarios.id_beneficiario,
                CONCAT(beneficiarios.tipo_doc,'-',beneficiarios.num_doc) AS identificacion,
                CONCAT(beneficiarios.nombres,' ',beneficiarios.apellido_1,' ',beneficiarios.apellido_2) AS nombre,
                proyecto_beneficiarios.id_proyecto_beneficiario,
                beneficiarios_grupo.id_accion_formacion,
                beneficiarios_grupo.id_grupo
                FROM beneficiarios_grupo
                INNER JOIN proyecto_beneficiarios ON proyecto_beneficiarios.id_proyecto_beneficiario = beneficiarios_grupo.id_proyecto_beneficiario
                INNER JOIN beneficiarios ON beneficiarios.id_beneficiario = proyecto_beneficiarios.id_beneficiario
                WHERE beneficiarios_grupo.id_grupo = '".$id_grupo."'
                AND beneficiarios_grupo.estado_registro = 'ACTIVO'";

        $consulta = $db->createCommand($sql)->queryAll();

        return $consulta;
    }

    // Consultar beneficiarios registrados en una accion de formacion seleccionada
    public static function beneficiariosAccionFormacion($id_accion_formacion)
    {
        $db = Mysql::connection();

        $sql = "SELECT
                beneficiarios.id_beneficiario,
                CONCAT(beneficiarios.tipo_doc,'-',beneficiarios.num_doc) AS identificacion,
                CONCAT(beneficiarios.nombres,' ',beneficiarios.apellido_1,' ',beneficiarios.apellido_2) AS nombre,
                beneficiarios.tipo_doc,
                beneficiarios.num_doc,
                proyecto_beneficiarios.id_proyecto_beneficiario,
                beneficiarios_grupo.id_accion_formacion,
                beneficiarios_grupo.id_grupo,
                acciones_formacion.nombre AS accion_formacion,
                accion_formacion_grupos.nombre AS grupo
                FROM beneficiarios_grupo
                INNER JOIN accion_formacion_grupos ON accion_formacion_grupos.id_accion_formacion = beneficiarios_grupo.id_accion_formacion AND accion_formacion_grupos.id_grupo = beneficiarios_grupo.id_grupo
                INNER JOIN acciones_formacion ON acciones_formacion.id_accion_formacion = accion_formacion_grupos.id_accion_formacion
                INNER JOIN proyecto_beneficiarios ON proyecto_beneficiarios.id_proyecto_beneficiario = beneficiarios_grupo.id_proyecto_beneficiario
                INNER JOIN beneficiarios ON beneficiarios.id_beneficiario = proyecto_beneficiarios.id_beneficiario
                WHERE beneficiarios_grupo.id_accion_formacion = '".$id_accion_formacion."'
                AND beneficiarios_grupo.estado_registro = 'ACTIVO'";

        $consulta = $db->createCommand($sql)->queryAll();

        return $consulta;
    }

    // Consultar listado de documentos requeridos para el registro en un banco
    public static function documentosRegistro($banco)
    {
        $db = Mysql::connection();

        $sql = "SELECT
                documentos_bancos.id_documento_banco,
                documentos_bancos.id_documento,
                documentos.nombre,
                documentos_bancos.obligatorio,
                documentos_bancos.tipo_banco,
                documentos_bancos.input_name,
                documentos_bancos.fecha_registro,
                documentos_bancos.estado_registro
                FROM documentos_bancos
                INNER JOIN documentos ON documentos.id_documento = documentos_bancos.id_documento
                WHERE documentos_bancos.tipo_banco = '".$banco."'
                AND documentos_bancos.estado_registro = 'ACTIVO'";

        $consulta = $db->createCommand($sql)->queryAll();

        return $consulta;
    }

    // Consultar acciones de formacion de un proyecto
    public static function getFormationActions($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT
                id_accion_formacion,
                id_proyecto,
                nombre
                FROM acciones_formacion
                WHERE id_proyecto = ".$id_proyecto."
                AND nombre != 'TRANSFERENCIA'
                AND estado_registro = 'ACTIVO'
                ORDER BY nombre ASC";

        $consulta = $db->createCommand($sql)->queryAll();

        return $consulta;
    }

    // Consultar grupos de una accion de formacion
    public static function getGroupsAF($id_accion_formacion)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                id_grupo,
                id_accion_formacion,
                nombre
                FROM accion_formacion_grupos
                WHERE id_accion_formacion = '".$id_accion_formacion."'
                AND estado_registro = 'ACTIVO'
                ORDER BY id_grupo ASC";

        $consulta = $db->createCommand($sql)->queryAll();

        // Consultar informacion de la accion de formacion
        $sql2 = "SELECT 
                modalidad,
                evento_formacion
                FROM acciones_formacion
                WHERE id_accion_formacion = '".$id_accion_formacion."'";

        $info = $db->createCommand($sql2)->queryOne();

        if (!empty($consulta)) {
            $response = json_encode(['status'=>'success','grupos'=>$consulta,'info'=>$info]);
        }else{
            $response = json_encode(['status'=>'vacio']);
        }

        return $response;
    }

    // Obtener numero de radicado de cronograma
    public static function obtenerRadicado($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT  radicado
                FROM    proyectos
                WHERE id_proyecto = '".$id_proyecto."'";

        $consulta = $db->createCommand($sql)->queryOne();

        $radicado = ($consulta['radicado'] == null) ? 1 : $consulta['radicado']+1;

        return $radicado;

    }
}
