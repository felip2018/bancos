<?php

namespace app\models;

use Yii;
use yii\base\Model;

use app\models\Mysql;
use app\models\Generalidades;

class Tematicas extends Model
{
    // Verificar acciones de formacion con economia naranja
    public function verificarEconomiaNaranja()
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                id_accion_formacion_descriptor,
                id_accion_formacion,
                respuesta
                FROM acciones_formacion_descriptores
                WHERE respuesta LIKE '%economia naranja%'
                OR respuesta LIKE '%economía naranja%'
                AND estado_registro = 'ACTIVO'
                GROUP BY id_accion_formacion";

        $consulta = $db->createCommand($sql)->queryAll();

        if (!empty($consulta)) {
            foreach ($consulta as $key => $value) {
                
                $id_accion_formacion_tematicas_d = Generalidades::getConsecutivo("accion_formacion_tematicas_d","id_accion_formacion_tematicas_d");

                $sql2 = "INSERT INTO accion_formacion_tematicas_d(id_accion_formacion_tematicas_d, id_accion_formacion, id_tematica_dirigida, estado_registro) VALUES (".$id_accion_formacion_tematicas_d.",".$value['id_accion_formacion'].",1,'ACTIVO')";

                $insert = $db->createCommand($sql2)->execute();

                if (!$insert) {
                    $response = false;
                }else{
                    $response = true;
                }

            }   
        }

        return $response;
    }

    // Verificar acciones de formacion con revolucion industrial
    public function verificarRevolucionIndustrial()
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                id_accion_formacion_descriptor,
                id_accion_formacion,
                respuesta
                FROM acciones_formacion_descriptores
                WHERE respuesta LIKE '%revolución industrial%'
                OR respuesta LIKE '%revolucion industrial%'
                AND estado_registro = 'ACTIVO'
                GROUP BY id_accion_formacion";

        $consulta = $db->createCommand($sql)->queryAll();

        if (!empty($consulta)) {
            foreach ($consulta as $key => $value) {
                
                $id_accion_formacion_tematicas_d = Generalidades::getConsecutivo("accion_formacion_tematicas_d","id_accion_formacion_tematicas_d");

                $sql2 = "INSERT INTO accion_formacion_tematicas_d(id_accion_formacion_tematicas_d, id_accion_formacion, id_tematica_dirigida, estado_registro) VALUES (".$id_accion_formacion_tematicas_d.",".$value['id_accion_formacion'].",2,'ACTIVO')";

                $insert = $db->createCommand($sql2)->execute();

                if (!$insert) {
                    $response = false;
                }else{
                    $response = true;
                }

            }   
        }

        return $response;
    }

    // Verificar acciones de formacion con focos tematicos
    public function verificarFocosTematicos()
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                id_accion_formacion_descriptor,
                id_accion_formacion,
                respuesta
                FROM acciones_formacion_descriptores
                WHERE respuesta LIKE '%focos tematicos%'
                OR respuesta LIKE '%focos temáticos%'
                OR respuesta LIKE '%foco tematico%'
                OR respuesta LIKE '%foco temático%'
                OR respuesta LIKE '%foco tematicos%'
                OR respuesta LIKE '%foco temáticos%'
                OR respuesta LIKE '%focos tematico%'
                OR respuesta LIKE '%focos temático%'
                AND estado_registro = 'ACTIVO'
                GROUP BY id_accion_formacion";

        $consulta = $db->createCommand($sql)->queryAll();

        if (!empty($consulta)) {
            foreach ($consulta as $key => $value) {
                
                $id_accion_formacion_tematicas_d = Generalidades::getConsecutivo("accion_formacion_tematicas_d","id_accion_formacion_tematicas_d");

                $sql2 = "INSERT INTO accion_formacion_tematicas_d(id_accion_formacion_tematicas_d, id_accion_formacion, id_tematica_dirigida, estado_registro) VALUES (".$id_accion_formacion_tematicas_d.",".$value['id_accion_formacion'].",4,'ACTIVO')";

                $insert = $db->createCommand($sql2)->execute();

                if (!$insert) {
                    $response = false;
                }else{
                    $response = true;
                }

            }   
        }

        return $response;
    }

    // Verificar acciones de formacion con competencias blandas
    public function verificarCompetenciasBlandas()
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                id_accion_formacion_descriptor,
                id_accion_formacion,
                respuesta
                FROM acciones_formacion_descriptores
                WHERE respuesta LIKE '%competencias blandas%'
                OR respuesta LIKE '%competencia blanda%'
                OR respuesta LIKE '%competencias blanda%'
                OR respuesta LIKE '%competencia blandas%'
                AND estado_registro = 'ACTIVO'
                GROUP BY id_accion_formacion";

        $consulta = $db->createCommand($sql)->queryAll();

        if (!empty($consulta)) {
            foreach ($consulta as $key => $value) {
                
                $id_accion_formacion_tematicas_d = Generalidades::getConsecutivo("accion_formacion_tematicas_d","id_accion_formacion_tematicas_d");

                $sql2 = "INSERT INTO accion_formacion_tematicas_d(id_accion_formacion_tematicas_d, id_accion_formacion, id_tematica_dirigida, estado_registro) VALUES (".$id_accion_formacion_tematicas_d.",".$value['id_accion_formacion'].",5,'ACTIVO')";

                $insert = $db->createCommand($sql2)->execute();

                if (!$insert) {
                    $response = false;
                }else{
                    $response = true;
                }

            }   
        }

        return $response;
    }



    /********************************************** REPORTES **************************************************/


    // Obtener proyectos registrados en el sistema
    public function obtenerProyectos()
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                proyectos.id_proyecto,
                proyectos.id_tipo_convocatoria,
                proyectos.id_entidad,
                proyectos.titulo,
                proyectos.acciones_formacion,
                proyectos.total_beneficiarios,
                proyectos.contra_especie,
                proyectos.contra_dinero,
                proyectos.monto_solicitado,
                (proyectos.contra_especie + proyectos.contra_dinero + proyectos.monto_solicitado) AS valor_total,
                proyectos.n_radicado,
                proyectos.estado_proyecto,
                entidades.tipo_doc,
                entidades.num_doc,
                departamentos.nombre AS regional,
                sectores.nombre AS sector,
                convocatorias_tipo.modalidad AS modalidad_proyecto
                FROM proyectos
                INNER JOIN entidades ON entidades.id_entidad = proyectos.id_entidad
                INNER JOIN ciudades ON ciudades.id_ciudad = entidades.id_ciudad
                INNER JOIN departamentos ON departamentos.id_dpto = ciudades.id_dpto
                INNER JOIN subsectores ON subsectores.id_subsector = entidades.id_subsector
                INNER JOIN sectores ON sectores.id_sector = subsectores.id_sector
                INNER JOIN convocatorias_tipo ON convocatorias_tipo.id_tipo_convocatoria = proyectos.id_tipo_convocatoria
                WHERE proyectos.estado_final = 'APROBADO'
                AND proyectos.n_radicado = 25052
                ORDER BY proyectos.id_proyecto";

        $consulta = $db->createCommand($sql)->queryAll();
        return $consulta;
    }

    // Obtener cantidad de participacion de acciones por eje tematico
    public function ejesAccionFormacion($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT
                (SELECT COUNT(*)
                FROM accion_formacion_tematicas_d 
                WHERE id_accion_formacion IN (SELECT id_accion_formacion FROM acciones_formacion WHERE id_proyecto = '".$id_proyecto."') AND id_tematica_dirigida = 1) economia_naranja,
                (SELECT COUNT(*)
                FROM accion_formacion_tematicas_d 
                WHERE id_accion_formacion IN (SELECT id_accion_formacion FROM acciones_formacion WHERE id_proyecto = '".$id_proyecto."') AND id_tematica_dirigida = 2) revolucion_industrial,
                (SELECT COUNT(*)
                FROM accion_formacion_tematicas_d 
                WHERE id_accion_formacion IN (SELECT id_accion_formacion FROM acciones_formacion WHERE id_proyecto = '".$id_proyecto."') AND id_tematica_dirigida = 4) focos_tematicos,
                (SELECT COUNT(*)
                FROM accion_formacion_tematicas_d 
                WHERE id_accion_formacion IN (SELECT id_accion_formacion FROM acciones_formacion WHERE id_proyecto = '".$id_proyecto."') AND id_tematica_dirigida = 5) competencias_blandas
                FROM DUAL";

        $consulta = $db->createCommand($sql)->queryOne();
        return $consulta;
    }

    // Consultamos las acciones de formacion por evento de formacion
    public function eventosFormacion($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                evento_formacion, 
                COUNT(*) cant 
                FROM acciones_formacion 
                WHERE id_proyecto = '".$id_proyecto."' 
                AND acciones_formacion.estado_registro = 'ACTIVO'
                AND acciones_formacion.nombre != 'TRANSFERENCIA'
                GROUP BY evento_formacion";
    
        $consulta = $db->createCommand($sql)->queryAll();

        return $consulta;

    }

    // Consultamos las acciones de formacion por evento de formacion
    public function eventosFormacionBloques($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT
                (SELECT COUNT(*)
                 FROM acciones_formacion 
                 WHERE evento_formacion = 'CURSO' AND id_proyecto = '".$id_proyecto."' AND estado_registro = 'ACTIVO' AND nombre != 'TRANSFERENCIA') cursos,
                (SELECT COUNT(*)
                 FROM acciones_formacion 
                 WHERE evento_formacion = 'SEMINARIO' AND id_proyecto = '".$id_proyecto."' AND estado_registro = 'ACTIVO' AND nombre != 'TRANSFERENCIA') seminario,
                (SELECT COUNT(*)
                 FROM acciones_formacion 
                 WHERE evento_formacion = 'TALLER' AND id_proyecto = '".$id_proyecto."' AND estado_registro = 'ACTIVO' AND nombre != 'TRANSFERENCIA') taller,
                (SELECT COUNT(*)
                 FROM acciones_formacion 
                 WHERE evento_formacion = 'DIPLOMADO' AND id_proyecto = '".$id_proyecto."' AND estado_registro = 'ACTIVO' AND nombre != 'TRANSFERENCIA') diplomado,
                (SELECT COUNT(*)
                 FROM acciones_formacion 
                 WHERE evento_formacion = 'CONFERENCIA' AND id_proyecto = '".$id_proyecto."' AND estado_registro = 'ACTIVO' AND nombre != 'TRANSFERENCIA') conferencia,
                (SELECT COUNT(*)
                 FROM acciones_formacion 
                 WHERE evento_formacion = 'ORIENTACION CON EXPERTO' AND id_proyecto = '".$id_proyecto."' AND estado_registro = 'ACTIVO' AND nombre != 'TRANSFERENCIA') orientacion
                FROM DUAL";

        $consulta = $db->createCommand($sql)->queryOne();
        return $consulta;
    }

    // Consultamos las acciones de formacion del proyecto
    public function accionesFormacion($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT
                acciones_formacion.id_accion_formacion,
                acciones_formacion.id_proyecto,
                acciones_formacion.nombre,
                acciones_formacion.beneficiarios_empresa,
                acciones_formacion.numero_grupos,
                acciones_formacion.modalidad,
                (acciones_formacion.contra_especie + acciones_formacion.contra_dinero + acciones_formacion.monto_solicitado) AS valor_total,
                acciones_formacion.evento_formacion/*,
                acciones_formacion_descriptores.respuesta AS objetivo*/
                FROM acciones_formacion
                INNER JOIN acciones_formacion_descriptores ON acciones_formacion_descriptores.id_accion_formacion = acciones_formacion.id_accion_formacion
                WHERE acciones_formacion.id_proyecto = '".$id_proyecto."'
                AND acciones_formacion_descriptores.id_descriptor = 7";

        $consulta = $db->createCommand($sql)->queryAll();

        return $consulta;
    }

    // Consultamos la cobertura del proyecto
    public function coberturaProyecto($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                departamentos.nombre AS dpto, 
                SUM(proyectos_cobertura.grupos) AS total_grupos
                FROM proyectos_cobertura 
                INNER JOIN ciudades ON ciudades.id_ciudad = proyectos_cobertura.id_ciudad 
                INNER JOIN departamentos ON departamentos.id_dpto = ciudades.id_dpto 
                WHERE proyectos_cobertura.id_proyecto = ".$id_proyecto." 
                AND proyectos_cobertura.id_ciudad != 1 
                GROUP BY departamentos.nombre";

        $consulta = $db->createCommand($sql)->queryAll();
        return $consulta;
    }

    // Consultamos las tematicas a las que apuntan las acciones de formacion de un proyecto
    public function tematicasAccionesFormacion($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                tematicas_dirigidas.descripcion AS tematica,
                SUM(proyectos_ejes_tematicos.acciones_formacion) AS total_acciones 
                FROM proyectos_ejes_tematicos 
                INNER JOIN tematicas_dirigidas ON tematicas_dirigidas.id_tematica_dirigida = proyectos_ejes_tematicos.id_tematica_dirigida 
                WHERE proyectos_ejes_tematicos.id_proyecto = ".$id_proyecto." 
                GROUP BY tematicas_dirigidas.descripcion";

        $consulta = $db->createCommand($sql)->queryAll();
        return $consulta;
    }

    // Actualizar dato de acciones de formacion
    public function updateActions()
    {
        $db = Mysql::connection();

        // 1. Consultamos el numero de acciones de formacion por proyecto
        $sql = "SELECT 
                proyectos.id_proyecto, 
                proyectos.n_radicado, 
                proyectos.titulo, 
                COUNT(*) AS acciones
                FROM proyectos 
                INNER JOIN acciones_formacion ON acciones_formacion.id_proyecto = proyectos.id_proyecto 
                WHERE proyectos.estado_proyecto IN ('CUMPLE','CUMPLE PARCIALMENTE') 
                AND acciones_formacion.estado_registro = 'ACTIVO' 
                AND acciones_formacion.nombre != 'TRANSFERENCIA' 
                GROUP BY proyectos.id_proyecto";

        $consulta = $db->createCommand($sql)->queryAll();

        $resultado = [];

        if (!empty($consulta)) {
            $i = 0;
            foreach ($consulta as $key => $value) {
                $sql2 = "UPDATE proyectos SET acciones_formacion = ".$value['acciones']." WHERE id_proyecto = ".$value['id_proyecto']." AND n_radicado = ".$value['n_radicado'];

                $update = $db->createCommand($sql2)->execute();

                if ($update) {
                    $resultado[$i]['id_proyecto']   = $value['id_proyecto'];
                    $resultado[$i]['ejecucion']     = "Ok";
                    $resultado[$i]['sql']           = $sql2;
                }else{
                    $resultado[$i]['id_proyecto']   = $value['id_proyecto'];
                    $resultado[$i]['ejecucion']     = "Error";
                    $resultado[$i]['sql']           = $sql2;
                }

                $i++;
            }
        }

        return $resultado;
    }

    // Actualizar dato de beneficiarios
    public function updateBeneficiarios()
    {
        $db = Mysql::connection();

        // 1. Consultamos el numero de beneficiarios de formacion por proyecto
        $sql = "SELECT 
                proyectos.id_proyecto,
                proyectos.n_radicado,
                proyectos.titulo,
                SUM(acciones_formacion.beneficiarios_empresa) AS beneficiarios
                FROM proyectos 
                INNER JOIN acciones_formacion ON acciones_formacion.id_proyecto = proyectos.id_proyecto
                WHERE proyectos.estado_proyecto IN ('CUMPLE','CUMPLE PARCIALMENTE')
                AND acciones_formacion.estado_registro = 'ACTIVO'
                AND acciones_formacion.nombre != 'TRANSFERENCIA'
                GROUP BY proyectos.id_proyecto";

        $consulta = $db->createCommand($sql)->queryAll();

        $resultado = [];

        if (!empty($consulta)) {
            $i = 0;
            foreach ($consulta as $key => $value) {
                $sql2 = "UPDATE proyectos SET total_beneficiarios = ".$value['beneficiarios']." WHERE id_proyecto = ".$value['id_proyecto']." AND n_radicado = ".$value['n_radicado'];

                $update = $db->createCommand($sql2)->execute();

                if ($update) {
                    $resultado[$i]['id_proyecto']   = $value['id_proyecto'];
                    $resultado[$i]['ejecucion']     = "Ok";
                    $resultado[$i]['sql']           = $sql2;
                }else{
                    $resultado[$i]['id_proyecto']   = $value['id_proyecto'];
                    $resultado[$i]['ejecucion']     = "Error";
                    $resultado[$i]['sql']           = $sql2;
                }

                $i++;
            }
        }

        return $resultado;
    }
}
