<?php

namespace app\models;

use Yii;
use yii\base\Model;

use app\models\Mysql;
use app\models\Generalidades;

class Beneficiarios extends Model
{
    // Varibles para registro de Beneficiarios
    public $id_proyecto;
    public $tipo_doc;
    public $num_doc;
    public $nombres;
    public $apellido_1;
    public $apellido_2;
    public $genero;
    public $estrato;
    public $fecha_nacimiento;
    public $celular;

    public $id_pais;
    public $id_departamento;
    public $id_ciudad;

    public $barrio_vereda;
    public $direccion;
    public $fecha_registro;

    public $edad;
    public $antiguedad;
    public $empresa_labora;
    public $tamanio_empresa_labora;
    public $caracterizacion;
    public $nivel_ocupacional;
    public $transferencia;
    public $perfil_transferencia;
    

    // Reglas de validacion para el formulario de registro de Beneficiarios
    public function rules()
    {
        return [
            // username and password are both required
            [
              [
                'tipo_doc',
                'num_doc',
                'nombres',
                'apellido_1',
                //'apellido_2',
                'genero',
                'estrato',
                'fecha_nacimiento',
                'celular',
                'id_pais',
                'id_departamento',
                'id_ciudad',
                'barrio_vereda',
                'direccion',
                'edad',
                'antiguedad',
                'empresa_labora',
                'tamanio_empresa_labora',
                'caracterizacion',
                'nivel_ocupacional',
                'transferencia',
                'perfil_transferencia'
              ], 'required', 'message' => 'Este campo no puede estar vacío.'
            ],
            [
              [
                'tipo_doc',
                'num_doc',
                'nombres',
                'apellido_1',
                'apellido_2',
                'genero',
                'barrio_vereda',
                'direccion',
                'fecha_nacimiento',
                'antiguedad',
                'empresa_labora',
                'caracterizacion',
                'nivel_ocupacional',
                'transferencia',
                'tamanio_empresa_labora',
                'perfil_transferencia'
              ],'string', 'message' => 'Este campo es de tipo texto'
            ],
            [
              [
                'id_pais',
                'id_departamento',
                'id_ciudad',
                'estrato',
                'celular',
                'edad'
              ],'number', 'message' => 'Este campo es de tipo numérico'
            ],
        ];
    }

    // Consultar listado de beneficiarios registrados
    public function listaBeneficiarios($id_proyecto)
    {
        $db = Mysql::connection();

        $sql1 = "SELECT 
                    beneficiarios.id_beneficiario,
                    CONCAT(beneficiarios.tipo_doc,'-',beneficiarios.num_doc) AS identificacion,
                    CONCAT(beneficiarios.nombres,' ',beneficiarios.apellido_1,' ',beneficiarios.apellido_2) AS nombre,
                    beneficiarios.fecha_registro,
                    beneficiarios.estado_registro
                    FROM beneficiarios
                    INNER JOIN proyecto_beneficiarios ON proyecto_beneficiarios.id_beneficiario = beneficiarios.id_beneficiario
                    WHERE proyecto_beneficiarios.id_proyecto = '". $id_proyecto ."'";

        $consulta1 = $db->createCommand($sql1)->queryAll();
        return $consulta1;
    }

    // Consultar datos de beneficiario
    public function buscarBeneficiario($tipo_doc,$num_doc)
    {
        $db = Mysql::connection();
    }


    // Registrar beneficiario en el sistema
    public function registrarBeneficiario($POST,$id_proyecto)
    {
        $db = Mysql::connection();

        // Consultamos si el tipo y numero de identificacion existen en el sistema
        $sql1 = "SELECT     
                    id_beneficiario,
                    COUNT(*) cant
                    FROM    beneficiarios
                    WHERE   tipo_doc = '". $POST['tipo_doc'] ."'
                    AND     num_doc  = '". $POST['num_doc'] ."'";

        $consulta1 = $db->createCommand($sql1)->queryOne();

        if ($consulta1['cant'] == 0) {

            // PASO 1: Procedemos con el registro del beneficiario
            $id_beneficiario = Generalidades::getConsecutivo("beneficiarios","id_beneficiario");

            $sql3 = "INSERT INTO beneficiarios(id_beneficiario, tipo_doc, num_doc, nombres, apellido_1, apellido_2, genero, estrato, fecha_nacimiento, celular, id_ciudad, barrio_vereda, direccion, fecha_registro, usuario_creador, estado_registro) VALUES ('". $id_beneficiario ."','". $POST['tipo_doc'] ."','". $POST['num_doc'] ."','". strtoupper($POST['nombres']) ."','".strtoupper($POST['apellido_1'])."','". strtoupper($POST['apellido_2']) ."','". $POST['genero'] ."','". $POST['estrato'] ."','". $POST['fecha_nacimiento'] ."','". $POST['celular'] ."','". $POST['id_ciudad'] ."','". strtoupper($POST['barrio_vereda']) ."','". strtoupper($POST['direccion']) ."',NOW(),0,'ACTIVO')";

            $insert = $db->createCommand($sql3)->execute();

            if ($insert) {

                // Consultamos si el beneficiario esta registrado en el proyecto indicado
                $sql4 = "SELECT
                            proyecto_beneficiarios.id_proyecto_beneficiario,
                            proyecto_beneficiarios.id_beneficiario,
                            COUNT(*) cant
                            FROM proyecto_beneficiarios
                            INNER JOIN beneficiarios ON beneficiarios.id_beneficiario = proyecto_beneficiarios.id_beneficiario
                            WHERE id_proyecto = '".$id_proyecto."'
                            AND beneficiarios.tipo_doc = '".$POST['tipo_doc']."'
                            AND beneficiarios.num_doc = '".$POST['num_doc']."'";

                $consulta4 = $db->createCommand($sql4)->queryOne();

                if ($consulta4['cant'] == 0) {

                    // PASO 2: Registrar beneficiario al proyecto en sesion
                    $id_proyecto_beneficiario = Generalidades::getConsecutivo("proyecto_beneficiarios","id_proyecto_beneficiario");

                    $sql3 = "INSERT INTO proyecto_beneficiarios(id_proyecto_beneficiario, id_proyecto, id_beneficiario, fecha_registro, estado_registro) VALUES ('". $id_proyecto_beneficiario ."','". $id_proyecto ."','". $id_beneficiario ."',NOW(),'ACTIVO')";

                    $insert2 = $db->createCommand($sql3)->execute();

                    //$response = json_encode(['status'=>'test','sql'=>$sql3]);

                    if ($insert2) {

                        // PASO 3: Registrar beneficiario en los grupos de acciones de formacion seleccionados
                        $asignacion = true;

                        foreach ($POST['acciones'] as $key => $id_af) {

                            $rango_edad = Generalidades::calcularRangoEdad($POST['edad']);

                            $sql5 = "INSERT INTO beneficiarios_grupo(id_proyecto_beneficiario, id_accion_formacion, id_grupo, edad, rango_edad, antiguedad, empresa_labora, tamanio_empresa, transferencia, perfil_transferencia, caracterizacion, nivel_ocupacional, hrs_asistencia, por_asistencia, hrs_cumplimiento, por_cumplimiento, certifica, fecha_registro, estado_registro) VALUES (".$id_proyecto_beneficiario.",".$id_af.",".$POST['grupos'][$key].",'".$POST['edad']."','".strtoupper($rango_edad)."','".$POST['antiguedad']."','".strtoupper($POST['empresa_labora'])."','".$POST['tamanio_empresa']."','".$POST['transferencia']."','".$POST['perfil_transferencia']."','".$POST['caracterizacion']."','".$POST['nivel_ocupacional']."',0,0,0,0,'',NOW(),'ACTIVO')";

                            $insert3 = $db->createCommand($sql5)->execute();

                            if (!$insert3) {
                                $asignacion = false;
                            }
                        }

                        if ($asignacion) {
                            $response = json_encode([
                                'status'=>'success',
                                'message'=>'El beneficiario fue registrado en el proyecto y se realizó la asignación de grupos correctamente.',
                                'alert'=>'alert-success'
                            ]);
                        }else{
                            $response = json_encode([
                                'status'=>'error',
                                'message'=>'El beneficiario fue registrado en el proyecto pero ha ocurrido un problema con la asignación de grupos.',
                                'alert'=>'alert-warning'
                            ]);
                        }

                    }else{
                        $response = json_encode([
                            'status'=>'error',
                            'message'=>'No ha sido posible registrar el beneficiario al proyecto actual.<br>'.$sql3,
                            'alert'=>'alert-warning'
                        ]);
                    }

                }else{
                    $response = json_encode([
                        'status'=>'error',
                        'message'=>'El beneficiario ya se encuentra registrado en el presente proyecto y cuenta con una asignación a grupos.',
                        'alert'=>'alert-warning'
                    ]);
                }

            }else {
                // Error en el registro
                $response = json_encode([
                    'status'    => 'error',
                    'message'   => 'No fue posible registrar el beneficiario:<br>'.$sql3,
                    'alert'     => 'alert-danger'
                ]);        
            }

        }else{
            
            // Consultamos si el beneficiario esta registrado en el proyecto indicado
            $sql4 = "SELECT
                        proyecto_beneficiarios.id_proyecto_beneficiario,
                        proyecto_beneficiarios.id_beneficiario,
                        COUNT(*) cant
                        FROM proyecto_beneficiarios
                        INNER JOIN beneficiarios ON beneficiarios.id_beneficiario = proyecto_beneficiarios.id_beneficiario
                        WHERE id_proyecto = '".$id_proyecto."'
                        AND beneficiarios.tipo_doc = '".$POST['tipo_doc']."'
                        AND beneficiarios.num_doc = '".$POST['num_doc']."'";

            $consulta4 = $db->createCommand($sql4)->queryOne();

            if ($consulta4['cant'] == 0) {

                // PASO 2: Registrar beneficiario al proyecto en sesion
                $id_proyecto_beneficiario = Generalidades::getConsecutivo("proyecto_beneficiarios","id_proyecto_beneficiario");

                $sql3 = "INSERT INTO proyecto_beneficiarios(id_proyecto_beneficiario, id_proyecto, id_beneficiario, fecha_registro, estado_registro) VALUES ('". $id_proyecto_beneficiario ."','". $id_proyecto ."','". $consulta1['id_beneficiario'] ."',NOW(),'ACTIVO')";

                $insert3 = $db->createCommand($sql3)->execute();

                if ($insert3) {

                    // PASO 3: Registrar beneficiario en los grupos de acciones de formacion seleccionados
                    $asignacion = true;

                    foreach ($POST['acciones'] as $key => $id_af) {

                        $rango_edad = Generalidades::calcularRangoEdad($POST['edad']);

                        $sql5 = "INSERT INTO beneficiarios_grupo(id_proyecto_beneficiario, id_accion_formacion, id_grupo, edad, rango_edad, antiguedad, empresa_labora, tamanio_empresa, transferencia, perfil_transferencia, caracterizacion, nivel_ocupacional, hrs_asistencia, por_asistencia, hrs_cumplimiento, por_cumplimiento, certifica, fecha_registro, estado_registro) VALUES (".$id_proyecto_beneficiario.",".$id_af.",".$POST['grupos'][$key].",'".$POST['edad']."','".strtoupper($rango_edad)."','".$POST['antiguedad']."','".strtoupper($POST['empresa_labora'])."','".$POST['tamanio_empresa']."','".$POST['transferencia']."','".$POST['perfil_transferencia']."','".$POST['caracterizacion']."','".$POST['nivel_ocupacional']."',0,0,0,0,'',NOW(),'ACTIVO')";

                        $insert3 = $db->createCommand($sql5)->execute();

                        if (!$insert3) {
                            $asignacion = false;
                        }
                    }

                    if ($asignacion) {
                        $response = json_encode([
                            'status'=>'success',
                            'message'=>'El beneficiario fue registrado en el proyecto y se realizó la asignación de grupos correctamente.',
                            'alert'=>'alert-success'
                        ]);
                    }else{
                        $response = json_encode([
                            'status'=>'error',
                            'message'=>'El beneficiario fue registrado en el proyecto pero ha ocurrido un problema con la asignación de grupos.',
                            'alert'=>'alert-warning'
                        ]);
                    }

                }else{
                    $response = json_encode([
                        'status'=>'error',
                        'message'=>'No ha sido posible registrar el beneficiario al proyecto actual.<br>'.$sql3,
                        'alert'=>'alert-warning'
                    ]);
                }

            }else{
                $response = json_encode([
                    'status'=>'error',
                    'message'=>'El beneficiario ya se encuentra registrado en el presente proyecto y cuenta con una asignación a grupos.',
                    'alert'=>'alert-warning'
                ]);
            }

        } 

        return $response;
    }

    // Registrar beneficiario al proyecto
    public static function registrarBeneficiarioProyecto($POST,$id_proyecto)
    {
        $db = Mysql::connection();

        $sql1 = "SELECT 
                id_beneficiario
                FROM beneficiarios
                WHERE tipo_doc = '".$POST['tipo_doc']."'
                AND num_doc = '".$POST['num_doc']."'";

        $datos = $db->createCommand($sql1)->queryOne();

        

        return $response;
    }

    // Buscar información del beneficiario para editar
    public function buscarInformacionBeneficiario($id_proyecto,$tipo_doc,$num_doc)
    {
        $db = Mysql::connection();

        // Evaluamos si la identificacion corresponde a un beneficiario registrado en el proyecto actual
        $sql = "SELECT COUNT(*) cant 
                FROM proyecto_beneficiarios 
                INNER JOIN beneficiarios ON beneficiarios.id_beneficiario = proyecto_beneficiarios.id_beneficiario
                WHERE proyecto_beneficiarios.id_proyecto = '".$id_proyecto."'
                AND beneficiarios.tipo_doc = '".$tipo_doc."'
                AND beneficiarios.num_doc = '".$num_doc."'";

        $consulta = $db->createCommand($sql)->queryOne();

        if ($consulta['cant'] > 0) {
            
            // Consultamos informacion del beneficiario
            $sql2 = "SELECT 
                        beneficiarios.id_beneficiario,
                        beneficiarios.tipo_doc,
                        beneficiarios.num_doc,
                        beneficiarios.nombres,
                        beneficiarios.apellido_1,
                        beneficiarios.apellido_2,
                        beneficiarios.genero,
                        beneficiarios.estrato,
                        DATE_FORMAT(beneficiarios.fecha_nacimiento,'%Y-%m-%d') AS fecha_nacimiento,
                        beneficiarios.celular,
                        beneficiarios.id_ciudad,
                        beneficiarios.barrio_vereda,
                        beneficiarios.direccion,
                        proyecto_beneficiarios.id_proyecto_beneficiario,
                        proyecto_beneficiarios.id_proyecto,
                        beneficiarios_grupo.edad,
                        beneficiarios_grupo.antiguedad,
                        beneficiarios_grupo.empresa_labora,
                        beneficiarios_grupo.tamanio_empresa,
                        beneficiarios_grupo.transferencia,
                        beneficiarios_grupo.perfil_transferencia,
                        beneficiarios_grupo.caracterizacion,
                        beneficiarios_grupo.nivel_ocupacional
                        FROM beneficiarios
                        INNER JOIN proyecto_beneficiarios ON proyecto_beneficiarios.id_beneficiario = beneficiarios.id_beneficiario
                        INNER JOIN beneficiarios_grupo ON beneficiarios_grupo.id_proyecto_beneficiario = proyecto_beneficiarios.id_proyecto_beneficiario
                        WHERE proyecto_beneficiarios.id_proyecto = '".$id_proyecto."'
                        AND beneficiarios.tipo_doc = '".$tipo_doc."'
                        AND beneficiarios.num_doc = '".$num_doc."'
                        LIMIT 1";

            $informacion = $db->createCommand($sql2)->queryOne();

            if (!empty($informacion)) {
                $response = json_encode([
                    'status'        => 'success',
                    'message'       => 'Se ha encontrado informacion del beneficiario',
                    'alert'         => 'alert-success',
                    'informacion'   => $informacion
                ]);
            }else{
                $response = json_encode([
                    'status'    => 'error',
                    'message'   => 'No se ha encontrado información del beneficiario.',
                    'alert'     => 'alert-warning'
                ]);
            }

        }else{
            $response = json_encode([
                'status'    => 'error',
                'message'   => 'El beneficiario ingresado no pertenece al proyecto actual.',
                'alert'     => 'alert-warning'
            ]);
        }

        return $response;

    }

    // Actualizar informacion de registro del beneficiario
    public function actualizarInformacion($POST)
    {
        $db = Mysql::connection();

        $id_proyecto_beneficiario   = $POST['id_proyecto_beneficiario'];
        $id_beneficiario            = $POST['id_beneficiario'];
        $tipo_doc                   = $POST['tipo_doc'];
        $num_doc                    = $POST['num_doc'];
        $nombres                    = strtoupper($POST['nombres']);
        $apellido_1                 = strtoupper($POST['apellido_1']);
        $apellido_2                 = strtoupper($POST['apellido_2']);
        $genero                     = $POST['genero'];
        $estrato                    = $POST['estrato'];
        $fecha_nacimiento           = $POST['fecha_nacimiento'];
        $celular                    = $POST['celular'];
        $id_departamento            = $POST['id_departamento'];
        $id_ciudad                  = $POST['id_ciudad'];
        $barrio_vereda              = strtoupper($POST['barrio_vereda']);
        $direccion                  = strtoupper($POST['direccion']);
        $edad                       = $POST['edad'];
        $rango_edad                 = Generalidades::calcularRangoEdad($edad); 
        $antiguedad                 = $POST['antiguedad'];
        $empresa_labora             = strtoupper($POST['empresa_labora']);
        $tamanio_empresa            = $POST['tamanio_empresa'];
        $caracterizacion            = $POST['caracterizacion'];
        $nivel_ocupacional          = $POST['nivel_ocupacional'];
        $transferencia              = $POST['transferencia'];
        $perfil_transferencia       = $POST['perfil_transferencia'];

        
        $sql = "SELECT num_doc FROM beneficiarios WHERE id_beneficiario = '".$id_beneficiario."'";
        $documento = $db->createCommand($sql)->queryOne();

        if ($documento['num_doc'] == $num_doc) {
            
            // Actualizacion en tabla: beneficiarios
            $sql1 = "UPDATE beneficiarios SET nombres = '".$nombres."', apellido_1 = '".$apellido_1."', apellido_2 = '".$apellido_2."', genero = '".$genero."', estrato = '".$estrato."', fecha_nacimiento = '".$fecha_nacimiento."', celular = '".$celular."', id_ciudad = '".$id_ciudad."', barrio_vereda = '".$barrio_vereda."', direccion = '".$direccion."' WHERE id_beneficiario = '".$id_beneficiario."'";

            $updateBeneficiario = $db->createCommand($sql1)->execute();

            $beneficiarios = ($updateBeneficiario)?"Se actualizo la información del beneficiario":"No se realiza actualización de datos.";

            // Actualizacion en tabla: beneficiarios_grupo
            $sql2 = "UPDATE beneficiarios_grupo SET edad = '".$edad."', rango_edad = '".$rango_edad."', antiguedad = '".$antiguedad."', empresa_labora = '".$empresa_labora."', tamanio_empresa = '".$tamanio_empresa."', transferencia = '".$transferencia."', perfil_transferencia = '".$perfil_transferencia."', caracterizacion = '".$caracterizacion."', nivel_ocupacional = '".$nivel_ocupacional."' WHERE id_proyecto_beneficiario = '".$id_proyecto_beneficiario."'";

            $updateGrupos = $db->createCommand($sql2)->execute();

            $beneficiarios_grupo = ($updateGrupos)?"Se actualizo la información del beneficiario en cada grupo":"No se realiza actualización de datos.";

            $response = json_encode([
                'status'                => 'success',
                'alert'                 => 'alert-info',
                'beneficiarios'         => $beneficiarios,
                'beneficiarios_grupo'   => $beneficiarios_grupo
            ]);

        }else{
            // Evaluamos que el nuevo documento este disponible para actualizar el registro
            $sql = "SELECT COUNT(*) cant FROM beneficiarios WHERE num_doc = '".$num_doc."'";
            $validacion = $db->createCommand($sql)->queryOne();

            if ($validacion['cant'] == 0) {
                // Numero de identificacion disponible
                // Actualizacion en tabla: beneficiarios
                $sql1 = "UPDATE beneficiarios SET num_doc = '".$num_doc."', nombres = '".$nombres."', apellido_1 = '".$apellido_1."', apellido_2 = '".$apellido_2."', genero = '".$genero."', estrato = '".$estrato."', fecha_nacimiento = '".$fecha_nacimiento."', celular = '".$celular."', id_ciudad = '".$id_ciudad."', barrio_vereda = '".$barrio_vereda."', direccion = '".$direccion."' WHERE id_beneficiario = '".$id_beneficiario."'";

                $updateBeneficiario = $db->createCommand($sql1)->execute();

                $beneficiarios = ($updateBeneficiario)?"Se actualizo la información del beneficiario":"No se realiza actualización de datos.";

                // Actualizacion en tabla: beneficiarios_grupo
                $sql2 = "UPDATE beneficiarios_grupo SET edad = '".$edad."', rango_edad = '".$rango_edad."', antiguedad = '".$antiguedad."', empresa_labora = '".$empresa_labora."', tamanio_empresa = '".$tamanio_empresa."', transferencia = '".$transferencia."', perfil_transferencia = '".$perfil_transferencia."', caracterizacion = '".$caracterizacion."', nivel_ocupacional = '".$nivel_ocupacional."' WHERE id_proyecto_beneficiario = '".$id_proyecto_beneficiario."'";

                $updateGrupos = $db->createCommand($sql2)->execute();

                $beneficiarios_grupo = ($updateGrupos)?"Se actualizo la información del beneficiario en cada grupo":"No se realiza actualización de datos.";

                $response = json_encode([
                    'status'                => 'success',
                    'alert'                 => 'alert-info',
                    'beneficiarios'         => $beneficiarios,
                    'beneficiarios_grupo'   => $beneficiarios_grupo
                ]);
            }else{
                // Numero de identificacion no disponible

                // Actualizacion en tabla: beneficiarios_grupo
                $sql2 = "UPDATE beneficiarios_grupo SET edad = '".$edad."', rango_edad = '".$rango_edad."', antiguedad = '".$antiguedad."', empresa_labora = '".$empresa_labora."', tamanio_empresa = '".$tamanio_empresa."', transferencia = '".$transferencia."', perfil_transferencia = '".$perfil_transferencia."', caracterizacion = '".$caracterizacion."', nivel_ocupacional = '".$nivel_ocupacional."' WHERE id_proyecto_beneficiario = '".$id_proyecto_beneficiario."'";

                $updateGrupos = $db->createCommand($sql2)->execute();

                $beneficiarios_grupo = ($updateGrupos)?"Se actualizo la información del beneficiario en cada grupo":"No se realiza actualización de datos.";

                $response = json_encode([
                    'status'                => 'success',
                    'alert'                 => 'alert-info',
                    'beneficiarios'         => 'No fue posible actualizar el beneficiario por que el número de documento ingresado ya esta registrado para otro beneficiario.',
                    'beneficiarios_grupo'   => $beneficiarios_grupo
                ]);
            }

        }

        return $response;

    }

    // Consultar informacion del proyecto por id_proyecto
    public static function infoProyecto($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT
                proyectos.titulo,
                proyectos.n_radicado
                FROM proyectos
                WHERE proyectos.id_proyecto = '".$id_proyecto."'";

        $consulta = $db->createCommand($sql)->queryOne();

        return $consulta;
    }

    // Consultar beneficiarios registrados del proyecto
    public static function beneficiarios($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT
                beneficiarios.id_beneficiario,
                CONCAT(beneficiarios.tipo_doc,'-',beneficiarios.num_doc) AS identificacion,
                CONCAT(beneficiarios.nombres,' ',beneficiarios.apellido_1,' ',beneficiarios.apellido_2) AS nombre,
                beneficiarios.tipo_doc,
                beneficiarios.num_doc,
                proyecto_beneficiarios.id_proyecto_beneficiario,
                beneficiarios_grupo.id_accion_formacion,
                beneficiarios_grupo.id_grupo,
                acciones_formacion.nombre AS accion_formacion,
                accion_formacion_grupos.nombre AS grupo
                FROM beneficiarios_grupo
                INNER JOIN accion_formacion_grupos ON accion_formacion_grupos.id_accion_formacion = beneficiarios_grupo.id_accion_formacion AND accion_formacion_grupos.id_grupo = beneficiarios_grupo.id_grupo
                INNER JOIN acciones_formacion ON acciones_formacion.id_accion_formacion = accion_formacion_grupos.id_accion_formacion
                INNER JOIN proyecto_beneficiarios ON proyecto_beneficiarios.id_proyecto_beneficiario = beneficiarios_grupo.id_proyecto_beneficiario
                INNER JOIN beneficiarios ON beneficiarios.id_beneficiario = proyecto_beneficiarios.id_beneficiario
                WHERE proyecto_beneficiarios.id_proyecto = '".$id_proyecto."'
                AND beneficiarios_grupo.estado_registro = 'ACTIVO'";

        $consulta = $db->createCommand($sql)->queryAll();

        return $consulta;
    }
}
