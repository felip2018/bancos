<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\Session;

use app\models\Mysql;
use app\models\Generalidades;

class LoginForm extends Model
{
    public $username;
    public $password;

    // Reglas de validacion para el formulario de inicio de sesion
    public function rules()
    {
        return [
            // username and password are both required
            [
              [
                'username',
                'password'
              ], 'required', 'message' => 'Este campo no puede estar vacío.'
            ],
            [
                ['username'],'number', 'message' => 'Campo de tipo numérico'
            ],
            [
                ['password'],'string', 'message' => 'Campo de tipo texto'
            ],
        ];
    }

    // Realizamos la validacion del usuario en la base de datos
    public function validateLogin()
    {
        $db = Mysql::connection();

        // Consultamos si el usuario ingresado existe
        $sql = 'SELECT
                proyectos.id_proyecto,
                proyectos.n_radicado,
                proyectos.clave,
                proyectos.intentos,
                proyectos.estado_final,
                proyectos.estado_registro AS estado_cuenta,
                convocatorias.estado_convocatoria
                FROM proyectos 
                INNER JOIN convocatorias_tipo ON convocatorias_tipo.id_tipo_convocatoria = proyectos.id_tipo_convocatoria
                INNER JOIN convocatorias ON convocatorias.id_convocatoria = convocatorias_tipo.id_convocatoria
                WHERE proyectos.n_radicado = "' . $this->username . '"';

        $consulta = $db->createCommand($sql)->queryOne();

        if (!empty($consulta)) {

            if ($consulta['estado_convocatoria'] != 'CERRADA') {

                if ($consulta['estado_cuenta'] == 'ACTIVO') {
                    
                    if ($consulta['estado_final'] == 'APROBADO') {
                        
                        //if (Yii::$app->getSecurity()->validatePassword($this->password,$consulta['Clave']))
                        if ($this->password == $consulta['clave']) {

                            // Inicio de sesion exitoso

                            $session = Yii::$app->session;
                            $session->open();

                            $session['isLogged'] = true;

                            // Consultamos la informacion para las variables de sesion
                            $sql_info = 'SELECT 
                                            proyectos.id_proyecto,
                                            proyectos.n_radicado,
                                            proyectos.clave,
                                            proyectos.sesion,
                                            proyectos.estado_registro AS estado_cuenta,
                                            proyectos.titulo,
                                            DATE_FORMAT(proyectos.fecha_registro,"%d-%m-%Y") registro_proyecto,
                                            proyectos.estado_proyecto,
                                            convocatorias_tipo.modalidad,
                                            convocatorias_tipo.porcentaje_cofinanciacion,
                                            convocatorias_tipo.porcentaje_contrapartida,
                                            convocatorias.fecha_inicio,
                                            convocatorias.fecha_cierre,
                                            convocatorias.presupuesto_total,
                                            convocatorias.anio,
                                            convocatorias.nombre convocatoria,
                                            convocatorias.programa_base,
                                            convocatorias.presupuesto_maximo,
                                            convocatorias.meses_proyectos,
                                            convocatorias.tipo_financiacion,
                                            convocatorias.estado_convocatoria
                                            FROM proyectos
                                            INNER JOIN convocatorias_tipo ON convocatorias_tipo.id_tipo_convocatoria = proyectos.id_tipo_convocatoria
                                            INNER JOIN convocatorias ON convocatorias.id_convocatoria = convocatorias_tipo.id_convocatoria
                                            WHERE proyectos.n_radicado = "' . $this->username . '"';

                            $consulta_info = $db->createCommand($sql_info)->queryOne();

                            if (!empty($consulta_info)) {
                                // Creación de variables de sesion
                                $session['data'] = $consulta_info;
                            }

                            // Resetearmos el numero de intentos de inicio de sesion
                            $sql_4 = 'UPDATE proyectos SET intentos = 0, sesion = "CONECTADO" WHERE n_radicado = "' . $this->username . '"';
                            $update_3 = $db->createCommand($sql_4)->execute();

                            $response = json_encode([
                                'status'    => 'success',
                                'message'   => 'Inicio de sesión exitoso',
                                'alert'     => 'alert-success',
                            ]);

                        }else {

                            // Intento fallido
                            if ($consulta['intentos'] < 5) {

                                $sql_3 = 'UPDATE proyectos SET intentos = intentos + 1 WHERE n_radicado = "' . $this->username . '"';

                                $update_2 = $db->createCommand($sql_3)->execute();

                                if ($update_2) {
                                    $message = 'Clave incorrecta.Intentos ('. ($consulta['intentos'] + 1) . ' de 5)';
                                }

                            }else {
                                // Se ha excedido el limite de intentos
                                $sql_2 = 'UPDATE proyectos SET estado_registro = "BLOQUEO DE SEGURIDAD" WHERE n_radicado = "' . $this->username . '"';
                                $update = $db->createCommand($sql_2)->execute();

                                if ($update) {
                                    // Se ha bloqueado la cuenta
                                    $message = 'Clave incorrecta.<br>Ha excedido el número de intentos permitido para iniciar sesión. La cuenta ha sido bloqueada como medida de seguridad, por favor comuniquese con el administrador para reactivar la cuenta.';

                                }

                            }


                            $response = json_encode([
                                'status'    => 'Error',
                                'message'   => $message,
                                'alert'     => 'alert-danger',
                            ]);
                        }

                    }else{
                        $response = json_encode([
                            'status'    => 'Error',
                            'message'   => 'No es posible ingresar al sistema, el estado del proyecto es: <b>' . $consulta['estado_final'] . '</b>',
                            'alert'     => 'alert-danger',
                        ]);  
                    }

                }else {
                    $response = json_encode([
                        'status'    => 'Error',
                        'message'   => 'No es posible ingresar al sistema, el estado de la cuenta es: <b>' . $consulta['estado_cuenta'] . '</b>',
                        'alert'     => 'alert-danger',
                    ]);    
                }

            }else {
                $response = json_encode([
                    'status'    => 'Error',
                    'message'   => 'No es posible ingresar al sistema, el estado de la convocatoria es: <b>' . $consulta['estado_convocatoria'] . '</b>',
                    'alert'     => 'alert-danger',
                ]);
            }

        }else {
            $response = json_encode([
                'status'    => 'Error',
                'message'   => 'El número de radicado ingresado no esta registrado en el sistema.',
                'alert'     => 'alert-danger',
            ]);
        }

        return $response;
    }

    // Cerrar sesion en la base de datos
    public function logout($nick)
    {
        $db = Mysql::connection();

        $sql = 'UPDATE proyectos SET sesion = "DESCONECTADO" WHERE n_radicado = "' . $nick . '"';

        $update = $db->createCommand($sql)->execute();

        return $update;
    }
}
