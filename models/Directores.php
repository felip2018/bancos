<?php

namespace app\models;

use Yii;
use yii\base\Model;

use app\models\Mysql;
use app\models\Generalidades;

class Directores extends Model
{
    // Varibles para registro de Directores de proyecto
    public $tipo_doc;
    public $num_doc;
    public $nombres;
    public $primer_apellido;
    public $segundo_apellido;
    public $correo;
    

    // Reglas de validacion para el formulario de registro de Beneficiarios
    public function rules()
    {
        return [
            // username and password are both required
            [
                [
                    'tipo_doc',
                    'num_doc',
                    'nombres',
                    'primer_apellido',
                    //'segundo_apellido',
                    'correo',
                ], 'required', 'message' => 'Este campo no puede estar vacío.'
            ],
            [
                [
                    'tipo_doc',
                    'num_doc',
                    'nombres',
                    'primer_apellido',
                    'segundo_apellido'
                ],'string', 'message' => 'Este campo es de tipo texto'
            ],
            [
                [
                    'correo'
                ],'email','message'=> 'Formato de correo electronico inválido. Verifique que no tenga espacios en blanco.'
            ]
        ];
    }

    // Consultar listado de directores de proyecto registrados
    public function listaDirectores($id_proyecto)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                    directores.id_director,
                    CONCAT(directores.tipo_doc,'-',directores.num_doc) AS identificacion,
                    CONCAT(directores.nombres,' ',directores.primer_apellido,' ',directores.segundo_apellido) AS nombre,
                    directores.fecha_registro,
                    directores.estado_registro
                    FROM directores
                    INNER JOIN directores_proyectos ON directores_proyectos.id_director = directores.id_director
                    WHERE directores_proyectos.id_proyecto = '". $id_proyecto ."'";

        $consulta = $db->createCommand($sql)->queryAll();
        return $consulta;
    }

    // Consultar datos de director
    public function buscarDirector($tipo_doc,$num_doc)
    {
        $db = Mysql::connection();

        $sql = "SELECT * 
                FROM directores 
                WHERE tipo_doc = '".$tipo_doc."'
                AND num_doc = '".$num_doc."'";

        $consulta = $db->createCommand($sql)->queryOne();

        if (!empty($consulta)) {
            $response = json_encode([
                'status' => 'success',
                'datos' => $consulta
            ]);
        }else{
            $response = json_encode([
                'status' => 'vacio',
                'message' => 'No se han encontrado resultados.'
            ]);
        }

        return $response;
    }

    // Registrar director de proyecto en el sistema
    public function registrarDirector($POST,$id_proyecto)
    {
        $db = Mysql::connection();

        // Evaluamos si el proyecto ya cuenta con un director registrado
        $sql0 = "SELECT COUNT(*) cant FROM directores_proyectos WHERE id_proyecto = '".$id_proyecto."' AND estado_registro = 'ACTIVO'";

        $consulta0 = $db->createCommand($sql0)->queryOne();


        if ($consulta0['cant'] == 0) {
            
            // Consultamos si el tipo y numero de identificacion existen en el sistema
            $sql1 = "SELECT     COUNT(*) cant
                        FROM    directores
                        WHERE   tipo_doc = '". $POST['tipo_doc'] ."'
                        AND     num_doc  = '". $POST['num_doc'] ."'";

            $consulta1 = $db->createCommand($sql1)->queryOne();

            if ($consulta1['cant'] == 0) {
                
                $nombre = $POST['nombres'].' '.$POST['primer_apellido'].' '.$POST['segundo_apellido'];
                
                $sql2 = "SELECT COUNT(*) cant
                            FROM    directores
                            WHERE   CONCAT(nombres,' ',primer_apellido,' ',segundo_apellido) = '". $nombre ."'";

                $consulta2 = $db->createCommand($sql2)->queryOne();

                if ($consulta2['cant'] == 0) {

                    // Procedemos con el registro del director
                    $id_director = Generalidades::getConsecutivo("directores","id_director");

                    $sql3 = "INSERT INTO directores(id_director, tipo_doc, num_doc, nombres, primer_apellido, segundo_apellido, correo, fecha_registro, estado_registro) VALUES (".$id_director.",'".$POST['tipo_doc']."','".$POST['num_doc']."','".strtoupper($POST['nombres'])."','".strtoupper($POST['primer_apellido'])."','".strtoupper($POST['segundo_apellido'])."','".strtoupper($POST['correo'])."',NOW(),'ACTIVO')";

                    $insert = $db->createCommand($sql3)->execute();

                    if ($insert) { 

                        // Registrar el director al proyecto en sesion
                        Directores::registrarDirectorProyecto($id_proyecto,$id_director);

                        // Subir documentos requeridos
                        Directores::uploadDirectorFiles($id_director);

                        // Registro exitoso
                        $response = json_encode([
                            'status'        => 'success',
                            'message'       => 'El director ha sido registrado correctamente en el sistema. Código: '.$id_director,
                            'id_director'   => $id_director,
                            'alert'         => 'alert-success'
                        ]); 

                    }else {
                        // Error en el registro
                        $response = json_encode([
                            'status'    => 'error',
                            'message'   => 'No fue posible registrar el director:<br>'.$sql3,
                            'alert'     => 'alert-danger'
                        ]);        
                    }

                }else{
                    // El director ya esta registrado bajo otro numero de documento
                    $response = json_encode([
                        'status'    => 'error',
                        'message'   => 'El director "'.$nombre.'" ya esta registrado en el sistema con otro número de identificación',
                        'alert'     => 'alert-danger'
                    ]);    
                }
                  

            }else{
                
                $response = json_encode([
                    'status'    => 'error',
                    'message'   => 'El tipo y número de identificación ya estan en uso para un director registrado.',
                    'alert'     => 'alert-danger'
                ]);
            } 
            
        }else{
            $response = json_encode([
                'status'    => 'error',
                'message'   => 'El proyecto actual ya cuenta con un director registrado en el sistema.',
                'alert'     => 'alert-warning'
            ]);            
        }

        return $response;

    }

    // Upload director files 
    public static function uploadDirectorFiles($id_director)
    {
        $db = Mysql::connection();

        // Consultamos la informacion registrada del director 
        $sql = "SELECT * FROM directores WHERE id_director = '".$id_director."'";
        $consulta = $db->createCommand($sql)->queryOne();

        // Consultamos los documentos requeridos por el banco

        $sql2 = "SELECT
                documentos_bancos.id_documento_banco,
                documentos_bancos.id_documento,
                documentos.nombre,
                documentos_bancos.obligatorio,
                documentos_bancos.tipo_banco,
                documentos_bancos.input_name,
                documentos_bancos.fecha_registro,
                documentos_bancos.estado_registro
                FROM documentos_bancos
                INNER JOIN documentos ON documentos.id_documento = documentos_bancos.id_documento
                WHERE documentos_bancos.tipo_banco = 'BANCO DE DIRECTORES'
                AND documentos_bancos.estado_registro = 'ACTIVO'";

        $consulta2 = $db->createCommand($sql2)->queryAll();

        if (!empty($consulta2)) {
            foreach ($consulta2 as $key => $value) {
                
                if ($_FILES[$value['input_name']]['name'] != "") {

                    $mimeType = $_FILES[$value['input_name']]['type'];

                    if ($mimeType == "application/pdf") {
                    
                        $year = date('Y');
                        $directorio = "../web/convocatoria_".$year."/bancos/directores/".$consulta['num_doc']."/";

                        if (!is_dir($directorio)) {
                            mkdir($directorio);
                        }

                        $mimeType = $_FILES[$value['input_name']]['type'];

                        //$target_file = $directorio."/".basename($_FILES[$value['input_name']]['name']);

                        $nombre_doc = $value['input_name'].".pdf";
                        
                        $target_file    = $directorio."/".$nombre_doc;

                        move_uploaded_file($_FILES[$value['input_name']]['tmp_name'], $target_file);

                        $id_director_documento = Generalidades::getConsecutivo("directores_documentos","id_director_documento");

                        // Registrar documento en el sistema para el director de proyecto
                        $sql3 = "INSERT INTO directores_documentos(id_director_documento, id_director, id_documento_banco, id_usuario_sena, url, estado_doc, fecha_registro, estado_registro) VALUES (".$id_director_documento.",".$id_director.",".$value['id_documento_banco'].",0,'".$nombre_doc."','PENDIENTE',NOW(),'ACTIVO')";

                        $insertDoc = $db->createCommand($sql3)->execute();
                    }

                }

            }
        }

    }

    // Registrar director al proyecto
    public static function registrarDirectorProyecto($id_proyecto,$id_director)
    {
        $db = Mysql::connection();

        // Consultamos si el director esta registrado en el proyecto indicado
        $sql2 = "SELECT
                    COUNT(*) cant
                    FROM directores_proyectos
                    WHERE id_proyecto = '". $id_proyecto ."'
                    AND id_director = '". $id_director ."'";

        $consulta2 = $db->createCommand($sql2)->queryOne();

        if ($consulta2['cant'] == 0) {
            // Registramos el beneficiario al proyecto

            $id_director_proyecto = Generalidades::getConsecutivo("directores_proyectos","id_director_proyecto");

            $sql3 = "INSERT INTO directores_proyectos(id_director_proyecto, id_director, id_proyecto, fecha_registro, estado_registro) VALUES ('". $id_director_proyecto ."','". $id_director ."','". $id_proyecto ."',NOW(),'ACTIVO')";

            $insert = $db->createCommand($sql3)->execute();

        }

    }

    // Registrar experiencia a un director selecconado
    public function registrarExperiencia($POST)
    {
        $db = Mysql::connection();

        // Consultamos la informacion basica del director del proyecto
        $sql = "SELECT 
                id_director,
                tipo_doc,
                num_doc
                FROM directores
                WHERE id_director = '".$POST['id_director']."'";

        $consulta = $db->createCommand($sql)->queryOne();

        // Evaluamos si hay un documento para cargar al servidor
        if (isset($_FILES['soporte_experiencia']) && !empty($_FILES['soporte_experiencia'])) {

            //$response = json_encode($_FILES['soporte_experiencia']);
            $year = date('Y');
            $directorio = "../web/convocatoria_".$year."/bancos/directores/".$consulta['num_doc']."/";

            $mimeType       = $_FILES['soporte_experiencia']['type'];

            if ($mimeType == "application/pdf") {

                // validamos si existe el directorio para crearlo o no
                if (!is_dir($directorio)) {
                    mkdir($directorio);
                }

                $id_director_experiencia = Generalidades::getConsecutivo("directores_experiencias","id_director_experiencia");

                $nombre_doc = "Experiencia_id_".$id_director_experiencia.".pdf";

                $ext = explode('/', $mimeType);

                //$target_file    = $directorio."/".basename($_FILES['soporte_experiencia']['name']);
                $target_file    = $directorio."/".$nombre_doc;

                // validamos si el archivo ya existe
                if (!file_exists($target_file)) {

                    if (move_uploaded_file($_FILES['soporte_experiencia']['tmp_name'], $target_file)) {

                        // Registramos la experiencia del director despues de cargar el archivo

                        $sql2 = "INSERT INTO directores_experiencias(id_director_experiencia, id_director, id_usuario_sena, tipo, descripcion, url, estado_doc, fecha_registro, estado_registro) VALUES (".$id_director_experiencia.",'".$POST['id_director']."',0,'".$POST['tipo']."','".strtoupper($POST['descripcion'])."','".$nombre_doc."','PENDIENTE',NOW(),'ACTIVO')";

                        $insert = $db->createCommand($sql2)->execute();
                        //$response = json_encode(["sql"=>$sql2]);
                        if ($insert) {

                            $response = json_encode([
                                'status' => 'success',
                                'alert' => 'alert-warning',
                                'message'=> 'La experiencia ha sido registrada correctamente.'
                            ]);

                        }else{
                            
                            $response = json_encode([
                                'status' => 'error',
                                'alert' => 'alert-warning',
                                'message'=> 'Ha ocurrido un error al registrar la experiencia.'
                            ]);

                        }
                    }else{
                        $response = json_encode([
                            'status' => 'error',
                            'alert' => 'alert-danger',
                            'message'=> 'No se ha podido subir el documento al servidor.'
                        ]);
                    }

                }else{
                    // El archivo ya se encuentra en el servidor
                    $response = json_encode([
                        'status' => 'error',
                        'alert' => 'alert-warning',
                        'message'=> 'El archivo ya se encuentra cargado al servidor.'
                    ]);    
                }
            }else{
                // Formato invalido
                $response = json_encode([
                    'status' => 'error',
                    'alert' => 'alert-warning',
                    'message'=> 'Formato de documento inválido.'
                ]);
            }

        }else{
            // Documento vacio
            $response = json_encode([
                'status' => 'vacio',
                'alert' => 'alert-warning',
                'message' => 'Ingrese el soporte de la experiencia.'
            ]);
        }

        return $response;

    }

    // Buscar experiencias registradas de un director seleccionado
    public function buscarExperiencias($id_director)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                directores_experiencias.id_director_experiencia,
                directores_experiencias.id_director,
                directores.num_doc,
                directores_experiencias.tipo,
                directores_experiencias.descripcion,
                directores_experiencias.url,
                directores_experiencias.estado_doc,
                directores_experiencias.fecha_registro,
                DATE_FORMAT(directores_experiencias.fecha_registro,'%Y') AS year,
                directores_experiencias.estado_registro
                FROM directores_experiencias
                INNER JOIN directores ON directores.id_director = directores_experiencias.id_director
                WHERE directores_experiencias.id_director = '".$id_director."'";


        $consulta = $db->createCommand($sql)->queryAll();

        $response = json_encode([
            'status'        => 'success',
            'experiencias'  => $consulta
        ]);

        return $response;

    }

    // Buscar titulos registrados de un director seleccionado
    public function buscarTitulos($id_director)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                directores_titulos.id_director_titulo,
                directores_titulos.id_director,
                directores.num_doc,
                directores_titulos.tipo_titulo,
                directores_titulos.descripcion,
                directores_titulos.url,
                directores_titulos.estado_doc,
                directores_titulos.fecha_registro,
                DATE_FORMAT(directores_titulos.fecha_registro,'%Y') AS year,
                directores_titulos.estado_registro
                FROM directores_titulos
                INNER JOIN directores ON directores.id_director = directores_titulos.id_director
                WHERE directores_titulos.id_director = '".$id_director."'";


        $consulta = $db->createCommand($sql)->queryAll();

        $response = json_encode([
            'status'        => 'success',
            'titulos'  => $consulta
        ]);

        return $response;

    }

    // Registrar titulo a un director selecconado
    public function registrarTitulo($POST)
    {
        $db = Mysql::connection();

        // Consultamos la informacion basica del director del proyecto
        $sql = "SELECT 
                id_director,
                tipo_doc,
                num_doc
                FROM directores
                WHERE id_director = '".$POST['id_director']."'";

        $consulta = $db->createCommand($sql)->queryOne();

        // Evaluamos si hay un documento para cargar al servidor
        if (isset($_FILES['soporte_titulo']) && !empty($_FILES['soporte_titulo'])) {

            //$response = json_encode($_FILES['soporte_titulo']);
            $year = date('Y');
            $directorio = "../web/convocatoria_".$year."/bancos/directores/".$consulta['num_doc']."/";

            $mimeType       = $_FILES['soporte_titulo']['type'];

            if ($mimeType == "application/pdf") {

                // validamos si existe el directorio para crearlo o no
                if (!is_dir($directorio)) {
                    mkdir($directorio);
                }

                $id_director_titulo = Generalidades::getConsecutivo("directores_titulos","id_director_titulo");

                $nombre_doc = "Titulo_id_".$id_director_titulo.".pdf";

                //$target_file    = $directorio."/".basename($_FILES['soporte_titulo']['name']);
                $target_file    = $directorio."/".$nombre_doc;

                // validamos si el archivo ya existe
                if (!file_exists($target_file)) {

                    if (move_uploaded_file($_FILES['soporte_titulo']['tmp_name'], $target_file)) {

                        // Registramos la experiencia del director despues de cargar el archivo

                        $sql2 = "INSERT INTO directores_titulos(id_director_titulo, id_director, id_usuario_sena, tipo_titulo, descripcion, url, estado_doc, fecha_registro, estado_registro) VALUES (".$id_director_titulo.",".$POST['id_director'].",0,'".$POST['tipo_titulo']."','".strtoupper($POST['descripcion'])."','".$nombre_doc."','PENDIENTE',NOW(),'ACTIVO')";

                        $insert = $db->createCommand($sql2)->execute();

                        if ($insert) {

                            $response = json_encode([
                                'status' => 'success',
                                'alert' => 'alert-warning',
                                'message'=> 'El titulo ha sido registrado correctamente.'
                            ]);

                        }else{
                            
                            $response = json_encode([
                                'status' => 'error',
                                'alert' => 'alert-warning',
                                'message'=> 'Ha ocurrido un error al registrar el titulo.'
                            ]);

                        }
                    }else{
                        $response = json_encode([
                            'status' => 'error',
                            'alert' => 'alert-danger',
                            'message'=> 'No se ha podido subir el documento al servidor.'
                        ]);
                    }

                }else{
                    // El archivo ya se encuentra en el servidor
                    $response = json_encode([
                        'status' => 'error',
                        'alert' => 'alert-warning',
                        'message'=> 'El archivo ya se encuentra cargado al servidor.'
                    ]);    
                }
            }else{
                // Formato invalido
                $response = json_encode([
                    'status' => 'error',
                    'alert' => 'alert-warning',
                    'message'=> 'Formato de documento inválido.'
                ]);
            }

        }else{
            // Documento vacio
            $response = json_encode([
                'status' => 'vacio',
                'alert' => 'alert-warning',
                'message' => 'Ingrese el soporte del titulo.'
            ]);
        }

        return $response;

    }

    // Buscar referencias registradas de un director seleccionado
    public function buscarReferencias($id_director)
    {
        $db = Mysql::connection();

        $sql = "SELECT 
                directores_referencias.id_director_referencia,
                directores_referencias.id_director,
                directores.num_doc,
                directores_referencias.nombre,
                directores_referencias.empresa,
                directores_referencias.cargo,
                directores_referencias.telefono,
                directores_referencias.url,
                directores_referencias.estado_doc,
                directores_referencias.fecha_registro,
                DATE_FORMAT(directores_referencias.fecha_registro,'%Y') AS year,
                directores_referencias.estado_registro
                FROM directores_referencias
                INNER JOIN directores ON directores.id_director = directores_referencias.id_director
                WHERE directores_referencias.id_director = '".$id_director."'";


        $consulta = $db->createCommand($sql)->queryAll();

        $response = json_encode([
            'status'        => 'success',
            'referencias'  => $consulta
        ]);

        return $response;

    }


    // Registrar referencia de un director selecconado
    public function registrarReferencia($POST)
    {
        $db = Mysql::connection();

        // Consultamos la informacion basica del director del proyecto
        $sql = "SELECT 
                id_director,
                tipo_doc,
                num_doc
                FROM directores
                WHERE id_director = '".$POST['id_director']."'";

        $consulta = $db->createCommand($sql)->queryOne();

        // Evaluamos si hay un documento para cargar al servidor
        if (isset($_FILES['soporte_referencia']) && !empty($_FILES['soporte_referencia'])) {

            //$response = json_encode($_FILES['soporte_referencia']);
            $year = date('Y');
            $directorio = "../web/convocatoria_".$year."/bancos/directores/".$consulta['num_doc']."/";

            $mimeType       = $_FILES['soporte_referencia']['type'];

            if ($mimeType == "application/pdf") {

                // validamos si existe el directorio para crearlo o no
                if (!is_dir($directorio)) {
                    mkdir($directorio);
                }

                $id_director_referencia = Generalidades::getConsecutivo("directores_referencias","id_director_referencia");

                $nombre_doc = "Referencia_id_".$id_director_referencia.".pdf";

                $target_file    = $directorio."/".$nombre_doc;

                // validamos si el archivo ya existe
                if (!file_exists($target_file)) {

                    if (move_uploaded_file($_FILES['soporte_referencia']['tmp_name'], $target_file)) {

                        // Registramos la experiencia del director despues de cargar el archivo

                        $sql2 = "INSERT INTO directores_referencias(id_director_referencia, id_director, id_usuario_sena, nombre, empresa, cargo, telefono, url, estado_doc, fecha_registro, estado_registro) VALUES (".$id_director_referencia.",".$POST['id_director'].",0,'".strtoupper($POST['nombre'])."','".strtoupper($POST['empresa'])."','".strtoupper($POST['cargo'])."','".$POST['telefono']."','".$nombre_doc."','PENDIENTE',NOW(),'ACTIVO')";

                        $insert = $db->createCommand($sql2)->execute();

                        if ($insert) {

                            $response = json_encode([
                                'status' => 'success',
                                'alert' => 'alert-warning',
                                'message'=> 'La referencia ha sido registrada correctamente.'
                            ]);

                        }else{
                            
                            $response = json_encode([
                                'status' => 'error',
                                'alert' => 'alert-warning',
                                'message'=> 'Ha ocurrido un error al registrar la referencia.'
                            ]);

                        }
                    }else{
                        $response = json_encode([
                            'status' => 'error',
                            'alert' => 'alert-danger',
                            'message'=> 'No se ha podido subir el documento al servidor.'
                        ]);
                    }

                }else{
                    // El archivo ya se encuentra en el servidor
                    $response = json_encode([
                        'status' => 'error',
                        'alert' => 'alert-warning',
                        'message'=> 'El archivo ya se encuentra cargado al servidor.'
                    ]);    
                }
            }else{
                // Formato invalido
                $response = json_encode([
                    'status' => 'error',
                    'alert' => 'alert-warning',
                    'message'=> 'Formato de documento inválido.'
                ]);
            }

        }else{
            // Documento vacio
            $response = json_encode([
                'status' => 'vacio',
                'alert' => 'alert-warning',
                'message' => 'Ingrese el soporte de la referencia.'
            ]);
        }

        return $response;

    }

    // Validar si el director pertenece al proyecto en sesion
    public function validarDirector($id_proyecto,$id_director)
    {
        $db = Mysql::connection();

        $sql = "SELECT COUNT(*) cantidad FROM directores_proyectos WHERE id_director = '".$id_director."' AND id_proyecto = '".$id_proyecto."'";

        $consulta = $db->createCommand($sql)->queryOne();

        if ($consulta['cantidad'] == 0) {
            // El director no pertenece al proyecto
            $response = false;
        }else{
            // Se encontro coincidencia
            $response = true;
        }

        return $response;
    }

    // Consultar la informacion registrada del director del proyecto
    public function informacionDirector($id_director)
    {
        $db = Mysql::connection();

        $sql = "SELECT * FROM directores WHERE id_director = '".$id_director."'";
        $consulta = $db->createCommand($sql)->queryOne();

        return $consulta;
    }

    // Consultar los documentos cargados del director
    public function documentosDirector($id_director,$tipo)
    {
        $db = Mysql::connection();

        switch ($tipo) {
            case 'experiencias':
                $sql = "SELECT  directores_experiencias.id_director_experiencia, 
                                directores_experiencias.id_director,
                                directores_experiencias.tipo,
                                directores_experiencias.descripcion,
                                directores_experiencias.url,
                                directores_experiencias.estado_doc,
                                directores_experiencias.fecha_registro,
                                directores_experiencias.estado_registro,
                                DATE_FORMAT(directores_experiencias.fecha_registro,'%Y') AS year,
                                directores.num_doc
                                FROM directores_experiencias 
                                INNER JOIN directores ON directores.id_director = directores_experiencias.id_director
                                WHERE directores_experiencias.id_director = '".$id_director."'";

                $experiencias = $db->createCommand($sql)->queryAll();

                if (!empty($experiencias)) {
                    $response = json_encode([
                        'status' => 'success',
                        'experiencias' => $experiencias
                    ]);
                }else{
                    $response = json_encode([
                        'status' => 'vacio'
                    ]);
                }

                break;
            
            case 'titulos':
                $sql = "SELECT  directores_titulos.id_director_titulo,
                                directores_titulos.id_director,
                                directores_titulos.tipo_titulo,
                                directores_titulos.descripcion,
                                directores_titulos.url,
                                directores_titulos.estado_doc,
                                directores_titulos.fecha_registro,
                                directores_titulos.estado_registro,
                                DATE_FORMAT(directores_titulos.fecha_registro,'%Y') AS year,
                                directores.num_doc
                                FROM directores_titulos 
                                INNER JOIN directores ON directores.id_director = directores_titulos.id_director
                                WHERE directores_titulos.id_director = '".$id_director."'";

                $titulos = $db->createCommand($sql)->queryAll();

                if (!empty($titulos)) {
                    $response = json_encode([
                        'status' => 'success',
                        'titulos' => $titulos
                    ]);
                }else{
                    $response = json_encode([
                        'status' => 'vacio'
                    ]);
                }

                break;

            case 'referencias':
                $sql = "SELECT  directores_referencias.id_director_referencia,
                                directores_referencias.id_director,
                                directores_referencias.nombre,
                                directores_referencias.empresa,
                                directores_referencias.cargo,
                                directores_referencias.telefono,
                                directores_referencias.url,
                                directores_referencias.estado_doc,
                                directores_referencias.fecha_registro,
                                directores_referencias.estado_registro,
                                DATE_FORMAT(directores_referencias.fecha_registro,'%Y') AS year,
                                directores.num_doc
                                FROM directores_referencias 
                                INNER JOIN directores ON directores.id_director = directores_referencias.id_director
                                WHERE directores_referencias.id_director = '".$id_director."'";

                $referencias = $db->createCommand($sql)->queryAll();

                if (!empty($referencias)) {
                    $response = json_encode([
                        'status' => 'success',
                        'referencias' => $referencias
                    ]);
                }else{
                    $response = json_encode([
                        'status' => 'vacio'
                    ]);
                }

                break;

            case 'documentos':
                $sql = "SELECT  directores_documentos.id_director_documento,
                                directores_documentos.id_director,
                                directores_documentos.id_documento_banco,
                                directores_documentos.url,
                                directores_documentos.estado_doc,
                                directores_documentos.fecha_registro,
                                directores_documentos.estado_registro,
                                DATE_FORMAT(directores_documentos.fecha_registro,'%Y') AS year,
                                documentos.nombre AS documento,
                                directores.num_doc
                                FROM directores_documentos 
                                INNER JOIN directores ON directores.id_director = directores_documentos.id_director
                                INNER JOIN documentos_bancos ON documentos_bancos.id_documento_banco = directores_documentos.id_documento_banco
                                INNER JOIN documentos ON documentos.id_documento = documentos_bancos.id_documento
                                WHERE directores_documentos.id_director = '".$id_director."'";

                $documentos = $db->createCommand($sql)->queryAll();

                if (!empty($documentos)) {
                    $response = json_encode([
                        'status' => 'success',
                        'documentos' => $documentos
                    ]);
                }else{
                    $response = json_encode([
                        'status' => 'vacio'
                    ]);
                }

                break;
        }

        return $response;

    }

    // Editar documento
    public function editarDocumento($id_director_documento,$ruta)
    {
        $db = Mysql::connection();

        if ($_FILES['document']['name'] != "") {

            $mimeType = $_FILES['document']['type'];

            if ($mimeType == "application/pdf") {

                // Evaluamos si el documento existe
                $filename = '../web/'.$ruta;
                if (file_exists($filename)) {
                    unlink($filename);
                }

                // Cargamos el documento
                if (move_uploaded_file($_FILES['document']['tmp_name'], $filename)) {
                    $response = json_encode([
                        'status' => 'success',
                        'message' => 'Se ha actualizado el documento correctamente.',
                        'alert' => 'alert-success'
                    ]);
                }else{
                    $response = json_encode([
                        'status' => 'error',
                        'message' => 'No se pudo cargar el archivo al sistema.',
                        'alert' => 'alert-warning'
                    ]);
                }

            }else{
                $response = json_encode([
                    'status' => 'error',
                    'message' => 'El formato del archivo es incorrecto, recuerde que el formato permitido es (.pdf)',
                    'alert' => 'alert-warning'
                ]);
            }
        }else{
            $response = json_encode([
                'status' => 'error',
                'message' => 'No ha seleccinado ningún documento.',
                'alert' => 'alert-warning'
            ]);
        }

        return $response;

    }

    // Actualizar informacion del director de proyecto
    public function actualizarInfo($POST)
    {
        $db = Mysql::connection();

        $tipo_doc           = $POST['tipo_doc'];
        $num_doc            = $POST['num_doc'];
        $nombres            = $POST['nombres'];
        $primer_apellido    = $POST['primer_apellido'];
        $segundo_apellido   = $POST['segundo_apellido'];
        $correo             = $POST['correo'];

        $sql = "UPDATE directores SET nombres = '".strtoupper($nombres)."',primer_apellido = '".strtoupper($primer_apellido)."',segundo_apellido = '".strtoupper($segundo_apellido)."',correo = '".strtoupper($correo)."' WHERE   tipo_doc         = '".$tipo_doc."' AND     num_doc          = '".$num_doc."'";

        $update = $db->createCommand($sql)->execute();

        if ($update) {
            $response = json_encode([
                'status'    => 'success',
                'message'   => 'Se ha actualizado la información del director de proyecto correctamente.',
                'alert'     => 'alert-success'
            ]);
        }else{
            $response = json_encode([
                'status'    => 'error',
                'message'   => 'No ha sido posible actualizar la información del director de proyecto.',
                'alert'     => 'alert-warning'
            ]);
        }

        return $response;
    }

}
