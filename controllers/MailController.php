<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class MailController extends Controller
{
    public function actionIndex()
    {
      $data = [
        'name' => 'Felipe',
        'email' => 'felipegarxon@hotmail.com',
        'phone' => '3144352585',
        'subject' => 'Test',
        'message' => 'Prueba de mensaje'
      ];

      $message = Yii::$app->mailer->compose('Template_Test',[
        'data' => $data
      ])
      ->setFrom('felipe.sena.sigp@gmail.com')
      ->setTo('felipegarxon@hotmail.com')
      ->setSubject('SIGP - Registro exitoso.')
      ->send();

      echo $message;
    }
}
