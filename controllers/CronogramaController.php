<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\Session;

use app\models\Generalidades;
use app\models\Cronograma;
use app\models\RadicacionC;
use app\models\CronogramaModificacion;

class CronogramaController extends Controller
{
	// Seccion de registro del cronograma
	public function actionRegister()
	{
		$model = new Cronograma();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

        	$accionesFormacion = Generalidades::getFormationActions($session['data']['id_proyecto']); 
        	$departamentos = Generalidades::getLocations('Departamentos',170);
            $ciudades = Generalidades::getLocations('All',0);

        	return $this->render('register',[
        		'model'	=>	$model,
        		'accionesFormacion'    => $accionesFormacion,
        		'listaDptos' 	       => $departamentos,
                'listaCiudades'        => $ciudades
        	]);

        }else {
        	return $this->redirect(['site/login']);
        }
	}

	// Consultar grupos de la accion de formacion seleccionada
    public function actionBuscarGruposAccionFormacion()
    {
        if (isset($_POST['id_accion_formacion'])) {
            $id_accion_formacion = $_POST['id_accion_formacion'];

            $grupos = Generalidades::getGroupsAF($id_accion_formacion);

            return $grupos;
        }
    }

    // Registrar cronograma presencial
    public function actionRegistrarCronogramaPresencial()
    {
    	$model = new Cronograma();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

        	$response = $model->registrarCronogramaPresencial($_POST); 
        	return $response;
        	//print_r($_POST);
        }
    }

    // Registrar cronograma virtual
    public function actionRegistrarCronogramaVirtual()
    {
    	$model = new Cronograma();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

        	$response = $model->registrarCronogramaVirtual($_POST); 
        	return $response;
        	//print_r($_POST);
        }
    }

    // Registrar cronograma combinado
    public function actionRegistrarCronogramaCombinado()
    {
    	$model = new Cronograma();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

        	$response = $model->registrarCronogramaCombinado($_POST); 
        	return $response;
        	//return json_encode($_POST);
        }
    }

    // Panel de busqueda de cronogramas
    public function actionBuscar()
    {
    	$model = new Cronograma();

    	$session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

        	$accionesFormacion = Generalidades::getFormationActions($session['data']['id_proyecto']); 
        	$departamentos = Generalidades::getLocations('Departamentos',170);
            $ciudades = Generalidades::getLocations('All',0);

        	return $this->render('buscar',[
        		'model'	=>	$model,
        		'accionesFormacion'    => $accionesFormacion,
        		'listaDptos' 	       => $departamentos,
                'listaCiudades'        => $ciudades
        	]);

        }else {
        	return $this->redirect(['site/login']);
        }
    }

    // Buscar unidades tematicas del cronograma de la accion y grupo seleccionado
    public function actionBuscarUnidadesTematicas()
    {
    	$model = new Cronograma();

    	if (isset($_POST['id_accion_formacion']) &&
			isset($_POST['id_grupo'])) {
    		
    		$id_accion_formacion 	= $_POST['id_accion_formacion'];
    		$id_grupo 				= $_POST['id_grupo'];

    		$response = $model->buscarUnidadesTematicas($id_accion_formacion,$id_grupo);

    		return $response;
    	}

    }

    // Buscar cronograma registrado por accion de formacion, grupo y numero de unidad tematica
    public function actionBuscarCronograma()
    {
        $model = new Cronograma();

        if (isset($_POST)) {
            
            $response = $model->buscarCronograma($_POST);
            return $response;

        }

    }

    // Buscar informacion de sesion presencial para editar
    public function actionBuscarSesionPresencial()
    {
        $model = new Cronograma();

        if (isset($_POST['id_cronograma_presencial'])) {
            $response = $model->buscarSesionPresencial($_POST['id_cronograma_presencial']);
            return $response;
            //print_r($_POST);
        }
    }

    // Actualizar informacion de sesion presencial
    public function actionActualizarSesion()
    {
        $model = new Cronograma();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_POST)) {
                $response = $model->actualizarSesion($_POST,$session['data']['id_proyecto']);
                return $response;
            }

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Buscar informacion de actividad virtual para editar
    public function actionBuscarActividadVirtual()
    {
        $model = new Cronograma();

        if (isset($_POST['id_cronograma_virtual'])) {
            $response = $model->buscarActividadVirtual($_POST['id_cronograma_virtual']);
            return $response;
            //print_r($_POST);
        }
    }    

    // Actualizar informacion de actividad virtual
    public function actionActualizarActividad()
    {
        $model = new Cronograma();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_POST)) {
                $response = $model->actualizarActividad($_POST,$session['data']['id_proyecto']);
                return $response;
            }

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Vista de radicación del cronograma
    public function actionRadicacion()
    {
        $model = new RadicacionC();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {
            
            $id_proyecto = $session['data']['id_proyecto'];

            return $this->render('radicar',[
                'model'         => $model,
                'id_proyecto'   => $id_proyecto
            ]);

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Proceso de validacion del cronograma
    public function actionValidarRadicacion()
    {
        $model = new RadicacionC();

        if (isset($_POST['id_proyecto'])) {
            $response = $model->validarRadicacion($_POST['id_proyecto']);
            return $response;
        }
    }

    // Proceso de radicacion del cronograma
    public function actionRadicarCronograma()
    {
        $model = new RadicacionC();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {
            
            $response = $model->radicarCronograma($session['data']['id_proyecto']);

            return $response;

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Actualizar cronograma desde la seccion de busqueda
    public function actionActualizarCronograma()
    {
        /*$model = new Cronograma();

        if (isset($_POST)) {
            $response = $model->actualizarCronograma($_POST);
            return $response;
        }*/
        $model = new Cronograma();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_POST)) {
                //$response = $model->actualizarActividad($_POST,$session['data']['id_proyecto']);
                $response = $model->actualizarCronograma($_POST,$session['data']['id_proyecto']); 
                return $response;
            }

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Exportar cronograma registrado del proyecto
    public function actionExportarCronogramaGlobal()
    {
        $model = new Cronograma();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            $cronograma = $model->buscarListadoCronograma($session['data']['n_radicado']);

            return $this->render('exportar-cronograma-global',[
                'cronograma' => $cronograma,
                'proyecto' => $session['data']['titulo'],
            ]);

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Registrar sesion presencial al cronograma
    public function actionRegistrarSesionPresencial()
    {
        $model = new Cronograma();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_POST)) {
                //print_r($_POST);
                $response = $model->registrarSesionPresencial($_POST);
                return $response;
            }

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Registrar actividad virtual al cronograma
    public function actionRegistrarActividadVirtual()
    {
        $model = new Cronograma();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_POST)) {
                //print_r($_POST);
                $response = $model->registrarActividadVirtual($_POST);
                return $response;
            }

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Eliminar sesion presencial
    public function actionEliminarSesionPresencial()
    {
        $model = new Cronograma();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_POST['id_cronograma_presencial'])) {

                $id_cronograma_presencial = $_POST['id_cronograma_presencial'];
                $response = $model->eliminarSesionPresencial($id_cronograma_presencial);

                return $response;
            }

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Eliminar actividad virtual
    public function actionEliminarActividadVirtual()
    {
        $model = new Cronograma();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_POST['id_cronograma_virtual'])) {

                $id_cronograma_virtual = $_POST['id_cronograma_virtual'];
                $response = $model->eliminarActividadVirtual($id_cronograma_virtual);

                return $response;
            }

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Solicitud de modificacion del cronograma
    public function actionSolicitudModificacion()
    {
        $model = new CronogramaModificacion();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            return $this->render('solicitud-modificacion',[
                'model' =>  $model,
            ]);

        }else {
            return $this->redirect(['site/login']);
        }
    }
}