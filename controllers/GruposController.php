<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

use app\models\Generalidades;
use app\models\Grupos;
use app\models\FPDF;
use app\models\Certificacion;


class GruposController extends Controller
{
    public function actionIndex()
    {
        $grupos = new Grupos();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {


            $accionesFormacion = $grupos->obtenerAccionesFormacion($session['data']['id_proyecto']);

            return $this->render('index',[
            	'model'				=> $grupos,
                'data'              => $session['data'],
            	'accionesFormacion'	=> $accionesFormacion,
            ]);

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Evento para registrar los grupos de todas las acciones de formacion
    public function actionCrearGrupos()
    {
        $grupos = new Grupos();

        if (isset($_POST['id_proyecto'])) {
            
            $id_proyecto = $_POST['id_proyecto'];

            $response = $grupos->crearGruposAF($id_proyecto);

            echo $response;

        }else{
            // Se ha recibido un parametro incorrecto
            $response = json_encode(['status'    => 'error','message'   => 'El paramentro recibido es incorrecto.']);
            echo $response;
        }

    }

    // Registrar grupos de la acción de formación seleccionada
    public function actionCrearGruposAccionFormacion()
    {
        $grupos = new Grupos();

        if (isset($_POST['id_proyecto']) && 
            isset($_POST['id_accion_formacion']) &&
            isset($_POST['numero_grupos'])) {
            
            $id_proyecto            = $_POST['id_proyecto'];
            $id_accion_formacion    = $_POST['id_accion_formacion'];
            $numero_grupos          = $_POST['numero_grupos'];

            $response = $grupos->crearGruposAccionFormacion($id_proyecto,$id_accion_formacion,$numero_grupos);

            echo $response;

        }
    }

    // Consultar acciones de formacion de un proyecto seleccionado
    public function actionConsultarAccionesFormacion()
    {
        $grupos = new Grupos();

        if (isset($_POST['id_proyecto'])) {

            $id_proyecto = $_POST['id_proyecto'];

            $acciones = $grupos->consultarAccionesFormacion($id_proyecto);

            echo $acciones;
        }
    }

    // Configuracion de grupos de una accion de formacion agregar beneficiarios
    public function actionConfiguracion()
    {
        $grupos = new Grupos();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_GET['id_accion_formacion'])) {
                
                $info_accion    = $grupos->infoAccionFormacion($_GET['id_accion_formacion']);
                $grupos         = $grupos->gruposAccionFormacion($_GET['id_accion_formacion']);

                return $this->render('configuracion',[
                    'model'       => $grupos,
                    'info_accion' => $info_accion,
                    'grupos'      => $grupos  
                ]);   
            }

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Buscar beneficiarios de un grupo seleccionado
    public function actionBuscarBeneficiarios()
    {
        $grupos = new Grupos();

        if (isset($_POST['id_grupo'])) {
            $id_grupo = $_POST['id_grupo'];
            $beneficiarios = $grupos->buscarBeneficiarios($id_grupo);
            return $beneficiarios;
        }

    }

    // Buscar beneficiarios por tipo y numero de documentos
    public function actionBuscarBeneficiarioDoc()
    {
        $grupos = new Grupos();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_POST['tipo_documento']) && isset($_POST['num_documento'])) {
                
                $tipo_documento = $_POST['tipo_documento'];
                $num_documento  = $_POST['num_documento'];
                $id_proyecto    = $session['data']['id_proyecto'];

                $info = $grupos->buscarBeneficiarioDoc($tipo_documento,$num_documento,$id_proyecto);

                echo $info;
            }

        }else{
            return $this->redirect(['site/login']);
        }

    }

    // Agregar beneficiario a un grupo seleccionado
    public function actionAgregarBeneficiario()
    {
        $grupos = new Grupos();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_POST)) {
                $respuesta = $grupos->agregarBeneficiario($_POST,$session['data']['id_proyecto']);
                return $respuesta;
                //print_r($_POST);
            }

        }else{
            return $this->redirect(['site/login']);
        }
    }

    // Cambiar estado de beneficiario en un grupo seleccionado
    public function actionEstadoBeneficiarioGrupo()
    {
        $grupos = new Grupos();

        if (isset($_POST['id_proyecto_beneficiario']) &&
            isset($_POST['id_accion_formacion']) &&
            isset($_POST['id_grupo']) && 
            isset($_POST['estado'])) {

            $id_proyecto_beneficiario   = $_POST['id_proyecto_beneficiario'];
            $id_accion_formacion        = $_POST['id_accion_formacion'];
            $id_grupo                   = $_POST['id_grupo'];
            $estado                     = $_POST['estado'];
            
            $respuesta = $grupos->estadoBeneficiarioGrupo($id_proyecto_beneficiario,$id_accion_formacion,$id_grupo,$estado);

            echo $respuesta;
        }
    }

    // Consultar grupos para el cambio de grupo de un beneficiario
    public function actionConsultarCambioGrupo()
    {
        $grupos = new Grupos();

        if (isset($_POST['id_accion_formacion']) &&
            isset($_POST['id_grupo'])) {
            
            $id_accion_formacion    = $_POST['id_accion_formacion'];
            $id_grupo               = $_POST['id_grupo'];

            $listado = $grupos->buscarGruposCambio($id_accion_formacion,$id_grupo);

            echo $listado;
        }

    }

    // Actualizar grupo de un beneficiario seleccionado
    public function actionActualizarGrupoBeneficiario()
    {
        $grupos = new Grupos();

        if (isset($_POST['id_proyecto_beneficiario']) &&
            isset($_POST['id_accion_formacion']) &&
            isset($_POST['id_grupo'])) {

            $id_proyecto_beneficiario = $_POST['id_proyecto_beneficiario'];
            $id_accion_formacion = $_POST['id_accion_formacion'];
            $id_grupo = $_POST['id_grupo'];

            $respuesta = $grupos->actualizarGrupoBeneficiario($id_proyecto_beneficiario,$id_accion_formacion,$id_grupo);
            
            echo $respuesta;
        }
    }

    // Ver PDF de beneficiarios de un grupo seleccionado
    public function actionReporteBeneficiariosGrupo() {

        $grupos = new Grupos();

        $session = Yii::$app->session;
        $session->open();

        if (isset($_GET['id_grupo'])) {
            
            $convocatoria   = Generalidades::infoConvocatoria($session['data']['id_proyecto']);
            $proyecto       = Generalidades::infoProyecto($_GET['id_grupo']);
            $beneficiarios  = Generalidades::beneficiariosGrupo($_GET['id_grupo']);

            // Contenido del documento PDF
            $content = $this->renderPartial('_reporteBeneficiariosGrupo',[
                'model'         => $grupos,
                'convocatoria'  => $convocatoria,
                'proyecto'      => $proyecto,
                'beneficiarios' => $beneficiarios
            ]);
            
            // setup kartik\mpdf\Pdf component
            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE, 
                // A4 paper format
                'format' => Pdf::FORMAT_A4, 
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT, 
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER, 
                // your html content input
                'content' => $content,  
                'filename' => 'Reporte_benficiarios_'.$proyecto['grupo'].'.pdf',
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                //'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}', 
                 // set mPDF properties on the fly
                'options' => ['title' => 'reporte'],
                 // call mPDF methods on the fly
                'methods' => [
                    'SetHeader'=>[''], 
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            
            return $pdf->render(); 

        }
    }

    // Ver PDF de beneficiarios de una accion de formacion seleccionada
    public function actionReporteBeneficiariosAccionFormacion() {

        $grupos = new Grupos();

        $session = Yii::$app->session;
        $session->open();

        if (isset($_GET['id_accion_formacion'])) {
            
            $convocatoria   = Generalidades::infoConvocatoria($session['data']['id_proyecto']);
            $proyecto       = Generalidades::infoProyectoByAccion($_GET['id_accion_formacion']);
            $beneficiarios  = Generalidades::beneficiariosAccionFormacion($_GET['id_accion_formacion']);

            // Contenido del documento PDF
            $content = $this->renderPartial('_reporteBeneficiariosAccionFormacion',[
                'model'         => $grupos,
                'convocatoria'  => $convocatoria,
                'proyecto'      => $proyecto,
                'beneficiarios' => $beneficiarios
            ]);
            
            // setup kartik\mpdf\Pdf component
            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE, 
                // A4 paper format
                'format' => Pdf::FORMAT_A4, 
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT, 
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER, 
                // your html content input
                'content' => $content,  
                'filename' => 'Reporte_Benficiarios_Accion_Formacion.pdf',
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                //'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}', 
                 // set mPDF properties on the fly
                'options' => ['title' => 'reporte'],
                 // call mPDF methods on the fly
                'methods' => [
                    'SetHeader'=>[''], 
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            
            return $pdf->render(); 

        }
    }

    // Exporta excel de beneficiarios por grupo
    public function actionExportarBeneficiariosAccionFormacion()
    {
        $model = new Grupos();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_GET['id_accion_formacion'])) {
            
                $convocatoria   = Generalidades::infoConvocatoria($session['data']['id_proyecto']);
                $proyecto       = Generalidades::infoProyectoByAccion($_GET['id_accion_formacion']);
                $beneficiarios  = Generalidades::beneficiariosAccionFormacion($_GET['id_accion_formacion']);

                // Contenido del documento PDF
                $content = $this->render('exportar-beneficiarios-accion-formacion',[
                    'model'         => $model,
                    'convocatoria'  => $convocatoria,
                    'proyecto'      => $proyecto,
                    'beneficiarios' => $beneficiarios
                ]);

            }


        }else {
            return $this->redirect(['site/login']);
        }
    }
    
}
