<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\Session;

use app\models\Generalidades;
use app\models\Beneficiarios;


class BeneficiariosController extends Controller
{
    public function actionIndex()
    {
        $model = new Beneficiarios();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

        	$listadoBeneficiarios = $model->listaBeneficiarios($session['data']['id_proyecto']);

        	return $this->render('index',[
        		'model'	=>	$model,
        		'listadoBeneficiarios' => $listadoBeneficiarios 
        	]);

        }else {
        	return $this->redirect(['site/login']);
        }
    }

    // Ver formulario de registro del beneficiario
    public function actionRegister()
    {
        $model = new Beneficiarios();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

        	$departamentos = Generalidades::getLocations('Departamentos',170);
            $ciudades = Generalidades::getLocations('All',0);
            $accionesFormacion = Generalidades::getFormationActions($session['data']['id_proyecto']); 

    		return $this->render('register',[
        		'model'			       => $model,
        		'listaDptos' 	       => $departamentos,
                'listaCiudades'        => $ciudades,
                'accionesFormacion'    => $accionesFormacion
        	]);

        }else {
        	return $this->redirect(['site/login']);
        }
    }

    // Buscar ciudades por departamento seleccionado
    public function actionBuscarCiudades()
    {
    	if (isset($_POST['id_departamento'])) {
    		$id_departamento = $_POST['id_departamento'];
    		$ciudades = json_encode(Generalidades::getLocations('Ciudades',$id_departamento));
    		//echo $ciudades;
            return $ciudades;
    	}
    }

    // Consultar informacion de beneficiario por tipo y numero de documento
    public function actionValidarDatos()
    {
        if (isset($_POST['num_doc']) && 
            isset($_POST['tipo_doc'])) {

            $num_doc    = $_POST['num_doc'];
            $tipo_doc   = $_POST['tipo_doc'];

            $data = Generalidades::getInfoByDoc($tipo_doc,$num_doc);
            echo $data;

        }
    }

    // Consultar grupos de la accion de formacion seleccionada
    public function actionBuscarGruposAccionFormacion()
    {
        if (isset($_POST['id_accion_formacion'])) {
            $id_accion_formacion = $_POST['id_accion_formacion'];

            $grupos = Generalidades::getGroupsAF($id_accion_formacion);

            return $grupos;
        }
    }

    // Agregar beneficiario al sistema
    public function actionAgregarBeneficiario()
    {
        if (isset($_POST)) {

            $session = Yii::$app->session;
            $session->open();

            if (isset($session['isLogged']) && $session['isLogged'] == true) {

                $model = new Beneficiarios();

                $response = $model->registrarBeneficiario($_POST,$session['data']['id_proyecto']);
                //print_r($_POST);

            }else{

                $response = json_encode([
                    'status' => 'error',
                    'message' => 'La sesión ha caducado, por favor inicie sesión para continuar con este proceso.'
                ]);

            }
        }

        return $response;
    }

    // Vista de edición de beneficiarios
    public function actionEdit()
    {
        $model = new Beneficiarios();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            $departamentos = Generalidades::getLocations('Departamentos',170);
            $ciudades = Generalidades::getLocations('All',0);
            $accionesFormacion = Generalidades::getFormationActions($session['data']['id_proyecto']); 

            return $this->render('edit',[
                'model'                => $model,
                'listaDptos'           => $departamentos,
                'listaCiudades'        => $ciudades,
                'accionesFormacion'    => $accionesFormacion
            ]);

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Buscar informacion del beneficiario para editar
    public function actionBuscarInformacion()
    {
        $model = new Beneficiarios();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_POST['tipo_doc']) &&
                isset($_POST['num_doc'])) {

                $tipo_doc       = $_POST['tipo_doc'];
                $num_doc        = $_POST['num_doc'];
                $id_proyecto    = $session['data']['id_proyecto'];

                $response = $model->buscarInformacionBeneficiario($id_proyecto,$tipo_doc,$num_doc);
                return $response;
            }

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Actualizar informacion del beneficiario
    public function actionActualizarInformacion()
    {
        $model = new Beneficiarios();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            //print_r($_POST);
            $response = $model->actualizarInformacion($_POST);
            return $response;

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Exportar excel de beneficiarios registrados en el proyecto
    public function actionExportarBeneficiarios()
    {
        $model = new Beneficiarios();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            $convocatoria   = Generalidades::infoConvocatoria($session['data']['id_proyecto']);
            $proyecto       = $model->infoProyecto($session['data']['id_proyecto']);
            $beneficiarios  = $model->beneficiarios($session['data']['id_proyecto']);

            // Contenido del documento PDF
            $content = $this->render('exportar-beneficiarios',[
                'model'         => $model,
                'convocatoria'  => $convocatoria,
                'proyecto'      => $proyecto,
                'beneficiarios' => $beneficiarios
            ]);

        }else {
            return $this->redirect(['site/login']);
        }
    }
}
