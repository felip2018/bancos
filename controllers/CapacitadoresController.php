<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\Session;
use app\models\Generalidades;
use app\models\Capacitadores;
use app\models\CapacitadoresJ;


class CapacitadoresController extends Controller
{
    // Indice de capacitadores naturales
    public function actionIndex()
    {
        $model = new Capacitadores();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            $listadoCapacitadores = $model->listaCapacitadores($session['data']['id_proyecto']);

            return $this->render('index',[
                'model' =>  $model,
                'listadoCapacitadores' => $listadoCapacitadores
            ]);

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Formulario de registro de capacitadores naturales
    public function actionRegister()
    {
        $model = new Capacitadores();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            $listadoDocumentosRequeridos = Generalidades::documentosRegistro('BANCO DE CAPACITADORES');

            return $this->render('register',[
                'model'         =>  $model,
                'listadoDocumentosRequeridos' => $listadoDocumentosRequeridos
            ]);

        }else {
            return $this->redirect(['site/login']);
        }
    }

    // Formulario de registro de capacitadores juridicos
    public function actionRegisterJuridic()
    {
        $model = new CapacitadoresJ();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            $listadoDocumentosRequeridos = Generalidades::documentosRegistro('BANCO DE CAPACITADORES JURIDICOS');

            return $this->render('register-juridic',[
                'model'         =>  $model,
                'listadoDocumentosRequeridos' => $listadoDocumentosRequeridos
            ]);

        }else {
            return $this->redirect(['site/login']);
        }
    }

    public function actionAddNewCapacitador()
      {
        $model = new Capacitadores();

        $session = Yii::$app->session;
        $session->open();

            
        if (!empty($_POST)) {

            $response = $model->registrarCapacitador($_POST['Capacitadores'],$session['data']['id_proyecto']);
            echo $response;

        }

    }

    public function actionBuscarCapacitadorDoc()
    {
        $model = new Capacitadores();

        if (isset($_POST['tipo_doc']) &&
            isset($_POST['num_doc'])) {

            $tipo_doc   = $_POST['tipo_doc'];
            $num_doc    = $_POST['num_doc'];

            $response = $model->buscarCapacitador($tipo_doc,$num_doc);

            echo $response;
        }
    }

    public function actionSearchExperiences()
    {
        $model = new Capacitadores();

        if (isset($_POST['id_capacitador'])) {
            
            $id_capacitador = $_POST['id_capacitador'];

            $response = $model->buscarExperiencias($id_capacitador);

            echo $response;
        }
    }

    public function actionAddExperience()
    {
        $model = new Capacitadores();

        if (isset($_POST['id_capacitador']) &&
            isset($_POST['tipo']) &&
            isset($_POST['descripcion'])) {
            
            $response = $model->registrarExperiencia($_POST);

            echo $response;

        }
        //print_r($_POST);
    }

    public function actionSearchTitles()
    {
        $model = new Capacitadores();

        if (isset($_POST['id_capacitador'])) {
            
            $id_capacitador = $_POST['id_capacitador'];

            $response = $model->buscarTitulos($id_capacitador);

            echo $response;
        }
    }

    public function actionAddTitle()
    {
        $model = new Capacitadores();

        if (isset($_POST['id_capacitador']) &&
            isset($_POST['tipo_titulo']) &&
            isset($_POST['descripcion'])) {
            
            $response = $model->registrarTitulo($_POST);

            echo $response;

        }
    }

    public function actionSearchReferences()
    {
        $model = new Capacitadores();

        if (isset($_POST['id_capacitador'])) {
            
            $id_capacitador = $_POST['id_capacitador'];

            $response = $model->buscarReferencias($id_capacitador);

            echo $response;
        }
    }

    public function actionAddReference()
    {
        $model = new Capacitadores();

        if (isset($_POST['id_capacitador']) &&
            isset($_POST['nombre']) &&
            isset($_POST['empresa']) && 
            isset($_POST['cargo']) && 
            isset($_POST['telefono'])) {
            
            $response = $model->registrarReferencia($_POST);

            echo $response;

        }
    }

    public function actionValidarDatos()
    {
        if (isset($_POST['num_doc'])) {
            $num_doc = $_POST['num_doc'];
            $data = Capacitadores::getInfoByDoc($num_doc);
            echo $data;
        }
        //print_r($_POST);
    }

    // Consulta de palabras clave
    public function actionSearchWords($value='')
    {
        if (isset($_POST['id_capacitador'])) {
            $id_capacitador = $_POST['id_capacitador'];
            $palabras = Capacitadores::buscarPalabrasClave($id_capacitador);
            return $palabras;
        }
    }

    // Agregar palabra clave a un capacitador
    public function actionAddWord()
    {
        $model = new Capacitadores();

        if (isset($_POST['id_palabra_clave']) && 
            isset($_POST['id_capacitador'])) {

            $id_palabra_clave   = $_POST['id_palabra_clave'];
            $id_capacitador     = $_POST['id_capacitador'];

            $resultado = $model->agregarPalabraClave($id_palabra_clave,$id_capacitador);
            return $resultado;

        }
    }

    // Remover palabra clave a un capacitador
    public function actionRemoveWord()
    {
        $model = new Capacitadores();

        if (isset($_POST['id_capacitador_palabra']) && 
            isset($_POST['id_capacitador'])) {

            $id_capacitador_palabra = $_POST['id_capacitador_palabra'];
            $id_capacitador         = $_POST['id_capacitador'];

            $resultado = $model->removerPalabraClave($id_capacitador_palabra,$id_capacitador);
            return $resultado;
            
        }
    }
}
