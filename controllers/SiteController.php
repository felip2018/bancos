<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\Session;
use yii\filters\VerbFilter;

use app\models\LoginForm;
use app\models\Entidades;
use app\models\Generalidades;
use app\models\Recovery;
use app\models\Consultas;

class SiteController extends Controller
{

    public function actionIndex()
    {
        $consultas = new Consultas();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            $panelInicio    = $consultas->informacionPanelInicio($session['data']['id_proyecto']);
            $datos_grafica  = $consultas->beneficiariosAccionesFormacion($session['data']['id_proyecto']);
            $infoDirector   = $consultas->informacionDirectorProyecto($session['data']['id_proyecto']);
            $docsDirector   = $consultas->documentosDirectorProyecto($session['data']['id_proyecto']);
            $estimados      = $consultas->beneficiariosEstimadosAccionFormacion($session['data']['id_proyecto']);
            $registrados    = $consultas->beneficiariosRegistradosAccionFormacion($session['data']['id_proyecto']);

            $this->layout = 'main';
            return $this->render('index',[
                'panelInicio'   => $panelInicio,
                'datos_grafica' => $datos_grafica,
                'infoDirector'  => $infoDirector,
                'docsDirector'  => $docsDirector,
                'estimados'     => $estimados,
                'registrados'   => $registrados
            ]);

        }else{

            return $this->redirect(['site/login']);

        }

    }

    /*Accion de login*/
    public function actionLogin()
    {
        $model = new LoginForm();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            return $this->redirect(['site/index']);

        } else{

            if ($model->load(Yii::$app->request->post())) {

                $responseLogin = $model->validateLogin();

                $response = json_decode($responseLogin);

                if ($response->status == 'success') {

                    return $this->redirect(['site/index']);

                }else{
                    
                    $this->layout = 'main-login';

                    return $this->render('login', [
                        'model' => $model,
                        'response' => $response,
                    ]);

                }

            }else{

                $model->password = '';
                $this->layout = 'main-login';

                return $this->render('login', ['model' => $model,]);
            }
        }
    }

    /*Accion de registro*/
    public function actionRegister()
    {
        $model = new Entidades();

        $countriesList      = Generalidades::getCountries();
        $CIIUList           = Generalidades::getCIIU();
        $sectoresList       = Generalidades::getSubTipos(1);
        $tamanosEntidadList = Generalidades::getSubTipos(2);
        $tiposEmpresaList   = Generalidades::getSubTipos(3);
        $tiposEntidadList   = Generalidades::getSubTipos(4);

        if ($model->load(Yii::$app->request->post())) {

           $response = $model->register();
           $this->layout = 'main-login';
           return $this->render(
             'register',
             [
               'model'               => $model,
               'response'            => $response,
               'countriesList'       => $countriesList,
               'CIIUList'            => $CIIUList,
               'sectoresList'        => $sectoresList,
               'tamanosEntidadList'  => $tamanosEntidadList,
               'tiposEmpresaList'    => $tiposEmpresaList,
               'tiposEntidadList'    =>  $tiposEntidadList,
             ]
           );

        }else{

            $this->layout = 'main-login';
            return $this->render(
              'register',
              [
                'model'               => $model,
                'countriesList'       => $countriesList,
                'CIIUList'            => $CIIUList,
                'sectoresList'        => $sectoresList,
                'tamanosEntidadList'  => $tamanosEntidadList,
                'tiposEmpresaList'    => $tiposEmpresaList,
                'tiposEntidadList'    => $tiposEntidadList,
              ]
            );
        }
    }

    /*Accion de activacion de cuenta*/
    public function actionActivation()
    {
        if (isset($_GET['email']) && isset($_GET['token'])) {
            //  Activar cuenta
            $response = Generalidades::activationAccount($_GET['email'],$_GET['token']);
            $this->layout = 'main-login';

            return $this->render(
                'response',
                [
                    'response'  => $response
                ]
            );

        }else{
            //  Error de datos
            echo 'Error de variables';
        }
    }

    /*Accion de cierre de sesion*/
    public function actionClose()
    {
        $model = new LoginForm();

        $session = Yii::$app->session;
        $session->open();

        // Cerrar sesion en base de datos
        $close_session = $model->logout($session['data']['n_radicado']);

        unset($session['isLogged']);
        $session->close();
        $session->destroy();

        return $this->redirect(['site/index']);
    }

    public function actionClave()
    {
        $clave = '12';
        $hash = Yii::$app->getSecurity()->generatePasswordHash($clave);
        echo 'Clave => '.$clave.'<br>';
        echo 'Nuevo Hash => '.$hash.'<br>';
        echo 'Clave Hash => $2y$13$C0eK6vJq0vnsw3VXtU8w6.K3ikJtjJHvCDAkvxmWQeyOfDanrazUa.<br>';

        if (Yii::$app->getSecurity()->validatePassword($clave,'$2y$13$C0eK6vJq0vnsw3VXtU8w6.K3ikJtjJHvCDAkvxmWQeyOfDanrazUa')) {
            echo 'Ok';
        }else{
            echo 'Error';
        }

    }

    public function actionRecovery()
    {
        $model = new Recovery();

        $this->layout = 'main-login';

        if ($model->load(Yii::$app->request->post())) {

            $response = $model->accountRecovery();

            return $this->render('recovery',[
                'model'     => $model,
                'response'  => $response,
            ]);

        }else {

            return $this->render('recovery',[
                'model' => $model
            ]);
        }
    }

    public function actionDate()
    {
        $fecha_actual = date("d-m-Y H:i:s");
        echo "Fecha Actual -> " . $fecha_actual. "<br>";
        //sumo 1 día
        echo date("d-m-Y H:i:s",strtotime($fecha_actual."+ 1 days"))."<br>"; 
        //resto 1 día
        echo date("d-m-Y H:i:s",strtotime($fecha_actual."- 1 days")); 
    }

}
