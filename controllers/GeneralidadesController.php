<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

use app\models\Generalidades;

class GeneralidadesController extends Controller
{
    // Action para consultar Departamentos y Ciudades
    public function actionBuscar()
    {
        $Generalidades = new Generalidades();

        $POST = Yii::$app->request->post();

        $response = $Generalidades->getLocations($POST['tipo_busqueda'], $POST['identificador']);

        return $response;
    }

    // Action para generar caso de cambio de contraseña
	public function actionChangepassword()
    {
        $Generalidades = new Generalidades();

        if (Yii::$app->request->post()) {
                
            $post = Yii::$app->request->post();

            $response = $Generalidades->generateChangePassword($post['usuario']);
            
            return $response;
        }
    }

    // Action para consultar todos los Departamentos y Ciudades formato JSON
    public function actionBuscarAll()
    {
        $Generalidades = new Generalidades();

        if (isset($_POST['tipo_busqueda']) && isset($_POST['identificador'])) {
            
            $response = $Generalidades->getAllLocations($_POST['tipo_busqueda'], $_POST['identificador']);

            return $response;
        }

    }
}
