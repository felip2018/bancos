<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\Session;
use yii\filters\VerbFilter;

use app\models\Tematicas;
use kartik\mpdf\Pdf;

class TematicasController extends Controller
{
    
    public function actionEconomiaNaranja()
    {
        $model = new Tematicas();

        $this->layout = 'main';

        $response = $model->verificarEconomiaNaranja();
        
        return $this->render('economia-naranaja',['response' => $response]);        

    }

    public function actionRevolucionIndustrial()
    {
        $model = new Tematicas();

        $this->layout = 'main';

        $response = $model->verificarRevolucionIndustrial();
        
        return $this->render('revolucion-industrial',['response' => $response]);        

    }

    public function actionFocosTematicos()
    {
        $model = new Tematicas();

        $this->layout = 'main';

        $response = $model->verificarFocosTematicos();
        
        return $this->render('focos-tematicos',['response' => $response]);        

    }

    public function actionCompetenciasBlandas()
    {
        $model = new Tematicas();

        $this->layout = 'main';

        $response = $model->verificarCompetenciasBlandas();
        
        return $this->render('competencias-blandas',['response' => $response]);        

    }

    // Actualizar dato de acciones de formacion por proyecto
    public function actionUpdateActions()
    {
        $model = new Tematicas();

        $response = $model->updateActions();

        return $this->render('update-actions',['response'=>$response]);
    }

    // Actualizar dato de beneficiarios por proyecto
    public function actionUpdateBeneficiarios()
    {
        $model = new Tematicas();

        $response = $model->updateBeneficiarios();

        return $this->render('update-beneficiarios',['response'=>$response]);
    }

    // Ver PDF de acciones de formacion
    public function actionReporteAccionesFormacion() {

        $model = new Tematicas();

        $session = Yii::$app->session;
        $session->open();

        return $this->render('_reporteAccionesFormacion',[
            'model'         => $model,
        ]);
        
    }

}
