<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

use app\models\Generalidades;
use app\models\Certificacion;


class CertificacionController extends Controller
{

    // Certificacion de beneficiarios por asistencia y/o participacion
    public function actionCertificacion()
    {
        $model = new Certificacion();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            if (isset($_GET['id_accion_formacion'])) {
                
                $beneficiarios   = $model->beneficiariosAccionFormacion($_GET['id_accion_formacion']);
                $accionFormacion = $model->infoAccionFormacion($_GET['id_accion_formacion']);

                $filtro = json_decode($accionFormacion);

                switch ($filtro->accion->modalidad) {
                    case 'PRESENCIAL':

                        return $this->render('certificacion-presencial',[
                            'model'             => $model,
                            'beneficiarios'     => $beneficiarios,
                            'accionFormacion'   => $accionFormacion
                        ]);

                        break;
                    
                    case 'VIRTUAL':
                        
                        return $this->render('certificacion-virtual',[
                            'model'             => $model,
                            'beneficiarios'     => $beneficiarios,
                            'accionFormacion'   => $accionFormacion
                        ]);

                        break;

                    case 'COMBINADA':
                        
                        return $this->render('certificacion-combinada',[
                            'model'             => $model,
                            'beneficiarios'     => $beneficiarios,
                            'accionFormacion'   => $accionFormacion
                        ]);

                        break;
                }  
            }

        }else {
            return $this->redirect(['site/login']);
        }
    }
    
    // Registrar certificacion de beneficiarios en modalidad presencial
    public function actionRegistrarCertificacionPresencial()
    {
        $model = new Certificacion();
        if (isset($_POST)) {
            $respuesta = $model->registrarCertificacionPresencial($_POST);
            return $respuesta;
        }
    }

    // Registrar certificacion de beneficiarios en modalidad presencial
    public function actionRegistrarCertificacionVirtual()
    {
        $model = new Certificacion();
        if (isset($_POST)) {
            $respuesta = $model->registrarCertificacionVirtual($_POST);
            return $respuesta;
        }
    }

    // Registrar certificacion de beneficiarios en modalidad combinada
    public function actionRegistrarCertificacionCombinada()
    {
        $model = new Certificacion();
        if (isset($_POST)) {
            $respuesta = $model->registrarCertificacionCombinada($_POST);
            return $respuesta;
        }
    }
}
