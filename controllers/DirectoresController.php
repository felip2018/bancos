<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\Session;

use app\models\Generalidades;
use app\models\Directores;


class DirectoresController extends Controller
{
    public function actionIndex()
    {
        $model = new Directores();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

        	$listadoDirectores = $model->listaDirectores($session['data']['id_proyecto']);
            

        	return $this->render('index',[
        		'model'	=>	$model,
        		'listadoDirectores' => $listadoDirectores
        	]);

        }else {
        	return $this->redirect(['site/login']);
        }
    }

    public function actionRegister()
    {
        $model = new Directores();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {

            $listadoDocumentosRequeridos = Generalidades::documentosRegistro('BANCO DE DIRECTORES');

        	return $this->render('register',[
        		'model'			=>	$model,
                'listadoDocumentosRequeridos' => $listadoDocumentosRequeridos
        	]);

        }else {
        	return $this->redirect(['site/login']);
        }
    }

    // Accion para recibir data del formulario de registro de directores
    public function actionAddNewDirector()
    {
        $model = new Directores();

        $session = Yii::$app->session;
        $session->open();

        if (isset($session['isLogged']) && $session['isLogged'] == true) {
            
            if (!empty($_POST)) {

                $response = $model->registrarDirector($_POST['Directores'],$session['data']['id_proyecto']);
                return $response;
            }

        }else{
            return $this->redirect(['site/login']);
        }
        //print_r($_POST['Directores']);
    }

    // Buscar informacion de director por numero de documento
    public function actionBuscarDirectorDoc()
    {
        $model = new Directores();

        if (isset($_POST['tipo_doc']) &&
            isset($_POST['num_doc'])) {

            $tipo_doc   = $_POST['tipo_doc'];
            $num_doc    = $_POST['num_doc'];

            $response = $model->buscarDirector($tipo_doc,$num_doc);

            echo $response;
        }
    }

    // Buscar experiencias de un director seleccionado
    public function actionSearchExperiences()
    {
        $model = new Directores();

        if (isset($_POST['id_director'])) {
            
            $id_director = $_POST['id_director'];

            $response = $model->buscarExperiencias($id_director);

            echo $response;
        }
    }

    // Agregar experiencia a un director seleccionado
    public function actionAddExperience()
    {
        $model = new Directores();

        if (isset($_POST['id_director']) &&
            isset($_POST['tipo']) &&
            isset($_POST['descripcion'])) {
            
            $response = $model->registrarExperiencia($_POST);

            echo $response;

        }
    }

    // Buscar titulos de un director seleccionado
    public function actionSearchTitles()
    {
        $model = new Directores();

        if (isset($_POST['id_director'])) {
            
            $id_director = $_POST['id_director'];

            $response = $model->buscarTitulos($id_director);

            echo $response;
        }
    }

    // Agregar titulo a un director seleccionado
    public function actionAddTitle()
    {
        $model = new Directores();

        if (isset($_POST['id_director']) &&
            isset($_POST['tipo_titulo']) &&
            isset($_POST['descripcion'])) {
            
            $response = $model->registrarTitulo($_POST);

            echo $response;

        }
    }

    // Buscar referencias de un director seleccionado
    public function actionSearchReferences()
    {
        $model = new Directores();

        if (isset($_POST['id_director'])) {
            
            $id_director = $_POST['id_director'];

            $response = $model->buscarReferencias($id_director);

            echo $response;
        }
    }

     // Agregar referencia de un director seleccionado
    public function actionAddReference()
    {
        $model = new Directores();

        if (isset($_POST['id_director']) &&
            isset($_POST['nombre']) &&
            isset($_POST['empresa']) && 
            isset($_POST['cargo']) && 
            isset($_POST['telefono'])) {
            
            $response = $model->registrarReferencia($_POST);

            echo $response;

        }
    }

    // Editar informacion del director de proyecto
    public function actionEdit()
    {
        $model = new Directores();

        if (isset($_GET['id_director'])) {

            $session = Yii::$app->session;
            $session->open();

            if (isset($session['isLogged']) && $session['isLogged'] == true) {

                $id_director = $_GET['id_director'];

                $validarDirector = $model->validarDirector($session['data']['id_proyecto'],$id_director);
                
                if ($validarDirector) {

                    $infoDirector                   = $model->informacionDirector($id_director);

                    //$listadoDocumentosRequeridos    = Generalidades::documentosRegistro('BANCO DE DIRECTORES');

                    $experiencias                   = $model->documentosDirector($id_director,'experiencias');
                    $titulos                        = $model->documentosDirector($id_director,'titulos');
                    $referencias                    = $model->documentosDirector($id_director,'referencias');
                    $documentos                     = $model->documentosDirector($id_director,'documentos');


                    return $this->render('edit',[
                        'model' =>  $model,
                        'infoDirector' => $infoDirector,
                        'experiencias' => $experiencias,
                        'titulos'      => $titulos,
                        'referencias'  => $referencias,
                        'documentos'   => $documentos  
                    ]);

                }else{
                    return $this->render('error-edit');
                }

            }else {
                return $this->redirect(['site/login']);
            }
        }

    }

    // Actualizar documento del director de proyecto
    public function actionEditarDocumento()
    {
        $model = new Directores();

        if (isset($_POST['id_director_documento']) &&
            isset($_POST['ruta'])) {
            
            $id_director_documento = $_POST['id_director_documento'];
            $ruta = $_POST['ruta'];

            $response = $model->editarDocumento($id_director_documento,$ruta);

            return $response;

        }
    }

    // Actualizar documento de experiencia del director de proyecto
    public function actionEditarExperiencia()
    {
        $model = new Directores();

        if (isset($_POST['id_director_experiencia']) &&
            isset($_POST['ruta'])) {
            
            $id_director_experiencia = $_POST['id_director_experiencia'];
            $ruta = $_POST['ruta'];

            $response = $model->editarDocumento($id_director_experiencia,$ruta);

            return $response;

        }

    }

    // Actualizar documento de titulo del director de proyecto
    public function actionEditarTitulo()
    {
        $model = new Directores();

        if (isset($_POST['id_director_titulo']) &&
            isset($_POST['ruta'])) {
            
            $id_director_titulo = $_POST['id_director_titulo'];
            $ruta = $_POST['ruta'];

            $response = $model->editarDocumento($id_director_titulo,$ruta);

            return $response;

        }

    }

    // Actualizar documento de referencia del director de proyecto
    public function actionEditarReferencia()
    {
        $model = new Directores();

        if (isset($_POST['id_director_referencia']) &&
            isset($_POST['ruta'])) {
            
            $id_director_referencia = $_POST['id_director_referencia'];
            $ruta = $_POST['ruta'];

            $response = $model->editarDocumento($id_director_referencia,$ruta);

            return $response;

        }

    }

    // Actualizar informacion del director de proyecto
    public function actionActualizarInfo()
    {
        $model = new Directores();

        if (isset($_POST)) {
            
            $response = $model->actualizarInfo($_POST);
            return $response;

        }
    }
}
