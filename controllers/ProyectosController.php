<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\Session;
use yii\filters\VerbFilter;

use app\models\UpdateProjects;

class ProyectosController extends Controller
{
    //Actualizar tabla "proyectos" con los datos de clave
    public function actionUpdatePasswords()
    {
        $model = new UpdateProjects();

        $response = $model->updatePasswordProjects();

        return $this->render('update',[
            'model'     => $model,
            'response'  => $response
        ]);
    }   

}
