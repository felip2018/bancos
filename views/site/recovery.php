<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'RECUPERAR CUENTA';

?>
<style media="screen">
.logotipoSena{
  width: 80px;
  height: 80px;
}
.register-logo{
  background: #0097A7;
  padding: 10px;
}
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #000;
}
</style>
<div class="container container-form">
  <a href="index.php?r=site%2Flogin" class="btn">
    <i class="fa fa-undo"></i> Volver
  </a>
  <div class="register-logo">
      <img class="logotipoSena" src="../web/img/logoSena.png" alt="Logotipo Sena">
  </div>
  <div class="row">
    <div class="col-xs-12">
      <h3>Recuperación de cuenta</h3>
      <p>Bienvenido a la sección de recuperación de cuenta, por favor diligencie el formulario presentado a continuación para realizar el restablecimiento de su cuenta.</p>
    </div>
  </div>

  <?php $form = ActiveForm::begin(['id' => 'register-form',]);?>
  <div class="row">
    <div class="col-xs-12">
      <?= $form
          ->field($model, 'email')
          ->label('<i class="fa fa-asterisk"></i> Usuario') ?>
    </div>
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 col-md-6">
          <?=
            $form->field($model, 'password')
            ->label('<i class="fa fa-asterisk"></i> Nueva Contraseña')
            ->passwordInput();
          ?>
        </div>
        <div class="col-xs-12 col-md-6">
          <?=
            $form->field($model, 'repeat_password')
            ->label('<i class="fa fa-asterisk"></i> Repetir Contraseña')
            ->passwordInput();
          ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <?= Html::submitButton(
            '<i class="fa fa-key"></i> Recuperar cuenta',
            [
              'class' => 'btn btn-primary pull-right',
              'name' => 'recovery-button'
            ])
          ?>
        </div>
      </div>
    </div>
  </div>
  <?php ActiveForm::end(); ?>
  <?php
    if (isset($response)) {
      $res = json_decode($response);
  ?>
      <div class="row">
        <div class="col-xs-12" style="padding-top: 10px;">
          <div class="alert <?php echo $res->alert;?>">
            <b><?php echo $res->status;?></b>
            <p><?php echo $res->message;?></p>
          </div>
        </div>
        <div class="col-xs-12">
          <a href="index.php?r=site%2Flogin" class="btn btn-primary">
            <i class="fa fa-sign-in"></i> Ir al inicio de sesión
          </a>
        </div>
      </div>
  <?php
    }
  ?>
</div>