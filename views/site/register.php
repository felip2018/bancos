<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

$this->title = 'SIGP';

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

$Tipos_Doc = [
    'NIT' => 'NIT (Nit de empresa)',
    'CC'  => 'CC (Cédula de ciudadanía)',
    'CEX' => 'CEX (Cédula de extranjería)',
    'PAS' => 'PAS (Pasaporte)',
];

$countries = [''=>'-Seleccione'];
if (isset($countriesList)) {
  foreach ($countriesList as $key => $value) {
     $countries[$value['ID_Pais']] = $value['Nombre'];
  }
}

$ciiu = [''=>'-Seleccione'];
if (isset($CIIUList)) {
  foreach ($CIIUList as $key => $value) {
     $ciiu[$value['Codigo']] = $value['Codigo'] . ' - ' . $value['Descripcion'];
  }
}

$sectores = [''=>'-Seleccione'];
if (isset($sectoresList)) {
  foreach ($sectoresList as $key => $value) {
     $sectores[$value['ID_Sub_Tipo_Definicion']] = $value['Descripcion'];
  }
}

$tipos_entidades = [''=>'-Seleccione'];
if (isset($tiposEntidadList)) {
  foreach ($tiposEntidadList as $key => $value) {
     $tipos_entidades[$value['ID_Sub_Tipo_Definicion']] = $value['Descripcion'];
  }
}

$tamanos_entidades = [''=>'-Seleccione'];
if (isset($tamanosEntidadList)) {
  foreach ($tamanosEntidadList as $key => $value) {
     $tamanos_entidades[$value['ID_Sub_Tipo_Definicion']] = $value['Descripcion'];
  }
}

$tipos_empresas = [''=>'-Seleccione'];
if (isset($tiposEmpresaList)) {
  foreach ($tiposEmpresaList as $key => $value) {
     $tipos_empresas[$value['ID_Sub_Tipo_Definicion']] = $value['Descripcion'];
  }
}


$this->registerJsFile(
  'http://localhost/sena_forms/web/js/register.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>
<style media="screen">
.logotipoSena{
  width: 80px;
  height: 80px;
}
.register-logo{
  background: #0097A7;
  padding: 10px;
}
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #000;
}
.fa-asterisk{
  color:red;
}
.alerta{
  width: 100%;
  height: 100%;
  position: fixed;
  z-index: 100;
  background: rgba(0,0,0,0.5);
}
.alerta-mensaje{
  margin: auto;
  width: 30%;
  margin-top: 200px;
  background: #FFFFFF;
  padding: 10px;
}
.alert{
  border-radius: 0px !important;
}
</style>
<?php
    if (isset($response)) {
      $data = json_decode($response);
?>
      <div class="alerta">
        <div class="alerta-mensaje">
            <div class="alert <?php echo $data->alert;?>">
              <b><?php echo $data->status;?></b>
              <p><?php echo $data->message;?></p>
            </div>
            <button type="button" name="button" class="btn btn-primary pull-right" onclick="closeAlert()">Aceptar</button>
        </div>
      </div>
<?php
    }
?>
<div class="container container-form">
  <a href="index.php?r=site%2Flogin" class="btn">
    <i class="fa fa-undo"></i> Volver
  </a>
  <div class="register-logo">
      <img class="logotipoSena" src="../web/img/logoSena.png" alt="Logotipo Sena">
  </div>
  <?php
      $form = ActiveForm::begin(['id' => 'register-form',]);
  ?>
  <div class="row">
    <div class="col-xs-12">
      <div class="alert alert-info">Generalidades de la entidad</div>
    </div>
    <div class="col-xs-12">
      <?= $form
          ->field($model, 'Entidad')
          ->label('<i class="fa fa-asterisk"></i> Razón social') ?>
    </div>
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 col-md-4">
          <?=
            $form->field($model, 'Tipo_Doc')
            ->dropdownList($Tipos_Doc,['prompt'=>'-Seleccione'])
            ->label('<i class="fa fa-asterisk"></i> Tipo de identificación');
          ?>
        </div>
        <div class="col-xs-12 col-md-4">
          <?= $form
              ->field($model, 'Doc')
              ->label('<i class="fa fa-asterisk"></i> Número de Identificación') ?>
        </div>
        <div class="col-xs-12 col-md-4">
          <?= $form
              ->field($model, 'Digito_Verificacion')
              ->label('<i class="fa fa-asterisk"></i> Dígito de verificación') ?>
        </div>
      </div>
    </div>
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 col-md-4">
          <?=
            $form->field($model, 'Pais')
            ->dropdownList($countries,
            [
              //'prompt'=>'-Seleccione',
              'onchange'=>'search("Departamentos",this.value)'
            ])
            ->label('<i class="fa fa-asterisk"></i> País');
          ?>
        </div>
        <div class="col-xs-12 col-md-4">
          <?=
            $form
            ->field($model, 'Departamento')
            ->dropdownList([],
            [
              'prompt'=>'-Seleccione',
              'disabled' => true,
              'onchange'=>'search("Ciudades",this.value)'
            ])
            ->label('<i class="fa fa-asterisk"></i> Departamento')
          ?>
        </div>
        <div class="col-xs-12 col-md-4">
          <?=
            $form
            ->field($model, 'ID_Ciudad')
            ->dropdownList($countries,
            [
              'prompt'=>'-Seleccione',
              'disabled' => true,
            ])
            ->label('<i class="fa fa-asterisk"></i> Ciudad')
          ?>
        </div>
      </div>
    </div>
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 col-md-6">
          <?= $form
              ->field($model, 'Direccion')
              ->label('<i class="fa fa-asterisk"></i> Dirección') ?>
        </div>
        <div class="col-xs-12 col-md-6">
          <?= $form
              ->field($model, 'Direccion_Electronica')
              ->label('<i class="fa fa-asterisk"></i> Correo electrónico') ?>
        </div>
      </div>
    </div>

    <div class="col-xs-12">
      <div class="alert alert-info">Representante legal</div>
    </div>

    <div class="col-xs-12">
      <?= $form
          ->field($model, 'RNombre')
          ->label('<i class="fa fa-asterisk"></i> Nombre') ?>
    </div>

    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 col-md-6">
          <?= $form
              ->field($model, 'RTipo_Doc')
              ->dropdownList($Tipos_Doc,['prompt'=>'-Seleccione'])
              ->label('<i class="fa fa-asterisk"></i> Tipo de identificación') ?>
        </div>

        <div class="col-xs-12 col-md-6">
          <?= $form
              ->field($model, 'RDoc')
              ->label('<i class="fa fa-asterisk"></i> Número de identificación') ?>
        </div>
      </div>
    </div>

    <div class="col-xs-12">
      <div class="alert alert-info">Clasificación</div>
    </div>

    <div class="col-xs-12">
      <?= $form
          ->field($model, 'ID_CIIU')
          ->dropdownList($ciiu)
          ->label('Código CIIU') ?>
    </div>

    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 col-md-6">
          <?= $form
              ->field($model, 'ID_Sector')
              ->dropdownList($sectores)
              ->label('<i class="fa fa-asterisk"></i> Sector') ?>
        </div>
        <div class="col-xs-12 col-md-6">
          <?= $form
              ->field($model, 'ID_Tipo_Entidad')
              ->dropdownList($tipos_entidades)
              ->label('<i class="fa fa-asterisk"></i> Tipo de entidad') ?>
        </div>
      </div>
    </div>

    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 col-md-6">
          <?= $form
              ->field($model, 'ID_Tamano_Entidad')
              ->dropdownList($tamanos_entidades)
              ->label('Tamaño') ?>
        </div>
        <div class="col-xs-12 col-md-6">
          <?= $form
              ->field($model, 'ID_Tipo_Empresa')
              ->dropdownList($tipos_empresas)
              ->label('Tipo de empresa') ?>
        </div>
      </div>
    </div>

  </div>

  <div class="row">
      <div class="col-xs-12">
          <?= Html::submitButton(
            'Registrarse',
            [
              'class' => 'btn btn-primary pull-right',
              'name' => 'login-button'
            ])
          ?>
      </div>
  </div>


  <?php ActiveForm::end(); ?>
</div>
