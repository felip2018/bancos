<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\Session;

$session = Yii::$app->session;
$session->open();

$this->title = 'SEP';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

$this->registerJsFile(
  '@web/js/login.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>
<div class="container-fluid" style="background: #FFFFFF;height: 100vh;">
    <div class="row">
        <div class="col-xs-12" style="background: #FA7223;color: #FFFFFF;padding: 10px;">
            <img src="img/logoSena.png" alt="logo" style="width: 80px;height: 80%;">
            <b style="margin-left: 30px;font-size: 18pt;">Programa de Formación Continua Especializada </b>
        </div>
        <div class="col-xs-12">
            <div class="login-box">
                <div style="padding: 10px;text-align: center;color:#FA7223;">
                    <h3><b>SEP</b></h3>
                    <h4><b>Sistema Empresarial de Proyectos</b></h4>
                    <h4><b>Bancos</b></h4>
                </div>
                <div class="login-box-body" style="border-radius: 5px !important;box-shadow: 0px 0px 5px #CCCCCC;">
                    
                    <?php
                        $form = ActiveForm::begin(['id' => 'login-form',]);
                    ?>

                    <?= $form
                        ->field($model, 'username', $fieldOptions1)
                        ->label('No. Radicado')
                        ->textInput(['placeholder' => 'No. Radicado']) ?>

                    <?= $form
                        ->field($model, 'password', $fieldOptions2)
                        ->label('Clave')
                        ->passwordInput(['placeholder' => 'Clave']) ?>

                    <div class="row">
                        <!--<div class="col-xs-6">
                          <a href="index.php?r=site%2Fregister" class="btn btn-success btn-block"><i class="fa fa-user-plus"></i> Registrarse</a>
                        </div>-->
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block" style="background: #FA7223;color: #FFFFFF;border:none;height: 50px;"><span class="fa fa-sign-in"></span> INGRESAR</button>
                        </div>
                        <!--<div class="col-xs-12" style="padding-top: 10px;">
                            <button type="button" class="btn btn-block" style="border: 1px solid #DDD;" onclick="changePassword()">
                                <i class="fa fa-key"></i> Olvidé mi contraseña
                            </button>
                        </div>-->
                    </div>

                    <?php ActiveForm::end(); ?>

                    <?php
                        if (isset($response)) {
                    ?>
                        <hr>
                            <div class="alert <?php echo $response->alert;?>">
                                <b><?php echo $response->status;?></b>
                                <p><?php echo $response->message;?></p>
                            </div>
                    <?php
                        }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>

