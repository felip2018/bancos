<?php

use yii\helpers\Html;
$this->title = 'RESPUESTA';

?>
<style media="screen">
.logotipoSena{
  width: 80px;
  height: 80px;
}
.register-logo{
  background: #0097A7;
  padding: 10px;
}
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #000;
}
</style>
<div class="container container-form">
  <a href="index.php?r=site%2Flogin" class="btn">
    <i class="fa fa-undo"></i> Volver
  </a>
  <div class="register-logo">
      <img class="logotipoSena" src="../web/img/logoSena.png" alt="Logotipo Sena">
  </div>
  <?php
  	if (isset($response)) {
  		$res = json_decode($response);
  ?>
		<div class="alert <?php echo $res->alert;?>">
			<b><?php echo $res->status;?></b>
			<p><?php echo $res->message;?></p>
		</div>
  <?php
  	}
  ?>
  <div>
  	<a class="btn btn-primary" href="index.php?r=site/login">
  		<i class="fa fa-sign-in"></i> Volver al inicio de sesión
  	</a>
  </div>
</div>