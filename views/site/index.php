<?php
use miloschuman\highcharts\Highcharts;
//use miloschuman\highcharts\Highstock;
use yii\web\JsExpression;

use yii\web\Session;

$session = Yii::$app->session;
$session->open();


if (isset($estimados) && !empty($estimados)) {
	$i = 1;
	$categories = [];
	$est = [];
	foreach ($estimados as $key => $value) {
		
		$categories[$key] = "AF_".$i;
		$est[$key] = $value['total_beneficiarios'];

		$i++;
	}
}

if (isset($registrados) && !empty($registrados)) {
	$i = 1;
	$reg = [];
	foreach ($registrados as $key => $value) {
		$reg[$key] = $value['registrados'];
		$i++;
	}
}


?>
<script src="js/code/highcharts.js"></script>
<script src="js/code/modules/exporting.js"></script>
<script src="js/code/modules/export-data.js"></script>

<div class="row">

	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-4" style="padding: 5px !important;">
				<div class="row estadistica back-blue">
					<div class="col-xs-12 col-sm-12 col-md-6">
						<b class="est-dato count"><?php echo $panelInicio[0]['acciones_formacion'];?></b>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<i class="fas fa-graduation-cap icono"></i>
					</div>
					<div class="col-xs-12 foot-back-blue">
						<p class="dato" style="color:#FFFFFF;font-size: 15pt;">Acciones de formación</p>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4" style="padding: 5px !important;">
				<div class="row estadistica back-green">
					<div class="col-xs-12 col-sm-12 col-md-6">
						<b class="est-dato count"><?php echo $panelInicio[0]['grupos'];?></b>
						<!--<b class="est-dato count">100</b>-->
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<i class="fa fa-sitemap icono"></i>
					</div>
					<div class="col-xs-12 foot-back-green">
						<p class="dato" style="color:#FFFFFF;font-size: 15pt;">Grupos</p>
					</div>
				</div>	
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4" style="padding: 5px !important;">
				<div class="row estadistica back-red">
					<div class="col-xs-12 col-sm-12 col-md-6">
						<b class="est-dato count"><?php echo $panelInicio[0]['beneficiarios'];?></b>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<i class="fa fa-users icono"></i>
					</div>
					<div class="col-xs-12 foot-back-red">
						<p class="dato" style="color:#FFFFFF;font-size: 15pt;">Beneficiarios</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div id="container" style="width: 100%; height: 1500px;"></div>
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="row">
			<div class="col-xs-12 instructivos" style="background: #FFFFFF;margin-bottom: 10px;">
				<table class="table">
					<caption><h3>INSTRUCTIVOS</h3></caption>
					<tr>
						<td>
							<a href="instructivos/Instructivo Banco de Beneficiarios.pdf" target="_blank"><span class="fa fa-file-pdf"></span> Instructivo Banco de Beneficiarios</a>
						</td>
					</tr>
					<tr>
						<td>
							<a href="instructivos/Instructivo Banco de Directores.pdf" target="_blank"><span class="fa fa-file-pdf"></span> Instructivo Banco de Directores</a>
						</td>
					</tr>
					<tr>
						<td>
							<a href="instructivos/Instructivo Cronograma.pdf" target="_blank"><span class="fa fa-file-pdf"></span> Instructivo Cronograma</a>
						</td>
					</tr>
				</table>
			</div>
			<div class="col-xs-12 infoDirector">
				<h3>Director de Proyecto</h3>
				<table class="table table-striped">
					<tr>
						<td><p class="dato"><?php echo $infoDirector['nombre'];?></p></td>
					</tr>
					<tr>
						<td><p class="dato"><?php echo $infoDirector['identificacion'];?></p></td>
					</tr>
					<tr>
						<td><p class="dato"><?php echo $infoDirector['correo'];?></p></td>
					</tr>
				</table>
				<?php
					if (isset($docsDirector) && !empty($docsDirector)) {
						foreach ($docsDirector as $key => $value) {
				?>
							<h3><?php echo $value['nombre'];?></h3>
							<p><a href="convocatoria_<?php echo $value['year'];?>/bancos/directores/<?php echo $value['num_doc'];?>/<?php echo $value['url'];?>" target="_blank"><?php echo $value['url'];?></a></p>
				<?php
						}
					}
				?>
			</div>
		</div>
	</div>

	
    
    
</div>
<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Beneficiarios registrados por Acción de Formación'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
        	<?php
        		foreach ($categories as $key => $value) {
        			echo "'".$value."',";
        		}
        	?>
        ],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Beneficiarios',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Beneficiarios'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Estimados',
        color: '#10ac84',
        data: [
			<?php
        		foreach ($est as $key => $value) {
        			echo $value.",";
        		}
        	?>
        ]
    }, {
        name: 'Registrados',
        color: '#FC7323',
        data: [
        	<?php
        		foreach ($reg as $key => $value) {
        			echo $value.",";
        		}
        	?>
        ]
    }, 
    ]
});
</script>