<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'SIGP';

$this->registerJsFile(
  '@web/js/beneficiarios.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
  '@web/js/beneficiarios_edit.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

$tipo_documentos =[
  ''=>'-Seleccione',
  'CC'  => 'CC (Cédula de ciudadanía)',
  'CE' => 'CE (Cédula de extranjería)',
  'PAS' => 'PAS (Pasaporte)',
  'TI'  => 'TI (Tarjeta de Identidad)',
];

$tipo_genero =[
  ''=>'-Seleccione',
  'MASCULINO' => 'MASCULINO',
  'FEMENINO' => 'FEMENINO',
];

$tipo_estratos =[
  ''=>'-Seleccione',
  '1' => 'Estrato 1',
  '2' => 'Estrato 2',
  '3' => 'Estrato 3',
  '4' => 'Estrato 4',
  '5' => 'Estrato 5',
  '6' => 'Estrato 6',
];

$caracterizacion = [
  '' => '-Seleccione',
  'NINGUNA' => 'NINGUNA',
  'ADOLESCENTE  DESVINCULADO  DE  GRUPOS  ARMADOS  ORGANIZADOS AL  MARGEN  DE  LA  LEY'=>'ADOLESCENTE  DESVINCULADO  DE  GRUPOS  ARMADOS  ORGANIZADOS AL  MARGEN  DE  LA  LEY',
  'ADOLESCENTE  TRABAJADOR' => 'ADOLESCENTE  TRABAJADOR',
  'AFROCOLOMBIANOS  DESPLAZADOS  POR  LA  VIOLENCIA'=>'AFROCOLOMBIANOS  DESPLAZADOS  POR  LA  VIOLENCIA',
  'AFROCOLOMBIANOS  DESPLAZADOS  POR  LA  VIOLENCIA CABEZA  DE  FAMILIA'=>'AFROCOLOMBIANOS  DESPLAZADOS  POR  LA  VIOLENCIA CABEZA  DE  FAMILIA',
  'ARTESANOS'=>'ARTESANOS',
  'DESPLAZADOS  DISCAPACITADO'=>'DESPLAZADOS  DISCAPACITADO',
  'DESPLAZADOS  POR  FENOMENOS  NATURALES'=>'DESPLAZADOS  POR  FENOMENOS  NATURALES',
  'DESPLAZADOS  POR  FENOMENOS  NATURALES  CABEZA  DE  FAMILIA'=>'DESPLAZADOS  POR  FENOMENOS  NATURALES  CABEZA  DE  FAMILIA',
  'DESPLAZADOS  POR  LA  VIOLENCIA'=>'DESPLAZADOS  POR  LA  VIOLENCIA',
  'DESPLAZADOS  POR  LA  VIOLENCIA  CABEZA  DE  FAMILIA'=>'DESPLAZADOS  POR  LA  VIOLENCIA  CABEZA  DE  FAMILIA',
  'DISCAPACITADO  COGNITIVO'=>'DISCAPACITADO  COGNITIVO',
  'DISCAPACITADO  LIMITACION  AUDITIVA'=>'DISCAPACITADO  LIMITACION  AUDITIVA',
  'DISCAPACITADO  LIMITACION  FISICA'=>'DISCAPACITADO  LIMITACION  FISICA',
  'DISCAPACITADO  LIMITACION  VISUAL'=>'DISCAPACITADO  LIMITACION  VISUAL',
  'DISCAPACITADO  MENTAL'=>'DISCAPACITADO  MENTAL',
  'DISCAPACITADOS'=>'DISCAPACITADOS',
  'ADOLESCENTE  EN  CONFLICTO  CON  LA  LEY  PENAL'=>'ADOLESCENTE  EN  CONFLICTO  CON  LA  LEY  PENAL',
  'EMPRENDEDORES'=>'EMPRENDEDORES',
  'INDIGENAS'=>'INDIGENAS',
  'INDIGENAS  DESPLAZADOS  POR  LA  VIOLENCIA'=>'INDIGENAS  DESPLAZADOS  POR  LA  VIOLENCIA',
  'INDIGENAS  DESPLAZADOS  POR  LA  VIOLENCIA  CABEZA  DE  FAMILIA'=>'INDIGENAS  DESPLAZADOS  POR  LA  VIOLENCIA  CABEZA  DE  FAMILIA',
  'INPEC'=>'INPEC',
  'JOVENES  VULNERABLES'=>'JOVENES  VULNERABLES',
  'MICROEMPRESAS'=>'MICROEMPRESAS',
  'MUJER  CABEZA  DE  FAMILIA'=>'MUJER  CABEZA  DE  FAMILIA',
  'NEGRO'=>'NEGRO',
  'PROC  REINTEGRACION'=>'PROC  REINTEGRACION',
  'REMITIDOS  POR  EL  CI'=>'REMITIDOS  POR  EL  CI',
  'REMITIDOS  POR  EL  PAL'=>'REMITIDOS  POR  EL  PAL',
  'SOBREVIVIENTES  MINAS  ANTIPERSONALES'=>'SOBREVIVIENTES  MINAS  ANTIPERSONALES',
  'SOLDADOS  CAMPESINOS'=>'SOLDADOS  CAMPESINOS',
  'TERCERA  EDAD'=>'TERCERA  EDAD',
  'AFROCOLOMBIANO'=>'AFROCOLOMBIANO',
  'PALENQUEROS'=>'PALENQUEROS',
  'RAIZALES'=>'RAIZALES',
  'ROM'=>'ROM',
  'MINAS ANTIPERSONALES Y EXPLOSIVOS'=>'MINAS ANTIPERSONALES Y EXPLOSIVOS',
  'AMENAZA'=>'AMENAZA',
  'DELITOS SEXUALES'=>'DELITOS SEXUALES',
  'DESAPARICION FORZADA'=>'DESAPARICION FORZADA',
  'HOMICIDIO / MASACRE'=>'HOMICIDIO / MASACRE',
  'SECUESTRO'=>'SECUESTRO',
  'TORTURA'=>'TORTURA',
  'USO DE MENORES POR GRUPOS ARMADOS'=>'USO DE MENORES POR GRUPOS ARMADOS',
  'DESPOJO'=>'DESPOJO',
  'ACTOS DE GRUPOS ARMADOS'=>'ACTOS DE GRUPOS ARMADOS',
  'HERIDO'=>'HERIDO',
  'RECLUTAMIENTO FORZADO'=>'RECLUTAMIENTO FORZADO',
];

$dptos = [''=>'-Seleccione'];
if (isset($listaDptos)) {
  foreach ($listaDptos as $key => $value) {
     $dptos[$value['id_dpto']] = $value['nombre'];
  }
}

$ciudades = [''=>'-Seleccione'];
if (isset($listaCiudades)) {
  foreach ($listaCiudades as $key => $value) {
     $ciudades[$value['id_ciudad']] = $value['nombre'];
  }
}

$tamanio_empresa_labora = [
  ''=>'-Seleccione',
  'MICROEMPRESA (HASTA 500 SMMLV - $414.058.000)'=>'MICROEMPRESA (HASTA 500 SMMLV - $414.058.000)',
  'PEQUEÑA (SUPERIOR A 500 SMMLV Y HASTA 5.000 SMMLV | $414-058.000 - $4.140.580.000)'=>'PEQUEÑA (SUPERIOR A 500 SMMLV Y HASTA 5.000 SMMLV | $414-058.000 - $4.140.580.000)',
  'MEDIANA (SUPERIOR A 5.000 SMMLV Y HASTA 30.000 SMMLV | $4.140.580.000 - $24.843.480.000)'=>'MEDIANA (SUPERIOR A 5.000 SMMLV Y HASTA 30.000 SMMLV | $4.140.580.000 - $24.843.480.000)',
  'GRANDE (SUPERIOR A 30.000 SMMLV | $24.843.480.000)'=>'GRANDE (SUPERIOR A 30.000 SMMLV | $24.843.480.000)'
];

$nivel_ocupacional = [
  ''=>'-Seleccione',
  'NIVEL OPERATIVO'=>'NIVEL OPERATIVO',
  'NIVEL MEDIO'=>'NIVEL MEDIO',
  'NIVEL ALTA DIRECCION'=>'NIVEL ALTA DIRECCION',
];

$perfil_transferencia = [
  ''=>'-Seleccione',
  'NINGUNO'=>'NINGUNO',
  'FUNCIONARIO'=>'FUNCIONARIO',
  'CONTRATISTA'=>'CONTRATISTA',
  'INSTRUCTOR'=>'INSTRUCTOR',
  'APRENDIZ'=>'APRENDIZ',
  'EGRESADO'=>'EGRESADO',
  'BENEFICIARIO DEL FONDO EMPRENDER'=>'BENEFICIARIO DEL FONDO EMPRENDER',
  'REGISTRADO EN AGENCIA PÚBLICA DE EMPLEO (APE)'=>'REGISTRADO EN AGENCIA PÚBLICA DE EMPLEO (APE)'
]

//echo "<pre>";
//print_r($accionesFormacion);
//echo "</pre>";
?>
<style media="screen">
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #ccc;
  width: 100%;
}
.fa-asterisk{
  color: red;
}
</style>
<div class="container container-form">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <a class="btn btn-primary pull-right" href="index.php?r=beneficiarios/index"><i class="fa fa-undo"></i> Volver</a>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>Editar beneficiario</h1>
        <p>
          Formulario de actualización de beneficiarios.
        </p>
    </div>
    <div class="col-xs-12" id="alerta-busqueda">
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
      <label>Tipo de identificación</label>
      <select class="form-control" name="tipo_identi" id="tipo_identi">
        <option value="">-Seleccione</option>
        <option value="CC">CC (Cédula de ciudadanía)</option>
        <option value="CE">CE (Cédula de extranjería)</option>
        <option value="PAS">PAS (Pasaporte)</option>
        <option value="TI">TI (Tarjeta de Identidad)</option>
      </select>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
      <label>Número de identificación</label>
      <input type="number" min="0" class="form-control" id="num_identi">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <label>Buscar</label><br>
      <button class="btn btn-primary" onclick="buscar_info_beneficiario()"><span class="fa fa-search"></span></button>
    </div>
  </div>
  <?php
      $form = ActiveForm::begin(['id' => 'register-form',]);
  ?>
  <div class="row">
    <div class="col-xs-12" id="alerta-registro">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <h2>Datos del beneficiario</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <input type="hidden" id="id_proyecto_beneficiario">
          <input type="hidden" id="id_beneficiario">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?=
            $form->field($model, 'tipo_doc')
            ->dropdownList($tipo_documentos,['name'=>'Tipo de identificación'])
            ->label('<i class="fa fa-asterisk"></i> Tipo de identificación');
          ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
           <?= $form
              ->field($model, 'num_doc')
              ->input('number',['name'=>'Número de identificación','min'=>'0','oninput'=>'validarNumero(this)'])
              ->label('<i class="fa fa-asterisk"></i> Número de Identificación') ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <?= $form
              ->field($model, 'nombres')
              ->input('text',['name'=>'Nombres'])
              ->label('<i class="fa fa-asterisk"></i> Nombres') ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?= $form
              ->field($model, 'apellido_1')
              ->input('text',['name'=>'Primer apellido'])
              ->label('<i class="fa fa-asterisk"></i> Primer apellido') ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?= $form
              ->field($model, 'apellido_2')
              ->input('text',['name'=>'Segundo apellido'])
              ->label('Segundo apellido') ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <?= $form
              ->field($model, 'genero')
              ->dropdownList($tipo_genero,['name'=>'Género'])
              ->label('<i class="fa fa-asterisk"></i> Género') ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <?= $form
              ->field($model, 'estrato')
              ->dropdownList($tipo_estratos,['name'=>'Estrato'])
              ->label('<i class="fa fa-asterisk"></i> Estrato') ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <?= $form
              ->field($model, 'fecha_nacimiento')
              ->input('date',['name'=>'Fecha de nacimiento','onblur'=>'calcularEdad(this.value)'])
              ->label('<i class="fa fa-asterisk"></i> Fecha de nacimiento') ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <?= $form
              ->field($model, 'celular')
              ->input('text',['name'=>'Celular'])
              ->label('<i class="fa fa-asterisk"></i> Número celular') ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <?= $form
              ->field($model, 'id_departamento')
              ->dropdownList($dptos,['name'=>'Departamento','onchange'=>'buscarCiudades(this.value)'])
              ->label('<i class="fa fa-asterisk"></i> Departamento') ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <?= $form
              ->field($model, 'id_ciudad')
              ->dropdownList($ciudades,['name'=>'Ciudad'])
              ->label('<i class="fa fa-asterisk"></i> Ciudad') ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?= $form
              ->field($model, 'barrio_vereda')
              ->input('text',['name'=>'Barrio / Vereda'])
              ->label('<i class="fa fa-asterisk"></i> Barrio / Vereda') ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?= $form
              ->field($model, 'direccion')
              ->input('text',['name'=>'Dirección'])
              ->label('<i class="fa fa-asterisk"></i> Dirección') ?>
        </div>
      </div>
      
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <h2>Datos de postulación</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?=
            $form->field($model, 'edad')
            ->input('number',['name'=>'Edad','min'=>'0','oninput'=>'validarNumero(this)'])
            ->label('<i class="fa fa-asterisk"></i> Edad');
          ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
           <?= $form
              ->field($model, 'antiguedad')
              ->dropdownList([''=>'-Seleccione','SI'=>'SI','NO'=>'NO'],['name'=>'Se ha beneficiado anteriormente del programa?'])
              ->label('<i class="fa fa-asterisk"></i> Se ha beneficiado anteriormente del programa?') ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?=
            $form->field($model, 'empresa_labora')
            ->input('text',['name'=>'Empresa donde labora'])
            ->label('<i class="fa fa-asterisk"></i> Empresa donde labora');
          ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?=
            $form->field($model, 'tamanio_empresa_labora')
            ->dropdownList($tamanio_empresa_labora,['name'=>'Tamaño de empresa donde labora'])
            ->label('<i class="fa fa-asterisk"></i> Tamaño de empresa donde labora');
          ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
           <?= $form
              ->field($model, 'caracterizacion')
              ->dropdownList($caracterizacion,['name'=>'Caracterización'])
              ->label('<i class="fa fa-asterisk"></i> Caracterización') ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <?= $form
              ->field($model, 'nivel_ocupacional')
              ->dropdownList($nivel_ocupacional,['name'=>'Nivel ocupacional'])
              ->label('<i class="fa fa-asterisk"></i> Nivel ocupacional') ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <?= $form
              ->field($model, 'transferencia')
              ->dropdownList([''=>'-Seleccione','SI'=>'SI','NO'=>'NO'],['name'=>'Transferencia'])
              ->label('<i class="fa fa-asterisk"></i> Transferencia') ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <?= $form
              ->field($model, 'perfil_transferencia')
              ->dropdownList($perfil_transferencia,['name'=>'Perfil de transferencia'])
              ->label('<i class="fa fa-asterisk"></i> Perfil de transferencia') ?>
        </div>
      </div>
      
      <div class="row">
        <div class="col-xs-12">
          <button type="button" id="btn-guardar-beneficiario" class="btn btn-primary pull-right" onclick="actualizar_beneficiario()">Actualizar beneficiario</button>
        </div>
      </div>

    </div>
    
    
  </div>
  <?php 
      ActiveForm::end(); 
  ?>
</div>
