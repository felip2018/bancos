<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Beneficiarios';

?>
<style>
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #ccc;
  width: 100%;
}
.btn{
  margin:5px !important;
}
</style>

<div class="container container-form">
  <?php
      $form = ActiveForm::begin(['id' => 'register-form',]);
  ?>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <a class="btn btn-primary pull-right" href="index.php?r=beneficiarios/register"><i class="fa fa-plus"></i> Registrar nuevo</a>
      <a class="btn btn-primary pull-right" href="index.php?r=beneficiarios/edit"><i class="fa fa-edit"></i> Editar beneficiario</a>
      <a class="btn btn-primary pull-right" href="index.php?r=beneficiarios/exportar-beneficiarios"><i class="fa fa-file-excel"></i> Descargar Listado de Beneficiarios</a>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <!--<div class="alert alert-info">-->
        <h1>Beneficiarios</h1>
        <p>
          Listado de beneficiarios registrados en el proyecto.
        </p>
      <!--</div>-->
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th>Nombre</th>
            <th>Identificación</th>
            <th>Estado</th>
            <!--<th>Opciones</th>-->
          </tr>
        </thead>
        <tbody>
          <?php
            if (!empty($listadoBeneficiarios)) {
              $i=1;
              foreach ($listadoBeneficiarios as $key => $value) {

                $fondo = ($value['estado_registro'] == "ACTIVO") ? "#10ac84" : "#ee5253";
          ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $value['nombre'];?></td>
                  <td><?php echo $value['identificacion'];?></td>
                  <td style="background: <?php echo $fondo;?>;color:#FFFFFF;"><?php echo $value['estado_registro'];?></td>
                  <!--<td>
                    <a href="#" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                  </td>-->
                </tr>
          <?php
                $i++;
              }
            }else{
          ?>
                <tr>
                  <td>0</td>
                  <td>Vacío</td>
                  <td>Vacío</td>
                  <td>Vacío</td>
                  <td>Vacío</td>
                </tr>
          <?php
            }
          ?>
        </tbody>
      </table>
    </div>
  </div>


  <?php ActiveForm::end(); ?>
</div>
