<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'SEP';

$this->registerJsFile(
  '@web/js/capacitadores_juridicos.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);


$tipos_doc =[
  ''=>'-Seleccione',
  'NIT'  => 'NIT',
];


?>
<style media="screen">
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #ccc;
  width: 100%;
}
.fa-asterisk{
  color: red;
}
.documents{
  box-shadow: 0px 0px 5px #CCC;
  border-radius: 5px;
  margin-top: 10px;
}
</style>

<div class="container container-form">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <a class="btn btn-primary pull-right" href="index.php?r=capacitadores/index"><i class="fa fa-undo"></i> Volver</a>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
       <h1>Registro de capacitador jurídico del proyecto</h1>
      <p> Formulario de registro de capacitadores del proyecto.</p>
    </div>
    <div class="col-xs-12" id="alerta"></div>
   </div> 
    <?php
        if (isset($registro)) {
          $res = json_decode($registro);
    ?>
    <div class="col-xs-12 alert <?php echo $res->alert;?>">
       <b><?php echo $res->status;?></b>
       <p><?php echo $res->message;?></p>
    </div>
    <?php
        }
    ?>
    <?php
      $form = ActiveForm::begin(['id' => 'registro_capacitador']);
  ?>
   <div class="row">
    <input type="hidden" id="id_capacitador">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?= $form
              ->field($model, 'nit')
              ->input('number',['onblur'=>'buscarCapacitadorJ(this.value)'])
              ->label('<i class="fa fa-asterisk"></i> Número de Identificación')?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?= $form
              ->field($model, 'digito_verificacion')
              ->input('number',[])
              ->label('<i class="fa fa-asterisk"></i> Dígito de Verificacion')?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <?= $form
          ->field($model, 'razon_social')
          ->label('<i class="fa fa-asterisk"></i> Razón Social') ?>
       </div>
     </div> 
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?= $form
              ->field($model, 'pagina_web')
              ->label('<i class="fa fa-asterisk"></i> Pagina web') ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?= $form
              ->field($model, 'telefono')
              ->label('<i class="fa fa-asterisk"></i> Telefono') ?>
        </div>
     </div>  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <?= $form
          ->field($model, 'correo_electronico')
          ->input('email')
          ->label('<i class="fa fa-asterisk"></i> Correo Electronico')  ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <?php
            if (!empty($listadoDocumentosRequeridos)) {
              
              foreach ($listadoDocumentosRequeridos as $key => $value) {
            
                $required = ($value['obligatorio'] == 'SI') ? true : false;
          ?>
                <p><?php echo $value['nombre'];?></p>
                <input class="form-control registerDoc" type="file" name="<?php echo $value['input_name'];?>" id="<?php echo $value['input_name'];?>" required="<?php $required;?>" document="<?php echo $value['nombre'];?>">
          <?php
              }
            }else{
              echo "Vacio";
            }
          ?>
        </div>
      </div>
      <div class="row" style="padding-top: 10px;">
        <div class="col-xs-12">
          <button type="button" id="btnRegisterCapacitador" class="btn btn-primary pull-right" onclick="registrar_capacitador()">Guardar capacitador</button>
        </div>
      </div>
      </div>
    </div>
  <?php 
    ActiveForm::end(); 
  ?>
  
   <!--Registro de experiencias-->
  <div class="row documents">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <h2>Experiencias</h2>
      <button class="btn btn-primary pull-right" onclick="registrar_experiencias()">
        <i class="fa fa-plus"></i> Agregar experiencia
      </button>
      <button class="btn btn-primary pull-left" onclick="actualizar_experiencias()">
        <i class="fa fa-sync"></i>
      </button>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th>Experiencia</th>
            <th>Descripción</th>
            <th>Soporte</th>
          </tr>
        </thead>
        <tbody id="listadoExperienciasCapacitador"></tbody>
      </table>
    </div>
  </div>
  
  <!--Registro de titulos-->
  <div class="row documents">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <h2>Titulos</h2>
      <button class="btn btn-primary pull-right" onclick="registrar_titulos()">
        <i class="fa fa-plus"></i> Agregar titulo
      </button>
      <button class="btn btn-primary pull-left" onclick="actualizar_titulos()">
        <i class="fa fa-sync"></i>
      </button>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th>Titulo</th>
            <th>Descripcion</th>
            <th>Soporte</th>
          </tr>
        </thead>
        <tbody id="listadoTitulosCapacitador"></tbody>
      </table>
    </div>
  </div>

  <!--Registro de referencias-->
  <div class="row documents">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <h2>Referencias</h2>
      <button class="btn btn-primary pull-right" onclick="registrar_referencias()">
        <i class="fa fa-plus"></i> Agregar referencia
      </button>
      <button class="btn btn-primary pull-left" onclick="actualizar_referencias()">
        <i class="fa fa-sync"></i>
      </button>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th>Nombre</th>
            <th>Empresa</th>
            <th>Cargo</th>
            <th>Soporte</th>
          </tr>
        </thead>
        <tbody id="listadoReferenciasCapacitador"></tbody>
      </table>
    </div>
  </div>

  <!--Registro de palabras clave-->
  <div class="row documents">
    <div class="col-xs-12 col-sm-12 col-md-6">
      <h2>Palabras Clave</h2>
      <div id="listaPalabrasClavePorAdicionar"></div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
      <h2>Palabras Asociadas</h2>
      <div id="listaPalabrasClaveAsociadas"></div>
    </div>
  </div>  


</div>