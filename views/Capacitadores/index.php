<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Capacitadores';
//print_r($listadoCapacitadores);
?>
<style media="screen">
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #ccc;
  width: 100%;
}
</style>

<div class="container container-form">
  <?php
      $form = ActiveForm::begin(['id' => 'register-form',]);
  ?>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <a class="btn btn-primary pull-right" href="index.php?r=capacitadores/register-juridic" style="margin-right: 5px;"><i class="fa fa-plus"></i> Registrar Capacitador Jurídico</a>
      <a class="btn btn-primary pull-right" href="index.php?r=capacitadores/register" style="margin-right: 5px;"><i class="fa fa-plus"></i> Registrar Capacitador Natural</a>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h1>Capacitadores naturales</h1>
      <p>Listado de capacitadores naturales registrados en el proyecto.</p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th>Nombre</th>
            <th>Identificación</th>
            <th>Estado</th>
          </tr>
        </thead>
        <tbody>
          <?php
            if (!empty($listadoCapacitadores)) {
              $i=1;
              foreach ($listadoCapacitadores as $key => $value) {

                $fondo = ($value['estado_registro'] == "ACTIVO") ? "#10ac84" : "#ee5253";
          ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $value['nombre'];?></td>
                  <td><?php echo $value['identificacion'];?></td>
                  <td style="background: <?php echo $fondo;?>;color:#FFFFFF;"><?php echo $value['estado_registro'];?></td>
                  <td></td>
                </tr>
          <?php
                $i++;
              }
            }else{
          ?>
                <tr>
                  <td>0</td>
                  <td>Vacío</td>
                  <td>Vacío</td>
                  <td>Vacío</td>
                  <td>Vacío</td>
                </tr>
          <?php
            }
          ?>
        </tbody>
      </table>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h1>Capacitadores jurídicos</h1>
      <p>Listado de capacitadores jurídicos registrados en el proyecto.</p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th>Razón Social</th>
            <th>Nit</th>
            <th>Digito Verificación</th>
            <th>Estado</th>
          </tr>
        </thead>
        <tbody>
          <?php
            if (!empty($listadoCapacitadores)) {
              $i=1;
              /*foreach ($listadoCapacitadores as $key => $value) {

                $fondo = ($value['estado_registro'] == "ACTIVO") ? "#10ac84" : "#ee5253";
          ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $value['nombre'];?></td>
                  <td><?php echo $value['identificacion'];?></td>
                  <td style="background: <?php echo $fondo;?>;color:#FFFFFF;"><?php echo $value['estado_registro'];?></td>
                  <td></td>
                </tr>
          <?php
                $i++;
              }*/
            }else{
          ?>
                <tr>
                  <td>0</td>
                  <td>Vacío</td>
                  <td>Vacío</td>
                  <td>Vacío</td>
                  <td>Vacío</td>
                </tr>
          <?php
            }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <?php ActiveForm::end(); ?>
</div>