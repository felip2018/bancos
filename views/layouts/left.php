<?php
use yii\helpers\Html;
use yii\web\Session;

$session = Yii::$app->session;
$session->open();
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <div class="user-panel">
            <div class="pull-left image">
                <img src="img/logoSena.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?php echo "No. Radicado: " . $session['data']['n_radicado'];?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $session['data']['sesion'];?></a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' =>  [
                    ['label' => 'Inicio', 'icon' => 'home', 'url' => ['site/index'],],
                    [
                        'label' => 'Beneficiarios', 
                        'icon' => 'users', 
                        //'url' => ['beneficiarios/index'],
                        'url' => ['#'],
                        'items' => [
                            [
                                'label' => 'Grupos A.F.',
                                'icon'  => '',
                                'url'   => ['grupos/index']
                            ],
                            [
                                'label' => 'Beneficiarios',
                                'icon'  => '',
                                'url'   => ['beneficiarios/index']
                            ],
                        ]
                    ],
                    ['label' => 'Director de proyecto', 'icon' => 'user', 'url' => ['directores/index'],],
                    /*[
                        'label' => 'Capacitadores', 
                        'icon'  => 'users', 
                        'url'   => ['#'],
                        'items' => [
                            [
                                'label' => '1. Registrar',
                                'icon'  => 'plus', 
                                'url'   => ['capacitadores/index'],
                            ],
                        ],
                    ],*/
                    [
                        'label' => 'Cronograma', 
                        'icon'  => 'clock', 
                        'url'   => ['#'],
                        'items' => [
                            [
                                'label' => 'Configurar cronograma',
                                'icon'  => 'cog', 
                                'url'   => ['cronograma/register'],
                            ],
                            [
                                'label' => 'Buscar cronograma',
                                'icon'  => 'search', 
                                'url'   => ['cronograma/buscar'],
                            ],
                            [
                                'label' => 'Radicación y publicación',
                                'icon'  => 'file', 
                                'url'   => ['cronograma/radicacion'],
                            ],
                            [
                                'label' => 'Solicitud modificación',
                                'icon'  => 'pencil', 
                                'url'   => ['cronograma/solicitud-modificacion'],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
