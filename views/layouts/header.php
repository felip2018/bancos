<?php
use yii\helpers\Html;
use yii\web\Session;

$session = Yii::$app->session;
$session->open();
?>

<header class="main-header">

    <?php 
        //Html::a('<span class="logo-mini">SM</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) 
    ?>
    <a class="logo" href="index.php?r=site/index">
        <span class="logo-mini">SEP</span>
        <span class="logo-lg">SEP</span>
    </a>

    <nav class="navbar navbar-static-top header_1" role="navigation">
        
            
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        
        <div style="float: left;padding: 10px;">
            <b class="dato" style="color:#FFFFFF;padding-top: 10px;"><?php echo $session['data']['titulo'];?></b>
        </div>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="border:1px solid #FFF;">
                        <span class="hidden-xs dato" style="font-size: 15pt;color:#FFFFFF;"><?php echo "No. Radicado: " . $session['data']['n_radicado'];?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header" style="background: #FC7323;">
                            <img src="img/logoSena.png" class="img-circle" alt="User Image"/>

                            <p>
                                <?php echo "No. Radicado: " . $session['data']['n_radicado'];?>
                                <small>Registro <?php echo date($session['data']['registro_proyecto']);?></small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                
                            </div>
                            <div class="pull-right">
                                <a href="index.php?r=site/close" class="btn btn-danger btn-flat">
                                    Salir <i class="fa fa-sign-out"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
