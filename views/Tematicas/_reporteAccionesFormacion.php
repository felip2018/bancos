<?php
	$proyectos = $model->obtenerProyectos();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
</head>
<script src="js/code/highcharts.js"></script>
<script src="js/code/modules/exporting.js"></script>
<script src="js/code/modules/export-data.js"></script>
<body>
	<?php
		if (!empty($proyectos)) {
			foreach ($proyectos as $key => $value) {

				$ejesTematicos 		= $model->ejesAccionFormacion($value['id_proyecto']);
				$eventosFormacion 	= $model->eventosFormacion($value['id_proyecto']);
				$evFormacion 		= $model->eventosFormacionBloques($value['id_proyecto']);
				//$accionesFormacion 	= $model->accionesFormacion($value['id_proyecto']);
				$cobertura 			= $model->coberturaProyecto($value['id_proyecto']);
				$tematicas 			= $model->tematicasAccionesFormacion($value['id_proyecto']);
	?>
				<div class="informe" style="background: #FFFFFF;padding: 10px;border-radius: 5px;font-size: 18pt;">
					<div class="row" style="text-align: center;">
						<div class="col-xs-2">
							<img src="img/logoSenaPDF.png" alt="Logo Sena" style="width: 80px;height: 80px;">
						</div>
						<div class="col-xs-8">
							<p><b>PROGRAMA DE FORMACIÓN CONTINUA ESPECIALIZADA</b></p>
							<p><b>CONVOCATORIA DG-0001 DE 2019</b></p>
							<p><b>FICHA RESUMEN PROYECTOS</b></p>
							<p><b>DIRIGIDO A:</b></p>
							<b>CONSEJO DIRECTIVO NACIONAL</b>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<table class="table table-striped">
								<tr>
									<td style="width: 10%;padding: 10px;"><b>PROPONENTE</b></td>
									<td style="width: 90%;padding: 10px;"><b style="font-size: 18pt;"><?php echo strtoupper($value['titulo']);?></b></td>
								</tr>
							</table>
						</div>
						<div class="col-xs-12" style="text-align: center;">
							<h3>INFORMACIÓN GENERAL</h3>
						</div>
						<div class="col-xs-12">
							<table class="table table-striped">
								<!--<tr>
									<td style="width: 20%;"><b>NIT</b></td>
									<td style="width: 80%;"><?php //echo $value['num_doc'];?></td>
								</tr>-->
								<tr>
									<td style="width: 20%;"><b>CODIGO SIGP</b></td>
									<td style="width: 80%;"><?php echo $value['n_radicado'];?></td>
								</tr>
								<tr>
									<td><b>MODALIDAD</b></td>
									<td><?php echo $value['modalidad_proyecto'];?></td>
								</tr>
								<tr>
									<td><b>ACCIONES DE FORMACIÓN</b></td>
									<td><?php echo $value['acciones_formacion'];?></td>
								</tr>
								<tr>
									<td><b>NÚMERO DE BENEFICIARIOS</b></td>
									<td><?php echo $value['total_beneficiarios'];?></td>
								</tr>
								<!--<tr>
									<td><b>REGIONAL</b></td>
									<td><?php //echo $value['regional'];?></td>
								</tr>-->
								<!--<tr>
									<td><b>SECTOR</b></td>
									<td><?php //echo $value['sector'];?></td>
								</tr>-->
							</table>
						</div>
						<div class="col-xs-12" style="text-align: center;">
							<h3>INFORMACIÓN FINANCIERA</h3>
						</div>
						<div class="col-xs-12">
							<table class="table table-striped">
								<tr>
									<td style="width: 20%;"><b>VALOR TOTAL</b></td>
									<td style="width: 80%;"><?php echo number_format($value['valor_total'])." COP";?></td>
								</tr>
								<tr>
									<td><b>COFINANCIACIÓN SENA</b></td>
									<td><?php echo number_format($value['monto_solicitado'])." COP";?></td>
								</tr>
								<tr>
									<td><b>CONTRAPARTIDA EMPRESA</b></td>
									<td><?php echo number_format($value['contra_dinero'] + $value['contra_especie'])." COP";?></td>
								</tr>
							</table>
						</div>

						<div class="col-xs-12" style="text-align: center;">
							<div class="row">
								<div class="col-xs-4">
									<h3>COBERTURA</h3>
									<table class="table table-striped">
										<thead>
											<tr>
												<th style="text-align: center;"><h3>DEPARTAMENTO</h3></th>
												<th style="text-align: center;"><h3>NÚMERO DE GRUPOS</h3></th>
											</tr>
										</thead>
										<tbody>
											<?php
												if (!empty($cobertura)) {
													foreach ($cobertura as $key => $cob) {
											?>
													<tr>
														<td><?php echo $cob['dpto'];?></td>
														<td><?php echo $cob['total_grupos'];?></td>
													</tr>
											<?php
													}
												}
											?>
										</tbody>
									</table>
								</div>
								<div class="col-xs-8">
									<h3>TEMÁTICAS</h3>
									<table class="table table-striped">
										<thead>
											<tr>
												<th style="text-align: center;"><h3>TEMÁTICA</h3></th>
												<th style="text-align: center;"><h3>NÚMERO DE A.F.</h3></th>
											</tr>
										</thead>
										<tbody>
											<?php
												if (!empty($tematicas)) {
													foreach ($tematicas as $key => $tem) {
											?>
													<tr>
														<td><?php echo $tem['tematica'];?></td>
														<td><?php echo $tem['total_acciones'];?></td>
													</tr>
											<?php
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="col-xs-12" style="text-align: center;">
							<h3>EVENTOS DE FORMACIÓN POR ACCIONES DE FORMACIÓN</h3>
						</div>
						
						<div class="col-xs-12">
							<div class="row">
								<div class="col-xs-12">
									<div id="container_<?php echo $value['id_proyecto'];?>" style="width: 100%; height: auto; margin: 0 auto"></div>
								</div>
							</div>
							<script type="text/javascript">
								Highcharts.chart('container_<?php echo $value['id_proyecto'];?>', {
								    chart: {
								        plotBackgroundColor: null,
								        plotBorderWidth: null,
								        plotShadow: false,
								        type: 'pie'
								    },
								    title: {
								        text: ''
								    },
								    tooltip: {
								        pointFormat: '{series.name}: <b>{point.y:,.0f}</b>'
								    },
								    plotOptions: {
								        pie: {
								            allowPointSelect: true,
								            cursor: 'pointer',
								            dataLabels: {
								                enabled: true,
								                format: '<b>{point.name}</b>: {point.y:,.0f}',
								                style: {
								                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
								                }
								            }
								        }
								    },
								    series: [{
								        name: 'Brands',
								        colorByPoint: true,
								        data: [
								        <?php
								        	foreach ($eventosFormacion as $key => $value) {
								        ?>
									        	{
									            	name: '<?php echo $value["evento_formacion"];?>',
									            	y: <?php echo $value['cant'];?>,
									        	},
								        <?php

								        	}
								        ?>
								        ]
								    }]
								});
							</script>
						</div>
						<div class="col-xs-12" style="text-align: center;">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-2" style="padding: 5px !important;">
									<div style="border: 1px solid #F39C12;color: #000000;float: left;padding-top: 10px;width:100%;">
										<div class="col-xs-12 col-sm-12 col-md-6">
											<b class="est-dato count"><?php echo $evFormacion['cursos'];?></b>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6">
											<!--<i class="fas fa-book icono" style="color: #CF850F;font-size: 50pt;"></i>-->
											<img src="img/cursos.png" alt="cursos" style="width: 60px;height: 65px;">
										</div>
										<div class="col-xs-12">
											<p class="dato" style="color:#000000;font-size: 15pt;">Cursos</p>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-2" style="padding: 5px !important;">
									<div style="border: 1px solid #DD4B39;color: #000000;float: left;padding-top: 10px;width:100%;">
										<div class="col-xs-12 col-sm-12 col-md-6">
											<b class="est-dato count"><?php echo $evFormacion['seminario'];?></b>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6">
											<!--<i class="fas fa-person-booth icono" style="color: #BC4031;font-size: 50pt;"></i>-->
											<img src="img/seminario.png" alt="seminario" style="width: 60px;height: 65px;">
										</div>
										<div class="col-xs-12">
											<p class="dato" style="color:#000000;font-size: 15pt;">Seminario</p>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-2" style="padding: 5px !important;">
									<div style="border: 1px solid #00C0EF;color: #000000;float: left;padding-top: 10px;width:100%;">
										<div class="col-xs-12 col-sm-12 col-md-6">
											<b class="est-dato count"><?php echo $evFormacion['taller'];?></b>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6">
											<!--<i class="fas fa-pen icono" style="color: #00A3CB;font-size: 50pt;"></i>-->
											<img src="img/taller.png" alt="taller" style="width: 60px;height: 65px;">
										</div>
										<div class="col-xs-12">
											<p class="dato" style="color:#000000;font-size: 15pt;">Taller</p>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-2" style="padding: 5px !important;">
									<div style="border: 1px solid #00A65A;color: #000000;float: left;padding-top: 10px;width:100%;">
										<div class="col-xs-12 col-sm-12 col-md-6">
											<b class="est-dato count"><?php echo $evFormacion['diplomado'];?></b>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6">
											<!--<i class="fas fa-scroll icono" style="color: #008D4D;font-size: 50pt;"></i>-->
											<img src="img/diplomado.png" alt="diplomado" style="width: 60px;height: 65px;">
										</div>
										<div class="col-xs-12">
											<p class="dato" style="color:#000000;font-size: 15pt;">Diplomado</p>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-2" style="padding: 5px !important;">
									<div style="border: 1px solid #F39C12;color: #000000;float: left;padding-top: 10px;width:100%;">
										<div class="col-xs-12 col-sm-12 col-md-6">
											<b class="est-dato count"><?php echo $evFormacion['conferencia'];?></b>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6">
											<!--<i class="fas fa-hand-paper icono" style="color: #CF850F;font-size: 50pt;"></i>-->
											<img src="img/conferencia.png" alt="conferencia" style="width: 60px;height: 65px;">
										</div>
										<div class="col-xs-12">
											<p class="dato" style="color:#000000;font-size: 15pt;">Conferencia</p>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-2" style="padding: 5px !important;">
									<div style="border: 1px solid #00A65A;color: #000000;float: left;padding-top: 10px;width:100%;">
										<div class="col-xs-12 col-sm-12 col-md-6">
											<b class="est-dato count"><?php echo $evFormacion['orientacion'];?></b>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6">
											<!--<i class="fas fa-user-graduate icono" style="color: #008D4D;font-size: 50pt;"></i>-->
											<img src="img/orientacion_expertos.png" alt="orientacion expertos" style="width: 60px;height: 65px;">
										</div>
										<div class="col-xs-12">
											<p class="dato" style="color:#000000;font-size: 15pt;">Orientación con expertos</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--<div class="col-xs-12" style="text-align: center;">
							<p>ACCIONES DE FORMACIÓN</p>
						</div>
						<div class="col-xs-12">
							<?php
								/*if (!empty($accionesFormacion)) {
									foreach ($accionesFormacion as $key => $value) {
							?>
										<div class="row" style="border:1px solid #DDDDDD;padding: 10px;border-radius: 5px;margin: 10px;background: #FFFFFF;">
											<div class="col-xs-12">
												<p><b>NOMBRE DE LA ACCIÓN DE FORMACIÓN:</b></p>
												<p><?php echo $value['nombre'];?></p>
											</div>
											<div class="col-xs-12">
												<div class="row">
													<div class="col-xs-4">
														<p><b>NÚMERO DE BENEFICIARIOS</b></p>
														<p><?php echo $value['beneficiarios_empresa'];?></p>
													</div>
													<div class="col-xs-4">
														<p><b>VALOR TOTAL</b></p>
														<p><?php echo number_format($value['valor_total'])." COP";?></p>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-4">
														<p><b>GRUPOS</b></p>
														<p><?php echo $value['numero_grupos'];?></p>
													</div>
													<div class="col-xs-4">
														<p><b>EVENTO DE FORMACIÓN</b></p>
														<p><?php echo $value['evento_formacion'];?></p>
													</div>
													<div class="col-xs-4">
														<p><b>MODALIDAD</b></p>
														<p><?php echo $value['modalidad'];?></p>
													</div>
												</div>
											</div>
										</div>
							<?php
									}
								}*/
							?>
						</div>-->
					</div>

				</div>
				<div class="separador" style="border: 1px dotted #000000;margin-bottom: 100px;margin-top: 100px;"></div>



	<?php
			}
		}
	?>

</body>

</html>
<?php
