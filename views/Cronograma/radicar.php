<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Radicación';

$this->registerJsFile(
  '@web/js/radicacion_cronograma.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);


?>
<style>
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #ccc;
  width: 100%;
}
</style>

<div class="container container-form">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>Radicar cronograma</h1>
        <p>
          En esta sección podrá realizar la radicación oficial del cronograma, tenga en cuenta que una vez radicado no podrá realizar modificaciones del mismo. Siga las siguientes instrucciones:
        </p>
        <ol>
          <li>Clic en el botón "Validar Cronograma" para realizar la validación del cronograma antes de habilitar la radicación.</li>
          <li>En caso de que la validación sea exitosa deberá observar el botón "Radicar Cronograma".</li>
          <li>Si la validación es erronea podrá observar el listado de grupos que estan pendientes por registrar cronogramas.</li>
        </ol>
        <button class="btn btn-primary pull-right" id="btn_validar" onclick="validar_cronograma(<?php echo $id_proyecto;?>)">Validar Cronograma</button>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="listadoGrupos" style="margin-top: 15px;"></div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="contenedorBtnRadicacion" style="margin-top: 15px;"></div>
  </div>
</div>