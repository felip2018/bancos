<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Solicitud Modificacion';

$this->registerJsFile(
  '@web/js/solicitud_modificacion.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);


?>
<style>
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #ccc;
  width: 100%;
}
</style>

<div class="container container-form">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>Solicitud de modificación</h1>
        <p>
          En desarrollo...
        </p>
    </div>
  </div>
</div>