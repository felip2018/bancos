<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Cronograma';

$this->registerJsFile(
  '@web/js/cronograma_busqueda.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
  '@web/js/cronograma_agregar.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>
<style>
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #ccc;
  width: 100%;
}
</style>

<div class="container container-form">
  
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>Buscar cronograma</h1>
        <p>
          Realiza la consulta de cronogramas registrados
        </p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="alerta-registro-cronograma">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
          <label>Acción de formación</label>
          <select class="form-control" name="Acción de formación" id="listadoAccionesFormacion" onchange="buscarGruposAF(this.value)">
            <option value="">-Seleccione</option>
            <?php
              if (isset($accionesFormacion) && !empty($accionesFormacion)) {
                $i = 1;
                foreach ($accionesFormacion as $key => $value) {
                  $accion = $value['id_accion_formacion']."_".$i."-".$value['nombre'];
            ?> 
                  <option value="<?php echo $accion;?>"><?php echo $i."- ".$value['nombre'];?></option>
            <?php
                  $i++;
                }
              }
            ?>
          </select>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
          <label>Grupo</label>
          <select class="form-control" disabled="true" name="Grupo" id="listadoGrupos" onchange="buscarUnidadesTematicas(this.value)">
            <option value="">-Seleccione</option>
          </select>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
          <label>No. Unidad temática</label>
          <select class="form-control" disabled="true" name="No. Unidad Temática" id="listadoUnidadesTematicas">
            <option value="">-Seleccione</option>
          </select>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
          <label>Buscar</label>
          <button class="btn btn-primary" onclick="buscarCronograma()"><span class="fa fa-search"></span></button>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
     <hr> 
    </div>
    
    <!--DATOS DEL CRONOGRAMA GLOBAL-->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <input type="hidden" id="id_cronograma">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Número de beneficiarios por grupo</label>
          <input type="numer" min="0" class="form-control" name="Número de beneficiarios por grupo" id="num_beneficiarios_grupo">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Duración total evento formación</label>
          <input type="numer" min="0" class="form-control" name="Duración total evento de formación" id="duracion_total_eformacion">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Total horas de la unidad temática</label>
          <input type="number" min="0" class="form-control" name="Total horas de la unidad temática" id="total_hrs_unidad_tematica">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Número de la unidad temática</label>
          <input type="number" class="form-control" name="Número de la unidad temática" id="num_unidad_tematica">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <label>Nombre de la unidad temática</label>
          <textarea name="Nombre de la unidad temática" id="nombre_unidad_tematica" class="form-control" rows="3"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Fecha de inicio de la unidad temática</label>
          <input type="date" name="Fecha de inicio de la unidad temática" class="form-control" id="fecha_inicio_unidad_tematica">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Fecha de finalización de la unidad temática</label>
          <input type="date" name="Fecha de finalización de la unidad temática" class="form-control" id="fecha_finalizacion_unidad_tematica">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Metodología presencial</label>
          <select class="form-control" name="Metodología presencial" id="metodologia_presencial">
            <option value="">-Seleccione</option>
          </select>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Metodología virtual</label>
          <select class="form-control" name="Metodología virtual" id="metodologia_virtual">
            <option value="">-Seleccione</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Número de personas</label>
          <input type="number" min="0" class="form-control" name="Numero de personas" id="num_personas">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Sesiones por persona</label>
          <input type="number" min="0" class="form-control" name="Sesiones por persona" id="sesiones_por_persona">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Número total de sesiones</label>
          <input type="number" min="0" class="form-control" name="Número total de sesiones" id="num_sesiones">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Número total de actividades</label>
          <input type="number" min="0" class="form-control" name="Número total de actividades" id="num_actividades">
        </div>
      </div>
      <div class="row">
        <br>
        <button class="btn btn-primary pull-right" onclick="actualizarCronograma()">Actualizar</button>
      </div>
    </div>

    <!--LISTADO DE SESIONES O ACTIVIDADES REGISTRADAS DEL CRONOGRAMA SELECCIONADO-->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 10px;">
      <h3>LISTADO DE SESIONES REGISTRADAS</h3>
      <div id="listado_sesiones">
      </div>
      <div id="div-btn-sesiones"></div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 10px;">
      <h3>LISTADO DE ACTIVIDADES REGISTRADAS</h3>
      <div id="listado_actividades">
      </div>
      <div id="div-btn-actividades"></div>
    </div>

  </div>
</div>