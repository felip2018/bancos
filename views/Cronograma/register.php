<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Cronograma';

$this->registerJsFile(
  '@web/js/cronograma.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);


?>
<style>
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #ccc;
  width: 100%;
}
.fa-asterisk{
  color:red;
}
</style>

<div class="container container-form">
  
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <a href="index.php?r=cronograma/exportar-cronograma-global" class="btn btn-primary pull-right"><span class="fa fa-file-excel"></span> Exportar cronograma</a>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>Cronograma</h1>
        <p>
          Configurar el cronograma detallado para cada grupo de Acción de Formación.
        </p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="alerta-registro-cronograma">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
          <label>Acción de formación</label>
          <select class="form-control" name="Acción de formación" id="listadoAccionesFormacion" onchange="buscarGruposAF(this.value)">
            <option value="">-Seleccione</option>
            <?php
              if (isset($accionesFormacion) && !empty($accionesFormacion)) {
                $i = 1;
                foreach ($accionesFormacion as $key => $value) {
                  $accion = $value['id_accion_formacion']."_".$i."-".$value['nombre'];
            ?>
                  <option value="<?php echo $accion;?>"><?php echo $i."- ".$value['nombre'];?></option>
            <?php
                  $i++;
                }
              }
            ?>
          </select>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
          <label>Grupo</label>
          <select class="form-control" disabled="true" name="Grupo" id="listadoGrupos">
            <option value="">-Seleccione</option>
          </select>
        </div>
      </div>
    </div>
    <div class="col-xs-12">
      <hr>
    </div>
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <label><i class="fa fa-asterisk"></i> Número de beneficiarios por grupo</label>
          <input type="number" min="0" class="form-control" name="Número de beneficiarios por grupo" id="num_beneficiarios_grupo" oninput="validarNumero(this)">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <label><i class="fa fa-asterisk"></i> Evento de formación</label>
          <input type="text" class="form-control" name="Evento de formación" id="evento_formacion" readonly="true">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <label><i class="fa fa-asterisk"></i> Duración total evento formación (hrs)</label>
          <input type="number" min="0" class="form-control" name="Duración total evento de formación" id="duracion_total_eformacion" oninput="validarNumero(this)">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <label><i class="fa fa-asterisk"></i> Modalidad</label>
          <input type="text" class="form-control" name="Modalidad" id="modalidad" readonly="true">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <label><i class="fa fa-asterisk"></i> Total horas de la unidad temática</label>
          <input type="number" min="0" class="form-control" name="Total horas de la unidad temática" id="total_hrs_unidad_tematica" oninput="validarNumero(this)">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <label><i class="fa fa-asterisk"></i> Número de la unidad temática</label>
          <input type="number" min="0" class="form-control" name="Número de la unidad temática" id="num_unidad_tematica" oninput="validarNumero(this)">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <label><i class="fa fa-asterisk"></i> Nombre de la unidad temática</label>
          <textarea name="Nombre de la unidad temática" id="nombre_unidad_tematica" class="form-control" rows="3"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label><i class="fa fa-asterisk"></i> Fecha de inicio de la unidad temática</label>
          <input type="date" name="Fecha de inicio de la unidad temática" value="" placeholder="" class="form-control" id="fecha_inicio_unidad_tematica">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label><i class="fa fa-asterisk"></i> Fecha de finalización de la unidad temática</label>
          <input type="date" name="Fecha de finalización de la unidad temática" value="" placeholder="" class="form-control" id="fecha_finalizacion_unidad_tematica">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label><i class="fa fa-asterisk"></i> Metodología presencial</label>
          <select class="form-control" name="Metodología presencial" id="metodologia_presencial" onchange="validarMetodologia(this.value)">
            <option value="">-Seleccione</option>
          </select>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label><i class="fa fa-asterisk"></i> Metodología virtual</label>
          <select class="form-control" name="Metodología virtual" id="metodologia_virtual">
            <option value="">-Seleccione</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Número de personas</label>
          <input type="number" min="0" class="form-control" name="Numero de personas" id="num_personas" readonly="true" oninput="validarNumero(this)">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Sesiones por persona</label>
          <input type="number" min="0" class="form-control" name="Sesiones por persona" id="sesiones_por_persona" readonly="true" onblur="calcularNumeroSesiones()" oninput="validarNumero(this)">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label><i class="fa fa-asterisk"></i> Número total de sesiones</label>
          <input type="number" min="0" class="form-control" name="Número total de sesiones" id="num_sesiones" onblur="validarNumeroSesiones()" oninput="validarNumero(this)">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label><i class="fa fa-asterisk"></i> Número total de actividades</label>
          <input type="number" min="0" class="form-control" name="Número total de actividades" id="num_actividades" onblur="validarNumeroActividades()" oninput="validarNumero(this)">
        </div>
      </div>
      
    </div>
    <div class="col-xs-12">
      <h2>Configurar sesiones y/o actividades</h2>
      <br>
      <div class="alert-configuracion-sesiones"></div>
    </div>
    <div class="col-xs-12" id="formulario_sesion">
      <div id="formulario-sesion-presencial" style="display: none;">
        <h3>Registrar sesiones presenciales</h3>
        <div class="row" id="alerta-sesion-presencial">
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Nombre de la sesión</label>
            <select class="form-control" name="Nombre de la sesión" id="nombre_sesion">
              <option value="">-Seleccione</option>
            </select>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Número de sesión</label>
            <input type="number" min="0" class="form-control" name="Número de sesión" id="num_sesion" oninput="validarNumero(this)">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <label><i class="fa fa-asterisk"></i> Fecha de inicio</label>
            <input type="date" class="form-control" name="Fecha de inicio" id="fecha_inicio">
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <label><i class="fa fa-asterisk"></i> Hora de inicio (formato 24hrs)</label>
            <input type="time" class="form-control" name="Hora de inicio" id="hora_inicial">
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <label><i class="fa fa-asterisk"></i> Hora de finalización (formato 24hrs)</label>
            <input type="time" class="form-control" name="Hora de finalización" id="hora_final">
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <label><i class="fa fa-asterisk"></i> Número de horas totales</label>
            <input type="number" min="0" class="form-control" name="Número de horas totales" id="num_hrs_totales" oninput="validarNumero(this)">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Departamento</label>
            <select class="form-control" name="Departamento" id="id_departamento" onchange="buscarCiudades(this.value)">
              <option value="">-Seleccione</option>
              <?php
                if (isset($listaDptos)) {
                  foreach ($listaDptos as $key => $value) {
              ?>
                    <option value="<?php echo $value['id_dpto'];?>"><?php echo $value['nombre'];?></option>
              <?php
                  }
                }
              ?>
            </select>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Ciudad</label>
            <select class="form-control" name="Ciudad" id="id_ciudad"><option value="">-Seleccione</option></select>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label><i class="fa fa-asterisk"></i> Nombre de la sede</label>
            <input type="text" class="form-control" name="Nombre de la sede" id="nombre_sede">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Dirección de la sede</label>
            <input type="text" class="form-control" name="Dirección de la sede" id="direccion_sede">
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Aula / Salón</label>
            <input type="text" class="form-control" name="Aula / Salón" id="aula_salon">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label><i class="fa fa-asterisk"></i> Nombre del capacitador</label>
            <input type="text" class="form-control" name="Nombre de capacitador" id="nombre_capacitador_presencial">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Tipo de documento del capacitador</label>
            <select class="form-control" name="Tipo de documento del capacitador" id="tipo_doc_capacitador_presencial">
              <option value="">-Seleccione</option>
              <option value="CC">CC (Cédula de ciudadanía)</option>
              <option value="CEX">CEX (Cédula de extranjeria)</option>
              <option value="PAS">PAS (Pasaporte)</option>
            </select>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Número de identificación del capacitador</label>
            <input type="number" min="0" class="form-control" name="Número de identificación del capacitador" id="num_doc_capacitador_presencial" oninput="validarNumero(this)">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label>Nombre del capacitador suplente 1</label>
            <input type="text" class="form-control" name="Nombre de capacitador" id="nombre_capacitador_presencial_s1">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label>Tipo de documento del capacitador suplente 1</label>
            <select class="form-control" name="Tipo de documento del capacitador" id="tipo_doc_capacitador_presencial_s1">
              <option value="">-Seleccione</option>
              <option value="CC">CC (Cédula de ciudadanía)</option>
              <option value="CEX">CEX (Cédula de extranjeria)</option>
              <option value="PAS">PAS (Pasaporte)</option>
            </select>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label>Número de identificación del capacitador suplente 1</label>
            <input type="number" min="0" class="form-control" name="Número de identificación del capacitador" id="num_doc_capacitador_presencial_s1" oninput="validarNumero(this)">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label>Nombre del capacitador suplente 2</label>
            <input type="text" class="form-control" name="Nombre de capacitador" id="nombre_capacitador_presencial_s2">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label>Tipo de documento del capacitador suplente 2</label>
            <select class="form-control" name="Tipo de documento del capacitador" id="tipo_doc_capacitador_presencial_s2">
              <option value="">-Seleccione</option>
              <option value="CC">CC (Cédula de ciudadanía)</option>
              <option value="CEX">CEX (Cédula de extranjeria)</option>
              <option value="PAS">PAS (Pasaporte)</option>
            </select>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label>Número de identificación del capacitador suplente 2</label>
            <input type="number" min="0" class="form-control" name="Número de identificación del capacitador" id="num_doc_capacitador_presencial_s2" oninput="validarNumero(this)">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Perfil del capacitador</label>
            <select class="form-control" name="Perfil del capacitador" id="perfil_capacitador_presencial">
              <option value="">-Seleccione</option>
              <option>TECNICO Y/O TECNOLOGO TITULADO, CON EXPERIENCIA RELACIONADA COMPROBADA MINIMO DE CINCO (5) AÑOS.</option>

              <option>PROFESIONAL TITULADO CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>
              <option>PROFESIONAL CON TITULO DE ESPECIALIZACION Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>
              <option>PROFESIONAL CON TITULO DE MAESTRIA Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>
              <option>PROFESIONAL CON TITULO DE DOCTORADO Y CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>

              <option>PROFESIONAL TITULADO CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>
              <option>PROFESIONAL CON TITULO DE ESPECIALIZACION Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>
              <option>PROFESIONAL CON TITULO DE MAESTRIA Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>
              <option>PROFESIONAL CON TITULO DE DOCTORADO, CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS, AFIN CON EL OBJETO DE LA FORMACION.</option>
            </select>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Tipo de capacitador</label>
            <select class="form-control" name="Tipo de capacitador" id="tipo_capacitador_presencial">
              <option value="">-Seleccione</option>
              <option>CAPACITADOR NACIONAL</option>
              <option>CAPACITADOR INTERNACIONAL</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <br>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <br>
            <button type="button" class="btn pull-right" onclick="agregarSesionPresencial()"><span class="fa fa-plus"></span> Agregar sesión presencial</button>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="listado-sesiones-presenciales">
              <div id="unidad-tematica-presencial"></div>
              <div id="listado-sesiones" style="border:1px solid #CCCCCC;padding: 5px;border-radius: 5px;overflow-y: scroll;height: 500px;margin-top: 10px;"></div>
            </div>
          </div>
        </div>
      </div>
      <div id="formulario-sesion-virtual" style="display: none;">
        <h3>Registrar sesiones virtuales</h3>
        <div class="row" id="alerta-sesion-virtual">
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label><i class="fa fa-asterisk"></i> Nombre de la actividad</label>
            <input type="text" class="form-control" name="Nombre de la actividad" id="nombre_actividad">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Fecha de inicio de la actividad</label>
            <input type="date" class="form-control" name="Fecha de inicio de la actividad" id="fecha_inicio_actividad">
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Fecha de finalización de la actividad</label>
            <input type="date" class="form-control" name="Fecha de finalización de la actividad" id="fecha_fin_actividad">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label><i class="fa fa-asterisk"></i> Proveedor de formación virtual</label>
            <input type="text" class="form-control" name="Proveedor de formación virtual" id="proveedor_formacion">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label><i class="fa fa-asterisk"></i> URL de plataforma virtual</label>
            <input type="text" class="form-control" name="URL de plataforma virtual" id="url_plataforma">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Usuario SENA</label>
            <input type="text" class="form-control" name="Usuario SENA" id="usuario_sena">
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Clave SENA</label>
            <input type="text" class="form-control" name="Clave SENA" id="clave_sena">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label><i class="fa fa-asterisk"></i> Nombre del capacitador</label>
            <input type="text" class="form-control" name="Nombre del capacitador" id="nombre_capacitador_virtual">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Tipo de documento del capacitador</label>
            <select class="form-control" name="Tipo de documento del capacitador" id="tipo_doc_capacitador_virtual">
              <option value="">-Seleccione</option>
              <option value="CC">CC (Cédula de ciudadanía)</option>
              <option value="CEX">CEX (Cédula de extranjeria)</option>
              <option value="PAS">PAS (Pasaporte)</option>
            </select>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Número de identificación del capacitador</label>
            <input type="number" min="0" class="form-control" name="Número de identificación del capacitador" id="num_doc_capacitador_virtual" oninput="validarNumero(this)">
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Perfil del capacitador</label>
            <select class="form-control" name="Perfil del capacitador" id="perfil_capacitador_virtual">
              <option value="">-Seleccione</option>
              <option>TECNICO Y/O TECNOLOGO TITULADO, CON EXPERIENCIA RELACIONADA COMPROBADA MINIMO DE CINCO (5) AÑOS.</option>

              <option>PROFESIONAL TITULADO CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>
              <option>PROFESIONAL CON TITULO DE ESPECIALIZACION Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>
              <option>PROFESIONAL CON TITULO DE MAESTRIA Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>
              <option>PROFESIONAL CON TITULO DE DOCTORADO Y CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE TRES (3) AÑOS.</option>

              <option>PROFESIONAL TITULADO CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>
              <option>PROFESIONAL CON TITULO DE ESPECIALIZACION Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>
              <option>PROFESIONAL CON TITULO DE MAESTRIA Y EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS.</option>
              <option>PROFESIONAL CON TITULO DE DOCTORADO, CON EXPERIENCIA RELACIONADA COMPROBADA, MINIMO DE CINCO (5) AÑOS, AFIN CON EL OBJETO DE LA FORMACION.</option>
            </select>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label><i class="fa fa-asterisk"></i> Tipo de capacitador</label>
            <select class="form-control" name="Tipo de capacitador" id="tipo_capacitador_virtual">
              <option value="">-Seleccione</option>
              <option>CAPACITADOR NACIONAL</option>
              <option>CAPACITADOR INTERNACIONAL</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <br>
            <button type="button" class="btn pull-right" onclick="agregarSesionVirtual()"><span class="fa fa-plus"></span> Agregar actividad virtual</button>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="listado-sesiones-virtuales">
              <div id="unidad-tematica-virtual"></div>
              <div id="listado-actividades" style="border:1px solid #CCCCCC;padding: 5px;border-radius: 5px;overflow-y: scroll;height: 500px;margin-top: 10px;"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <br>
        <button type="button" class="btn btn-primary pull-right" onclick="registrarCronograma()">Registrar cronograma</button>
      </div>
    </div>
  </div>

</div>
