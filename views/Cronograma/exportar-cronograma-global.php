<?php

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document properties
$spreadsheet->getProperties()->setCreator('Felipe Garzon')
    ->setLastModifiedBy('Felipe Garzon')
    ->setTitle('Office 2007 XLSX Test Document')
    ->setSubject('Office 2007 XLSX Test Document')
    ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
    ->setKeywords('office 2007 openxml php')
    ->setCategory('Test result file');

// Add some data
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', 'ACCION DE FORMACION')
    ->setCellValue('B1', 'GRUPO')
    ->setCellValue('C1', 'BENEFICIARIOS POR GRUPO')
    ->setCellValue('D1', 'DURACION TOTAL EVENTO FORMACION')
    ->setCellValue('E1', 'METODOLOGIA PRESENCIAL')
    ->setCellValue('F1', 'METODOLOGIA VIRTUAL')
    ->setCellValue('G1', 'NUMERO UNIDAD TEMATICA')
    ->setCellValue('H1', 'NOMBRE UNIDAD TEMATICA')
    ->setCellValue('I1', 'FECHA INICIO UNIDAD TEMATICA')
    ->setCellValue('J1', 'FECHA FINALIZACION UNIDAD TEMATICA')
    ->setCellValue('K1', 'TOTAL HRS UNIDAD TEMATICA')
    ->setCellValue('L1', 'NUMERO SESIONES')
    ->setCellValue('M1', 'NUMERO ACTIVIDADES')
    ->setCellValue('N1', 'NOMBRE DE SESION')
    ->setCellValue('O1', 'NUMERO DE SESION')
    ->setCellValue('P1', 'FECHA INICIO')
    ->setCellValue('Q1', 'HORA INICIO')
    ->setCellValue('R1', 'HORA FIN')
    ->setCellValue('S1', 'NUMERO HRS TOTALES')
    ->setCellValue('T1', 'CIUDAD')
    ->setCellValue('U1', 'SEDE')
    ->setCellValue('V1', 'DIRECCION')
    ->setCellValue('W1', 'AULA/SALON')
    ->setCellValue('X1', 'CAPACITADOR')
    ->setCellValue('Y1', 'TIPO DOCUMENTO')
	->setCellValue('Z1', 'NUMERO DOCUMENTO')
	->setCellValue('AA1', 'CAPACITADOR SUPLENTE 1')
	->setCellValue('AB1', 'TIPO DOCUMENTO')
	->setCellValue('AB1', 'NUMERO DOCUMENTO')
	->setCellValue('AC1', 'CAPACITADOR SUPLENTE 2')
	->setCellValue('AD1', 'TIPO DOCUMENTO')
	->setCellValue('AE1', 'NUMERO DOCUMENTO')
	->setCellValue('AF1', 'PERFIL CAPACITADOR')
	->setCellValue('AG1', 'TIPO CAPACITADOR')
	->setCellValue('AH1', 'ACTIVIDAD')
	->setCellValue('AI1', 'FECHA INICIO ACTIVIDAD')
	->setCellValue('AJ1', 'FECHA FIN ACTIVIDAD')
	->setCellValue('AK1', 'PROVEEDOR FORMACION')
	->setCellValue('AL1', 'URL PLATAFORMA')
	->setCellValue('AM1', 'USUARIO SENA')
	->setCellValue('AN1', 'CLAVE SENA')
	->setCellValue('AO1', 'CAPACITADOR')
	->setCellValue('AP1', 'TIPO DOCUMENTO')
	->setCellValue('AQ1', 'NUMERO DOCUMENTO')
	->setCellValue('AR1', 'PERFIL CAPACITADOR VIRTUAL')
	->setCellValue('AS1', 'TIPO CAPACITADOR VIRTUAL');

if (isset($cronograma)) {
	$i = 1;
	foreach ($cronograma as $key => $value) {
		$spreadsheet->setActiveSheetIndex(0)
    		->setCellValue('A'.($i+1), $value['accion_formacion'])
		    ->setCellValue('B'.($i+1), $value['grupo'])
		    ->setCellValue('C'.($i+1), $value['num_beneficiarios_grupo'])
		    ->setCellValue('D'.($i+1), $value['duracion_total_eformacion'])
		    ->setCellValue('E'.($i+1), $value['metodologia_presencial'])
		    ->setCellValue('F'.($i+1), $value['metodologia_virtual'])
		    ->setCellValue('G'.($i+1), $value['num_unidad_tematica'])
		    ->setCellValue('H'.($i+1), $value['nombre_unidad_tematica'])
		    ->setCellValue('I'.($i+1), $value['fecha_inicio'])
		    ->setCellValue('J'.($i+1), $value['fecha_finalizacion'])
		    ->setCellValue('K'.($i+1), $value['total_hrs_unidad_tematica'])
		    ->setCellValue('L'.($i+1), $value['num_sesiones'])
		    ->setCellValue('M'.($i+1), $value['num_actividades'])
		    ->setCellValue('N'.($i+1), $value['nombre_sesion'])
		    ->setCellValue('O'.($i+1), $value['num_sesion'])
		    ->setCellValue('P'.($i+1), $value['fecha_inicio_sesion'])
		    ->setCellValue('Q'.($i+1), $value['hora_inicial'])
		    ->setCellValue('R'.($i+1), $value['hora_final'])
		    ->setCellValue('S'.($i+1), $value['num_hrs_totales'])
		    ->setCellValue('T'.($i+1), $value['ciudad'])
		    ->setCellValue('U'.($i+1), $value['nombre_sede'])
		    ->setCellValue('V'.($i+1), $value['direccion_sede'])
		    ->setCellValue('W'.($i+1), $value['aula_salon'])
		    ->setCellValue('X'.($i+1), $value['capacitador'])
		    ->setCellValue('Y'.($i+1), $value['tipo_doc_capacitador_p'])
			->setCellValue('Z'.($i+1), $value['num_doc_capacitador_p'])
			->setCellValue('AA'.($i+1), $value['capacitador_suplente1'])
			->setCellValue('AB'.($i+1), $value['tipo_doc_capacitador_s1'])
			->setCellValue('AB'.($i+1), $value['num_doc_capacitador_s1'])
			->setCellValue('AC'.($i+1), $value['capacitador_suplente2'])
			->setCellValue('AD'.($i+1), $value['tipo_doc_capacitador_s2'])
			->setCellValue('AE'.($i+1), $value['num_doc_capacitador_s2'])
			->setCellValue('AF'.($i+1), $value['perfil_capacitador_p'])
			->setCellValue('AG'.($i+1), $value['tipo_capacitador_p'])
			->setCellValue('AH'.($i+1), $value['nombre_actividad'])
			->setCellValue('AI'.($i+1), $value['fecha_inicio_actividad'])
			->setCellValue('AJ'.($i+1), $value['fecha_fin_actividad'])
			->setCellValue('AK'.($i+1), $value['proveedor_formacion'])
			->setCellValue('AL'.($i+1), $value['url_plataforma'])
			->setCellValue('AM'.($i+1), $value['usuario_sena'])
			->setCellValue('AN'.($i+1), $value['clave_sena'])
			->setCellValue('AO'.($i+1), $value['capacitador_virtual'])
			->setCellValue('AP'.($i+1), $value['tipo_doc_capacitador_v'])
			->setCellValue('AQ'.($i+1), $value['num_doc_capacitador_v'])
			->setCellValue('AR'.($i+1), $value['perfil_capacitador_v'])
			->setCellValue('AS'.($i+1), $value['tipo_capacitador_v']);

		 $i++;		
	}
}


// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('Cronograma Global');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$proyecto.'_Cronograma.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;
