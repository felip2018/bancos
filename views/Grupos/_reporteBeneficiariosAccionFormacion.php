<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
</head>
<body>
	<div class="encabezado" style="text-align: center;">
		<p>DIRECCIÓN DE SISTEMA NACIONAL DE FORMACIÓN PARA EL TRABAJO</p>
		<p>PROGRAMA DE FORMACIÓN CONTINUA ESPECIALIZADA</p>
		<b><?php echo $convocatoria['nombre'];?></b>
	</div>
	<div style="text-align: left;padding-top: 50px;">
		<p><b><?php echo "No. Radicado ".$proyecto['n_radicado']." - ".$proyecto['titulo'];?></b></p>
		<p><?php echo "Acción de formación: ".$proyecto['accion_formacion'];?></p>
	</div>
	<div class="cuerpo">
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="width: 5%;">No.</th>
					<th style="width: 80%;">NOMBRE</th>
					<th style="width: 15%;">IDENTIFICACIÓN</th>
				</tr>
			</thead>
			<tbody>
				<?php
					if (!empty($beneficiarios)) {
						$i = 1;
						foreach ($beneficiarios as $key => $value) {
				?>
					<tr>
						<td><?php echo $i;?></td>
						<td><?php echo $value['nombre'];?></td>
						<td><?php echo $value['identificacion'];?></td>
					</tr>
				<?php
							$i++;
						}
					}else{
				?>
					<tr>
						<td>0</td>
						<td>VACIO</td>
						<td>VACIO</td>
					</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
	<div class="pie">
		
	</div>
</body>
</html>