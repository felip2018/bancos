<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

$this->title = 'SEP';


$this->registerJsFile(
  '@web/js/grupos.js?1.0.0',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>
<style>
.container-form{
  background: #FFFFFF;
  padding: 20px 30px 20px 30px;
  box-shadow: 0px 0px 10px #CCC;
}
</style>
<div class="container-fluid container-form">
  
  <div class="row">
    <div class="col-xs-12">
      <a class="btn btn-primary pull-right" href="index.php?r=grupos/index">
        <i class="fa fa-undo"></i> Volver
      </a>
    </div>
    <div class="col-xs-12">
      <h1><?php echo $info_accion['nombre'];?></h1>
      <button class="btn btn-primary pull-right" title="Ver listado de beneficiarios registrados" onclick="verListadoBeneficiariosAccionFormacion(<?php echo $info_accion['id_accion_formacion'];?>)">
        <i class="fa fa-file-pdf"></i> PDF - Beneficiarios Acción de Formación
      </button>
      <button class="btn btn-primary pull-right" style="margin-right: 10px;" title="Descargar listado de beneficiarios" onclick="descargarExcelBeneficiarios(<?php echo $info_accion['id_accion_formacion'];?>)">
        <i class="fa fa-file-excel"></i> Descargar Listado de Beneficiarios
      </button>
    </div>
    <div class="col-xs-12" id="alerta1">
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-xs-12">
      <button class="btn btn-primary pull-right" style="margin-right: 10px;" title="Ver listado de beneficiarios registrados" onclick="verListadoBeneficiariosGrupo()">
        <i class="fa fa-file-pdf"></i> Ver PDF - Beneficiarios Grupo
      </button>
      <button class="btn btn-primary pull-right" style="margin-right: 10px;" onclick="verFormularioRegistro()">
        <i class="fa fa-plus"></i> Agregar beneficiario
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-3">
      <?php
        if (isset($grupos)) {
          foreach ($grupos as $key => $value) {
      ?>
            <button class="btn btn-primary btn-block btn-grupo" id="btn_grupo_<?php echo $value['id_grupo'];?>" onclick="buscarBeneficiarios('<?php echo $value['id_grupo'];?>')"><?php echo $value['nombre'];?></button>
      <?php
          }
        }
      ?>
    </div>
    <div class="col-xs-9">
      <input type="hidden" id="campo_id_grupo">
      <p>Lista de beneficiarios registrados en el grupo.</p>
      <div id="contenedor">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>No.</th>
              <th>Nombre</th>
              <th>Identificación</th>
              <th>Estado</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody id="listadoBeneficiarios">
            <tr>
              <td>0</td>
              <td>Vacío</td>
              <td>Vacío</td>
              <td>Vacío</td>
              <td>Vacío</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
