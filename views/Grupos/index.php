<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

$this->title = 'SEP';


$this->registerJsFile(
  '@web/js/grupos.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>
<style>
.container-form{
  background: #FFFFFF;
  padding: 20px 30px 20px 30px;
  box-shadow: 0px 0px 10px #CCC;
}
.fa-asterisk{
  color:red;
}
.alerta{
  width: 100%;
  height: 100%;
  position: fixed;
  z-index: 100;
  background: rgba(0,0,0,0.5);
}
.alerta-mensaje{
  margin: auto;
  width: 30%;
  margin-top: 200px;
  background: #FFFFFF;
  padding: 10px;
}
.alert{
  border-radius: 0px !important;
}
.accion-container {
  border: 1px solid red;
  margin: 10px;
  padding: 10px;
  border: 1px solid #CCC;
  border-radius: 5px;
  box-shadow: 0px 0px 5px #CCCCCC;
}
</style>
<div class="container-fluid container-form">
  <?php
      $form = ActiveForm::begin(['id' => 'register-form',]);
  ?>
  <div class="row">
    <div class="col-xs-12">
      <!--<div class="alert alert-info">-->
        <h1>Grupos por Acciones de Formación</h1>
        <p>
          En esta sección podrá realizar la creación de grupos definidos para cada una de las acciones de formación registradas en el proyecto de formación.
        </p>
      <!--</div>-->
    </div>
    <div class="col-xs-12">
      <!--<button type="button" class="btn btn-primary pull-right" onclick="crearTodosLosGruposAF('<?php //echo $data["id_proyecto"];?>')" title="Generar grupos para todas las acciones de formación">Crear grupos</button>-->
    </div>
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12" id="listaAccionesFormacion">
        <?php
          if (!empty($accionesFormacion)) {
            $i = 1;
            foreach ($accionesFormacion as $key => $value) {

              $beneficiarios_registrados = $model->beneficiariosRegistrados($value['id_accion_formacion']);
              $grupos_registrados = $model->gruposCount($value['id_accion_formacion']);

              $id_accion_formacion = $value['id_accion_formacion'];
              $nombre = $value['nombre'];
        ?>
              
                <div class="accion-container" id="action_<?php echo $id_accion_formacion;?>">
                  <h3 id="title_accion_<?php echo $i;?>"><?php echo $value['nombre'];?></h3>
                  <table class="table">
                    <tr>
                      <td>Beneficiarios Empresa</td>
                      <td><?php echo $value['beneficiarios_empresa'];?></td>
                    </tr>
                    <tr>
                      <td>Beneficiarios SENA</td>
                      <td><?php echo $value['beneficiarios_sena'];?></td>
                    </tr>
                    <tr>
                      <td>Beneficiarios Totales</td>
                      <td><?php echo $value['total_beneficiarios'];?></td>
                    </tr>
                    <tr>
                      <td>Beneficiarios Registrados</td>
                      <td><?php echo $beneficiarios_registrados;?></td>
                    </tr>
                    <tr>
                      <td>Grupos</td>
                      <td class="datoGrupos"><?php echo $grupos_registrados." de ".$value['numero_grupos'];?></td>
                    </tr>
                  </table>
                  <a type="button" class="btn btn-info" href="index.php?r=grupos/configuracion&id_accion_formacion=<?php echo $id_accion_formacion;?>"><i class="fa fa-cog"></i> Configurar grupos</a>
                  <a type="button" class="btn btn-info" href="index.php?r=certificacion/certificacion&id_accion_formacion=<?php echo $id_accion_formacion;?>"><i class="fa fa-file"></i> Certificación de beneficiarios</a>
                  <button type="button" class="btn btn-primary pull-right" onclick='crearGruposAccionFormacion(<?php echo $data["id_proyecto"];?>,<?php echo $id_accion_formacion;?>,<?php echo $value['numero_grupos'];?>,"<?php echo $i;?>")'>
                    <i class="fa fa-plus"></i> Crear grupos
                  </button>
                </div>  
              
        <?php
              $i++;
            }
          }
        ?>
        </div>
      </div>
    </div>

  </div>


  <?php ActiveForm::end(); ?>
</div>
