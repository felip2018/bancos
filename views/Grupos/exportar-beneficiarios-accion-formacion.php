<?php

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document properties
$spreadsheet->getProperties()->setCreator('Felipe Garzon')
    ->setLastModifiedBy('Felipe Garzon')
    ->setTitle('Office 2007 XLSX Test Document')
    ->setSubject('Office 2007 XLSX Test Document')
    ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
    ->setKeywords('office 2007 openxml php')
    ->setCategory('Test result file');

// Add some data
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', 'NUMERO')
    ->setCellValue('B1', 'ACCION DE FORMACION')
    ->setCellValue('C1', 'GRUPO')
    ->setCellValue('D1', 'NOMBRE')
    ->setCellValue('E1', 'TIPO DE DOCUMENTO')
    ->setCellValue('F1', 'NUMERO DE DOCUMENTO');

if (isset($beneficiarios)) {
    $i = 1;
    foreach ($beneficiarios as $key => $value) {
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A'.($i+1), $i)
            ->setCellValue('B'.($i+1), $value['accion_formacion'])
            ->setCellValue('C'.($i+1), $value['grupo'])
            ->setCellValue('D'.($i+1), $value['nombre'])
            ->setCellValue('E'.($i+1), $value['tipo_doc'])
            ->setCellValue('F'.($i+1), $value['num_doc']);

         $i++;
    }
}


// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('Beneficiarios');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$proyecto['titulo'].'_Beneficiarios.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;
