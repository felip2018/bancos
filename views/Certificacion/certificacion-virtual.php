<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

$this->title = 'SEP';


$this->registerJsFile(
  '@web/js/certificacion-virtual.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

$accion = json_decode($accionFormacion);
$total_hrs = ($accion->accion->virtual_teorica + $accion->accion->virtual_practica);
//print_r($accion);
?>
<style>
.container-form{
  background: #FFFFFF;
  padding: 20px 30px 20px 30px;
  box-shadow: 0px 0px 10px #CCC;
}
.fa-asterisk{
  color:red;
}
.alerta{
  width: 100%;
  height: 100%;
  position: fixed;
  z-index: 100;
  background: rgba(0,0,0,0.5);
}
.alerta-mensaje{
  margin: auto;
  width: 30%;
  margin-top: 200px;
  background: #FFFFFF;
  padding: 10px;
}
.alert{
  border-radius: 0px !important;
}
.accion-container {
  border: 1px solid red;
  margin: 10px;
  padding: 10px;
  border: 1px solid #CCC;
  border-radius: 5px;
  box-shadow: 0px 0px 5px #CCCCCC;
}
</style>
<div class="container-fluid container-form">
  <div class="row">
    <div class="col-xs-12">
      <a class="btn btn-primary pull-right" href="index.php?r=grupos/index">
        <i class="fa fa-undo"></i> Volver
      </a>
    </div>
    <div class="col-xs-12">
      <h1>Certificación de Beneficiarios</h1>
      <p>
        En esta sección podrá registrar las horas de cumplimiento y asistencia del beneficiario para realizar la certificación del mismo.
      </p>
    </div>
    <div class="col-xs-12">
      <h3><?php echo $accion->accion->nombre;?></h3>
      <table class="table">
        <thead>
          <tr>
            <th>Modalidad</th>
            <th>Hrs Virtual Teórica</th>
            <th>Hrs Virtual Práctica</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><?php echo $accion->accion->modalidad;?></td>
            <td><?php echo number_format($accion->accion->virtual_teorica);?></td>
            <td><?php echo number_format($accion->accion->virtual_practica);?></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <table class="table table-striped">
            <caption style="text-align: center;">BENEFICIARIOS REGISTRADOS</caption>
            <thead>
              <tr>
                <th>No.</th>
                <th>Nombre</th>
                <th>Identificación</th>
                <th>Hrs cumplimiento</th>
                <th>% cumplimiento</th>
                <th>Certifica</th>
              </tr>
            </thead>
            <tbody>
              <?php
                if (isset($beneficiarios)) {
                  $beneficiarios = json_decode($beneficiarios);
                  if ($beneficiarios->status == "success") {
                    $i = 1;
                    foreach ($beneficiarios->beneficiarios as $key => $value) {
                      $beneficiario   = $value->id_proyecto_beneficiario.'_'.$value->id_accion_formacion.'_'.$value->id_grupo;
                        
                      if ($value->certifica == null) {
                        $hrs_cumplimiento = $total_hrs;
                        $por_cumplimiento = 100;

                        $fondo = '#10ac84';
                        $certifica = 'SI';
                      }else{
                        $hrs_cumplimiento = $value->hrs_cumplimiento;
                        $por_cumplimiento = $value->por_cumplimiento;

                        $fondo = ($por_cumplimiento >= 85) ? '#10ac84' : '#ee5253';
                        $certifica = ($por_cumplimiento >= 85) ? 'SI' : 'NO';
                      }

                      
                ?>
                      <tr class="row_<?php echo $i;?>">
                        <td style="width: 5%;"><?php echo $i;?></td>
                        <td style="width: 45%;"><?php echo $value->nombre;?></td>
                        <td style="width: 10%;"><?php echo $value->tipo_doc.'-'.$value->num_doc;?></td>
                        <td style="width: 15%;">
                          <input type="hidden" class="beneficiario" id="beneficiario_<?php echo $i;?>" value="<?php echo $beneficiario;?>"><input type="number" min="0" max="<?php echo $total_hrs;?>" value="<?php echo $hrs_cumplimiento;?>" class="form-control hrs_cumplimiento" id="hrs_cumplimiento_<?php echo $i;?>" oninput="evaluarHorasVirtuales(<?php echo $total_hrs;?>,<?php echo $i;?>)" value="<?php echo $hrs_cumplimiento;?>">
                        </td>
                        <td style="width: 10%;">
                          <input type="number" min="0" max="100" value="<?php echo $por_cumplimiento;?>" class="form-control por_cumplimiento" id="por_cumplimiento_<?php echo $i;?>" readonly="true" value="<?php echo $por_cumplimiento;?>">
                        </td>
                        <td style="width: 10%">
                          <input style="background: <?php echo $fondo;?>;color:#FFFFFF;" class="form-control" type="text" id="certifica_<?php echo $i;?>" value="<?php echo $certifica;?>" readonly="true">
                        </td>
                      </tr>
                <?php
                      $i++;
                    }
                  }else{
                ?>
                    <tr>
                      <td>-</td>
                      <td>-</td>
                      <td>-</td>
                      <td>-</td>
                      <td>-</td>
                      <td>-</td>
                    </tr>
                <?php
                  }
                }else{
              ?>
                  <tr>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                  </tr>
              <?php
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-xs-12">
      <button  class="btn btn-primary pull-right" onclick="validarCertificacionVirtual()"><span class="fa fa-save"></span> Guardar</button>
    </div>
  </div>
</div>