<?php

use yii\helpers\Html;

$this->title = "Error";
?>
<section class="content">

    <div class="error-page">
        <h2 class="headline text-info"><i class="fa fa-warning text-yellow"></i></h2>

        <div class="error-content">
            <h3>Error de validación</h3>

            <p>
                Ha ocurrido un error, el director ingresado no pertenece al proyecto en sesión.
            </p>
        </div>
    </div>

</section>
