<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'SEP';

$this->registerJsFile(
  '@web/js/directores_edit.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

$tipo_documentos =[
  ''=>'-Seleccione',
  'CC'  => 'CC (Cédula de ciudadanía)',
  'CE' => 'CE (Cédula de extranjería)',
  'PAS' => 'PAS (Pasaporte)',
];


$dptos = [''=>'-Seleccione'];
if (isset($listaDptos)) {
  foreach ($listaDptos as $key => $value) {
     $dptos[$value['id_dpto']] = $value['nombre'];
  }
}



?>
<style>
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #ccc;
  width: 100%;
}
.documents{
  box-shadow: 0px 0px 5px #CCC;
  border-radius: 5px;
  margin-top: 10px;
}
.document{
  border: 1px solid #DDD;
  padding: 10px;
  border-radius: 5px;
  margin-bottom: 10px;
}
</style>

<div class="container container-form">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <a class="btn btn-primary pull-right" href="index.php?r=directores/index"><i class="fa fa-undo"></i> Volver</a>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <!--<div class="alert alert-info">-->
        <h1>Editar Director de Proyecto</h1>
        <p>
          Editar información registrada del Director de Proyecto
        </p>
      <!--</div>-->
    </div>
  </div>
  <div class="row">
    <input type="hidden" id="id_director">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="alerta-info">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Tipo de identificación</label>
          <input type="text" class="form-control" name="tipo_doc" id="tipo_doc" value="<?php echo $infoDirector['tipo_doc'];?>" readonly="true">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label>Número de identificación</label>
            <input type="number" min="0" class="form-control" name="num_doc" id="num_doc" value="<?php echo $infoDirector['num_doc'];?>" readonly="true">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <label>Nombre</label>
          <input type="text" class="form-control" name="Nombre" id="nombres" value="<?php echo $infoDirector['nombres'];?>">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Primer apellido</label>
          <input type="text" class="form-control" name="Primer apellido" id="primer_apellido" value="<?php echo $infoDirector['primer_apellido'];?>">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <label>Segundo apellido</label>
          <input type="text" class="form-control" name="Segundo apellido" id="segundo_apellido" value="<?php echo $infoDirector['segundo_apellido'];?>">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <label>Correo electrónico</label>
          <input type="text" class="form-control" name="Correo electrónico" id="correo" value="<?php echo $infoDirector['correo'];?>">
        </div>
      </div>
      <div class="row" style="padding-top: 10px;">
        <div class="col-xs-12">
          <button type="button" id="btnRegisterDirector" class="btn btn-primary pull-right" onclick="actualizar_info()"><span class="fa fa-save"></span> Actualizar director</button>
        </div>
      </div>
    </div>
  </div>

  <div class="row documents">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <h2>Documentos</h2>
      
      <div id="listadoDocumentosDirector">
        <div id="alerta-documentos"></div>
        <?php
          if (isset($documentos)) {
            $documentos = json_decode($documentos);
            if ($documentos->status == 'success') {
              $i=1;
              foreach ($documentos->documentos as $key => $value) {
                $id_director_documento = $value->id_director_documento;
                $ruta = "convocatoria_".$value->year."/bancos/directores/".$value->num_doc."/".$value->url;
        ?>
                <div class="document">
                  <form id="form_document_<?php echo $i;?>">
                    <input type="hidden" class="form-control" name="id_director_documento" value="<?php echo $id_director_documento;?>">
                    <input type="hidden" class="form-control" name="ruta" value="<?php echo $ruta;?>">
                    <table class="table">
                      <tr>
                        <td><b>Documento</b></td>
                        <td><?php echo $value->documento;?></td>
                      </tr>
                      <tr>
                        <td><b>Soporte</b></td>
                        <td><a href="<?php echo $ruta;?>" target="_blank"><?php echo $value->url;?></a></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td><input type="file" class="form-control" name="document" id="document_<?php echo $i;?>"></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td><button type="button" class="btn btn-secondary pull-right" onclick="actualizar_documento(<?php echo $i;?>)">Actualizar documento</button></td>
                      </tr>
                    </table>                    
                  </form>
                </div>
        <?php
                $i++;
              }
            }else{
        ?>
              <div>
                <h3>--</h3>
              </div>
        <?php
            }
          }
        ?>
      </div>

    </div>
  </div>

  <div class="row documents">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <h2>Experiencias</h2>
        <div id="listadoExperienciasDirector">
          <div id="alerta-experiencias"></div>
          <?php
            if (isset($experiencias)) {
              $experiencias = json_decode($experiencias);
              if ($experiencias->status == 'success') {
                $i=1;
                foreach ($experiencias->experiencias as $key => $value) {

                  $id_director_experiencia = $value->id_director_experiencia;
                  $ruta = "convocatoria_".$value->year."/bancos/directores/".$value->num_doc."/".$value->url;

          ?>
                  <div class="document">
                    <form id="form_experiencia_<?php echo $i;?>">
                      <input type="hidden" name="id_director_experiencia" value="<?php echo $id_director_experiencia;?>">
                      <input type="hidden" name="ruta" value="<?php echo $ruta;?>">
                      <table class="table">
                        <tr>
                          <td><b>Tipo</b></td>
                          <td><?php echo $value->tipo;?></td>
                        </tr>
                        <tr>
                          <td><b>Descripción</b></td>
                          <td><?php echo $value->descripcion;?></td>
                        </tr>
                        <tr>
                          <td><b>Soporte</b></td>
                          <td><a href="<?php echo $ruta;?>" target="_blank"><?php echo $value->url;?></a></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td><input type="file" class="form-control" name="document" id="documento_experiencia_<?php echo $i;?>"></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td><button type="button" class="btn btn-secondary pull-right" onclick="actualizar_experiencia(<?php echo $i;?>)">Actualizar documento</button></td>
                        </tr>
                      </table>                      
                    </form>
                  </div>
          <?php
                  $i++;
                }
              }else{
          ?>
                <div>
                  <h3>--</h3>
                </div>
          <?php
              }
            }
          ?>
        </div>
    </div>
  </div>
  
  <!--Registro de titulos-->
  <div class="row documents">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <h2>Titulos</h2>
        <div id="listadoTitulosDirector">
          <div id="alerta-titulos"></div>
          <?php
            if (isset($titulos)) {
              $titulos = json_decode($titulos);
              if ($titulos->status == 'success') {
                $i=1;
                foreach ($titulos->titulos as $key => $value) {
                  $id_director_titulo = $value->id_director_titulo;
                  $ruta = "convocatoria_".$value->year."/bancos/directores/".$value->num_doc."/".$value->url;
          ?>
                  <div class="document">
                    <form id="form_titulo_<?php echo $i;?>">
                      <input type="hidden" name="id_director_titulo" value="<?php echo $id_director_titulo;?>">
                      <input type="hidden" name="ruta" value="<?php echo $ruta;?>">
                      <table class="table">
                        <tr>
                          <td><b>Titulo</b></td>
                          <td><?php echo $value->tipo_titulo;?></td>
                        </tr>
                        <tr>
                          <td><b>Descripción</b></td>
                          <td><?php echo $value->descripcion;?></td>
                        </tr>
                        <tr>
                          <td><b>Soporte</b></td>
                          <td><a href="<?php echo $ruta;?>" target="_blank"><?php echo $value->url;?></a></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td><input type="file" class="form-control" name="document" id="documento_titulo_<?php echo $i;?>"></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td><button type="button" class="btn btn-secondary pull-right" onclick="actualizar_titulo(<?php echo $i;?>)">Actualizar documento</button></td>
                        </tr>
                      </table>
                    </form>
                  </div>
          <?php
                  $i++;
                }
              }else{
          ?>
                <div>
                  <h3>--</h3>
                </div>
          <?php
              }
            }
          ?>
        </div>
    </div>
  </div>

  <!--Registro de referencias-->
  <div class="row documents">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <h2>Referencias</h2>
        <div id="listadoReferenciasDirector">
          <div id="alerta-referencias"></div>
          <?php
            if (isset($referencias)) {
              $referencias = json_decode($referencias);
              if ($referencias->status == 'success') {
                $i=1;
                foreach ($referencias->referencias as $key => $value) {
                  $id_director_referencia = $value->id_director_referencia;
                  $ruta = "convocatoria_".$value->year."/bancos/directores/".$value->num_doc."/".$value->url;
          ?>
                  <div class="document">
                    <form id="form_referencia_<?php echo $i;?>">
                      <input type="hidden" name="id_director_referencia" value="<?php echo $id_director_referencia;?>">
                      <input type="hidden" name="ruta" value="<?php echo $ruta;?>">
                      <table class="table">
                        <tr>
                          <td><b>Nombre</b></td>
                          <td><?php echo $value->nombre;?></td>
                        </tr>
                        <tr>
                          <td><b>Empresa</b></td>
                          <td><?php echo $value->empresa;?></td>
                        </tr>
                        <tr>
                          <td><b>Cargo</b></td>
                          <td><?php echo $value->cargo;?></td>
                        </tr>
                        <tr>
                          <td><b>Soporte</b></td>
                          <td><a href="convocatoria_<?php echo $value->year;?>/bancos/directores/<?php echo $value->num_doc;?>/<?php echo $value->url;?>" target="_blank"><?php echo $value->url;?></a></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td><input type="file" class="form-control" name="document" id="documento_referencia_<?php echo $i;?>"></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td><button type="button" class="btn btn-secondary pull-right" onclick="actualizar_referencia(<?php echo $i;?>)">Actualizar documento</button></td>
                        </tr>
                      </table>
                    </form>
                  </div>
          <?php
                  $i++;
                }
              }else{
          ?>
                <div>
                  <h3>--</h3>
                </div>
          <?php
              }
            }
          ?>
        </div>
    </div>
  </div>


</div>
