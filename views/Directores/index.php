<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Directores de proyecto';

?>
<style>
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #ccc;
  width: 100%;
}
</style>

<div class="container container-form">
  <?php
      $form = ActiveForm::begin(['id' => 'register-form',]);
  ?>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <a class="btn btn-primary pull-right" href="index.php?r=directores/register"><i class="fa fa-plus"></i> Registrar director</a>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <!--<div class="alert alert-info">-->
        <h1>Director del Proyecto</h1>
        <p>
          Director registrado en el proyecto.
        </p>
      <!--</div>-->
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th>Nombre</th>
            <th>Identificación</th>
            <th>Estado</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          <?php
            if (!empty($listadoDirectores)) {
              $i=1;
              foreach ($listadoDirectores as $key => $value) {
                $id_director = $value['id_director'];
                $fondo = ($value['estado_registro'] == "ACTIVO") ? "#10ac84" : "#ee5253";
          ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $value['nombre'];?></td>
                  <td><?php echo $value['identificacion'];?></td>
                  <td style="background: <?php echo $fondo;?>;color:#FFFFFF;"><?php echo $value['estado_registro'];?></td>
                  <td>
                    <a href="index.php?r=directores/edit&id_director=<?php echo $id_director;?>" class="btn btn-primary" title="Editar"><i class="fa fa-eye"></i></a>
                  </td>
                </tr>
          <?php
                $i++;
              }
            }else{
          ?>
                <tr>
                  <td>0</td>
                  <td>Vacío</td>
                  <td>Vacío</td>
                  <td>Vacío</td>
                  <td>Vacío</td>
                </tr>
          <?php
            }
          ?>
        </tbody>
      </table>
    </div>
  </div>


  <?php ActiveForm::end(); ?>
</div>
