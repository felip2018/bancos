<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'SIGP';

$this->registerJsFile(
  '@web/js/directores.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

$tipo_documentos =[
  ''=>'-Seleccione',
  'CC'  => 'CC (Cédula de ciudadanía)',
  'CE' => 'CE (Cédula de extranjería)',
  'PAS' => 'PAS (Pasaporte)',
];


?>
<style media="screen">
.container-form{
  background: #FFFFFF;
  padding: 20px 50px 20px 50px;
  box-shadow: 0px 0px 10px #ccc;
  width: 100%;
}
.fa-asterisk{
  color: red;
}
.documents{
  box-shadow: 0px 0px 5px #CCC;
  border-radius: 5px;
  margin-top: 10px;
}
</style>

<div class="container container-form">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <a class="btn btn-primary pull-right" href="index.php?r=directores/index"><i class="fa fa-undo"></i> Volver</a>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>Registro de Director del Proyecto</h1>
        <p>
          Formulario de registro para el director del proyecto.
        </p>
    </div>
    <div class="col-xs-12" id="alerta"></div>
  </div>
  <?php
      if (isset($respuesta)) {
        $res = json_decode($respuesta);
    ?>
      <div class="col-xs-12 alert <?php echo $res->alert;?>">
        <b><?php echo $res->status;?></b>
        <p><?php echo $res->message;?></p>
      </div>
    <?php
      }
    ?>
  <?php
      $form = ActiveForm::begin(['id' => 'registro_director']);
  ?>
  <div class="row">
    <input type="hidden" id="id_director">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?=
            $form->field($model, 'tipo_doc')
            ->dropdownList($tipo_documentos,[])
            ->label('<i class="fa fa-asterisk"></i> Tipo de identificación');
          ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
           <?= $form
              ->field($model, 'num_doc')
              ->input('number',['onblur'=>'buscarDirector(this.value)','min'=>'0','oninput'=>'validarNumero(this)'])
              ->label('<i class="fa fa-asterisk"></i> Número de Identificación') ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <?= $form
              ->field($model, 'nombres')
              ->label('<i class="fa fa-asterisk"></i> Nombre') ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?= $form
              ->field($model, 'primer_apellido')
              ->label('<i class="fa fa-asterisk"></i> Primer apellido') ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <?= $form
              ->field($model, 'segundo_apellido')
              ->label('Segundo apellido') ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <?= $form
              ->field($model, 'correo')
              ->label('<i class="fa fa-asterisk"></i> Correo electrónico') ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <?php
            if (!empty($listadoDocumentosRequeridos)) {
              foreach ($listadoDocumentosRequeridos as $key => $value) {
                //$name = strtolower(str_replace(' ', '_', $value['nombre']));
                $required = ($value['obligatorio'] == 'SI') ? true : false;
          ?>
                <p><?php echo $value['nombre'];?></p>
                <input class="form-control registerDoc" type="file" name="<?php echo $value['input_name'];?>" id="<?php echo $value['input_name'];?>" required="<?php $required;?>" document="<?php echo $value['nombre'];?>">
          <?php
              }
            }
          ?>
        </div>
      </div>
      <div class="row" style="padding-top: 10px;">
        <div class="col-xs-12">
          <button type="button" id="btnRegisterDirector" class="btn btn-primary pull-right" onclick="registrar_director()">Guardar director</button>
        </div>
      </div>
    </div>
  </div>
  <?php 
      ActiveForm::end(); 
  ?>

  <!--Registro de experiencias-->
  <div class="row documents">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <h2>Experiencias</h2>
      <button class="btn btn-primary pull-right" onclick="registrar_experiencias()">
        <i class="fa fa-plus"></i> Agregar experiencia
      </button>
      <button class="btn btn-primary pull-left" onclick="actualizar_experiencias()">
        <i class="fa fa-sync"></i>
      </button>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th>Experiencia</th>
            <th>Descripción</th>
            <th>Soporte</th>
          </tr>
        </thead>
        <tbody id="listadoExperienciasDirector"></tbody>
      </table>
    </div>
  </div>
  
  <!--Registro de titulos-->
  <div class="row documents">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <h2>Titulos</h2>
      <button class="btn btn-primary pull-right" onclick="registrar_titulos()">
        <i class="fa fa-plus"></i> Agregar titulo
      </button>
      <button class="btn btn-primary pull-left" onclick="actualizar_titulos()">
        <i class="fa fa-sync"></i>
      </button>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th>Titulo</th>
            <th>Descripcion</th>
            <th>Soporte</th>
          </tr>
        </thead>
        <tbody id="listadoTitulosDirector"></tbody>
      </table>
    </div>
  </div>

  <!--Registro de referencias-->
  <div class="row documents">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <h2>Referencias</h2>
      <button class="btn btn-primary pull-right" onclick="registrar_referencias()">
        <i class="fa fa-plus"></i> Agregar referencia
      </button>
      <button class="btn btn-primary pull-left" onclick="actualizar_referencias()">
        <i class="fa fa-sync"></i>
      </button>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th>Nombre</th>
            <th>Empresa</th>
            <th>Cargo</th>
            <th>Soporte</th>
          </tr>
        </thead>
        <tbody id="listadoReferenciasDirector"></tbody>
      </table>
    </div>
  </div>

</div>
